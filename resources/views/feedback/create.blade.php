@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Feedback</a></li>
		<li class="active"><a href="#">Send Feedback</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-comments"></span> &nbsp;Feedback</h3>
		</div>
		<div class="box-body">
			<div class="form-group">
				We like to hear from you. Do you have features that you like to see? Send us a message.
			</div>
			{!! Form::open(['url'=>'feedback','id'=>'myForm']) !!}
			<div class="form-group" id="emailField">
				<label>Email</label>
				{!! Form::email('email', null, ['class'=>'form-control','id'=>'email']) !!}
				<div id="emailInput" style="font-size:11px; color:#dd4b39"></div>
			</div>
			<div class="form-group" id="messageField">
				<label>Message</label>
				{!! Form::textarea('message', '', ['class'=>'form-control','id'=>'message','style'=>'height:100px;resize:none']) !!}
				<div id="messageInput" style="font-size:11px; color:#dd4b39"></div>
			</div>
			<div class="form-group">
				{!! Form::button('Send Feedback', ['class'=>'btn btn-primary btn-sm btn-flat','id'=>'sendfeedbackbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
	$("#sendfeedbackbtn").click(function() {
		if($('#email').val()==''){
			swal({   
				title: "Email is a required field.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
			$('#email').focus();
		} else if($('#message').val()==''){
			swal({   
				title: "Message is a required field.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
			$('#message').focus();
		} else {
			$('#sendfeedbackbtn').attr('disabled', 'disabled');
			$('#sendfeedbackbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#myForm').submit();
		}
	});

	<?php if($errors->has('email')){ ?>
		swal({   
			title: "The Email you entered is not a valid Email Address.",   
			text: "",   
			timer: 2000,   
			showConfirmButton: false, 
			type: "warning" 
		});
	<?php } ?>

	<?php if(\Session::has('status')){ ?>
		swal("<?php echo \Session::get('status'); ?>", "", "success");  
	<?php } ?>
});
</script>
@endsection