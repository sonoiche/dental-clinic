<div class="modal fade bs-example-modal-md" id="patientListModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<div class="modal-header" id="noteheader">
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list"></span> Choose a Patient</h4>
            </div>
			<div class="modal-body" id="patient_list_content" style="height: 500px;">
                <div class="input-group">
                    <input type="text" class="form-control" id="patient_name" name="patient_name" placeholder="Search for patient name" aria-label="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary btn-primary" id="searchpatientbtn" type="button">Search</button>
                    </span>
                </div>
                <div class="box-body">
                    <table class="table table-hover" id="patientTable">
                        <tbody>
                        </tbody>
                    </table>
                </div>                    
			</div>
		</div>
	</div>
</div>