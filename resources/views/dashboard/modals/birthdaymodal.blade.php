<div class="modal fade bs-example-modal-md" id="birthdayModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<div class="modal-header" id="noteheader">
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-gift"></span> Upcoming Birthdays</h4>
            </div>
			<div class="modal-body" id="birthday_content">
			
			</div>
		</div>
	</div>
</div>