@extends('layouts/default')
@section('content')
<section class="content-header">
	
		<div class="row">
			
			<div class="col-xs-12 col-sm-6"><h2 class ="user_name">Welcome Dr. {{ \Auth::user()->first_name.' '.\Auth::user()->last_name }}!</h2></div>
			<div class="col-sm-6"><h3 id="timer"></h3></div>
		</div>
		{{-- <ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-gear"></i> Dashboard</a></li>
		</ol> --}}
	
</section>
<section class="content">
	{{-- @if($devices == 0)
	<div class="alert alert-danger alert-dismissable" style="font-size:12px;">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i> Reminder!</h4>
	You can access your dental clinic records from your mobile phone. You still don’t have the app on your mobile phone Or have not synced with it. Click to download for <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?ls=1&mt=8" target="_blank"><b>iOS</b></a> or <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental&hl=en" target="_blank"><b>Android</b></a>.<br><br>
	To sync your mobile app with your web account:<br/>
	1. Open your app, then access the web version page.<br/>
	2. Login your account, then you will be asked to sync your account. 

	</div>
	@endif --}}
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-danger" role="alert">Please be reminded that your patient data will not sync to the mobile app. The My Dental Clinic web version is independent from the mobile app.</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-xs-12 col-sm-12">
			<div class="info-box bg-aqua">
				<a href="{{ url('appointment','viewby') }}?viewby=View+Upcoming+Appointments">
					<span class="info-box-icon"><i class="pe-7s-note2"></i>
					<!-- 	<span class="info-box-text-mobile hidden-md hidden-lg">Upcoming Appointments</span> -->
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Upcoming Appointments</span>
						<span class="info-box-number">{{ $countappointment }}</span>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-xs-12 col-sm-12">
			<div class="info-box bg-yellow">
				<a href="{{ (count($dentalaccess2) && $dentalaccess2->can_read) ? url('patient') : 'javascript:void(0)' }}">
					<span class="info-box-icon"><i class="pe-7s-graph2"></i>
					<!-- 	<span class="info-box-text-mobile hidden-md hidden-lg"><br/>Total Patients</span> -->
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Total Patients</span>
						<span class="info-box-number">{{ $countpatient }}</span>
						
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-xs-12 col-sm-12">
			<div class="info-box bg-red">
				<a href="#" onclick="showUpcomingBirthdays()">
					<span class="info-box-icon"><i class="pe-7s-gift"></i>
						<!-- <span class="info-box-text-mobile hidden-md hidden-lg">Upcoming Birthdays</span> -->
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Upcoming Birthdays</span>
						<span class="info-box-number">{{ $birthdays }}</span>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="row">
				<div class="col-lg-12 hidden-xs hidden-tab">
					<div class="info-box bg-blue">
						<a href="{{ (count($dentalaccess1) && $dentalaccess1->can_read) ? url('appointment/create') : 'javascript:void(0)' }}">
							<span class="info-box-icon info-btn"><i class="pe-7s-id"></i>
								<span class="info-box-text-mobile hidden-md hidden-lg"><br/>ADD APPOINTMENT</span>
							</span>
							<div class="info-box-content hidden-xs"> 
								<span class="info-box-text-nonum">ADD APPOINTMENT</span>
								<!-- <span class="info-box-number text"></span> -->
								<!-- <span class="progress-description"> -->
								<!-- </span> -->
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-12 hidden-xs hidden-tab">
					<div class="info-box bg-blue">
						<a href="{{ (count($dentalaccess2) && $dentalaccess2->can_read) ? url('patient/create') : 'javascript:void(0)' }}">
							<span class="info-box-icon info-btn"><i class="pe-7s-add-user"></i>
								<span class="info-box-text-mobile hidden-md hidden-lg"><br/>ADD PATIENT</span>
							</span>
							<div class="info-box-content hidden-xs">
								<span class="info-box-text-nonum">ADD PATIENT</span>
								<!-- <span class="info-box-number text"></span> -->
								<!-- 	<span class="progress-description"> -->
								<!-- 70% Increase in 30 Days -->
								<!-- </span> -->
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-12 hidden-xs hidden-tab">
					<div class="info-box bg-blue">
						<a @if(count($dentalaccess2) && $dentalaccess2->can_read) onclick="showPatientsList()" @endif href="javascript:void(0)">
							<span class="info-box-icon info-btn"><i class="pe-7s-cash"></i>
								<span class="info-box-text-mobile hidden-md hidden-lg"></br>ADD PAYMENT</span>
							</span>
							<div class="info-box-content hidden-xs">
								<span class="info-box-text-nonum">ADD PAYMENT</span>
								<!-- <span class="info-box-number text"></span> -->
								<!-- <span class="progress-description"> -->
									<!-- 70% Increase in 30 Days -->
							<!-- 	</span> -->
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
	        <div class="panel panel-primary">
	            <div class="panel-heading">
	                <h3 class="panel-title"> <i class="pe-7s-date panel-icon"></i> Appointments for the day</h3>
	            </div>            
	            <div class="table-responsive">
	        		<table class="table table-condensed table-bordered">
	        			<thead>
		        			<tr>
		        				<th>No.</th>
		        				<th>Patient Name</th>
		        				<th>Time</th>
		        				<th>Notes</th>
		        			</tr>
	        			</thead>
	        			<tbody>
	        			@foreach ($patientappointments as $key => $appointment)
							<tr>
							  <td>{{ $key+1 }}</td>
							  <td>
						  		<a href="" style="color:#1a1a1a">
									<div class="image">
						        		@if($appointment->patient && $appointment->patient->image)
							          		{!! HTML::image('uploads/patients/'.$appointment->patient->image, '', ['class'=>'img-circle']) !!}
							        	@else
							        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle']) !!}
							        	@endif
							        </div>
							        <div class="info">
							        	<p>
							        		<span class="panel-text-title">{{($appointment->patient)?$appointment->patient->fullname:$appointment->name}} </span><br/>
						        			{{($appointment->patient)?$appointment->patient->contact_number:''}}
							        	</p>
							        </div>
							    </a>
							  </td>
							  <td><h4 class="sched_time">{{ display_time($appointment->schedule, true) }}</h4></td>
							  <td><h5 class="remarks">{{ $appointment->remarks }}</h5></td>
							</tr>
	        			@endforeach
	        			</tbody>
					</table>
	            </div>
	        </div>
		</div>
	</div>
	{{-- <div class="col-md-4 col-md-offset-4">
		<br><br><br><br>
		<div class="info-box" style="background-color:#196eee">
			<a href="" style="color:#fff !important">
				<span class="info-box-icon"><i class="fa fa-calendar-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Appointments</span>
					<span class="info-box-number">{{ $countappointment }}</span>
					<span class="progress-description">
						<!-- 70% Increase in 30 Days -->
					</span>
				</div>
			</a>
		</div>
		<div class="info-box" style="background-color:#196eee">
			<a href="" style="color:#fff !important">
				<span class="info-box-icon"><i class="fa fa-group"></i></span>
				<div class="info-box-content">
					<span class="info-box-text">Patients</span>
					<span class="info-box-number">{{ $countpatient }}</span>
					<span class="progress-description">
						<!-- 70% Increase in 30 Days -->
					</span>
				</div><!-- /.info-box-content -->
			</a>
		</div>
	</div> --}}
</section>
@include('dashboard.modals.birthdaymodal')
@include('dashboard.modals.patientlistmodal')
@endsection

@section('css')
<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
<style>
.swal2-content p {
    font-size: 17px;
    color: #3e3c3c;
}
</style>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {
	setTime();
	announcement();
	setInterval(function(){
		setTime();
	},10000);

	$('#searchpatientbtn').click(function() {
		$('#patientTable').empty();
		$('#patientTable').append('<tr><td style="text-align: center"><img src="{{ url('images/loading.gif') }}" /></td></tr>');
		if($('#patient_name').val() != ''){
			$.ajax({
				url: "{{ url('dashboard/searchpatient') }}",
				type: 'POST',
				dataType: 'json',
				data: {name: $('#patient_name').val()},
				success: function(result){
					$('#patientTable').empty();
					var datas = result;         
		            $.each(datas, function(index, data) {
		            	if (data.image) {
		 					var image = '<img src="'+data.image+'" class="img-circle list-image" alt="">';
		 				} else {
		 					var image = '<img src="uploads/no_pic.jpg" class="img-circle list-image" alt="">';
		 				}
		            	$('#patientTable').append('<tr><td style="width:3%"></td><td style="width:95%"><a href="payments/create/'+data.hashid+'"><div class="pull-left image">'+image+'</div><div class="pull-left info" style="padding-left:15px"><p>'+data.full_name+'<br>Contact Number :'+data.contact_number+'</p></div></a></td></tr>');
		            });
				}
			});
		}else{
			showPatientsList();
		}
	});
});

var user = '<?php print auth()->user()->id ?>';
var storageName = 'announcement_pop_up'+user;
function announcement()
{
	var current_time = '<?php print date("Y-m-d")?>';
	var storageSt = window.localStorage.getItem(storageName) || "";

	if(storageSt){
		if(JSON.parse(storageSt).date != current_time){
	        localStorage.removeItem(storageSt);
	        var res = {date: current_time, clicked: 1};
   			announcementAlert(res);
	    }
	}else{
		var res = {date: current_time, clicked: 1};
		announcementAlert(res);
	}
		
}

function announcementAlert(res){
	swal({
        title: 'A N N O U N C E M E N T',
        width: 700,
        html: "<p align='left'>Hello!<br><br>"+
        	   "Since we are so excited to share (we can’t help but spill the bean, you know?), we are delighted to announce that a new upgrade is coming to My Dental Clinic web! (Hooray!) <br><br>"+
        	   "With this, My Dental Clinic web will be inaccessible on <strong>February 6, 2019 (Wednesday) at 09:00 AM up to 12:00 NOON</strong> to make way for the upgrade. But we promise that everything will be better than before when we come back. Just wait and see <i class='em em-wink'></i>.<br><br>"+
        	   "Don’t worry about your data though, as it will be safe and secured. <br><br>"+
        	   "For any concerns or questions you may email us at email@dentalclinicapp.com <br><br>"+
        	   "Thank you. <br><br>"+
        	   "<i>My Dental Clinic team<i></p>",
        type: 'info',
        confirmButtonColor: '#3085d6',
        allowOutsideClick: false,
    }).then(function(){
	   if(res){
	   		window.localStorage.setItem(storageName, JSON.stringify(res));
	   		$.ajax({
				url: "{{ url('dashboard/view_appointment') }}",
				type: 'GET',
				dataType: 'json'
			});
	   }
	});
}

function setTime() {
    var currentTimeString = moment(new Date()).format(' hh:mm a - MMMM D, YYYY');
    $("#timer").html(currentTimeString);
}

function showCode() {
	$.ajax({
	 	url: base_url+'/showapikey',
	 	type: 'GET',
	 	success: function(result){
 			swal(result, "Copy this code to your mobile to sync your accounts.", "success");
	 	}
	});
}

function showUpcomingBirthdays() {
	$.ajax({
	 	url: base_url+'/upcoming_birthdays',
	 	type: 'GET',
	 	success: function(result){
	 		console.log(result);
			$('#birthdayModal').modal();
			var birthday_list = '<div class="box-body"><table class="table table-hover"><tbody>'
								+'<div class="box-body">'
								+'<table class="table table-hover"><tbody>';

 			result.forEach(function(item, index) {
 				if (item.image) {
 					var image = '<div class="image">'
 								+'<img src="uploads/patients/'+item.image+'" class="img-circle list-image" alt=""></div>';
 				}
 				else {
 					var image = '<div class="image">'
 								+'<img src="uploads/no_pic.jpg" class="img-circle list-image" alt=""></div>';
 				}

 				birthday_list += '<tr><td class="list-index">'+ (index+1) +'</td>';
 				birthday_list += '<td class="list-content">'
 								 +'<a href="patient/'+item.hashid+'">'
 								 +image
						         +'<div class="info" style="padding-left:15px">'
						       	 +'<p>'+item.full_name+'<br>Birthday :'+moment(new Date(item.birthdate)).format('MMMM D, YYYY')+'</p>'
						         +'</div></a></td></tr>';
 			});
			birthday_list += '</tbody></table></div>';

			$('#birthday_content').html(birthday_list);
	 	}
	});
}

function showPatientsList() {
	$('#patientListModal').modal();
	$('#patient_name').val('');
	$('#patientTable').empty();
	$.ajax({
	 	url: base_url+'/patient_list',
	 	type: 'GET',
	 	success: function(result){
	 		console.log(result);
			var datas = result;         
            $.each(datas, function(index, data) {
            	if (data.image) {
            		if(ImageExist(data.image)){
	            		var responseimg = data.image;
	            	} else {
	            		var responseimg = "{{ url('uploads/patients') }}/"+data.image;
	            	}
 					var image = '<img src="'+responseimg+'" class="img-circle list-image" alt="">';
 				} else {
 					var image = '<img src="uploads/no_pic.jpg" class="img-circle list-image" alt="">';
 				}
            	$('#patientTable').append('<tr><td style="width:3%"></td><td style="width:95%"><a href="payments/create/'+data.hashid+'"><div class="image">'+image+'</div><div class="info" style="padding-left:15px"><p>'+data.full_name+'<br>Contact Number :'+data.contact_number+'</p></div></a></td></tr>');
            });
	 	}
	});
}

function ImageExist(url) {
   var img = new Image();
   img.src = url;
   return img.height != 0;
}
</script>
@endsection