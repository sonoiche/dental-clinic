<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
        <meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
        <meta name="author" content="Quantum X Inc.">
        <title>My Dental Clinic</title>
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
        <!--  Custom -->
        {!! HTML::style('home/css/customadmin.css') !!}
        <!-- Bootstrap -->
        {!! HTML::style('css/bootstrap.css') !!}
        {!! HTML::style('css/font-awesome.min.css') !!}
        {!! HTMl::style('css/ionicons.min.css') !!}
        {!! HTML::style('website/css/style.css') !!}
        {!! HTML::style('css/style.css') !!}
        {!! HTML::style('website/css/animate.css') !!}
        {!! HTML::style('library/sweetalert/sweetalert.css') !!}
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('website/images/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body >
        <section class="header parallax home-parallax page" id="HOME">
            <div class="section_overlay">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header" style="background-color:#0D24C7;vertical-align: middle;">
                            <div class="pull-left div-responsive"><img class="responsive" src="{{ url('website/images/logo.png') }}" alt=""></div>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color:#0D24C7;">
                            {{-- 
                            <div class="pull-left logo"><img src="{{ url('website/images/logo.png') }}" class="img-responsive" alt=""></div>
                            --}}
                            <ul class="nav navbar-nav navbar-right">
                                <!-- NAV -->
                                <li><a href="{{ route('auth.logout') }}">SIGN OUT </a> </li>
                                <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                                <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container- -->
                </nav>
            </div>
        </section>
        <div class="padding-top-responsive" style="text-align:center;">
            <br/><br/>
            <h3>
                Syncing of mobile data to My Dental Clinic web version is currently under development. 
                <br/><br/>
                This will be available soon.
            </h3>
        </div>