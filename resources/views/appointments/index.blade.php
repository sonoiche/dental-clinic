@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Appointments</a></li>
		<li class="active"><a href="#">Appointment's List</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<h3 class="box-title"><span class="fa fa-calendar-o"></span> &nbsp;Appointment's List</h3><br>
				<span style="font-size:14px; padding:10px 24px">Total of {{ $countappointments }} appointments.</span>
			</div>
			@if($dentalaccess1->can_write)
			<div class="pull-right"><a href="{{ url('appointment/create') }}" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Appointment</a></div>
			@endif
		</div>
		<div class="box-body">
			<div class="pull-left" style="padding-bottom:10px">
				{!! Form::open(['url' => 'appointment/viewby','id'=>'viewForm','method'=>'GET']) !!}
				{!! Form::select('viewby',['Today'=>'View Today','View Upcoming Appointments'=>'View Upcoming Appointments','View Past Appointments'=>'View Past Appointments'],isset($viewby) ? $viewby : 'View Upcoming Appointments',['class'=>'form-control','id'=>'viewby']) !!}
				{!! Form::close() !!}
			</div>
			{!! Form::open(['url'=>'appointment/deleteall','id'=>'appointmentlistform']) !!}
			@if(count($patientappointments))
			<table class="table table-hover">
				<tbody>
					@foreach($patientappointments as $key => $patientappointment)
					<tr>
						<td style="vertical-align:middle; width:3%">{{ ($patientappointments->currentpage()-1) * $patientappointments->perpage() + $key + 1 }}</td>
						<td>
							{{-- <div class="pull-left image">
								@if(count($patientappointment->patient) && $patientappointment->patient->image)
					          		{!! HTML::image('uploads/patients/'.$patientappointment->patient->image, '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
					        	@else
					        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
					        	@endif
							</div> --}}
							<div class="pull-left info" style="padding-left:15px">
								<b>{{ $patientappointment->name }}</b><br>
								{{ display_time($patientappointment->schedule) }}<br/>
								{!! ($patientappointment->remarks!='') ? $patientappointment->remarks : '' !!}
							</div>
							@if($dentalaccess1->can_write)
							<div class="pull-right">
								@if($viewby == 'View Past Appointments')
								<div style="padding:20px 15px">{!! Form::checkbox('appointment_ids[]', $patientappointment->id, '') !!}</div>
								@else
								<a href="{{ url('appointment',\Hashids::encode($patientappointment->id)) }}/edit" class="btn btn-primary btn-sm"><span class="fa fa-pencil"></span></a>
								<a onclick="deleteappointment('{{ \Hashids::encode($patientappointment->id) }}')" class="btn btn-primary btn-sm"><span class="fa fa-trash"></span></a>
								@endif
							</div>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<div class="pull-left" style="padding:7px 5px">No Appointments Available</div>
			@endif
			<div class="pull-left">{!! (count($patientappointments)) ? $patientappointments->appends(['viewby' => $viewby])->render() : '' !!}</div>
	    	@if($viewby == 'View Past Appointments')
	    	<div class="pull-right">{!! Form::button('<i class="fa fa-trash"></i> &nbsp;Delete Appointments', ['class'=>'btn btn-primary btn-sm btn-flat','id'=>'deleteappointmentbtn']) !!}</div>
	    	@endif
	    	{!! Form::close() !!}
	    </div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
ga('send', 'appointments_list');

$(document).ready(function() {
	$('#viewby').change(function() {
		$('#viewForm').submit();
	});

	$('#deleteappointmentbtn').click(function() {
		if ($("#appointmentlistform input:checkbox:checked").length > 0){
			if(confirm('Are you sure you want to delete these appointments?')){
		    	$('#appointmentlistform').submit();
				$('#deleteappointmentbtn').attr('disabled', true);
				$('#deleteappointmentbtn').html('<span class="fa fa-spinner fa-spin"></span> &nbsp;Processing...');
			}
			return false;
		} else {
		   	alert('No checkbox selected.');
		}
	});
});
function deleteappointment(id){
	ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Appointment');
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Appointment!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){   
		$.ajax({
			url: "{{ url('appointment') }}/"+id,
			type: 'DELETE',
			success: function(result){
				swal({
		            title: 'Success!',
		            text: 'Appointment has been deleted.',
		            type: 'success',
		            showConfirmButton: false
		        })
				setTimeout(function(){ location.reload(); }, 1000);
			}
		});
	});
}
</script>
@endsection