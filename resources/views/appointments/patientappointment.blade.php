@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">Patient Appointments</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-info"></span> &nbsp;Patient Information</h3>
		</div>
		<div class="box-body">
			<div class="pull-left image">
	          	@if($patient->image)
	          		{!! HTML::image('uploads/patients/'.$patient->image, '', ['class'=>'img-circle','style'=>'height:100px;width:100px']) !!}
	        	@else
	        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
	        	@endif
	        </div>
	        <div class="pull-left info" style="padding-left:15px">
	        	<p style="font-size:16px !important">
	        		<b>{{ $patient->first_name.' '.$patient->last_name }}</b><br>
	        		{{ $patient->contact_number }}<br>
	        		{{ $patient->address }}<br>
	        		{{ \Carbon\Carbon::parse($patient->birthdate)->format('F d, Y').' - '.\Carbon\Carbon::parse($patient->birthdate)->age.' years old' }}
	        	</p>
	        </div>
	        <div class="pull-right">
	        	<a href="{{ route('patient.show', \Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-user"></span> &nbsp;Patient Info</a>
	        </div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-calendar-o"></span> &nbsp;Add Appointment</h3>
		</div>
		<div class="box-body">
			{!! Form::open(['url'=>'appointment','id'=>'myForm']) !!}
			{!! Form::hidden('patient_id', $patient->id) !!}
			@include('patient_appointments.appointmentform')
			{!! Form::close() !!}
	    </div>
	</div>
</section>
@endsection

@section('css')
{!! HTML::style('library/timepicker/bootstrap-timepicker.min.css') !!}
@endsection

@section('js')
{!! HTML::script('library/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.extensions.js') !!}
{!! HTML::script('library/timepicker/bootstrap-timepicker.min.js') !!}
{!! HTML::script('js/fileinput.js') !!}
<script type="text/javascript">
$(document).ready(function($) {
	$('#myForm').on('submit', function(){
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Add', 'Add Patient Appointment');
	});
	
	$(".fdatemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	$(".timepicker").timepicker({
	  	showInputs: false
	}).val('');

	$('#saveappointmentbtn').click(function() {
		if($('#appointmentdate').val()==''){
			$('#appointmentdateField').addClass('has-error');
		    $('#appointmentdateInput').html('<i class="fa fa-times-circle-o"></i> Appointment Date is Required.');
		    $('#appointmentdate').focus();
		    return false;
		} else {
			$('#saveappointmentbtn').attr('disabled', 'disabled');
			$('#saveappointmentbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#myForm').submit();
		}
	});
});
</script>
@endsection