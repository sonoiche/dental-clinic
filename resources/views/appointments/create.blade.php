@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">Patient Appointments</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-calendar-o"></span> &nbsp;Add Appointment</h3></div>
			<div class="pull-right"><a href="{{ url('appointment') }}" class="btn btn-primary btn-sm"><span class="fa fa-calendar-o"></span> &nbsp;Appointment's List</a></div>
		</div>
		<div class="box-body">
			{!! Form::open(['url' => 'appointment','id'=>'myForm']) !!}
			@include('appointments.appointmentform')
			{!! Form::hidden('patient_id', isset($patient->id) ? $patient->id : '') !!}
			{!! Form::close() !!}
	    </div>
	</div>
</section>
@endsection

@section('css')
{!! HTML::style('library/timepicker/bootstrap-timepicker.min.css') !!}
@endsection

@section('js')
{!! HTML::script('library/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.extensions.js') !!}
{!! HTML::script('library/timepicker/bootstrap-timepicker.min.js') !!}
{!! HTML::script('js/fileinput.js') !!}
<script type="text/javascript">
$(document).ready(function($) {
	$('#myForm').on('submit', function(){
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Add', 'Add Appointment');
	});

	$(".fdatemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	$(".timepicker").timepicker({
	  	showInputs: false
	}).val('');

	$('#saveappointmentbtn').click(function() {
		if($('#patient_name').val()==''){
			$('#nameField').addClass('has-error');
		    $('#nameInput').html('<i class="fa fa-times-circle-o"></i> Patient Name is Required.');
		    $('#patient_name').focus();
		    return false;
		} else if($('#appointmentdate').val()==''){
			$('#appointmentdateField').addClass('has-error');
		    $('#appointmentdateInput').html('<i class="fa fa-times-circle-o"></i> Appointment Date is Required.');
		    $('#appointmentdate').focus();
		    return false;
		} else {
			$('#saveappointmentbtn').attr('disabled', 'disabled');
			$('#saveappointmentbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#myForm').submit();
		}
	});
});
</script>
@endsection