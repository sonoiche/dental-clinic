<div class="form-group" id="nameField">
	<label>Patient Name</label>
	@if(isset($patient->id))
	{!! Form::text('patient_name', $patient->first_name.' '.$patient->last_name, ['class'=>'form-control','id'=>'patient','readonly'=>'readonly']) !!}
	@elseif(isset($patientappointment->patient_id) && $patientappointment->patient_id!='')
	{!! Form::text('patient_name', $patientappointment->name, ['class'=>'form-control','id'=>'patient','readonly'=>'readonly']) !!}
	@else
	{!! Form::text('name', isset($patientappointment->name) ? $patientappointment->name : null, ['class'=>'form-control','id'=>'patient_name']) !!}
	@endif
	<div id="nameInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="appointmentdateField">
	<label>Date</label>
	<div class="input-group">
		<div class="input-group-addon" style="padding:0 10px 0 10px">
			<i class="fa fa-calendar"></i>
		</div>
		{!! Form::text('appointmentdate', (isset($patientappointment->schedule) && $patientappointment->schedule!='0000-00-00 00:00:00') ? display_time(date('Y-m-d', strtotime($patientappointment->schedule))) : null, ['id'=>'appointmentdate','class'=>'form-control fdatemask']) !!}
	</div>
	<div id="appointmentdateInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group bootstrap-timepicker">
	<label>Time</label>
	<div class="input-group">
		<div class="input-group-addon" style="padding:0 10px 0 10px">
			<i class="fa fa-clock-o"></i>
		</div>
		{!! Form::text('appointmenttime', (isset($patientappointment->schedule) && $patientappointment->schedule!='0000-00-00 00:00:00') ? date('g:i A', strtotime($patientappointment->schedule)) : null, ['class'=>'form-control timepicker', 'placeholder' => 'Set Time']) !!}
	</div>
</div>
<div class="form-group" id="remarksField">
	<label>Remarks</label>
	{!! Form::textarea('remarks', isset($patientappointment->remarks) ? $patientappointment->remarks : null, ['class'=>'form-control','id'=>'remarks','style'=>'height:100px; resize:none']) !!}
	<div id="remarksInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group">
	{!! Form::button('Save Appointment', ['class'=>'btn btn-primary btn-flat btn-sm','id'=>'saveappointmentbtn']) !!}
</div>