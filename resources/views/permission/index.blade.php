@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> Manage Permission</a></li>
		<li class="active"><a href="#">Permission</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-certificate"></span> &nbsp;Access Level</h3></div>
		</div>
		<div class="box-body">
			<div style="">
				{!! Form::open(['id'=>'roleForm']) !!}
				<div class="input-group input-access">
					<input type="text" name="name" class="form-control" placeholder="Access Name">
					<span class="input-group-btn">
                      	<button type="submit" class="btn btn-primary btn-flat" id="addrolebtn">Add New Role</button>
                    </span>
				</div>
				{!! Form::close() !!}
			</div>
			<div class="box-body">
                <table class="table table-striped">
					<thead>
						<tr>
							<th style="text-align:center; width:2%">#</th>
							<th style="width:75%">Role Name</th>
							<th style="text-align:center; width:25%">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($roles as $key => $role)
							<tr>
								<td style="text-align:center">{{ $key+1 }}</td>
								<td>{{ $role->name }}</td>
								<td style="text-align:center">
									<a href="{{ url('permission',Hashids::encode($role->id)) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-cog"></i> &nbsp; Manage Access</a>
									<a href="javascript:void(0)" onclick="deleterole({{ $role->id }})" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i> &nbsp; Delete</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
            </div>
		</div>
	</div>
</section>
@endsection

@section('js')
{!! HTML::script('library/jquery-validation/dist/jquery.validate.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	$('#roleForm').validate({
		errorClass: "error-class",
	    validClass: "valid-class",
	    errorElement: 'div',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    onError : function(){
	        $('.input-group.error-class').find('.help-block.form-error').each(function() {
	          	$(this).closest('.form-group').addClass('error-class').append($(this));
	        });
	    },
        rules: {
            name: { required : true }
        },
        messages : {
            name: "Please provide access name."
        },
        submitHandler: function () {
            $('#addrolebtn').attr('disabled', true);
            $('#addrolebtn').html("<i class=\"fa fa-spin fa-spinner\"></i> Saving...");
            var data = $('#roleForm').serializeArray();
            $.ajax({
                url: "{{ url('user/storerole') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == 2){
                        $('.form-role').find('.div-label').append('<label class="error" for="name">Role already exists.</label>');
                        $('#addrolebtn').removeAttr('disabled');
                        $('#addrolebtn').html('Save');
                    } else {
                        swal({
                            title: 'Success!',
                            text: 'Role has been saved.',
                            type: 'success',
                            showConfirmButton: false
                        })
                        setTimeout(function(){ location.reload(); }, 1000);
                    }
                }
            });
        }
    })
});

function deleterole(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this User!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){
		$.ajax({
			url: "{{ url('user/roledelete') }}/"+id,
			type: 'GET',
			dataType: 'json',
			success: function(result){
				swal({
                    title: 'Success!',
                    text: 'Role has been deleted.',
                    type: 'success',
                    showConfirmButton: false
                })
                setTimeout(function(){ location.reload(); }, 1700);
			}
		});
	});
}
</script>
@endsection