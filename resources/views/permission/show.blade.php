@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> Manage Permission</a></li>
		<li class="active"><a href="#">Permission</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-certificate"></span> &nbsp;Access Level : {{ $role->name }}</h3></div>
		</div>
		<div class="box-body">
			{!! Form::open(['id' => 'roleForm']) !!}
			<div class="form-group" style="width:50%">
				<label>Access Name</label>
				{!! Form::text('name', $role->name, ['class'=>'form-control']) !!}
				{!! Form::hidden('id', $role->id) !!}
			</div>
			<div class="form-group">
				{!! Form::button('Save Changes', ['class'=>'btn btn-primary btn-sm','type'=>'submit','id'=>'addrolebtn']) !!}
			</div>
			{!! Form::close() !!}
			<div style="margin-top:30px">
	            <table class="table table-striped">
					<thead>
						<tr>
							<th style="text-align:center; width:2%">#</th>
							<th style="width:80%">Menu Name</th>
							<th style="text-align:center;width:9%">View</th>
							<th style="text-align:center;width:9%">Edit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($menus as $key => $menu)
						<tr>
							<td style="text-align:center">{{ $key+1 }}</td>
							<td>{{ $menu->name }}</td>
							<td style="text-align:center"><input type="checkbox" name="chkpermission" class="check-class minimal" data-type="can_read" data-role-id="{{ $role->id }}" data-menu-id="{{ $menu->id }}" @if(count($menu->permission)) @foreach ($menu->permission as $permission) {{ ($permission->can_read == 1) ? 'checked=checked' : '' }} {{ $permission->can_read }} @endforeach @endif></td>
							<td style="text-align:center"><input type="checkbox" name="chkpermission" class="check-class minimal" data-type="can_write" data-role-id="{{ $role->id }}" data-menu-id="{{ $menu->id }}" @if(count($menu->permission)) @foreach ($menu->permission as $permission) {{ ($permission->can_write == 1) ? 'checked=checked' : '' }} @endforeach @endif></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
@endsection

@section('css')
{!! HTML::style('library/iCheck/all.css') !!}
@endsection

@section('js')
{!! HTML::script('library/iCheck/icheck.min.js') !!}
{!! HTML::script('library/jquery-validation/dist/jquery.validate.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	$('#roleForm').validate({
		errorClass: "error-class",
	    validClass: "valid-class",
	    errorElement: 'div',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    onError : function(){
	        $('.input-group.error-class').find('.help-block.form-error').each(function() {
	          $(this).closest('.form-group').addClass('error-class').append($(this));
	        });
	    },
        rules: {
            name: { required : true }
        },
        messages : {
            name: "Please provide access name."
        },
        submitHandler: function () {
            $('#addrolebtn').attr('disabled', true);
            $('#addrolebtn').html("<i class=\"fa fa-spin fa-spinner\"></i> Saving...");
            var data = $('#roleForm').serializeArray();
            $.ajax({
                url: "{{ url('user/storerole') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == 2){
                        $('.form-role').find('.div-label').append('<label class="error" for="name">Role already exists.</label>');
                        $('#addrolebtn').removeAttr('disabled');
                        $('#addrolebtn').html('Save');
                    } else {
                        swal({
                            title: 'Success!',
                            text: 'Role has been saved.',
                            type: 'success',
                            showConfirmButton: false
                        })
                        $('#addrolebtn').removeAttr('disabled');
                        $('#addrolebtn').html('Save Changes');
                        setTimeout(function(){ swal.close(); }, 1000);
                    }
                }
            });
        }
    })

	$('.check-class').change(function() {
		if ($(this).prop('checked')) {
			var checked = 'true';
			var role_id = $(this).attr('data-role-id');
			var menu_id = $(this).attr('data-menu-id');
			var data_type = $(this).attr('data-type');
		} else {
			var checked = 'false';
			var role_id = $(this).attr('data-role-id');
			var menu_id = $(this).attr('data-menu-id');
			var data_type = $(this).attr('data-type');
		}

		$.ajax({
			url: "{{ url('permission') }}",
			type: 'POST',
			dataType: 'json',
			data: {
				role_id: role_id,
				menu_id: menu_id,
				checked: checked,
				data_type: data_type 
			},
			success: function(result){

			}
		});
	});

	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
    });

    $('input').on('ifChecked', function(event){
		var checked = 'true';
		var role_id = $(this).attr('data-role-id');
		var menu_id = $(this).attr('data-menu-id');
		var data_type = $(this).attr('data-type');

		$.ajax({
			url: "{{ url('permission') }}",
			type: 'POST',
			dataType: 'json',
			data: {
				role_id: role_id,
				menu_id: menu_id,
				checked: checked,
				data_type: data_type 
			},
			success: function(result){

			}
		});
	});

	$('input').on('ifUnchecked', function(event){
		var checked = 'false';
		var role_id = $(this).attr('data-role-id');
		var menu_id = $(this).attr('data-menu-id');
		var data_type = $(this).attr('data-type');

		$.ajax({
			url: "{{ url('permission') }}",
			type: 'POST',
			dataType: 'json',
			data: {
				role_id: role_id,
				menu_id: menu_id,
				checked: checked,
				data_type: data_type 
			},
			success: function(result){

			}
		});
	});
});
</script>
@endsection