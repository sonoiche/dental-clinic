<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Dental Clinic</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {!! HTML::style('css/bootstrap.css') !!}
    {!! HTML::style('css/font-awesome.min.css') !!}
    {!! HTMl::style('css/ionicons.min.css') !!}
    {!! HTML::style('css/style.css') !!}
    {!! HTML::style('css/skins/_all-skins.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <b>My Dental Clinic</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Change Password for {{ $user->email }}</p>
        <form role="form" method="POST" action="{{ route('user.resetpassword') }}" id="myForm">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          @if($errors->has('password'))
          <div class="form-group has-error" style="margin-bottom:10px !important;">
          @else
          <div class="form-group">
          @endif
            <label>Password</label>
            {!! Form::password('password', ['class'=>'form-control']) !!}
            {!!$errors->first('password', '<p style="color:#dd4b39">:message</p>')!!}
          </div>
          @if($errors->has('password_confirmation'))
          <div class="form-group has-error" style="margin-bottom:10px !important;">
          @else
          <div class="form-group">
          @endif
            <label>Retype Password</label>
            {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
            {!!$errors->first('password_confirmation', '<p style="color:#dd4b39">:message</p>')!!}
          </div>
          <div class="row">
            <div class="col-xs-6">
              {!! Form::hidden('email', $user->email) !!}
              {!! Form::hidden('secretkey', $user->secretkey) !!}
              {!! Form::button('Change Password', ['class'=>'btn btn-primary btn-block btn-flat','id'=>'changepasswordbtn']) !!}
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    {!! HTML::script('js/jquery.js') !!}
    {!! HTML::script('js/bootstrap.js') !!}
    {!! HTML::script('js/jquery.slimscroll.js') !!}
    {!! HTML::script('js/fastclick.js') !!}
    {!! HTML::script('js/app.js') !!}
    <script type="text/javascript">
    $(document).ready(function() {
      $('#changepasswordbtn').click(function() {
        $('#changepasswordbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
        $('#changepasswordbtn').attr('disabled', 'disabled');
        $('#myForm').submit();
      });
    });
    </script>
  </body>
</html>