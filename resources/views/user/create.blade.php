@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Users</a></li>
		<li class="active"><a href="#">Add New User</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Add New User</h3>
			<div class="pull-right">All fields are required.</div>
		</div>
		<div class="box-body">
		{!! Form::open(['id'=>'userForm']) !!}
			{!! Form::open(['url'=>'user','files'=>'true','id'=>'myForm']) !!}
			<div class="form-group form-user">
				<label>First Name</label>
				<div class="div-label">{!! Form::text('first_name', isset($user->first_name) ? $user->first_name : '', ['class'=>'form-control']) !!}</div>
			</div>
			<div class="form-group form-user">
				<label>Last Name</label>
				<div class="div-label">{!! Form::text('last_name', isset($user->last_name) ? $user->last_name : '', ['class'=>'form-control']) !!}</div>
			</div>
			<div class="form-group">
				<label>Role</label>
				<div class="input-group input-group-md">
					<select name="access_id" class="form-control">
						<option value="">--</option>
						@foreach($roleList as $list)
						<option value="{{ $list->id }}" id="option-{{ $list->id }}" {{ (isset($user->access_id) && $user->access_id == $list->id) ? 'selected=selected' : '' }}>{{ $list->name }}</option>
						@endforeach
					</select>
					<span class="input-group-btn">
                      	<button type="button" class="btn btn-primary btn-flat" id="addnewrole">Add New Role</button>
                    </span>
				</div>
			</div>
			<div class="form-group form-email">
				<label>Email Address</label>
				<div class="div-label">{!! Form::text('email', isset($user->email) ? $user->email : '', ['class'=>'form-control']) !!}</div>
			</div>
			<div class="form-group">
				<label>Contact Number</label>
				{!! Form::text('contact_number', isset($user->contact_number) ? $user->contact_number : '', ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::hidden('user_id', isset($user->id) ? $user->id : '', ['id'=>'user_id']) !!}
				{!! Form::button('Save', ['class'=>'btn btn-primary btn-md','id'=>'saveuserbtn','type'=>'submit']) !!}
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</section>
<div class="modal fade bs-example-modal-md" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" id="noteheader">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list-alt"></span> Manage Roles</h4>
            </div>
			<div class="modal-body">
				{!! Form::open(['id'=>'roleForm']) !!}
				<div class="form-group form-role">
					<div class="input-group input-group-md">
						{!! Form::text('name', '', ['class'=>'form-control','placeholder'=>'Role Name']) !!}
						<span class="input-group-btn">
	                      	<button type="submit" class="btn btn-primary btn-flat" id="addrolebtn">Save</button>
	                    </span>
					</div>
				</div>
				{!! Form::hidden('id') !!}
				{!! Form::close() !!}
				<table class="table table-striped">
					<thead>
						<tr>
							<th style="text-align:center; width:2%">#</th>
							<th style="width:70%">Name</th>
							<th style="text-align:center; width:25%">Action</th>
						</tr>
					</thead>
					<tbody id="role-tbody">
						@foreach($roles as $key => $role)
						<tr id="row_{{ $role->id }}">
							<td style="text-align:center">{{ $key+1 }}</td>
							<td id="td-{{ $role->id }}">{{ $role->name }}</td>
							<td style="text-align:center">
								<a href="javascript:void(0)" onclick="editrole({{ $role->id }})" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> &nbsp;Edit</a>
								<a href="javascript:void(0)" onclick="deleterole({{ $role->id }})" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> &nbsp;Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('css')
<style type="text/css">
.error-class {
    font-size: 12px;
    color: #e74c3c;
}
</style>
@endsection

@section('js')
{!! HTML::script('library/jquery-validation/dist/jquery.validate.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	$('#addnewrole').click(function() {
		$('#roleForm')[0].reset();
		$('#roleModal').modal()
	});

	$('#userForm').validate({
		errorClass: "error-class",
	    validClass: "valid-class",
	    errorElement: 'div',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    onError : function(){
	        $('.input-group.error-class').find('.help-block.form-error').each(function() {
	          $(this).closest('.form-group').addClass('error-class').append($(this));
	        });
	    },
        rules: {
            first_name: { required : true },
            last_name: { required : true },
            access_id: { required : true },
            email: { required : true, email: true },
            contact_number: { required : true }
        },
        messages : {
            first_name: "Please provide first name.",
            last_name: "Please provide last name.",
            access_id: "Please provide role for this user.",
            email: {
                required: "Please provide a unique email address.",
                email: "Please provide a valid email address."
            },
            contact_number: "Please provide contact number.",
        },
        submitHandler: function () {
            $('#saveuserbtn').attr('disabled', true);
            $('#saveuserbtn').html("<i class=\"fa fa-spin fa-spinner\"></i> Saving...");
            var data = $('#userForm').serializeArray();
            $.ajax({
                url: "{{ url('user/storeuser') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == 2){
                        $('.form-user').find('.div-label').append('<label class="error" for="name">Users already exists.</label>');
                        $('#saveuserbtn').removeAttr('disabled');
                        $('#saveuserbtn').html('Save');
                    } else if(result == 3){
                        $('.form-email').find('.div-label').append('<label class="error" for="name">Email address already exists.</label>');
                        $('#saveuserbtn').removeAttr('disabled');
                        $('#saveuserbtn').html('Save');
                    } else {
                        swal({
                            title: 'Success!',
                            text: 'User info has been saved.',
                            type: 'success',
                            showConfirmButton: false
                        })
                        setTimeout(function(){ location.reload(); }, 900);
                    }
                }
            });
        }
    })

	$('#roleForm').validate({
		errorClass: "error-class",
	    validClass: "valid-class",
	    errorElement: 'div',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
	    onError : function(){
	        $('.input-group.error-class').find('.help-block.form-error').each(function() {
	          $(this).closest('.form-group').addClass('error-class').append($(this));
	        });
	    },
        rules: {
            name: { required : true }
        },
        messages : {
            name: "Please provide a role name."
        },
        submitHandler: function () {
            $('#addrolebtn').attr('disabled', true);
            $('#addrolebtn').html("<i class=\"fa fa-spin fa-spinner\"></i> Saving...");
            var data = $('#roleForm').serializeArray();
            $.ajax({
                url: "{{ url('user/storerole') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == 2){
                        $('.form-role').find('.div-label').append('<label class="error" for="name">Role already exists.</label>');
                        $('#addrolebtn').removeAttr('disabled');
                        $('#addrolebtn').html('Save');
                    } else {
                        swal({
                            title: 'Success!',
                            text: 'Role has been saved.',
                            type: 'success',
                            showConfirmButton: false
                        })
                        setTimeout(function(){ swal.close(); }, 2000);
                        if(result.result == 'add'){
	                        $('#addrolebtn').removeAttr('disabled');
	            			$('#addrolebtn').html('Save');
	            			$("[name='name']").val('');
	                        $('#role-tbody').append('<tr id="row_"'+result.id+'><td style="text-align:center">'+result.count+'</td><td id="td-'+result.id+'">'+result.name+'</td><td style="text-align:center"><a href="javascript:void(0)" onclick="editrole('+result.id+')" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> &nbsp;Edit</a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="deleterole('+result.id+')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> &nbsp;Delete</a></td></tr>');
                    		$("[name='access_id']").append('<option value="'+result.id+'" id="option-'+result.id+'">'+result.name+'</option>');
                    	} else {
                    		$('#addrolebtn').removeAttr('disabled');
	            			$('#addrolebtn').html('Save');
	            			$("[name='name']").val('');
	            			$('#td-'+result.id).html(result.name);
	            			$('#option-'+result.id).html(result.name);
                    	}
                    }
                }
            });
        }
    })
});

function editrole(id){
	$.ajax({
		url: "{{ url('user/roleedit') }}/"+id,
		type: 'GET',
		dataType: 'json',
		success: function(result){
			$("[name='name']").val(result.name);
			$("[name='id']").val(result.id);
		}
	});
}

function deleterole(id){
	swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }, 
	function(){
		$.ajax({
			url: "{{ url('user/roledelete') }}/"+id,
			type: 'GET',
			dataType: 'json',
			success: function(result){
				if(result == 1){
					swal({
		                title: 'Success!',
		                text: 'Role has been deleted.',
		                type: 'success',
		                showConfirmButton: false
		            })
		            setTimeout(function(){ swal.close(); }, 2000);
					$('#row_'+id).remove();
					$('#option-'+id).remove();
				} else {
					swal({
		                title: 'Oops!',
		                text: 'Unable to delete, some users uses this role.',
		                type: 'error',
		                showConfirmButton: false
		            })
		            setTimeout(function(){ swal.close(); }, 3000);
				}
			}
		});
	});
}
</script>
@endsection