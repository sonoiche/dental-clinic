@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-user"></i> Manage Users</a></li>
		<li class="active"><a href="#">User List</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-user"></span> &nbsp;List of Users</h3></div>
		</div>
		<div class="table-responsive box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th style="text-align:center">#</th>
						<th>Name</th>
						<th>Access Level</th>
						<th>Email</th>
						<th style="text-align:center"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $key => $user)
					<tr id="row_{{ $user->id }}">
						<td style="text-align:center">{{ $key+1 }}</td>
						<td>{{ $user->full_name }}</td>
						<td>{{ (count($user->access) <> 0) ? $user->access->name : '' }}</td>
						<td>{{ $user->email }}</td>
						<td style="text-align:center">
							@if($dentalaccess5->can_write)
								@if(Auth::user()->id!=$user->id)
								<a href="{{ url('user/edituser',$user->id) }}" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> &nbsp;Edit</a> &nbsp;
								@else
								<a href="{{ url('user',Hashids::encode($user->id)) }}/edit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> &nbsp;Edit</a> &nbsp;
								@endif
								@if(Auth::user()->id!=$user->id)
								<a href="javascript:void(0)" onclick="deleteuser({{ $user->id }})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> &nbsp;Delete</a>
								@endif
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
function deleteuser(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this User!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){
		$.ajax({
			url: "{{ url('user') }}/"+id,
			type: 'DELETE',
			dataType: 'json',
			success: function(result){
				swal({
                    title: 'Success!',
                    text: 'User has been deleted.',
                    type: 'success',
                    showConfirmButton: false
                })
                $('#row_'+id).remove();
                setTimeout(function(){ swal.close(); }, 1000);
			}
		});
	});
}
</script>
@endsection