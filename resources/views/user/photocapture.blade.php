<div class="modal fade bs-example-modal-sm" id="photoCaptureModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" style="width:30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-camera"></span> Photo Capture</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div align="center" id="photoCapture">
                    <div id="my_photo_booth">

                        <div id="my_camera"></div>
                            {!! Form::hidden('myImage', null,['id'=>'myImage']) !!}
                            <div id="pre_take_buttons" style="padding-top:15px" align="center">
                                <input type="button" class="btn btn-primary btn-xs" value="Take Snapshot" onClick="preview_snapshot()">
                                <input type="button" class="btn btn-primary btn-xs" value="Close" id="cancel_photo" />
                            </div>
                        <div id="post_take_buttons" style="padding-top:15px; display:none" align="center">
                            <input type="button" class="btn btn-primary btn-xs" value="Take Another" onClick="cancel_preview()">
                            <input type="button" class="btn btn-primary btn-xs" value="Save Photo" id="savephoto" onclick="save_photo()" />
                            <input type="button" class="btn btn-primary btn-xs" value="Close" id="close_photo" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>