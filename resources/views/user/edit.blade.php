@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Accounts</a></li>
		<li class="active"><a href="#">Update User</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-user"></span> &nbsp;Update Information</h3>
		</div>
		<div class="box-body">
		{!! Form::open(['url'=>['user',\Hashids::encode($user->id)], 'method'=>'PUT', 'files'=>'true','id'=>'myForm']) !!}
			{!! Form::hidden('user_image',$user->image,['id'=>'user_image']) !!}
			<div class="form-group" id="clinicnameField">
				<label>All fields with <span class="text-red"> *</span> are required.</label><br/>
			@if(Auth::user()->id == Auth::user()->clinic_id)
			<div class="kv-avatar center-block" style="width:200px">
			    {!! Form::file('image', ['class'=>'file-loading','id'=>'avatar']) !!}
			    <div style="font-size:12px; color:#dd4b39; text-align:center">Note: Max File size is 3MB</div>
			</div>
			@endif
			<label>Clinic Name</label>
				@if(Auth::user()->id == Auth::user()->clinic_id)
				{!! Form::text('clinic_name', isset($user->clinic_name) ? $user->clinic_name : null, ['class'=>'form-control','id'=>'clinic_name']) !!}
				@else
				{!! Form::text('clinic_name', isset($user->clinic_name) ? $user->clinic_name : null, ['class'=>'form-control','id'=>'clinic_name','readonly'=>'readonly']) !!}
				@endif
				<div id="clinicnameInput" class="user_custom_field"></div>
			</div>
			<div class="form-group" id="firstnameField">
				<label>First Name</label>
				{!! Form::text('first_name', isset($user->first_name) ? $user->first_name : null, ['class'=>'form-control','id'=>'first_name']) !!}
				<div id="firstnameInput" class="user_custom_field"></div>
			</div>
			<div class="form-group" id="lastnameField">
				<label>Last Name</label>
				{!! Form::text('last_name', isset($user->last_name) ? $user->last_name : null, ['class'=>'form-control','id'=>'last_name']) !!}
				<div id="lastnameInput" class="user_custom_field"></div>
			</div>
			<div class="form-group" id="emailField">
				<label>Email</label>
				{!! Form::email('email', isset($user->email) ? $user->email : null, ['class'=>'form-control','id'=>'email']) !!}
				<div id="emailInput" class="user_custom_field"></div>
			</div>
			<div class="form-group" id="contactField">
				<label>Contact Number</label>
				{!! Form::tel('contact_number', isset($user->contact_number) ? $user->contact_number : null, ['class'=>'form-control','id'=>'contact_number']) !!}
				<div id="contactInput" class="user_custom_field"></div>
			</div>
			{{-- <div class="form-group">
				<label>Code</label>
				<div class="input-group input-group-sm">
                    <input type="password" name="apikey" value="{{ $user->apikey }}" class="form-control" disabled="disabled">
                    <span class="input-group-btn">
                      	{!! Form::button('View Code',['class'=>'btn btn-primary btn-flat','id'=>'viewapibtn']) !!}
                      	{!! Form::button('Generate Code',['class'=>'btn btn-primary btn-flat','id'=>'apibtn']) !!}
                    </span>
                </div>
			</div> --}}
			<!-- <div class="form-group" id="accessField">
				<label>Access Level</label>
				{!! Form::select('access_id', [''=>'--','1'=>'Dentist','2'=>'Secretary'], isset($user->access_id) ? $user->access_id : null, ['class'=>'form-control','id'=>'access_id']) !!}
				<div id="accessInput" class="user_custom_field"></div>
			</div> -->
			@if(Auth::user()->id == Auth::user()->clinic_id)
			<div class="form-group" id="countryField">
				<label>Country</label>
				{!! Form::select('country_id', $countryList, isset($user->country_id) ? $user->country_id : null, ['class'=>'form-control','id'=>'country_id']) !!}
				<div id="countryInput" class="user_custom_field"></div>
			</div>
			@endif
			<div class="form-group">
				{!! Form::hidden('dentist_id', \Hashids::encode($user->id), ['id'=>'dentist_id']) !!}
				{!! Form::hidden('show_code', $show_code, ['id'=>'show_code']) !!}
				{!! Form::button('Save Changes',['class'=>'btn btn-flat btn-primary btn-sm','id'=>'saveinfobtn']) !!}
				{!! Form::button('Change Password', ['class'=>'btn btn-flat btn-warning btn-sm','id'=>'changepasswordbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
<div class="modal fade bs-example-modal-md" id="changepassModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<div class="modal-header" id="noteheader">
              	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-gift"></span> Change Password</h4>
            </div>
            {!! Form::open(['id'=>'passwordForm']) !!}
			<div class="modal-body">
				<div class="form-group">
					<label>Current Password</label>
					{!! Form::password('oldpassword', ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					<label>New Password</label>
					{!! Form::password('password', ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					<label>Retype Password</label>
					{!! Form::password('password_confirm', ['class'=>'form-control']) !!}
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::button('Change Password', ['class'=>'btn btn-primary btn-flat','id'=>'updatepasswordbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@include('user.photocapture')
@endsection
@section('css')
{!! HTML::style('css/fileinput.css') !!}
{!! HTML::script('library/photocapture/webcam.js') !!}
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>
@endsection
@section('js')
{!! HTML::script('js/fileinput.js') !!}
{!! HTML::script('library/photocapture/photocapture.js') !!}
{!! HTML::script('js/waitMe.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	var code = $('#show_code').val();
	if (code) 
	{	
		securityModal();
	}
	$('#saveinfobtn').click(function() {
		if($('#first_name').val()==''){
			$('#firstnameField').addClass('has-error');
		    $('#firstnameInput').html('<i class="fa fa-times-circle-o"></i> First Name is Required.');
		    $('#first_name').focus();
		    return false;
		} else if($('#last_name').val()==''){
			$('#lastnameField').addClass('has-error');
		    $('#lastnameInput').html('<i class="fa fa-times-circle-o"></i> Last Name is Required.');
		    $('#last_name').focus();
		    return false;
		} else if($('#email').val()==''){
			$('#emailField').addClass('has-error');
		    $('#emailInput').html('<i class="fa fa-times-circle-o"></i> Email is Required.');
		    $('#email').focus();
		    return false;
		} else if($('#country_id').val()==''){
			$('#countryField').addClass('has-error');
		    $('#countryInput').html('<i class="fa fa-times-circle-o"></i> Country is Required.');
		    $('#country_id').focus();
		    return false;
		} else {
			$('#saveinfobtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#saveinfobtn').attr('disabled', 'disabled');
			var id = $('#dentist_id').val();
			$('#myForm').submit();
			/*$.ajax({
				url: base_url+'/user/'+id,
				type: 'POST',
				data: {
					clinic_name: $('#clinic_name').val(),
					first_name: $('#first_name').val(),
					last_name: $('#last_name').val(),
					email: $('#email').val(),
					country_id: $('#country_id').val(),
					contact_number: $('#contact_number').val(),
					image: $('#avatar').val(),
					_method: 'PUT'
				},
				success: function(result){
					$('#saveinfobtn').html('Save Changes');
					$('#saveinfobtn').removeAttr('disabled');
					swal("Done Successfully!", "", "success");
				}
			});*/
			
		}
	});

	$('#changepasswordbtn').click(function() {
		$('#changepassModal').modal();
		$('#updatepasswordbtn').html('Change Password');
		$('#updatepasswordbtn').removeAttr('disabled');
		$('#passwordForm')[0].reset();
	});

	$('#updatepasswordbtn').click(function(event) {
		if($("[name='oldpassword']").val() == ''){
			swal({   
				title: "Oops!",   
				text: "Current password field is required.",   
				type: "error",
				showConfirmButton: false
			})
			setTimeout(function(){ swal.close() }, 2000);
			return false;
		} else if($("[name='password']").val() == ''){
			swal({   
				title: "Oops!",   
				text: "Password field is required.",   
				type: "error",
				showConfirmButton: false
			})
			setTimeout(function(){ swal.close() }, 2000);
			return false;
		} else if($("[name='password']").val() != $("[name='password_confirm']").val()){
			swal({   
				title: "Oops!",   
				text: "Password does not match.",   
				type: "error",
				showConfirmButton: false
			})
			setTimeout(function(){ swal.close() }, 2000);
			return false;
		} else {
			var data = $('#passwordForm').serializeArray();
			$('#updatepasswordbtn').html('<i class="fa fa-spin fa-spinner"></i> Saving...');
			$('#updatepasswordbtn').attr('disabled', true);
			$.ajax({
				url: "{{ url('user') }}",
				type: 'POST',
				dataType: 'json',
				data: data,
				success: function(result){
					if(result == 1){
						swal({   
							title: "Success!",   
							text: "Password has been changed!",   
							type: "success",
							showConfirmButton: false
						})
						$('#changepassModal').modal('hide');
						setTimeout(function(){ swal.close() }, 2000);
					}else{
						swal({   
							title: "Oops!",   
							text: "Incorrect current password!",   
							type: "error",
							showConfirmButton: false
						})
						setTimeout(function(){ swal.close() }, 2000);
						$('#updatepasswordbtn').html('Change Password');
						$('#updatepasswordbtn').removeAttr('disabled');
					}
				}
			});
		}
	});

	$('#apibtn').click(function() {
		swal({   
			title: "Generate New API?",   
			text: "This will allow to sync your data from Mobile to Desktop",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Yes please.",   
			closeOnConfirm: false,
			showLoaderOnConfirm: true 
		},
		function(){ 
			$.ajax({
				url: base_url+'/generateapi',
				type: 'GET',
				success: function(result){
					setTimeout(function(){   
						swal("Done Successfully!", "Copy this code to your mobile to sync your accounts.", "success"); 
					}, 2000);
					location.reload();
				}
			});
			
		});
	});

	$('#saveapibtn').click(function() {
		if($('#modal_apikey').val()==''){
			$('#apikeyField').addClass('has-error');
		    $('#apikeyInput').html('<i class="fa fa-times-circle-o"></i> Code is Required.');
		    $('#modal_apikey').focus();
		    return false;
		} else {
			$('#saveapibtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#saveapibtn').attr('disabled', 'disabled');
			var id = $('#dentist_id').val();
			$.ajax({
				url: base_url+'/generateapi/'+id,
				type: 'POST',
				data: {
					apikey: $('#modal_apikey').val(),
					_method: 'PUT'
				},
				success: function(result){
					$('#saveapibtn').html('Save Code');
					$('#saveapibtn').removeAttr('disabled');
					$('#inputAPIModal').modal('hide');
					$('#apikey').val(result);
				}
			});
		}
	});

	$('#viewapibtn').click(securityModal);

	$('.fileinput-remove').click(function() {
    	$('#user_image').val('');
    	$('.file-default-preview').html('<img src="/uploads/no_pic.jpg" alt="Your Clinic Logo/Image" style="width:auto; height:175px">');
    });

	function securityModal() {
		swal({   
			title: "Security",   
			text: "Please enter you password:",   
			type: "input",  
			inputType: "password", 
			showCancelButton: true,   
			closeOnConfirm: false,   
			animation: "slide-from-top",   
			inputPlaceholder: "Write something" 
		}, 
		function(inputValue){   
			if (inputValue === false) return false;      
			if (inputValue === "") {     
				swal.showInputError("You need to write something!");     
				return false   
			}     
			$.ajax({
			 	url: base_url+'/generatepassword',
			 	type: 'POST',
			 	data: {
			 		password: inputValue
			 	},
			 	success: function(result){
			 		if(result == 1){
			 			swal("Error!", "Password is incorrect!", "error"); 
			 		} else {
			 			swal(result, "Copy this code to your mobile to sync your accounts.", "success"); 
			 		}
			 	}
			});
		});
	}

});
function webcam(){
	$('#photoCaptureModal').modal();
	Webcam.set({
        width: 300,
        height: 300,
   
        dest_width: 630,
        dest_height: 470,
   
        crop_width: 0,
        crop_height: 0,
   
        image_format: 'jpeg',
        jpeg_quality: 90,
        flip_horiz: true
    });
    Webcam.attach( '#my_camera' );
}
function save_photo() {
  
	var patient_id = $('#patientid').val();

	$.ajax({
		type: "POST",
		url : base_url+ "/photocapture",
		data: {
		photo: $('#myImage').val(),
		token: $("input[name=_token]").val()
	},
		success : function(result){
			$("#photoCaptureModal").modal('hide');
			$("#image").attr('src', result.picture);
			cancel_preview();
		}
	},"json");

}
var btnCust = '<button type="button" class="btn btn-default" title="Webcam Capture" ' + 
    'onclick="webcam()">' +
    '<i class="glyphicon glyphicon-camera"></i>' +
    '</button>'; 
$("#avatar").fileinput({
    overwriteInitial: true,
    maxFileSize: 3072,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;&nbsp;Browse',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="<?php echo ($user->image) ? url($user->image) : "/uploads/no_pic.jpg"; ?>" alt="Your Clinic Logo/Image" style="width:175px; height:175px">',
    layoutTemplates: {main2: '{preview} '+btnCust+' {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});

</script>
@endsection