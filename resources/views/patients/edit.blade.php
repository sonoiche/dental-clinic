@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">Add New Patient</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left"><h3 class="box-title">Add New Patient</h3></div>
			<div class="pull-right">
				<a href="{{ url('patient', \Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-user"></span> &nbsp;Patient Info</a>
			</div>
		</div>
		<div class="box-body">
			@if($errors->has('image') || $errors->has('email'))
	        <div class="alert alert-danger" id="alertdiv">
	          	{!! $errors->first('email', '<p>:message</p>') !!}
	          	{!! $errors->first('image', '<p>:message</p>') !!}
	        </div>
	        @endif
			{!! Form::open(['url'=>['patient',\Hashids::encode($patient->id)], 'method'=>'PUT', 'files'=>'true','id'=>'myForm']) !!}
			{!! Form::hidden('patient_image',$patient->image,['id'=>'patient_image']) !!}
			@include('patients.patientform')
			{!! Form::close() !!}
		</div>
	</div>
</section>
@include('patients.photocapture')
@endsection

@section('css')
{!! HTML::style('css/fileinput.css') !!}
{!! HTML::script('library/photocapture/webcam.js') !!}
<style>
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
</style>
@endsection

@section('js')
{!! HTML::script('library/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.extensions.js') !!}
{!! HTML::script('js/fileinput.js') !!}
{!! HTML::script('library/photocapture/photocapture.js') !!}
{!! HTML::script('js/waitMe.js') !!}
<script type="text/javascript">
$(document).ready(function($) {
	$(".birthdatemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	$('#savepatientbtn').click(function() {
		if($('#first_name').val()==''){
			$('#firstnameField').addClass('has-error');
		    $('#firstnameInput').html('<i class="fa fa-times-circle-o"></i> First Name is Required.');
		    $('#first_name').focus();
		    return false;
		} else if($('#last_name').val()==''){
			$('#lastnameField').addClass('has-error');
		    $('#lastnameInput').html('<i class="fa fa-times-circle-o"></i> Last Name is Required.');
		    $('#last_name').focus();
		    return false;
		} else if($('#gender').val()==''){
			$('#genderField').addClass('has-error');
		    $('#genderInput').html('<i class="fa fa-times-circle-o"></i> Gender is Required.');
		    $('#gender').focus();
		    return false;
		} else if($('#email').val() !='' && !validEmail($('#email').val())){
			$('#emailField').addClass('has-error');
		    $('#emailInput').html('<i class="fa fa-times-circle-o"></i> Email Address is not valid.');
		    $('#email').focus();
		    return false;
		} else if($('#contact_number').val()==''){
			$('#contactField').addClass('has-error');
		    $('#contactInput').html('<i class="fa fa-times-circle-o"></i> Contact Number is Required.');
		    $('#contact_number').focus();
		    return false;
		} else {
			$('#savepatientbtn').attr('disabled', 'disabled');
			$('#savepatientbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#myForm').submit();
		}
	});
	setTimeout(function(){   
        $('#alertdiv').fadeOut('slow');
    }, 2000);

    $('.fileinput-remove').click(function() {
    	$('#patient_image').val('');
    	$('.file-default-preview').html('<img src="/uploads/no_pic.jpg" alt="Your Avatar" style="width:auto; height:175px">');
    });

    $('#cancel_photo, #close_photo').click(function(){
		$("#photoCaptureModal").modal('hide');
		$("#my_photo_booth").waitMe('hide');
	});

	$('#savephoto').click(function(){
		$('#my_photo_booth').waitMe({
			effect: 'win8_linear',
			text: 'Uploading on process...Please wait.',
			bg: 'rgba(255,255,255,0.7)',
			color:'#000',
			sizeW:'',
			sizeH:'',
			source: 'img.svg'
		});
	});
});

var btnCust = '<button type="button" class="btn btn-default hidden-xs hidden-sm hidden-md" title="Webcam Capture" ' + 
    'onclick="webcam()">' +
    '<i class="glyphicon glyphicon-camera"></i>' +
    '</button>'; 
if(navigator.userAgent.toLowerCase().indexOf("android") > -1 || navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
   $("#avatar").fileinput({
    overwriteInitial: true,
    maxFileSize: 3072,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-camera"></i> &nbsp;&nbsp;Capture',	
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ (isset($patient->image) && $patient->image!='') ? url($patient->image) : url("uploads/no_pic.jpg") }}" alt="Your Avatar" style="width:175px">',
    layoutTemplates: {main2: '{preview} '+btnCust+' {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});               
} else {
	$("#avatar").fileinput({
    overwriteInitial: true,
    maxFileSize: 3072,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;&nbsp;Browse',	
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ (isset($patient->image) && $patient->image!='') ? url($patient->image) : url("uploads/no_pic.jpg") }}" alt="Your Avatar" style="width:175px">',
    layoutTemplates: {main2: '{preview} '+btnCust+' {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});           
}

function webcam(){
	$('#photoCaptureModal').modal();
	Webcam.set({
        width: 300,
        height: 300,
   
        dest_width: 630,
        dest_height: 470,
   
        crop_width: 0,
        crop_height: 0,
   
        image_format: 'jpeg',
        jpeg_quality: 90,
        flip_horiz: true
    });
    Webcam.attach( '#my_camera' );
}
function save_photo() {
  
	var patient_id = $('#patientid').val();

	$.ajax({
		type: "POST",
		url : base_url+ "/patient/photocapture/"+patient_id,
		data: {
		photo: $('#myImage').val(),
		token: $("input[name=_token]").val()
	},
		success : function(result){
			$("#photoCaptureModal").modal('hide');
			$("#image").attr('src', result.picture);
			cancel_preview();
		}
	},"json");

}
function validEmail(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
</script>
@endsection