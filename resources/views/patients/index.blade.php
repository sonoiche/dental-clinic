@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">List of Patient</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
		{!! Form::open(['url'=>'patient/sort','id'=>'sortform']) !!}
		{!! Form::hidden('patient_name', isset($patient_name) ? $patient_name : '') !!}
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-users"></span> &nbsp;List of Patients</h3><br><span style="color:#dd4b39; font-size:14px; padding:7px 27px">{{ $countpatients }} patient records(s)</span></div>
			<div class="input-group pull-right" style="width:30%">
				<span class="input-group-addon">Sort By</span>
				{!! Form::select('sortby',['1'=>'Last Updated','2'=>'First Name','3'=>'Last Name'], isset($sortby) ? $sortby : null,['class'=>'form-control','id'=>'sortorder']) !!}
			</div>
		{!! Form::close() !!}
		</div>
		<div class="box-body">
			<table class="table table-hover" id="tablePatient">
				<thead>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@if(count($patients))
					@foreach($patients as $key => $patient)
					<tr>
						<td class="list-index">{{ $key + 1 }}</td>
						<td  class="list-content">
							<a href="{{ url('patient',\Hashids::encode($patient->id)) }}">
								<div class="image">
						          	@if($patient->image)
						          		@if(\File::exists($patient->image))
						          			{!! HTML::image($patient->image, '', ['class'=>'img-circle list-image']) !!}
						          		@else
						          			{!! HTML::image('uploads/patients/'.$patient->image, '', ['class'=>'img-circle list-image']) !!}
						        		@endif
						        	@else
						        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle list-image']) !!}
						        	@endif
						        </div>
						        <div class="info" style="padding-left:15px">
						        	<p>{{ $patient->full_name }}<br>{{ $patient->contact_number }}<br>Last Updated : {{ display_time($patient->updated_at) }}</p>
						        </div>
						    </a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td></td>
						<td>0 result for patient name "{{ isset($patient_name) ? $patient_name : '' }}"</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</section>
@endsection

@section('css')
{!! HTML::style('library/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('js')
{!! HTML::script('library/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('library/datatables/dataTables.bootstrap.min.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	$('#sortorder').change(function() {
		$('#sortform').submit();
	});

	$('#tablePatient').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": true,
		"autoWidth": false,
		"pageLength": 20
	});
});

ga('send', 'patients_list');
</script>
@endsection