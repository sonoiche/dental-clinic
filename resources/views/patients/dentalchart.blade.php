@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">Add Dental Chart</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><span class="fa fa-info"></span> &nbsp;Patient Information</h3>
			</div>
			<div class="box-body">
				<div class="pull-left image">
		          	@if($patient->image)
		          		{!! HTML::image('uploads/patients/'.$patient->image, '', ['class'=>'img-circle','style'=>'height:100px;width:100px']) !!}
		        	@else
		        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
		        	@endif
		        </div>
		        <div class="pull-left info" style="padding-left:15px">
		        	<p style="font-size:16px !important">
		        		<b>{{ $patient->first_name.' '.$patient->last_name }}</b><br>
		        		{{ $patient->contact_number }}<br>
		        		{{ $patient->address }}<br>
		        		{{ \Carbon\Carbon::parse($patient->birthdate)->format('F d, Y').' - '.\Carbon\Carbon::parse($patient->birthdate)->age.' years old' }}
		        	</p>
		        </div>
		        <div class="pull-right">
		        	<a href="{{ route('patient.show', \Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-user"></span> &nbsp;Patient Info</a>
		        </div>
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Add Dental Chart</h3>
		</div>
		<div class="box-body">
			<div class="text-center">  
				<img class="map" src="{{ url('uploads/teeth_new.jpg') }}" usemap="#chart" alt="" />
				<map name="chart" class="maphilight">
					<area id="1" onclick="checkDental(1)" href="#" alt="" title="" shape="poly" coords="177,586,205,585,232,589,257,598,275,609,292,627,301,647,302,662,297,677,286,692,270,706,255,716,225,730,206,730,175,727,150,715,137,704,129,693,125,672,124,648,130,625,140,603,158,593" />
					<area id="2" onclick="checkDental(2)" href="#" alt="" title="" shape="poly" coords="176,585,163,574,156,558,154,538,155,516,158,497,167,482,180,471,196,469,209,470,245,484,278,499,304,514,322,539,322,557,316,571,301,589,254,597,203,583" />
					<area id="3" onclick="checkDental(3)" href="#" alt="" title="" shape="poly" coords="205,468,250,486,278,490,308,490,328,489,342,481,351,466,350,446,342,425,330,404,315,396,289,386,245,372,217,373,201,382,192,398,190,421,192,439,194,455" />
					<area id="4" onclick="checkDental(4)" href="#" alt="" title="" shape="poly" coords="244,371,237,354,235,339,233,315,235,298,244,283,264,272,292,271,322,279,363,302,370,305,379,321,380,336,371,355,354,375,340,385,311,387,295,387" />
					<area id="5" onclick="checkDental(5)" href="#" alt="" title="" shape="poly" coords="293,270,331,282,362,302,385,303,399,296,412,291,429,275,438,252,436,232,421,213,362,185,339,183,318,186,301,200,291,223,292,270" />
					<area id="6" onclick="checkDental(6)" href="#" alt="" title="" shape="poly" coords="343,172,348,156,356,137,372,121,392,112,413,113,432,126,467,171,476,175,473,205,465,215,444,218,424,215,355,183" />
					<area id="7" onclick="checkDental(7)" href="#" alt="" title="" shape="poly" coords="425,121,423,108,435,90,454,75,483,68,504,69,519,102,527,128,527,152,516,171,498,176,479,176,470,172" />
					<area id="8" onclick="checkDental(8)" href="#" alt="" title="" shape="poly" coords="517,58,520,100,529,133,541,148,559,157,579,158,598,149,607,141,615,95,612,47,591,39,563,37,537,42" />
					<area id="9" onclick="checkDental(9)" href="#" alt="" title="" shape="poly" coords="619,48,617,93,619,119,623,139,631,155,649,165,672,165,690,156,704,129,710,100,714,58,695,44,669,37,639,40" />
					<area id="10" onclick="checkDental(10)" href="#" alt="" title="" shape="poly" coords="727,70,716,94,711,109,704,133,703,159,711,177,726,183,746,183,758,179,771,163,806,121,808,108,794,89,775,75,751,68" />
					<area id="11" onclick="checkDental(11)" href="#" alt="" title="" shape="poly" coords="758,182,759,205,765,217,779,223,796,223,805,220,870,187,888,173,881,149,869,132,856,119,839,114,824,113,808,120" />
					<area id="12" onclick="checkDental(12)" href="#" alt="" title="" shape="poly" coords="873,186,897,185,919,191,934,205,941,230,941,249,940,272,869,304,849,305,827,296,808,280,797,255,799,238,807,222" />
					<area id="13" onclick="checkDental(13)" href="#" alt="" title="" shape="poly" coords="861,306,879,298,938,272,962,271,981,279,994,291,998,304,998,331,994,353,992,367,984,375,935,389,889,387,862,360,851,332,859,308" />
					<area id="14" onclick="checkDental(14)" href="#" alt="" title="" shape="poly" coords="909,402,932,391,990,374,1010,373,1027,379,1039,397,1041,414,1041,432,1040,452,1026,471,975,490,931,492,904,490,886,482,880,466,883,441,898,412,904,405" />
					<area id="15" onclick="checkDental(15)" href="#" alt="" title="" shape="poly" coords="965,495,1029,470,1045,471,1062,481,1073,500,1078,522,1079,544,1076,559,1069,576,1051,588,971,598,930,590,910,563,910,541,923,519,962,497" />
					<area id="16" onclick="checkDental(16)" href="#" alt="" title="" shape="poly" coords="972,601,946,620,940,631,931,651,933,677,951,699,970,714,1008,731,1039,732,1068,724,1091,709,1104,691,1108,649,1097,613,1075,594,1053,588,977,599" />
					<area id="17" onclick="checkDental(17)" href="#" alt="" title="" shape="poly" coords="996,803,977,821,964,844,952,871,949,898,958,922,979,935,1039,943,1084,939,1109,926,1128,901,1134,874,1131,854,1077,806,1039,794,999,801" />
					<area id="18" onclick="checkDental(18)" href="#" alt="" title="" shape="poly" coords="973,935,954,943,942,954,931,978,928,1010,928,1022,975,1050,1021,1069,1046,1078,1068,1053,1088,1016,1091,988,1083,967,1046,947,976,935" />
					<area id="19" onclick="checkDental(19)" href="#" alt="" title="" shape="poly" coords="922,1048,974,1048,1013,1063,1028,1087,1043,1099,1046,1116,1043,1133,1028,1157,1003,1186,961,1173,920,1148,903,1128,895,1106,898,1080,917,1048" />
					<area id="20" onclick="checkDental(20)" href="#" alt="" title="" shape="poly" coords="901,1158,935,1165,964,1175,992,1195,1000,1210,998,1226,985,1242,966,1259,949,1261,877,1229,867,1211,871,1193,879,1178,899,1158" />
					<area id="21" onclick="checkDental(21)" href="#" alt="" title="" shape="poly" coords="830,1284,820,1259,823,1243,840,1231,856,1225,874,1226,952,1263,951,1287,934,1306,918,1318,894,1333,832,1286" />
					<area id="22" onclick="checkDental(22)" href="#" alt="" title="" shape="poly" coords="772,1320,794,1341,809,1370,813,1385,845,1391,868,1386,882,1375,891,1361,895,1336,827,1283,814,1277,794,1278,782,1283,771,1296,771,1318" />
					<area id="23" onclick="checkDental(23)" href="#" alt="" title="" shape="poly" coords="772,1320,802,1352,812,1384,815,1396,783,1422,758,1428,740,1427,725,1404,720,1372,712,1350,700,1341,708,1318,726,1312,747,1312,766,1318" />
					<area id="24" onclick="checkDental(24)" href="#" alt="" title="" shape="poly" coords="698,1339,711,1351,719,1373,724,1405,725,1428,707,1443,680,1447,654,1440,638,1427,629,1401,628,1374,633,1346,645,1334,671,1331,697,1338" />
					<area id="25" onclick="checkDental(25)" href="#" alt="" title="" shape="poly" coords="619,1352,624,1371,623,1398,614,1428,599,1441,572,1449,546,1444,527,1429,525,1409,528,1373,537,1354,554,1340,572,1334,585,1331,601,1334,618,1348" />
					<area id="26" onclick="checkDental(26)" href="#" alt="" title="" shape="poly" coords="438,1386,437,1397,461,1419,487,1430,512,1429,523,1409,527,1372,535,1354,551,1341,543,1318,524,1310,506,1309,487,1315,439,1384" />
					<area id="27" onclick="checkDental(27)" href="#" alt="" title="" shape="poly" coords="356,1335,422,1285,441,1279,459,1278,477,1285,485,1295,485,1317,435,1388,411,1392,383,1389,364,1371,358,1355,356,1340" />
					<area id="28" onclick="checkDental(28)" href="#" alt="" title="" shape="poly" coords="302,1260,299,1278,309,1299,322,1311,355,1334,424,1283,434,1261,429,1244,415,1232,397,1225,376,1228,304,1259" />
					<area id="29" onclick="checkDental(29)" href="#" alt="" title="" shape="poly" coords="279,1181,298,1171,321,1164,351,1156,371,1171,385,1192,384,1212,376,1229,302,1262,283,1259,264,1241,253,1223,252,1208,261,1193,276,1182" />
					<area id="30" onclick="checkDental(30)" href="#" alt="" title="" shape="poly" coords="249,1187,287,1174,320,1158,341,1142,351,1127,357,1097,337,1052,317,1049,289,1049,263,1054,232,1069,226,1082,213,1095,206,1111,210,1138,228,1164" />
					<area id="31" onclick="checkDental(31)" href="#" alt="" title="" shape="poly" coords="191,953,220,942,257,934,298,944,314,959,320,978,324,993,325,1008,324,1026,279,1049,207,1079,180,1048,164,1014,160,988,168,968,189,954" />
					<area id="32" onclick="checkDental(32)" href="#" alt="" title="" shape="poly" coords="213,793,183,803,165,813,144,831,122,853,118,878,124,903,140,924,160,938,183,943,217,945,270,937,290,929,303,906,303,881,291,851,280,829,263,808,243,796,216,793" />       
				</map>
		    </div>
		</div>
	</div>
</section>
@include('patients.dentalnotemodal')
@endsection

@section('css')
<style type="text/css">
.map {
	margin: 0 auto;
	border: none;
	height: 450px;
	width: auto;
}
</style>
@endsection

@section('js')
{!! HTML::script('js/jquery.rwdImageMaps.js') !!}
{!! HTML::script('js/jquery.maphilight.js') !!}
<script type="text/javascript">
$(document).ready(function() {	
	$('img[usemap]').rwdImageMaps();
	<?php foreach($dentalcharts as $dentalchart){ ?>
		$('#<?php echo $dentalchart->chart_id; ?>').data('maphilight', {
			fillColor: '<?php echo $dentalchart->color_code; ?>',
			fillOpacity: 0.5,
			stroke:false,
			// strokeColor: 'ffffff',
			alwaysOn:true
		});
	<?php } ?>

	setTimeout(function() {
		$('.map').maphilight();
	}, 1000);

	$('#savenotebtn').click(function() {
		if(!$("input[name='color_code']:checked").val()){
			swal({   
				title: "Please select a color.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
		} else if($('#notes').val()==''){
			swal({   
				title: "Note field is required.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    $('#notes').focus();
		    return false;
		} else {
			$('#savenotebtn').attr('disabled', 'disabled');
			$('#savenotebtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#dentalnoteform').submit();
		}
	});
});
function checkDental(id){
	$('#dentalcharttable').empty();
	$('#chart_id').val(id);
	$('#notes').val('');
	$('#colorbtn7F8C8D,#colorbtnffc900,#colorbtnef473a,#colorbtn33cd5f,#colorbtn11c1f3,#colorbtnF39C12,#colorbtn2E3E51,#colorbtnFDAEC6,#colorbtn8D6EB1').css('border', 'none');
	$.ajax({
		url: base_url+'/dentalchart/editnote/'+id+'/<?php echo $patient->id; ?>',
		type: 'GET',
		success: function(result){
			if(result.id != '0'){
				var datas = result;         
	            $.each(datas, function(index, data) {
	            	$('#dentalcharttable').append('<tr><td><div class="pull-left image" style="color:#'+data.color_code+'"><span class="fa fa-circle"></span></div><div class="pull-left info" style="padding-left:15px">Tooth Number : '+data.chart_id+'<br>'+data.notes+'</div></td></tr>');
	            });
				$('#dentalnoteviewModal').modal();
			} else {
				$('#dentalnoteModal').modal();
				$('#dentalnoteform').attr('action', base_url+'/dentalnote');
			}
		}
	});
	
}
function colorclick(id){
	$('#colorbtn7F8C8D,#colorbtnffc900,#colorbtnef473a,#colorbtn33cd5f,#colorbtn11c1f3,#colorbtnF39C12,#colorbtn2E3E51,#colorbtnFDAEC6,#colorbtn8D6EB1').css('border', 'none');
	$('#colorbtn'+id).css('border', '2px solid #000');
}
</script>
@endsection