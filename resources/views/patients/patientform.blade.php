<label>All fields with <span class="text-red"> *</span> are required.</label><br/>
<div class="kv-avatar center-block" style="width:200px">
    {!! Form::file('image', ['class'=>'file-loading','id'=>'avatar']) !!}
    <div style="font-size:12px; color:#dd4b39; text-align:center">Note: Max File size is 3MB</div>
</div>
<div class="form-group" id="firstnameField">
	<label>First Name  <span class="text-red"> *</span></label>
	{!! Form::text('first_name', isset($patient->first_name) ? $patient->first_name : null, ['class'=>'form-control','id'=>'first_name']) !!}
	<div id="firstnameInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="middlenameField">
	<label>Middle Name</label>
	{!! Form::text('middle_name', isset($patient->middle_name) ? $patient->middle_name : null, ['class'=>'form-control','id'=>'middle_name']) !!}
	<div id="middlenameInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="lastnameField">
	<label>Last Name  <span class="text-red"> *</span></label>
	{!! Form::text('last_name', isset($patient->last_name) ? $patient->last_name : null, ['class'=>'form-control','id'=>'last_name']) !!}
	<div id="lastnameInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="genderField">
	<label>Gender <span class="text-red"> *</span></label>
	{!! Form::select('gender', [''=>'--','Male'=>'Male','Female'=>'Female'], isset($patient->gender) ? $patient->gender : null, ['class'=>'form-control','id'=>'gender']) !!}
	<div id="genderInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="emailField">
	<label>Email</label>
	{!! Form::email('email', isset($patient->email) ? $patient->email : null, ['class'=>'form-control','id'=>'email']) !!}
	<div id="emailInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group" id="contactField">
	<label>Contact <span class="text-red"> *</span></label>
	{!! Form::text('contact_number', isset($patient->contact_number) ? $patient->contact_number : null, ['class'=>'form-control','id'=>'contact_number','onkeypress'=>'return isNumberKey(event)']) !!}
	<div id="contactInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group">
	<label>Address</label>
	{!! Form::text('address', isset($patient->address) ? $patient->address : null, ['class'=>'form-control','id'=>'address']) !!}
</div>
<div class="form-group" id="birthdateField">
	<label>Birthdate</label>
	<div class="input-group">
        <div class="input-group-addon" style="padding:0 10px 0 10px">
        	<i class="fa fa-calendar"></i>
        </div>
		{!! Form::text('birthdate', isset($patient->birthdate) ? $patient->birthdate_form : null, ['id'=>'birthdate','class'=>'form-control birthdatemask']) !!}
	</div>
	<div id="birthdateInput" style="font-size:11px; color:#dd4b39"></div>
</div>
<div class="form-group">
	<label>Notes</label>
	{!! Form::textarea('notes', isset($patient->notes) ? $patient->notes : null, ['class'=>'form-control','id'=>'notes','style'=>'height:100px;resize:none']) !!}
</div>
<div class="form-group">
	{!! Form::button('Save', ['class'=>'btn btn-primary btn-flat btn-sm','id'=>'savepatientbtn']) !!}
</div>