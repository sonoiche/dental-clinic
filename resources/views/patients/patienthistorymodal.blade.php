<div class="modal fade bs-example-modal-md" id="patientHistoryModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-medkit"></span> Medical History</h4>
            </div>
			<div class="modal-body">
				<div id="history-form">
					{!! Form::open(['id'=>'patienthistoryform']) !!}

					<div class="form-group" id="notesField">
						{!! Form::textarea('medical_history', $patient->medical_history, ['class'=>'form-control','id'=>'medical_history','style'=>'height:100px; resize:none']) !!}
					</div>
					<div class="form-group">
						{!! Form::hidden('patient_id', \Hashids::encode($patient->id), ['id'=>'patient_id']) !!}
						{!! Form::button('Save Medical History', ['class'=>'btn btn-primary btn-sm btn-flat','id'=>'savehistorybtn']) !!}
					</div>
					{!! Form::close() !!}
				</div>
				<div id="history-display">
					<div class="text-right">
						<a href="#" onclick="editPatientHistory()" class="btn btn-primary btn-sm"><span class="fa fa-pencil"></span> &nbsp;Edit</a>
					</div>
					<div id="history-content"></div>
				</div>
			</div>
		</div>
	</div>
</div>