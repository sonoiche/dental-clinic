<div class="modal fade bs-example-modal-md" id="photoModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	{!! Form::open(['url'=>'dentalphotos','id'=>'formPhoto','files'=>'true']) !!}
            {!! Form::hidden('_method','PUT',['id'=>'photomethodtype','disabled'=>'disabled']) !!}
            <div class="modal-header" id="noteheader">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-photo"></span> Dental Photos</h4>
            </div>
			<div class="modal-body">
				<div class="kv-avatar center-block" style="width:200px">
				    <input id="avatar" name="photo" type="file" class="file-loading">
				    <div style="font-size:12px; color:#dd4b39; text-align:center">Note: Max File size is 5MB</div>
				</div>
				<div class="form-group" id="captionField">
					<label>Caption</label>
					{!! Form::textarea('caption', null, ['class'=>'form-control','id'=>'caption','style'=>'height:100px; resize:none']) !!}
					<div id="captionInput" style="font-size:11px; color:#dd4b39"></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::hidden('patient_id', $patient->id) !!}
				{!! Form::hidden('photoavatar','',['id'=>'photoavatar']) !!}
				{!! Form::button('Save Photo', ['class'=>'btn btn-primary btn-flat btn-sm','id'=>'savephotobtn']) !!}
				<button class="btn btn-sm btn-default" data-dismiss="modal" id="closephotobtn" aria-label="Close">Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>