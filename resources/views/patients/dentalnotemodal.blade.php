<div class="modal fade bs-example-modal-md" id="dentalnoteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list-alt"></span> Dental Notes</h4>
            </div>
			<div class="modal-body">
				{!! Form::open(['url'=>'dentalnote','id'=>'dentalnoteform']) !!}
				{!! Form::hidden('_method','PUT',['id'=>'methodtype','disabled'=>'disabled']) !!}
				<div class="form-group" id="colorField">
					<label>Choose a Color</label><br>
					<div data-toggle="buttons">
						<label class="btn" id="colorbtn7F8C8D" style="background-color:#7F8C8D" onclick="colorclick('7F8C8D')">
							<input type="radio" name="color_code" autocomplete="off" id="7F8C8D" value="7F8C8D"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtnffc900" style="background-color:#ffc900" onclick="colorclick('ffc900')">
							<input type="radio" name="color_code" autocomplete="off" id="ffc900" value="ffc900"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtnef473a" style="background-color:#ef473a" onclick="colorclick('ef473a')">
							<input type="radio" name="color_code" autocomplete="off" id="ef473a" value="ef473a"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtn33cd5f" style="background-color:#33cd5f" onclick="colorclick('33cd5f')">
							<input type="radio" name="color_code" autocomplete="off" id="33cd5f" value="33cd5f"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtn11c1f3" style="background-color:#11c1f3" onclick="colorclick('11c1f3')">
							<input type="radio" name="color_code" autocomplete="off" id="11c1f3" value="11c1f3"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtnF39C12" style="background-color:#F39C12" onclick="colorclick('F39C12')">
							<input type="radio" name="color_code" autocomplete="off" id="F39C12" value="F39C12"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtn2E3E51" style="background-color:#2E3E51" onclick="colorclick('2E3E51')">
							<input type="radio" name="color_code" autocomplete="off" id="2E3E51" value="2E3E51"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtnFDAEC6" style="background-color:#FDAEC6" onclick="colorclick('FDAEC6')">
							<input type="radio" name="color_code" autocomplete="off" id="FDAEC6" value="FDAEC6"> &nbsp;&nbsp;&nbsp;
						</label>
						<label class="btn" id="colorbtn8D6EB1" style="background-color:#8D6EB1" onclick="colorclick('8D6EB1')">
							<input type="radio" name="color_code" autocomplete="off" id="8D6EB1" value="8D6EB1"> &nbsp;&nbsp;&nbsp;
						</label>
					</div>
				</div>
				<div class="form-group" id="notesField">
					<label>Notes</label>
					{!! Form::textarea('notes', '', ['class'=>'form-control','id'=>'notesxx','style'=>'height:100px; resize:none']) !!}
				</div>
				<div class="form-group">
					{!! Form::hidden('chart_id','',['id'=>'chart_id']) !!}
					{!! Form::hidden('chart_name','',['id'=>'chart_name']) !!}
					{!! Form::hidden('patient_id', \Hashids::encode($patient->id)) !!}
					{!! Form::button('Save Notes', ['class'=>'btn btn-primary btn-sm btn-flat','id'=>'savenotebtn']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-md" id="viewnoteModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" id="noteheader">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list-alt"></span> Dental Notes</h4>
            </div>
			<div class="modal-body">
				<div id="notecontent"></div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-md" id="dentalnoteviewModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" id="noteheader">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list-alt"></span> Dental Notes</h4>
            </div>
			<div class="modal-body">
				<table class="table table-hover">
					<tbody id="dentalcharttable">
						
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-primary" id="addanotherbtn" style="display:none">Add Another Note</button>
				<button class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>