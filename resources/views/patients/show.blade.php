@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">View Patient Info</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<a data-toggle="collapse" data-parent="#accordion" href="#info" style="color:#444">
				<h3 class="box-title"><span class="fa fa-info"></span> &nbsp;Patient Information</h3>
			</a>
		</div>
		<div id="info" class="panel-collapse collapse in">
			<div class="box-body">
				<div class="pull-left image">
		          	@if(isset($patient->image) && $patient->image!='')
		        		@if(\File::exists($patient->image))
		        			<img src="{{ url($patient->image) }}" class="img-circle" style="height:100px;width:100px" /> 
		        		@else
		        			<img src="{{ url('uploads/patients/'.$patient->image) }}" class="img-circle" style="height:100px;width:100px" /> 
		        		@endif
		        	@else
		        		<img src="{{ url('uploads/no_pic.jpg') }}" class="img-circle" style="height:65;width:65px" /> 
		        	@endif
		        </div>
		        <div class="pull-left info" style="padding-left:15px">
		        	<p style="font-size:16px !important">
		        		<h4><b>{{ $patient->full_name }}</b></h4>
		        		<div style="font-size:14px; margin-top: -8px; padding-bottom: 10px; !important">
		        		{!! ($patient->contact_number) ? '<i class="fa fa-phone fa-fw"></i> : '.$patient->contact_number.'<br>' : '<br>' !!}
		        		{!! Form::hidden('medical_history', $patient->medical_history) !!}
		        		</div>
		        		<a href="https://hangouts.google.com/" class="btn btn-round btn-default"><i class="fa fa-phone fa-dc-icon"></i></a>
		        		<a href="mailto:{{ $patient->email }}" class="btn btn-round btn-default"><i class="fa fa-envelope fa-dc-icon"></i></a>
		        		<a href="javascript:void(0)" onclick="exportpdf('{{ \Hashids::encode($patient->id) }}')" class="btn btn-round btn-default"><i class="fa fa-file-pdf-o fa-dc-icon"></i></a>
		        	</p>
		        </div>
		        <div class="pull-right">
		        	@if($dentalaccess2->can_write)
		        	<a href="{{ url('patient',\Hashids::encode($patient->id)) }}/edit" class="btn btn-primary btn-sm"><span class="fa fa-pencil"></span> &nbsp;Edit</a>
		        	<a onclick="deletepatient('{{ \Hashids::encode($patient->id) }}')" class="btn btn-primary btn-sm"><span class="fa fa-trash"></span> &nbsp;Delete</a>
		        	@endif
		        	<a onclick="addPatientHistory()" class="btn btn-primary btn-sm"><span class="fa fa-history"></span> &nbsp;Medical History</a>
		        </div>
				<table class="table">
					<tbody>
						<tr>
							<td style="width:15%">Email</td>
							<td>: {{ $patient->email }}</td>
						</tr>
						<tr>
							<td>Gender</td>
							<td>: {{ $patient->gender }}</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>: {{ $patient->address }}</td>
						</tr>
						<tr>
							<td>Birthdate</td>
							<td>: {{ $patient->birthdate_display }}</td>
						</tr>
						<tr>
							<td>Age</td>
							<td>: {{ $patient->age }}</td>
						</tr>
						<tr>
							<td>Notes</td>
							<td>: {{ $patient->notes }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	{{-- <div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#history" style="color:#444">
					<h3 class="box-title"><span class="fa fa fa-medkit"></span> &nbsp;Medical History &nbsp;&nbsp; {{ (count($patient_history)) ? '( '.count($patient_history).' )' : '' }}</h3><br>
					<span style="padding:10px 29px">Add Patient Medical History</span>
				</a>
			</div>
			<div class="pull-right btn-group">
				<!-- <a id="viewchartbtn" class="btn btn-primary btn-sm"><span class="fa fa-eye"></span> &nbsp;View Dental Chart</a> -->
				<a onclick="addPatientHistory()" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Medical History</a>
			</div>
		</div>
		<div id="history" class="panel-collapse collapse">
			<div class="box-body">
				@if(count($patient_history))
				<table class="table table-hover" id="tableappointment">
					<thead>
						<th></th>
					</thead>
					<tbody>
						@foreach($patient_history as $key => $history)
						<tr>
							<td>
								<div class="pull-left image" style="padding-top:10px">
									<span class="fa fa-circle"></span>
								</div>
								<div class="pull-left info" style="padding-left:15px">
									{{ $history->title }}<br>
									{!! ($history->history) ? $history->history : '' !!}
								</div>
								<div class="pull-right">
									<a onclick="addPatientHistory('{{ \Hashids::encode($history->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
									<a onclick="deletePatientHistory('{{ \Hashids::encode($history->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>
	</div> --}}
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#chart" style="color:#444">
					<h3 class="box-title"><span class="fa fa-bar-chart"></span> &nbsp;Dental Chart &nbsp;&nbsp; {{ (count($dentalcharts)) ? '( '.count($dentalcharts).' )' : '' }}</h3><br>
					<span style="padding:10px 29px">Edit patient's chart</span>
				</a>
			</div>
			@if($dentalaccess2->can_write)
			<div class="pull-right btn-group">
				<!-- <a id="viewchartbtn" class="btn btn-primary btn-sm"><span class="fa fa-eye"></span> &nbsp;View Dental Chart</a> -->
				<a id="viewchartbtn" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Dental Chart</a>
			</div>
			@endif
		</div>
		<div id="chart" class="panel-collapse collapse">
			<div class="box-body">
				@if(count($dentalcharts))
					<table class="table table-hover" id="tablechart">
						<thead>
							<th></th>
						</thead>
						<tbody>
							@foreach($dentalcharts as $key => $dentalchart)
							<tr>
								<td>
									<div class="pull-left image" style="color:#{{ $dentalchart->color_code }}">
										<span class="fa fa-circle"></span>
									</div>
									<div class="pull-left info" style="padding-left:15px">
										@if(ctype_alpha($dentalchart->chart_name) == 1)
										Tooth Letter: 
										@else
										Tooth Number: 
										@endif 
										{{ $dentalchart->chart_name }} <br>
										{{ str_limit($dentalchart->notes,70) }} &nbsp;
										{!! (strlen($dentalchart->notes) > 70) ? '<a onclick="viewdentalchart(\''.\Hashids::encode($dentalchart->id).'\')" class="btn btn-primary btn-xs">View More</a>' : '' !!}
										- &nbsp; {{ display_time($dentalchart->updated_at) }}
									</div>
									@if($dentalaccess2->can_write)
									<div class="pull-right info">
										<a onclick="adddentalnote('{{ \Hashids::encode($dentalchart->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
										<a onclick="deletedentalnote('{{ \Hashids::encode($dentalchart->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
									</div>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#notes" style="color:#444">
					<h3 class="box-title"><span class="fa fa-list-alt"></span> &nbsp;Dental Notes &nbsp;&nbsp; {{ (count($dentalnotes)) ? '( '.count($dentalnotes).' )' : '' }}</h3><br>
					<span style="padding:10px 29px">Review notes made from the patient's visit</span>
				</a>
			</div>
			@if($dentalaccess2->can_write)
			<div class="pull-right">
				<a onclick="adddentalnote()" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Dental Note</a>
			</div>
			@endif
		</div>
		<div id="notes" class="panel-collapse collapse">
			<div class="box-body">
				@if(count($dentalnotes))
					<table class="table table-hover" id="tablenote">
						<thead>
							<th></th>
						</thead>
						<tbody>
							@foreach($dentalnotes as $key => $dentalnote)
							<tr>
								<td>
									<div class="pull-left image" style="color:#{{ $dentalnote->color_code }}">
										<span class="fa fa-circle"></span>
									</div>
									<div class="pull-left info" style="padding-left:15px">
										{{ str_limit($dentalnote->notes,70) }} &nbsp;
										{!! (strlen($dentalnote->notes) > 70) ? '<a onclick="viewdentalnote(\''.\Hashids::encode($dentalnote->id).'\')" class="btn btn-primary btn-xs">View More</a>' : '' !!}
										- &nbsp; {{ display_time($dentalnote->updated_at) }}
									</div>
									@if($dentalaccess2->can_write)
									<div class="pull-right info">
										<a onclick="adddentalnote('{{ \Hashids::encode($dentalnote->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
										<a onclick="deletedentalnote('{{ \Hashids::encode($dentalnote->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
									</div>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@endif
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#appointment" style="color:#444">
					<h3 class="box-title"><span class="fa fa-calendar-o"></span> &nbsp;Appointment Schedule &nbsp;&nbsp; {{ (count($patientappointments)) ? '( '.count($patientappointments).' )' : '' }}</h3><br>
					<span style="padding:10px 29px">Your patient's next appointment list</span>
				</a>
			</div>
			@if($dentalaccess2->can_write)
			<div class="pull-right">
				<a href="{{ url('appointment/create',\Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Appointment</a>
			</div>
			@endif
		</div>
		<div id="appointment" class="panel-collapse collapse">
			<div class="box-body">
				@if(count($patientappointments))
				<table class="table table-hover" id="tableappointment">
					<thead>
						<th></th>
					</thead>
					<tbody>
						@foreach($patientappointments as $key => $patientappointment)
						<tr>
							<td>
								<div class="pull-left image" {!! ($patientappointment->remarks) ? 'style="padding-top:20px"' : 'style="padding-top:10px"' !!}>
									<span class="fa fa-circle"></span>
								</div>
								<div class="pull-left info" style="padding-left:15px">
									<b>{{ $patientappointment->name }}</b><br>
									{{ display_time($patientappointment->schedule) }}<br/>
									{!! ($patientappointment->remarks) ? $patientappointment->remarks : '' !!}
								</div>
								@if($dentalaccess2->can_write)
								<div class="pull-right">
									<a onclick="viewappointment('{{ \Hashids::encode($patientappointment->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
									<a onclick="deleteappointment('{{ \Hashids::encode($patientappointment->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
								</div>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@endif
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#photo" style="color:#444">
					<h3 class="box-title"><span class="fa fa-photo"></span> &nbsp;Photos &nbsp;&nbsp; {{ (count($dentalphotos)) ? '( '.count($dentalphotos).' )' : '' }}</h3><br>
					<span style="padding:10px 29px">Your patient's dental photos for reference</span>
				</a>
			</div>
			@if($dentalaccess2->can_write)
			<div class="pull-right">
			<a onclick="viewphotoModal()" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Photos</a>
			</div>
			@endif
		</div>
		<div id="photo" class="panel-collapse collapse">
			<div class="box-body">
			@if(count($dentalphotos))
			<table class="table table-hover" id="tablephoto">
				<thead>
					<th></th>
				</thead>
				<tbody>
					@foreach($dentalphotos as $key => $photo)
					<tr>
						<td>
							<div class="pull-left image">
								@if(\File::exists($photo->photo))
								<img src="{{ url($photo->photo) }}" style="height:150px; width:auto" /> 
								@else
								{!! HTML::image('uploads/photos/'.Auth::user()->id.'/'.$patient->id.'/'.$photo->photo,'',['style'=>'height:150px; width:auto']) !!}
								@endif
							</div>
							<div class="pull-left info" style="padding-left:15px">
								{{ $photo->caption }}<br>
								{{ display_time($photo->created_at) }}
							</div>
							@if($dentalaccess2->can_write)
							<div class="pull-right">
								<a onclick="viewphotoModal('{{ \Hashids::encode($photo->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
								<a onclick="deletephoto('{{ \Hashids::encode($photo->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
							</div>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			</div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<div class="pull-left">
				<a data-toggle="collapse" data-parent="#accordion" href="#payment" style="color:#444">
					<h3 class="box-title"><span class="fa fa-credit-card"></span> &nbsp;Payments &nbsp;&nbsp;{{ (count($payments) || count($installments)) ? '- &nbsp;Total Revenue : '.$patient->totalpayment($patient->id).' '.$user->currency.'' : '' }}</h3><br>
					<span style="padding:10px 29px">Check your patient's payment information in this list</span>
				</a>
			</div>
			@if($dentalaccess2->can_write)
			<div class="pull-right">
				<a href="{{ url('payments/create',\Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Payment</a>
			</div>
			@endif
		</div>
		<div id="payment" class="panel-collapse collapse">
			<div class="box-body">
			@if(count($payments) || count($installments))
				<ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#cash" aria-controls="cash" role="tab" data-toggle="tab"><b>Cash</b></a></li>
				    <li role="presentation"><a href="#installments" aria-controls="installments" role="tab" data-toggle="tab"><b>Installments</b></a></li>
			  	</ul>
			  	<div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="cash">
						<table class="table table-hover" id="tablepayment">
							<thead>
								<th></th>
								<th></th>
								<th></th>
							</thead>
							<tbody>
								@foreach($payments as $key => $payment)
								<tr>
									<td style="width:20%">
										@if($payment->chart_id!='0')<b>Tooth Number</b><br>@endif
										<b>Date</b><br>
										<b>Amount</b><br>
										<b>Remarks</b>
									</td>
									<td style="width:40%">
										@if($payment->chart_id!='0'){{ $payment->chart_id }}<br>@endif
										{{ \Carbon\Carbon::parse($payment->payment_date)->format('M d, Y') }}<br>
										{{ $payment->amount.' '.\Auth::user()->currency }}<br>
										{{ $payment->remarks }}
									</td>
									<td style="width:10%" align="right">
										@if($dentalaccess2->can_write)
										<a href="{{ url('payments',\Hashids::encode($payment->id)) }}/edit" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
										<a onclick="deletepayment({{ $payment->id }})" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="installments">
			    		<table class="table table-hover" id="tableinstallment">
							<thead>
								<th></th>
								<th></th>
								<th></th>
							</thead>
							<tbody>
								@foreach($installments as $key => $installment)
								<tr>
									<td style="width:20%">
										<b>Date</b><br>
										<b>Total Amount</b><br>
										<b>Paid</b><br>
										<b>Balance</b><br>
										<b>Last Payment</b><br>
										<b>No. of Payments</b><br>
										<b>Remarks</b>
									</td>
									<td style="width:40%">
										{{ \Carbon\Carbon::parse($installment->payment_date)->format('M d, Y') }}<br>
										{{ $installment->amount.' '.$user->currency }}<br>
										{{ $installment->paid }}<br/>
										<span class="text-red">{{ $installment->amount - $installment->paid }}</span><br/>
										{{ $installment->last_pay }}<br/>
										{{ $installment->total }}<br/>
										{{ $installment->remarks }}
									</td>
									<td style="width:10%" align="right">
										@if($dentalaccess2->can_write)
										<a title="View installment payments" href="{{ url('installments',\Hashids::encode($installment->id)) }}" class="btn btn-primary btn-xs"><span class="fa fa-credit-card"></span></a>
										<a title="Add new installment payment" href="{{ url('installments/create',\Hashids::encode($installment->id)) }}" class="btn btn-primary btn-xs"><span class="fa fa-plus"></span></a>
										<a href="{{ url('payments',\Hashids::encode($installment->id)) }}/edit" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
				    	</table>
				    </div>
				</div>
			@endif
			</div>
		</div>
	</div>
</section>
@include('patients.patienthistorymodal')
@include('patients.dentalnotemodal')
@include('patients.appointmentmodal')
@include('patients.photomodal')
@include('patients.photocapture')
@include('patients.dentalchartmodal')
@endsection

@section('css')
{!! HTML::style('css/fileinput.css') !!}
{!! HTML::style('library/timepicker/bootstrap-timepicker.min.css') !!}
{!! HTML::style('library/datatables/dataTables.bootstrap.css') !!}
{!! HTML::script('library/photocapture/webcam.js') !!}
<style type="text/css">
.ui-datepicker{
	z-index: 9999 !important;
}
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;
    max-width: 220px;
}
.adult, .pediatric{
	margin: 0 auto;
	border: none;
	height: 400px;
	width: auto;
}
#dentalnoteviewModal, #dentalnoteModal {
	z-index: 9999 !important;
}
.btn-round {
	border-radius: 50px;
    padding: 10px;
    width: 35px;
    height: 35px;
    border: 2px #196eee solid;
}
.btn-round:hover {
	border: 2px #0b55c4 solid !important;
}
.fa-dc-icon {
	vertical-align: top;
    margin-top: -2px;
    color: #196eee;
    margin-left: -1px;
}
</style>
@endsection

@section('js')
{!! HTML::script('library/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.extensions.js') !!}
{!! HTML::script('library/timepicker/bootstrap-timepicker.min.js') !!}
{!! HTML::script('js/fileinput.js') !!}
{!! HTML::script('library/photocapture/photocapture.js') !!}
{!! HTML::script('js/waitMe.js') !!}
{!! HTML::script('js/jquery.rwdImageMaps.js') !!}
{!! HTML::script('js/jquery.maphilight.js') !!}
{!! HTML::script('library/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('library/datatables/dataTables.bootstrap.min.js') !!}
<script type="text/javascript">
ga('send', 'patient_information');
$(document).ready(function() {
	$('#dentalnoteform').on('submit', function(){
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Add', 'Add Dental Note');
	});

	$('#formPhoto').on('submit', function(){
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Add', 'Add Patient Photo');
	});

	$(".datemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	$(".timepicker").timepicker({
	  	showInputs: false
	});
	$('#tablechart, #tablenote, #tableappointment').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": true,
		"autoWidth": false,
		"lengthMenu": [5, 20, 40, 60, 80, 100], 
		"pageLength": 5
	});
	$('#tablepayment').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": true,
		"autoWidth": false,
		"lengthMenu": [5, 20, 40, 60, 80, 100], 
		"pageLength": 5
	});
	$('#tableinstallment').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": false,
		"info": true,
		"autoWidth": false,
		"lengthMenu": [5, 20, 40, 60, 80, 100], 
		"pageLength": 5
	});
	$('#tablephoto').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": false,
		"info": true,
		"autoWidth": false,
		"lengthMenu": [2, 20, 40, 60, 80, 100], 
		"pageLength": 2
	});
	$('#savenotebtn').click(function() {
		if(!$("input[name='color_code']:checked").val()){
			swal({   
				title: "Please select a color.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
		} else if($('#notesxx').val()==''){
			swal({   
				title: "Note field is required.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    $('#notesxx').focus();
		    return false;
		} else {
			$('#savenotebtn').attr('disabled', 'disabled');
			$('#savenotebtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#dentalnoteform').submit();
		}
	});
	$('#saveappointmentbtn').click(function() {
		if($('#patient_name').val()==''){
			$('#nameField').addClass('has-error');
		    $('#nameInput').html('<i class="fa fa-times-circle-o"></i> Patient Name is Required.');
		    $('#patient_name').focus();
		    return false;
		} else if($('#appointmentdate').val()==''){
			$('#appointmentdateField').addClass('has-error');
		    $('#appointmentdateInput').html('<i class="fa fa-times-circle-o"></i> Appointment Date is Required.');
		    $('#appointmentdate').focus();
		    return false;
		} else {
			$('#saveappointmentbtn').attr('disabled', 'disabled');
			$('#saveappointmentbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			var id = $('#appointment_id').val();
			$.ajax({
				url: base_url+'/appointment/'+id,
				type: 'POST',
				data: {
					name: $('#patient_name').val(),
					appointmentdate: $('#appointmentdate').val(),
					appointmenttime: $('#appointmenttime').val(),
					remarks: $('#remarks').val(),
					appointment_id: $('#appointment_id').val(),
					_method: 'PUT'
				},
				success: function(result){
					$('#appointmentModal').modal('hide');
					swal({   
						title: "Appointment Updated Successfull!",   
						text: "",   
						type: "success",
						showConfirmButton: false
					});
					setTimeout(function(){ 
						location.reload();
					}, 2000);
				}
			});
			
		}
	});

	$('#savephotobtn').click(function() {
		if($('#avatar').val()=='' && $('#photoavatar').val()==''){
			swal({   
				title: "Please input a file.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
		} else if($('#caption').val()==''){
			swal({   
				title: "Caption is a required field.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    return false;
		} else {
			$('#savephotobtn').attr('disabled', 'disabled');
			$('#savephotobtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#formPhoto').submit();
		}
	});

	$('#savehistorybtn').click(function() {
		if($('textarea#medical_history').val()==''){
			swal({   
				title: "Medical History is required.",   
				text: "",   
				timer: 2000,   
				showConfirmButton: false, 
				type: "warning" 
			});
		    $('textarea#history').focus();
		    return false;
		} else {
			$('#savehistorybtn').attr('disabled', 'disabled');
			$('#savehistorybtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Add', 'Add/Edit Medical History');
			$.ajax({
				type: "POST",
				url : base_url+ "/patient/medical_history",
				data: {
					medical_history: $('#medical_history').val(),
					patient_id: $('#patient_id').val(),
					token: $("input[name=_token]").val()
				},
				success : function(result){
					$("#patienthistorymodal").modal('hide');
					swal({   
						title: "Medical History Updated!",   
						text: "",   
						type: "success",
						showConfirmButton: false
					});
					setTimeout(function(){ 
						location.reload();
					}, 2000);
				}
			},"json");
		}
	});

	$('#viewchartbtn').click(function() {
		$('#dentalchartModal').modal();
		var pediatric = JSON.parse(getCookie('pediatric_chart'));
        if (pediatric != null || pediatric != undefined) {
          var chart = pediatric.indexOf('<?php echo $patient->id; ?>');
          if (chart != -1) {
			$('.pediatric-tab').tab('show');
          }
          else {
          	$('.adult-tab').tab('show');
			setTimeout(function() {
          		initChart('adult');
			}, 0300);
    	  }
        }
        else {
          	$('.adult-tab').tab('show');
          	setTimeout(function() {
          		initChart('adult');
			}, 0300);
        }
	});

	$('#addanotherbtn').click(function() {
		$('#dentalnoteviewModal').modal('hide');
		$('#dentalnoteModal').modal();
	});

	<?php if(\Session::has('status')){ ?>
		swal("<?php echo \Session::get('status'); ?>", "", "success");  
	<?php } ?>

	$('#cancel_photo, #close_photo').click(function(){
		$("#photoCaptureModal").modal('hide');
		$("#my_photo_booth").waitMe('hide');
	});

	$('#savephoto').click(function(){
		$('#my_photo_booth').waitMe({
			effect: 'win8_linear',
			text: 'Uploading on process...Please wait.',
			bg: 'rgba(255,255,255,0.7)',
			color:'#000',
			sizeW:'',
			sizeH:'',
			source: 'img.svg'
		});
	});

	$('#chart').on('show.bs.collapse', function (e) {
	  ga('send', 'dental_chart');
	})

	$('#notes').on('show.bs.collapse', function (e) {
	  ga('send', 'dental_note');
	})

	$('#appointment').on('show.bs.collapse', function (e) {
	  ga('send', 'patient_appointments_list');
	})

	$('#photo').on('show.bs.collapse', function (e) {
	  ga('send', 'patient_photo');
	})

	$('#payment').on('show.bs.collapse', function (e) {
	  ga('send', 'patient_payment');
	})


	$('.adult-tab').on('shown.bs.tab', function (e) {
		setTimeout(function() {
			var pediatric = JSON.parse(getCookie('pediatric_chart'));
			if (pediatric != null || pediatric != undefined) {
		      var i = pediatric.indexOf('<?php echo $patient->id; ?>');
		      if (i != -1) {
		        pediatric.splice(i, 1);
		        setCookie(['pediatric_chart', JSON.stringify(pediatric)]);
		      }
		    }
		  	initChart('adult');
		}, 0300);
	})
	$('.pediatric-tab').on('shown.bs.tab', function (e) {
		setTimeout(function() {
			var pediatric = JSON.parse(getCookie('pediatric_chart'));
			if (pediatric == null || pediatric == undefined) {
		 		pediatric = ['<?php echo $patient->id; ?>'];
		 		setCookie(['pediatric_chart', JSON.stringify(pediatric)]);
		 	}
		 	else {
	 		 	var i = pediatric.indexOf('<?php echo $patient->id; ?>');
	 		 	if (i == -1) {
			 		pediatric.push('<?php echo $patient->id; ?>');
			 		setCookie(['pediatric_chart', JSON.stringify(pediatric)]);
		 		}
		 	}
		  	initChart('pediatric');
		}, 0300);
	})

	$('#medical_history').wysihtml5({
      toolbar: {
        "fa": true,
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": true, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": false, //Button to change color of font  
        "blockquote": true, //Blockquote  
        "size": 'sm' //default: none, other options are xs, sm, lg
      }
    });

});

/* function creates cooke with random key */
function setCookie(inputs) {
  /* cookie name */
  var name = (inputs[0]) ? inputs[0] : "key" + document.cookie.length;
  var date = new Date();
  date.setTime(date.getTime() + 100000);
  var expires = "; expires=" + date.toGMTString();
  setTimeout(function() {
  	document.cookie = name + "=" + encodeURIComponent(inputs[1]) + expires+";path=/";
  }, 0300);
};

/* get the cookie based on input */
function getCookie(input) {
  var cookies = "<?php echo addslashes(request()->cookie('pediatric_chart')); ?>";
  if (cookies == null || cookies == undefined || cookies == '') {
 	return null; 
  }
 return cookies;

};

function initChart(type) {
	$('img[usemap]').rwdImageMaps();
	<?php foreach($dentalcharts as $dentalchart){ ?>
		$('#<?php echo $dentalchart->chart_id; ?>').data('maphilight', {
			fillColor: '<?php echo $dentalchart->color_code; ?>',
			fillOpacity: 0.5,
			stroke:false,
			// strokeColor: 'ffffff',
			alwaysOn:true
		});
	<?php } ?>

	setTimeout(function() {
		$('.'+type).maphilight();
	}, 1000);
}

function adddentalnote(id){
	$('#dentalnoteModal').modal();
	if(id){
		$('#colorbtn7F8C8D,#colorbtnffc900,#colorbtnef473a,#colorbtn33cd5f,#colorbtn11c1f3,#colorbtnF39C12,#colorbtn2E3E51,#colorbtnFDAEC6,#colorbtn8D6EB1').css('border', 'none');
		$.ajax({
			url: base_url+'/dentalnote/'+id+'/edit',
			type: 'GET',
			success: function(result){
				$('#dentalnoteform').attr('action', base_url+'/dentalnote/'+result.id);
				$('#methodtype').removeAttr('disabled');
				$('#colorbtn'+result.color_code).css('border', '2px solid #000');
				$('#'+result.color_code).attr('checked','checked');
				$('#notesxx').val(result.notes);
				$('#chart_id').val(result.chart_id);
				$('#chart_name').val(result.chart_name);
			}
		});
	} else {
		$('#dentalnoteform').attr('action', base_url+'/dentalnote');
	}
}
function colorclick(id){
	$('#colorbtn7F8C8D,#colorbtnffc900,#colorbtnef473a,#colorbtn33cd5f,#colorbtn11c1f3,#colorbtnF39C12,#colorbtn2E3E51,#colorbtnFDAEC6,#colorbtn8D6EB1').css('border', 'none');
	$('#colorbtn'+id).css('border', '2px solid #000');
}
function viewdentalnote(id){
	$('#viewnoteModal').modal();
	$.ajax({
		url: base_url+'/dentalnote/'+id+'/edit',
		type: 'GET',
		success: function(result){
			$('#notecontent').html(result.notes);
			$('#noteheader').css('background-color', '#'+result.color_code);
		}
	});
}
function viewdentalchart(id){
	$('#viewnoteModal').modal();
	$.ajax({
		url: base_url+'/dentalnote/'+id+'/edit',
		type: 'GET',
		success: function(result){
			$('#notecontent').html(result.notes);
			$('#noteheader').css('background-color', '#'+result.color_code);
		}
	});
}

function addPatientHistory(){
	// $('#patientHistoryModal').modal();
	var history = $('input[name="medical_history"]').val();
	$('#patientHistoryModal').modal();
	if(history != '' && history != undefined){
		/*$.ajax({
			url: base_url+'/patient_history/'+id+'/edit',
			type: 'GET',
			success: function(result){
				$('#patienthistoryform').attr('action', base_url+'/patient_history/'+result.id);
				$('#methodtype').removeAttr('disabled');
				$('#title').val(result.title);
				$('textarea#history').val(result.history);
			}
		});*/
		$("#history-content").html("");
		$('<p>'+history+'</p>').appendTo("#history-content");
		$("#history-display").css("display", "block");
		$("#history-form").css("display", "none");

	} else {
		$("#history-display").css("display", "none");
		$("#history-form").css("display", "block");
		// $('textarea#medical_history').val(result.history);
		// $('#dentalnoteform').attr('action', base_url+'/dentalnote');
	}
}
function editPatientHistory() {
	$("#history-display").css("display", "none");
	$("#history-form").css("display", "block");
}
function viewdentalnote(id){
	$('#viewnoteModal').modal();
	$.ajax({
		url: base_url+'/dentalnote/'+id+'/edit',
		type: 'GET',
		success: function(result){
			$('#notecontent').html(result.notes);
			$('#noteheader').css('background-color', '#'+result.color_code);
		}
	});
}

function viewappointment(id){
	$('#appointmentModal').modal();
	$.ajax({
		url: base_url+'/appointment/'+id,
		type: 'GET',
		success: function(result){
			$('#patient_name').val(result.name);
			$('#appointmentdate').val(result.appointmentdate);
			$('#appointmenttime').val(result.appointmenttime);
			$('#remarks').val(result.remarks);
			$('#appointment_id').val(result.appointment_id);
		}
	});
}
function viewphotoModal(id){
	$('#photoModal').modal();
	if(id){
		$.ajax({
			url: base_url+'/dentalphotos/'+id+'/edit',
			type: 'GET',
			success: function(result){
				var btnCust = '<button type="button" class="btn btn-default" title="Webcam Capture" ' + 
				    'onclick="alert(\'Call your custom code here.\')">' +
				    '<i class="glyphicon glyphicon-camera"></i>' +
				    '</button>'; 
				$("#avatar").fileinput({
				    overwriteInitial: true,
				    maxFileSize: 5120,
				    showClose: false,
				    showCaption: false,
				    browseLabel: '',
				    removeLabel: '',
				    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;&nbsp;Browse',
				    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
				    removeTitle: 'Cancel or reset changes',
				    elErrorContainer: '#kv-avatar-errors',
				    msgErrorClass: 'alert alert-block alert-danger',
				    defaultPreviewContent: '<img src="'+base_url+'/'+result.photo+'" alt="Your Avatar" style="width:175px">',
				    layoutTemplates: {main2: '{preview} '+btnCust+' {remove} {browse}'},
				    allowedFileExtensions: ["jpg", "png", "gif"]
				});

				$('.fileinput-remove').click(function() {
			    	$('#photoavatar').val('');
			    	$('.file-default-preview').html('<img src="/uploads/no_pic.jpg" alt="Your Avatar" style="width:auto; height:175px">');
			    });

			    $('#caption').val(result.caption);
			    $('#photoavatar').val(result.photo);
			    $('#formPhoto').attr('action', base_url+'/dentalphotos/'+result.id);
				$('#photomethodtype').removeAttr('disabled');
			}
		});
		
	} else {
		var btnCust = '<button type="button" class="btn btn-default" title="Webcam Capture" ' + 
		    'onclick="webcam()">' +
		    '<i class="glyphicon glyphicon-camera"></i>' +
		    '</button>'; 
		$("#avatar").fileinput({
		    overwriteInitial: true,
		    maxFileSize: 5120,
		    showClose: false,
		    showCaption: false,
		    browseLabel: '',
		    removeLabel: '',
		    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;&nbsp;Browse',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="/uploads/no_pic.jpg" alt="Your Avatar" style="width:175px">',
		    layoutTemplates: {main2: '{preview} '+btnCust+' {remove} {browse}'},
		    allowedFileExtensions: ["jpg", "png", "gif"]
		});
		$('#formPhoto').attr('action', base_url+'/dentalphotos');
	}

	$('#closephotobtn').click(function() {
		location.reload();
	});
}
function checkDental(id,name){
	$('#dentalcharttable').empty();
	$('#chart_id').val(id);
	$('#chart_name').val(name);
	$('#notes').val('');
	$('#colorbtn7F8C8D,#colorbtnffc900,#colorbtnef473a,#colorbtn33cd5f,#colorbtn11c1f3,#colorbtnF39C12,#colorbtn2E3E51,#colorbtnFDAEC6,#colorbtn8D6EB1').css('border', 'none');
	$.ajax({
		url: base_url+'/dentalchart/editnote/'+id+'/<?php echo $patient->id; ?>/'+name,
		type: 'GET',
		success: function(result){
			if(result.id != '0'){
				var datas = result;
	            $.each(datas, function(index, data) {
					if (data.chart_name.match(/[a-z]/i)) {
	                 var label = "Tooth letter";
	                }
	                else {
	                  var label = "Tooth number";
	                }     
	            	$('#dentalcharttable').append('<tr><td><div class="pull-left image" style="color:#'+data.color_code+'"><span class="fa fa-circle"></span></div><div class="pull-left info" style="padding-left:15px">'+label+' : '+data.chart_name+'<br>'+data.notes+'<br/>'+$.datepicker.formatDate('MM dd, yy', new Date(data.updated_at))+'</div></td></tr>');
	            });
				$('#dentalnoteviewModal').modal();
				$('#addanotherbtn').show();
				$('#chart_id').val(result[0].chart_id);
			} else {
				$('#dentalnoteModal').modal();
				$('#dentalnoteform').attr('action', base_url+'/dentalnote');
			}
		}
	});
	
}
function exportpdf(id){
	/*swal({   
		title: "Unlock Feature Export patient record to PDF.",   
		text: "This feature requires a full version of this app.",   
		timer: 5000,   
		showConfirmButton: false, 
		type: "warning" 
	});*/
	swal({   
		title: "Download PDF?",   
		text: "This will download a PDF version of the patient information",   
		type: "info",
		showCancelButton: true,     
		confirmButtonText: "Yes, Download",   
		closeOnConfirm: true,
		showLoaderOnConfirm: false, 
	}, 
	function(){  
		location.replace(base_url+'/patient/generatepdf/'+id);
		// swal("Downloaded!", "Your PDF file has been downloaded.", "success");
	});
}
function deletedentalnote(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Dental Note!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){
		ga("send", "event", "{{ \Auth::user()->email }}", "Delete", "Delete Dental Note");
		$.ajax({
			url: base_url+'/dentalnote/'+id,
			type: 'DELETE',
			success: function(result){
				if(result == 'success'){
					setTimeout(function(){  
						swal("Deleted!", "Dental Note has been deleted.", "success"); 
						location.reload();
					}, 3000);
				}
			}
		});
	});
}
function deleteappointment(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Appointment!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){   
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Patient Appointment');
		$.ajax({
			url: base_url+'/appointment/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Appointment has been deleted.", "success"); 
					location.reload();
				}, 3000);
			}
		});
	});
}
function deletephoto(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Photo!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){  
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Patient Photo');
		$.ajax({
			url: base_url+'/dentalphotos/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Photo has been deleted.", "success"); 
					location.reload();
				}, 3000);
			}
		});
	});
}
function deletepayment(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Payment!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Patient Payment');
		$.ajax({
			url: base_url+'/payments/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Payment has been deleted.", "success"); 
					location.reload();
				}, 3000);
			}
		});
	});
}

function deletepatient(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Patient!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){  
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Patient');
		$.ajax({
			url: base_url+'/patient/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Patient has been deleted.", "success"); 
					window.location.href = base_url+'/patient';
				}, 3000);
			}
		});
	});
}

function deletePatientHistory(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Medical History!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){   
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Medical History');
		$.ajax({
			url: base_url+'/patient_history/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Medical History has been deleted.", "success"); 
					location.reload();
				}, 3000);
			}
		});
	});
}

function webcam(){
	$('#photoCaptureModal').modal();
	Webcam.set({
        width: 300,
        height: 300,
   
        dest_width: 630,
        dest_height: 470,
   
        crop_width: 0,
        crop_height: 0,
   
        image_format: 'jpeg',
        jpeg_quality: 90,
        flip_horiz: true
    });
    Webcam.attach( '#my_camera' );
}
function save_photo() {
  
	var patient_id = $('#patientid').val();

	$.ajax({
		type: "POST",
		url : base_url+ "/patient/photocapture/"+patient_id,
		data: {
		photo: $('#myImage').val(),
		token: $("input[name=_token]").val()
	},
		success : function(result){
			$("#photoCaptureModal").modal('hide');
			$("#image").attr('src', result.picture);
			cancel_preview();
		}
	},"json");

} 
</script>
@endsection