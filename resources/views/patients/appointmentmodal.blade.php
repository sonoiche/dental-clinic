<div class="modal fade bs-example-modal-md" id="appointmentModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" id="noteheader">
                <h4 class="modal-title" id="myModalLabel"><span class="fa fa-list-alt"></span> Update Appointment</h4>
            </div>
			<div class="modal-body">
				<div class="form-group" id="nameField">
					<label>Patient Name</label>
					{!! Form::text('name', null, ['class'=>'form-control','id'=>'patient_name']) !!}
					<div id="nameInput" style="font-size:11px; color:#dd4b39"></div>
				</div>
				<div class="form-group" id="appointmentdateField">
					<label>Date</label>
					<div class="input-group">
				        <div class="input-group-addon" style="padding:0 10px 0 10px">
				        	<i class="fa fa-calendar"></i>
				        </div>
						{!! Form::text('appointmentdate', null, ['id'=>'appointmentdate','class'=>'form-control datemask']) !!}
					</div>
					<div id="appointmentdateInput" style="font-size:11px; color:#dd4b39"></div>
				</div>
				<div class="form-group bootstrap-timepicker">
					<label>Time</label>
					<div class="input-group">
						<div class="input-group-addon" style="padding:0 10px 0 10px">
						  	<i class="fa fa-clock-o"></i>
						</div>
						{!! Form::text('appointmenttime', null, ['class'=>'form-control timepicker','id'=>'appointmenttime']) !!}
					</div>
				</div>
				<div class="form-group" id="remarksField">
					<label>Remarks</label>
					{!! Form::textarea('remarks', null, ['class'=>'form-control','id'=>'remarks','style'=>'height:100px; resize:none']) !!}
					<div id="remarksInput" style="font-size:11px; color:#dd4b39"></div>
				</div>
			</div>
			<div class="modal-footer">
				{!! Form::hidden('appointment_id','', ['id'=>'appointment_id']) !!}
				{!! Form::button('Save Appointment', ['class'=>'btn btn-primary btn-flat btn-sm','id'=>'saveappointmentbtn']) !!}
				<button class="btn btn-sm btn-default" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>