
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8"/>
<title>Dental Clinic - Error 403</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="dentalclinic" />
<meta name="keywords" content="dentalclinic" />
<meta name="description" content="Dental Clinic - Error 403" />

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" media="all" href="{{ url('css/error-style.css') }}">
</head>
	<body id="errorpage" class="error404">
        <div id="pagewrap">
            <div id="wrapper" class="clearfix">     
                <div id="parallax_wrapper">    
                    <div id="content" style="margin-top: 130px;">
                        <h1>You are not authorize to access this page.</h1>
                        <p>If you think this is an error. Please contact your My Dental Clinic Administrator or email us at <a href="mailto:info@dentalclinic.com." style="text-decoration:none; color:#1a1a1a"><b>info@dentalclinic.com</b></a>.</p>
                        <a href="{{ url('dashboard') }}" title="" class="button">Go Home</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>