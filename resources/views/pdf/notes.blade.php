<html>
<body>
	<p>
		<div style="font-size:25px;font-weight: bold;">
		{{ $patient->full_name }}
		@if($patient->image)
			@if(\File::exists($patient->image))
      			<img style="float:right;width:110px;height:110px;" src="{{ asset($patient->image) }}" />
      		@else
      			<img style="float:right;width:110px;height:110px;" src="{{ 'uploads/patients/'.$patient->image }}" />
    		@endif
		@endif
		</div>
		<span>Email: {{ $patient->email }}</span><br/>
		<span>Contact Number: {{ $patient->contact_number }}</span><br/>
		<span>Address: {{ $patient->address }}</span><br/>
		<span>Gender: {{ $patient->gender }}</span><br/>
		<span>Birthday: {{ $patient->birthdate_display.' - '.$patient->age.' years old' }}</span><br/>
	</p>
	<p>
		<span style="font-size:17px;">Dental Notes</span>
		@foreach ($dentalnotes as $key => $note)
			<p style="margin-left:12px;">
				{{ $key + 1 }}. Tooth number: 
				@if($note->chart_id == 0) 
					N/A
				@else
					{{ $note->chart_id }}
				@endif
				<br/>
				<span style="margin-left:17px;">Date: {{ display_time($note->updated_at) }}</span><br/>
				<span style="margin-left:17px;">Note: {{ $note->notes }}</span>
			</p>
		@endforeach
	</p>
</body>
</html>