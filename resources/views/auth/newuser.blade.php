<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
<meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
<meta name="author" content="Quantum X Inc.">
<title>My Dental Clinic</title>
<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
<!-- Bootstrap -->
{!! HTML::style('css/dentalclinicbootstrap.css') !!}
{!! HTML::style('css/font-awesome.min.css') !!}
{!! HTML::style('css/Pe-icon-7-stroke.css') !!}
{!! HTMl::style('css/ionicons.min.css') !!}
{!! HTML::style('website/css/style.css') !!}
{!! HTML::style('css/dentalclinicstyle.css') !!}
{!! HTML::style('website/css/animate.css') !!}
{!! HTML::style('library/sweetalert/sweetalert.css') !!}
<style type="text/css">
.error-class {
    font-size: 12px;
    color: #e74c3c;
}
</style>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ url('website/images/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section class="header parallax home-parallax page" id="HOME">
    <div class="section_overlay">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" style="background-color:#0D24C7;vertical-align: middle;">
                    <div class="pull-left div-responsive"><img class="responsive" src="{{ url('website/images/logo.png') }}" alt=""></div>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background-color:#0D24C7;">
                    {{-- <div class="pull-left logo"><img src="{{ url('website/images/logo.png') }}" class="img-responsive" alt=""></div> --}}
                    <ul class="nav navbar-nav navbar-right">
                        <!-- NAV -->
                        <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                        <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                        <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                        <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                        <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                        <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                        <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container- -->
        </nav>
    </div>
</section>

<section class="bg-content">
    <div class="padding-top-responsive bg-content-header" style="text-align:center;">
        You will be up and running in a few minutes. Sign up for FREE account.<br/>
        There are currently no limitations using My Dental Clinic App. And you have nothing to pay.
        {{-- <h2>Don’t have an account? </h2><h2 style="text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);"><a href="{{ url('register') }}" class="text-center" style="color:#fff">Sign up now for FREE</a>!</h2> --}}
    </div>
    <div class="bg-content-body">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <h2 class="header-class">FEATURES</h2>
                <div class="bg-content-feature">
                    <ul>
                        <li><span class="pe-7s-like"></span><div>Keep track of your patient information</div></li>
                        <li><span class="pe-7s-phone"></span><div>Call or send SMS to your patients in one click</div></li>
                        <li><span class="pe-7s-look"></span><div>Keep track of your appointments.</div></li>
                        <li><span class="pe-7s-monitor"></span><div>Attach images to your patient records.</div></li>
                        <li><span class="pe-7s-info"></span><div>Keep track of your patient’s payments.</div></li>
                        <li><span class="pe-7s-pen"></span><div>Dental chart that is very easy to use.</div></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="bg-content-tab">
                    <ul class="nav nav-tabs">
                        <li class="bg-content-tab-li active" style="width:100%"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Set Your Password</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane border-top active" id="tab_2">
                            <div class="login-box-body">
                            @if(isset($data))
                                <p class="login-box-msg text-red">
                                    Login details are required for security purposes.
                                </p>
                            @endif
                            <div class="alert alert-danger" id="alertdiv" style="display:none"></div>
                            <form role="form" id="passwordForm">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group has-feedback">
                                    {!! Form::password('password', ['class'=>'form-control','placeholder'=>'Password','id'=>'password']) !!}
                                </div>
                                <div class="form-group has-feedback">
                                    {!! Form::password('password_confirmation', ['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        {!! Form::hidden('user_id', $user->id) !!}
                                        {!! Form::button('Save Password', ['class'=>'btn btn-bright btn-block btn-flat','type'=>'submit','id'=>'savepasswordbtn']) !!}
                                    </div>
                                </div>
                            </form>
                            <br>
                            </div>
                        </div>
                    </div>
                </div>
            <br><br><br><br><br>
            </div>
        </div>
    </div>
</section>
<section class="download page" id="DOWNLOAD">
                 <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- DOWNLOAD NOW SECTION TITLE -->
                        <div class="section_title">
                            <h2>download now</h2>
                        </div>
                        </br>
                        </br>
                        <!--END DOWNLOAD NOW SECTION TITLE -->
                        <div class="row">
                            <div class="col-md-6 img-wrapper">
                                <img src="home/images/download.png" class="img-responsive" alt="Image">
                            </div>
                            <div class="col-md-5 text-left">
                                <p class="typo">Designed to keep things simple,
                                </br>
                                you will be up and running with 
                                </br><span class="blue">My Dental Clinic</span> in a few minutes!
                                </p>
                                <h2 class="section_subtitle">AVAILABLE ON: </h2>
                                </br>
                                <div class="col-md-6">
                                    <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new"><img src="home/images/googleappstore.png" class="img-responsive" alt="Image"></a>
                                </div>
                                <div class="col-md-6">
                                    <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new"> <img src="home/images/appleappstore.png" class="img-responsive" alt="Image"></a>
                                </div>
                            </div>   
                        </div>
                    </br>
                    </div>
                   </div>
            </div>
        </section>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="copy_right_text">
                    <p>Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="copy_right_text">
                    <p>  Emal Address: <a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a><br/>
                    Contact Number: (632) 7090411<br/>
                    Fax Number: (632) 4407454
                    </p>
                </div>
            </div>
            <div class="col-md-2">
                <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank" style="font-size:18px;"> LIKE US <span class="fa fa-facebook-official facebook-like-icon"></a>
            </div>
            <div class="col-md-2">
                <div class="scroll_top">
                    <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
{!! HTML::script('home/js/jquery.min.js') !!}
{!! HTML::script('home/js/bootstrap.min.js') !!}
{!! HTML::script('home/js/smoothscroll.js') !!}
{!! HTML::script('home/js/wow.min.js') !!}
{!! HTML::script('home/js/waypoints.min.js') !!}
{!! HTML::script('home/js/scriptx.js') !!}
{!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
{!! HTML::script('library/jquery-validation/dist/jquery.validate.js') !!}
<script type="text/javascript">
$(document).ready(function() {
    if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
        window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
    }
    if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
        window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
    }

    $('#passwordForm').validate({
        errorClass: "error-class",
        rules: {
            password: { required : true },
            password_confirmation: {
                equalTo: "#password"
            }
        },
        messages : {
            password: "Please provide a password.",
            password_confirmation: "Password did not match."
        },
        submitHandler: function () {
            $('#savepasswordbtn').attr('disabled', true);
            $('#savepasswordbtn').html("<i class=\"fa fa-spin fa-spinner\"></i> Saving...");
            var data = $('#passwordForm').serializeArray();
            $.ajax({
                url: "{{ url('register/setpassword') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    swal({
                        title: 'Success!',
                        text: 'Password has been saved.',
                        type: 'success',
                        showConfirmButton: false
                    })
                    setTimeout(function(){
                        swal.close();
                        location.reload(); 
                    }, 2000);
                }
            });
        }
    })
});
</script>
</body>
</html>