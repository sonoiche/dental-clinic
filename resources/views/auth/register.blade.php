<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
<meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
<meta name="author" content="Quantum X Inc.">
<title>My Dental Clinic</title>
<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
<!--  Custom -->
{!! HTML::style('home/css/custom.css') !!}       
 {!! HTML::style('home/css/style.css') !!}

<!-- Bootstrap -->
{!! HTML::style('css/dentalclinicbootstrap.css') !!}
{!! HTML::style('css/font-awesome.min.css') !!}
{!! HTML::style('css/Pe-icon-7-stroke.css') !!}
{!! HTMl::style('css/ionicons.min.css') !!}
{!! HTML::style('website/css/style.css') !!}
{!! HTML::style('css/dentalclinicstyle.css') !!}
{!! HTML::style('website/css/animate.css') !!}
{!! HTML::style('library/sweetalert/sweetalert.css') !!}
<style>
.sweet-alert p {
    font-size: 19px;
    color: #3e3c3c;
    font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
</style>
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ url('website/images/ms-icon-144x144.png') }}">
<meta name="theme-color" content="#ffffff">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<section class="header parallax home-parallax page" id="HOME">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header navbar-mobile">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="logo"><a href="{{ url('/') }}"><img src="home/images/logo.png" class="img-responsive" alt="My Dental Clinic"></a></div>
                <ul class="nav navbar-nav navbar-right nav-mobile">
                    <!-- NAV -->
                    <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                    <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                    <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                    <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                    <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                    <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                    <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container- -->
    </nav>
</section>
<section class="bg-content">
    <div class="container">
        <div class="padding-top-responsive bg-content-header" style="text-align:center;">
            You will be up and running in a few minutes. Sign up for FREE account.<br/>
            There are currently no limitations using My Dental Clinic App. And you have nothing to pay.
            {{-- 
            <h2>Don’t have an account? </h2>
            <h2 style="text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);"><a href="{{ url('register') }}" class="text-center" style="color:#fff">Sign up now for FREE</a>!</h2>
            --}}
        </div>
    </div>
    <div class="bg-content-body">
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <h2 class="header-class">FEATURES</h2>
        <div class="bg-content-feature">
            <ul class="features_list">
                <li>
                    <span class="pe-7s-like"></span>
                    <p>Keep track of your patient information</p>
                </li>
                <li>
                    <span class="pe-7s-phone"></span>
                    <p>Call or send SMS to your patients in one click</p>
                </li>
                <li>
                    <span class="pe-7s-look"></span>
                    <p>Keep track of your appointments.</p>
                </li>
                <li>
                    <span class="pe-7s-monitor"></span>
                    <p>Attach images to your patient records.</p>
                </li>
                <li>
                    <span class="pe-7s-info"></span>
                    <p>Keep track of your patient’s payments.</p>
                </li>
                <li>
                    <span class="pe-7s-pen"></span>
                    <p>Dental chart that is very easy to use.</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="bg-content-tab">
            <ul class="nav nav-tabs">
                <li class="bg-content-tab-li active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Register</a></li>
                <li class="bg-content-tab-li"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Login</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane border-top active" id="tab_1">
                    <div class="login-box-body">
                        <div class="alert alert-danger" id="alertdivx" style="display: none"></div>
                        <form action="{{ url('register') }}" method="post" id="myForm">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group has-feedback">
                                <input type="text" name="clinic_name" class="form-control" placeholder="Clinic Name" id="clinic_name" value="{{ old('clinic_name') }}">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" name="first_name" class="form-control" placeholder="First name" id="first_name" value="{{ old('first_name') }}">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" name="last_name" class="form-control" placeholder="Last name" id="last_name" value="{{ old('last_name') }}">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="email" name="email" class="form-control" placeholder="Email" id="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="tel" name="contact_number" class="form-control" placeholder="Contact Number" id="contact_number" value="{{ old('contact_number') }}" onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" name="password" class="form-control" placeholder="Password" id="password">
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password" id="password_confirmation">
                            </div>
                            <div class="form-group has-feedback">
                                <select name="country_id" class="form-control">
                                    <option value="">Country</option>
                                    @foreach($country as $value)
                                    <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="text" name="source" class="form-control" placeholder="How did you hear about us?" id="source" value="{{ old('source') }}">
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="hidden" name="apikey" class="form-control" id="apikey" value="">
                                    <button type="button" class="btn btn-bright btn-block btn-flat" id="registerbtn">Create my Account</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div style="text-align:center">Already heave an account? <a href="{{ url('login') }}" style="color:#196eee !important">Login Here</a></div>
                    </div>
                </div>
                <div class="tab-pane border-top" id="tab_2">
                    <div class="login-box-body">
                        @if(isset($data))
                        <p class="login-box-msg text-red">
                            Login details are required for security purposes.
                        </p>
                        @endif
                        <p class="login-box-msg text-primary">Start Your Session Now!</p>
                        <div class="alert alert-danger" id="alertdiv" style="display:none"></div>
                        <form role="form" method="POST" action="{{ url('login') }}" id="loginForm">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group has-feedback">
                                {!! Form::text('email', null, ['class'=>'form-control','placeholder'=>'Email Address','id'=>'login-email']) !!}
                            </div>
                            <div class="form-group has-feedback">
                                {!! Form::password('password', ['class'=>'form-control','placeholder'=>'Password','id'=>'login-password']) !!}
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    {!! Form::button('Login', ['class'=>'btn btn-bright btn-block btn-flat','id'=>'loginbtn']) !!}
                                </div>
                            </div>
                        </form>
                        <br>
                        <div style="text-align:center"><a href="{{ url('passwords') }}" style="color:#196eee !important">I forgot my password</a></div>
                        </div>
                        </div>
                    </div>
                </div>
                <br><br><br><br><br>
            </div>
        </div>
    </div>
</section>
<section class="download page" id="DOWNLOAD">
         <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <!-- DOWNLOAD NOW SECTION TITLE -->
                <div class="section_title">
                    <h2>download now</h2>
                </div>
                </br>
                </br>
                <!--END DOWNLOAD NOW SECTION TITLE -->
                <div class="row">
                    <div class="col-md-6 img-wrapper">
                        <img src="home/images/download.png" class="img-responsive" alt="Image">
                    </div>
                    <div class="col-md-5 text-left">
                        <p class="typo">Designed to keep things simple,
                        </br>
                        you will be up and running with 
                        </br><span class="blue">My Dental Clinic</span> in a few minutes!
                        </p>
                        <h2 class="section_subtitle">AVAILABLE ON: </h2>
                        </br>
                        <div class="col-md-6">
                            <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new"><img src="home/images/googleappstore.png" class="img-responsive" alt="Image"></a>
                        </div>
                        <div class="col-md-6">
                            <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new"> <img src="home/images/appleappstore.png" class="img-responsive" alt="Image"></a>
                        </div>
                    </div>   
                </div>
            </br>
            </div>
           </div>
    </div>
</section>

<div class="contact-footer">
    <div class="container">
        <div class="col-md-offset-1 col-md-3 text-center wrapper-hover"> 
            <div class="contact-detail-wrap">
                <div class="img-wrapper">
                    <img src="home/images/email.png" class="img-responsive" alt="Image">
                </div>
                <h5><a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a></h5>
            </div>
        </div>
        <div class="col-md-4 text-center wrapper-hover">
            <div class="contact-detail-wrap">
                <div class="img-wrapper">
                    <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"><img src="home/images/facebook.png" class="img-responsive" alt="Image"></a>
                </div>
                <h5><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> Like us on Facebook </a></h5>
            </div>
        </div>
        <div class="col-md-3 text-center wrapper-hover">
            <div class="contact-detail-wrap">
                <div class="img-wrapper">
                    <img src="home/images/phone.png" class="img-responsive" alt="Image">
                </div>  
                <h5> (632) 7094011 to 13</h5>
            </div>
        </div>
    </div>  
</div> 
<div class="copyright">
    <div class="copyright-text col-md-12 text-center">
        <div class="agreement-wrap">
            <a href="{{ url('terms-and-conditions') }}">Terms &amp; Conditions</a>
             <span style="color: white;">&nbsp; | &nbsp;</span>
            <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
        </div>
        <h5 class="white">Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></h5>   
    </div>
</div>
{!! HTML::script('home/js/jquery.min.js') !!}
{!! HTML::script('home/js/bootstrap.min.js') !!}
{!! HTML::script('home/js/smoothscroll.js') !!}
{!! HTML::script('home/js/wow.min.js') !!}
{!! HTML::script('home/js/waypoints.min.js') !!}
{!! HTML::script('home/js/scriptx.js') !!}
{!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    // if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
    //     window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
    // }
    // if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
    //     window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
    // } 
    $('#loginbtn').click(function() {
        if($('#login-email').val() == '' && $('#login-password').val() == ''){
            $('#alertdiv').show();
            return false;
        } else {
            var data = $('#loginForm').serializeArray();
            $.ajax({
                url: "{{ url('login/check') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == '1'){
                        $('#loginForm').submit();
                    } else if(result == '3'){
                        $('#alertdiv').show();
                        $('#alertdiv').html('The credentials you entered is not in our database.');
                    } else if(result == '4'){
                        swal({
                            title: '',
                            text: "Thank you for your interest to log-in your account to our website version, but we regret to inform you that the Mobile and Website data integration is still on the development. We will let you know once the development is done. For now, please manage your patients and appointments in the mobile app. Thank you very much!",
                            type: 'error',
                            confirmButtonColor: '#3085d6',
                        })
                    } else {
                        $('#alertdiv').show();
                        $('#alertdiv').html('The username and password did not match.');
                    }
                }
            });
        }
    });

    $('#registerbtn').click(function() {
        if($('#clinic_name').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#clinic_name').focus();
            return false;
        } else if($('#first_name').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#first_name').focus();
            return false;
        } else if($('#last_name').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#last_name').focus();
            return false;
        } else if($('#email').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#email').focus();
            return false;
        } else if($('#contact_number').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#contact_number').focus();
            return false;
        } else if($('#password').val()==''){
            $('#alertdivx').show();
            $('#alertdivx').html('All fields are Required!');
            $('#email').focus();
            return false;
        } else if($('#password').val()!=$('#password_confirmation').val()){
            $('#alertdivx').show();
            $('#alertdivx').html('Password did not match!');
            $('#email').focus();
            return false;
        } else {
            var data = $('#myForm').serializeArray();
            $.ajax({
                url: "{{ url('register/check') }}",
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(result){
                    if(result == '1'){
                        $('#registerbtn').attr('disabled', 'disabled');
                        $('#registerbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
                        $('#myForm').submit();
                    } else {
                        $('#alertdivx').show();
                        $('#alertdivx').html('The email address you entered is already exists.');
                    }
                }
            });
        }
    });
});

function isNumberKey(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
</body>
</html>