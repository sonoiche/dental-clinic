<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
        <meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
        <meta name="author" content="Quantum X Inc.">
        <title>My Dental Clinic</title>
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
        <!--  Custom -->
        {!! HTML::style('home/css/custom.css') !!}
        <!-- Bootstrap -->
        {!! HTML::style('css/dentalclinicbootstrap.css') !!}
        {!! HTML::style('css/font-awesome.min.css') !!}
        {!! HTML::style('css/Pe-icon-7-stroke.css') !!}
        {!! HTMl::style('css/ionicons.min.css') !!}
        {!! HTML::style('website/css/style.css') !!}
        {!! HTML::style('css/dentalclinicstyle.css') !!}
        {!! HTML::style('website/css/animate.css') !!}
        {!! HTML::style('library/sweetalert/sweetalert.css') !!}
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('website/images/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <section class="header parallax home-parallax page" id="HOME">
            <div class="section_overlay">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header navbar-mobile">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="logo"><a href="{{ url('/') }}"><img src="home/images/logo.png" class="img-responsive" alt="My Dental Clinic"></a></div>
                            <ul class="nav navbar-nav navbar-right nav-mobile">
                                <!-- NAV -->
                                <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                                <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                                <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                                <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                                <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                                <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                                <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container- -->
                </nav>
            </div>
        </section>
        <section class="bg-content">
            <div class="padding-top-responsive bg-content-header" style="text-align:center;">
                You will be up and running in a few minutes. Sign up for FREE account.<br/>
                There are currently no limitations using My Dental Clinic App. And you have nothing to pay.
                {{-- 
                <h2>Don’t have an account? </h2>
                <h2 style="text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);"><a href="{{ url('register') }}" class="text-center" style="color:#fff">Sign up now for FREE</a>!</h2>
                --}}
            </div>
            <div class="bg-content-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <h2 class="header-class">FEATURES</h2>
                            <div class="bg-content-feature">
                                <ul class="features_list">
                                    <li>
                                        <span class="pe-7s-like"></span>
                                        <p>Keep track of your patient information</p>
                                    </li>
                                    <li>
                                        <span class="pe-7s-phone"></span>
                                        <p>Call or send SMS to your patients in one click</p>
                                    </li>
                                    <li>
                                        <span class="pe-7s-look"></span>
                                        <p>Keep track of your appointments.</p>
                                    </li>
                                    <li>
                                        <span class="pe-7s-monitor"></span>
                                        <p>Attach images to your patient records.</p>
                                    </li>
                                    <li>
                                        <span class="pe-7s-info"></span>
                                        <p>Keep track of your patient’s payments.</p>
                                    </li>
                                    <li>
                                        <span class="pe-7s-pen"></span>
                                        <p>Dental chart that is very easy to use.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="bg-content-tab">
                                <ul class="nav nav-tabs">
                                    <li class="bg-content-tab-li active" style="width:100%"><a href="#tab_2" data-toggle="tab" aria-expanded="false">Reset Your Password</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane border-top active" id="tab_2">
                                        <div class="login-box-body">
                                            @if(isset($data))
                                            <p class="login-box-msg text-red">
                                                Login details are required for security purposes.
                                            </p>
                                            @endif
                                            <div class="alert alert-danger" id="alertdiv" style="display:none"></div>
                                            <form role="form" id="passwordForm">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-group has-feedback">
                                                    {!! Form::email('email','', ['class'=>'form-control','placeholder'=>'Email Address','id'=>'email']) !!}
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        {!! Form::button('Send Reset Link', ['class'=>'btn btn-bright btn-block btn-flat','id'=>'savepasswordbtn']) !!}
                                                    </div>
                                                </div>
                                            </form>
                                            <br>
                                            <div style="text-align:center">Already heave an account? <a href="{{ url('login') }}" style="color:#196eee !important">Login Here</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br><br><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="download page" id="DOWNLOAD">
            <div class="available_store">
                <div class="container  wow bounceInRight" data-wow-duration="1s">
                    <div class="col-md-6">
                        <div class="available_title">
                            <h2>Available on</h2>
                            <p>GOOGLE PLAY STORE AND APPLE APP STORE </p>
                        </div>
                    </div>
                    <!-- DOWNLOADABLE STORE -->
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-xs-6 no_padding">
                                <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new">
                                    <div class="single_store">
                                        <i class="fa fa-android"></i>
                                        <div class="store_inner">
                                            <h2>ANDROID</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-xs-6 no_padding">
                                <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new">
                                    <div class="single_store">
                                        <i class="fa fa-apple"></i>
                                        <div class="store_inner">
                                            <h2>IOS</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- END DOWNLOADABLE STORE -->
                </div>
            </div>
        </section>
        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="copy_right_text">
                            <p>Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="copy_right_text">
                            <p>  Emal Address: <a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a><br/>
                                Contact Number: (632) 7090411<br/>
                                Fax Number: (632) 4407454
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank" style="font-size:18px;"> LIKE US <span class="fa fa-facebook-official facebook-like-icon"></a>
                    </div>
                    <div class="col-md-2">
                        <div class="scroll_top">
                            <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {!! HTML::script('home/js/jquery.min.js') !!}
        {!! HTML::script('home/js/bootstrap.min.js') !!}
        {!! HTML::script('home/js/smoothscroll.js') !!}
        {!! HTML::script('home/js/wow.min.js') !!}
        {!! HTML::script('home/js/waypoints.min.js') !!}
        {!! HTML::script('home/js/scriptx.js') !!}
        {!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                    window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
                }
                if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                    window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
                }
            
                $('#savepasswordbtn').click(function() {
                    if($("[name='email']").val() == ''){
                        alert('Email Address is required!');
                        return false;
                    } else {
                        $('#savepasswordbtn').html('Sending...');
                        var data = $('#passwordForm').serializeArray();
                        $.ajax({
                            url: "{{ url('/passwords') }}",
                            type: 'POST',
                            dataType: 'json',
                            data: data,
                            success: function(result){
                                $('#savepasswordbtn').html('Send Reset Link');
                                if(result == 1){
                                    alert('We send a password reset link to your email. Please check your inbox or spam folder. If you received it in spam, please tag it as not spam. Thank you!');
                                    $("[name='email']").val('');
                                } else if(result == 2){
                                    alert('The email you enter is not in our database.');
                                } else {
                                    alert('Something went wrong. Please try again later.');
                                }
            
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>