@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Devices</a></li>
		<li class="active"><a href="#">List of Devices</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
		{!! Form::open(['url'=>'devices/sort','id'=>'sortform']) !!}
			<div class="pull-left"><h3 class="box-title"><span class="fa fa-mobile"></span> &nbsp;List of Connected Devices</h3><br><span style="color:#dd4b39; font-size:14px; padding:7px 27px">{{ count($devices) }} device(s) connected</span></div>
			<div class="input-group pull-right" style="width:30%">
				<span class="input-group-addon">Sort By</span>
				{!! Form::select('sortby',['1'=>'Last Updated','2'=>'Platform','3'=>'Sync Version'], isset($sortby) ? $sortby : null,['class'=>'form-control','id'=>'sortorder']) !!}
			</div>
		{!! Form::close() !!}
		</div>
		{{-- <div class="box-body">
			<div class="pull-right btn-group">
				<a href="{{ route('devices.invite')}}" id="viewchartbtn" class="btn btn-primary btn-sm"><span class="fa fa-envelope"></span> &nbsp;Invite Users</a>
			</div>
		</div> --}}
		<div class="box-body">
			<table class="table table-hover">
				<tbody>
					@if(count($devices))
					@foreach($devices as $key => $device)
					<tr>
						<td style="vertical-align:middle; width:3%">{{ ($devices->currentpage()-1) * $devices->perpage() + $key + 1 }}</td>
						<td style="max-height:50px">
							<div style="color:#1a1a1a">
						        <div class="pull-left info" style="padding-left:15px">
						        	@if ($device->device_number)
							        	<p>{{ $device->first_name}} {{$device->last_name}}
								        	<br/>Email: {{ $device->email }}
								        	<br/>Device: {{ $device->model }}
								        	<br/>Platform: {{ $device->platform }}
								        	<br/>Sync Version: {{ $device->sync_version }}
								        	<br/>Last Updated : {{ display_time($device->updated_at) }}
							        	</p>
							        @else
							        	<p>
							        		Email: {{ $device->email }}
							        		<br/> <span class="text-red">Invitation sent</span>
							        	</p>
						        	@endif
						        </div>
						       <div class="pull-right info">
									<a onclick="deleteDevice('{{ \Hashids::encode($device->id) }}')" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
								</div>
						    </div>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						0 results 
						<td>0 result for devices connected</td>
					</tr>
					@endif
				</tbody>
			</table>
			<div class="pull-right">{!! (count($devices)) ? $devices->render() : '' !!}</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
	$('#sortorder').change(function() {
		$('#sortform').submit();
	});

	<?php if(\Session::has('message')){ ?>
		swal("Sent!", "<?php echo \Session::get('message'); ?>", "success");  
	<?php } ?>
});

function deleteDevice(id){
		swal({   
			title: "Are you sure?",   
			text: "This device will be disconnected!",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Yes, delete it!",   
			closeOnConfirm: false,
			showLoaderOnConfirm: true 
		}, 
		function(){   
			$.ajax({
				url: base_url+'/devices/'+id,
				type: 'DELETE',
				success: function(result){
					setTimeout(function(){  
						swal("Deleted!", "Connected Device has been deleted.", "success"); 
						location.reload();
					}, 3000);
				}
			});
		});
	}
</script>
@endsection