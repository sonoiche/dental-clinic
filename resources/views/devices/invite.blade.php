@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Accounts</a></li>
		<li class="active"><a href="#">Update User</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-user"></span> &nbsp;Invite Users</h3>
		</div>
		<div class="box-body">
			{!! Form::open(['route'=>['devices.invite'], 'method'=>'POST', 'id'=>'myForm']) !!}
				<div class="form-group" id="invite_users">
					<label>Email</label> (Separate emails with a comma)<br/>
					Invite other users through their email address
					{!! Form::text('email', null, ['class'=>'form-control','id'=>'email', 'placeholder' => 'e.g. sample@mail.com, sample2@mail.com']) !!}
					<div id="emailInput" style="font-size:11px; color:#dd4b39"></div>
				</div>

				<div class="form-group">
					{!! Form::button('Send Invites',['class'=>'btn btn-flat btn-primary btn-sm','id'=>'send_invite_button']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function($) {
	$('#send_invite_button').click(function() {
		if ($('#email').val()=='')
		{
			$('#invite_users').addClass('has-error');
		    $('#emailInput').html('<i class="fa fa-times-circle-o"></i> Email is Required.');
		    $('#email').focus();
		    return false;
		}
		else 
		{
			$('#send_invite_button').attr('disabled', 'disabled');
			$('#send_invite_button').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#myForm').submit();
		} 
	});

	<?php if(\Session::has('message')){ ?>
		swal("Sent!", "<?php echo \Session::get('message'); ?>", "success");  
	<?php } ?>
});

</script>
@endsection