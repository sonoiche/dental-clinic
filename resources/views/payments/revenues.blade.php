@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-money"></i> Revenues</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
		<div class="pull-left"><h3 class="box-title"><span class="fa fa-users"></span> &nbsp;List of Revenues</h3><br></div>
		</div>
		<div class="box-body">
				{!! Form::open(['url'=>'revenues','id'=>'filter-form']) !!}
					<div class="form-group">
		                <label>Date range button:</label>

		                <div class="input-group">
		                  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
		                    <span>
		                      <i class="fa fa-calendar"></i> {{ $date_start }} - {{ $date_end}}
		                    </span>
		                    <i class="fa fa-caret-down"></i>
		                  </button>
		                </div>
		                  {!! Form::hidden('date_start',$date_start,['id'=>'date_start']) !!}
		                  {!! Form::hidden('date_end',$date_end,['id'=>'date_end']) !!}
		                  {!! Form::hidden('date_filter',null,['id'=>'date_filter']) !!}
	              	</div>
				{!! Form::close() !!}
			<h4>Total Revenue: <strong class="text-red">{{ $total_revenues.' '.\Auth::user()->currency }}</strong></h4>
			<span class="pull-left" style="color:#dd4b39; font-size:14px;">Revenue for {{ count($revenues) }} patient(s)</span>
			<br/><br/>
			<table class="table table-hover">
				<tbody>
					@if(count($revenues))
					@foreach($revenues as $key => $revenue)
					<tr>
						<td style="vertical-align:middle; width:3%">{{ ($revenues->currentpage()-1) * $revenues->perpage() + $key + 1 }}</td>
						<td style="max-height:50px">
						<a href="{{ route('patient.show', \Hashids::encode($revenue->patient_id)) }}" style="color:#1a1a1a">
						 	<div class="pull-left image">
						          	@if($revenue->image)
						        		@if(\File::exists($revenue->image))
						          			{!! HTML::image($revenue->image, '', ['class'=>'img-circle list-image']) !!}
						          		@else
						          			{!! HTML::image('uploads/patients/'.$revenue->image, '', ['class'=>'img-circle list-image']) !!}
						        		@endif
						        	@else
						        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
						        	@endif
						        </div>
						        <div class="pull-left info" style="padding-left:15px">
						        	<p>
							        	{{ $revenue->full_name }}<br/>
						        		Total revenue: <span class="text-red"><strong>{{ $revenue->cash_revenue + $revenue->installment_revenue.' '.\Auth::user()->currency }}</strong></span>
						        	</p>
						        	<p>
							        	Cash: {{$revenue->cash_revenue.' '.\Auth::user()->currency }}<br/>
							        	Installments: {{$revenue->installment_revenue.' '.\Auth::user()->currency }}
						        	</p>
						        </div>
						</a>
					</tr>
					@endforeach
					@else

					@endif
				</tbody>
			</table>
			<div class="pull-right">{!! (count($revenues)) ? $revenues->render() : '' !!}</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
	var start = moment($('#date_start').val());
    var end = moment($('#date_end').val());
    var date_today = '<?php echo date("F d, Y")?>';

    function display(start, end) {
    	if (start.format('YYYYMMDD') == '20100101') {
    		$('#daterange-btn span').html('All Revenues');
    		$('#date_filter').val('all');
    		var date_filter = 'all';
    	}
    	else if (start.format('MMMM D, YYYY') == date_today && end.format('MMMM D, YYYY') == date_today) {
    		$('#daterange-btn span').html('Today');
    		$('#date_filter').val('today');
    		var date_filter = 'today';
    	} 
    	else {
        	$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        	$('#date_filter').val('');
        	var date_filter = '';
    	}
    	$('#date_start').val(start.format('YYYY-MM-DD'));
    	$('#date_end').val(end.format('YYYY-MM-DD'));
    }

    function daterange_callback(start, end) {
    	display(start, end);
    	$('#filter-form').submit();
    }


    $('#daterange-btn').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'right',
        autoApply: true,
        ranges: {
           'Today': [moment(), moment()],
           'This Week': [moment().startOf('isoweek'), moment().endOf('isoweek')],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'This Year': [moment().startOf('year'), moment().endOf('year')],
           'All Revenues': [moment("20100101", "YYYYMMDD"), moment()],
        }
    }, daterange_callback);

    display(start, end);
});

</script>
@endsection