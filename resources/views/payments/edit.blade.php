@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Patients</a></li>
		<li class="active"><a href="#">Payments</a></li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-info"></span> &nbsp;Patient Information</h3>
		</div>
		<div class="box-body">
			<div class="pull-left image">
	          	@if($patient->image)
	          		@if(\File::exists($patient->image))
	          			{!! HTML::image($patient->image, '', ['class'=>'img-circle list-image']) !!}
	          		@else
	          			{!! HTML::image('uploads/patients/'.$patient->image, '', ['class'=>'img-circle list-image']) !!}
	        		@endif
	        	@else
	        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
	        	@endif
	        </div>
	        <div class="pull-left info" style="padding-left:15px">
	        	<p style="font-size:16px !important">
	        		<b>{{ $patient->first_name.' '.$patient->last_name }}</b><br>
	        		{{ $patient->contact_number }}<br>
	        		{{ $patient->address }}<br>
	        		{{ \Carbon\Carbon::parse($patient->birthdate)->format('F d, Y').' - '.\Carbon\Carbon::parse($patient->birthdate)->age.' years old' }}
	        	</p>
	        </div>
	        <div class="pull-right">
	        	<a href="{{ route('patient.show', \Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-user"></span> &nbsp;Patient Info</a>
	        </div>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-credit-card"></span> &nbsp;Update Payment</h3>
		</div>
		<div class="box-body">
			{!! Form::open(['url'=>['payments',Hashids::encode($payment->id)],'method'=>'PUT','id'=>'paymentform']) !!}
			@if(isset($dentalnote->chart_id))
			<div class="form-group">
				<label>Tooth Number {{ $dentalnote->chart_id }}</label>
			</div>
			@endif
			<div class="form-group" id="dateField">
				<label>Date</label>
				<div class="input-group">
			        <div class="input-group-addon" style="padding:0 10px 0 10px">
			        	<i class="fa fa-calendar"></i>
			        </div>
					{!! Form::text('payment_date', isset($payment->payment_date) ? $payment->payment_date : null, ['id'=>'payment_date','class'=>'form-control datemask']) !!}
				</div>
				<div id="dateInput" style="font-size:11px; color:#dd4b39"></div>
			</div>
			<div class="form-group" id="amountField">
				<label>Amount</label>
				{!! Form::text('amount', isset($payment->amount) ? $payment->amount : null, ['class'=>'form-control','id'=>'amount']) !!}
				<div id="amountInput" style="font-size:11px; color:#dd4b39"></div>
			</div>
			<div class="form-group" id="remarksField">
				<label>Remarks</label>
				{!! Form::textarea('remarks', isset($payment->remarks) ? $payment->remarks : null, ['class'=>'form-control','id'=>'remarks','style'=>'height:100px;resize:none']) !!}
				<div id="remarksInput" style="font-size:11px; color:#dd4b39"></div>
			</div>
			<div class="form-group">
				<label>Payment Type</label>
				<div class="radio">
					<label>
						{{-- <input type="radio" name="payment_type" value="Cash" id="cash" checked> --}}
						{!! Form::radio('payment_type', 'Cash', $payment_type->cash) !!}
						Cash
					</label>
				</div>
				<div class="radio">
					<label>
						{{-- <input type="radio" name="payment_type" value="Installment" id="installment"> --}}
						{!! Form::radio('payment_type', 'Installment', $payment_type->installment) !!}
						Installment
					</label>
				</div>
			</div>
			<div class="form-group">
				{!! Form::hidden('patient_id', $patient->id) !!}
				{!! Form::hidden('dental_note_id', isset($dentalnote->chart_id) ? $dentalnote->id : null) !!}
				{!! Form::button('Save Payment',['class'=>'btn btn-primary btn-flat btn-sm','id'=>'savepaymentbtn']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@endsection

@section('js')
{!! HTML::script('library/input-mask/jquery.inputmask.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! HTML::script('library/input-mask/jquery.inputmask.extensions.js') !!}
<script type="text/javascript">
$(document).ready(function() {
	$(".datemask").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
	$('#amount').keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {
        } else {
            event.preventDefault();
        }
        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault(); 
    });

    $('#savepaymentbtn').click(function() {
    	if($('#payment_date').val()==''){
			$('#dateField').addClass('has-error');
		    $('#dateInput').html('<i class="fa fa-times-circle-o"></i> Payment Date is Required.');
		    $('#payment_date').focus();
		    return false;
		} else if($('#amount').val()==''){
			$('#amountField').addClass('has-error');
		    $('#amountInput').html('<i class="fa fa-times-circle-o"></i> Amount is Required.');
		    $('#amount').focus();
		    return false;
		} else if($('#remarks').val()==''){
			$('#remarksField').addClass('has-error');
		    $('#remarksInput').html('<i class="fa fa-times-circle-o"></i> Remarks is Required.');
		    $('#remarks').focus();
		    return false;
		} else {
			$('#savepaymentbtn').attr('disabled', 'disabled');
			$('#savepaymentbtn').html('<span class="fa fa-spinner fa-spin"></span> Processing...');
			$('#paymentform').submit();
		}
    });

});
</script>
@endsection