<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
        <meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
        <meta name="author" content="Quantum X Inc.">
        <title>My Dental Clinic</title>
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
        <!--  Custom -->
        {!! HTML::style('home/css/custom.css') !!}
        <!-- Bootstrap -->
        {!! HTML::style('home/css/bootstrap.min.css') !!}
        <!-- Owl Carousel Assets -->
        {!! HTML::style('home/css/owl.carousel.css') !!}
        {!! HTML::style('home/css/owl.theme.css') !!}
        <!-- Pixeden Icon Font -->
        {!! HTML::style('home/css/Pe-icon-7-stroke.css') !!}
        <!-- Font Awesome -->
        {!! HTML::style('home/css/font-awesome.min.css') !!}
        <!-- PrettyPhoto -->
        {!! HTML::style('home/css/prettyPhoto.css') !!}
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <!-- Style -->
        {!! HTML::style('home/css/style.css') !!}
        {!! HTML::style('home/css/animate.css') !!}
        <!-- Responsive CSS -->
        {!! HTML::style('home/css/responsive.css') !!}
        {!! HTML::style('home/sweetalert/sweetalert.css') !!}
        <link rel="apple-touch-icon" sizes="57x57" href="home/images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="home/images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="home/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="home/images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="home/images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="home/images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="home/images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="home/images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="home/images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="home/images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="home/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="home/images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="home/images/favicon-16x16.png">
        <link rel="manifest" href="js/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="home/images/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-72502900-3', 'auto');
            ga('send', 'pageview');
            
        </script>
    </head>
    <body>
        <!-- PRELOADER -->
        <div class="spn_hol">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div class='scrolltop'>
            <div class="scroll icon"><img src="home/images/arrow-up.png" class="img-responsive" alt="Image">
            </div>

        </div>
        <!-- END PRELOADER -->
        <!-- =========================
            START ABOUT US SECTION
            ============================== -->
        <!-- Static navbar -->
        <section class="header parallax home-parallax page" id="HOME">         
            <nav class="navbar navbar-default navbar-fixed-top navbar-dental" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header navbar-mobile">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <div class="logo"><a href="{{ url('/') }}"><img src="home/images/logo.png" class="img-responsive" alt="My Dental Clinic"></a></div>
                        <ul class="nav navbar-nav navbar-right nav-mobile">
                            <!-- NAV -->
                            <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                            <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                            <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                            <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                            <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>
        </section>

        <div class="dental-home-container">
            <div class="container">
                <div class="ag-wrap">
                    <div class="section_title">
                        <h2 class="text-center">PRIVACY POLICY</h2>
                    </div>
                    </br>
                    <h4 class="ag-intro">
                        This policy covers the website at (the "Site") and explains our information practices, data security practices, defines your privacy options and describes how your information is collected and used by My Dental Clinic (we", or "us").
                    </h4>
                    </br>
                    <h4 class="ag-intro">
                      By using this Site, you agree to the collection and use of information in the manner described by this policy. We shall have the right at any time and without prior notice, at our sole discretion, to revise this policy. Such revisions and additions shall be effective immediately upon notice, which may be given by any means, including but not limited to posting the revised or additional terms and conditions on this Site. You are responsible for reviewing the Site periodically for any modification to this policy.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Personal Information we Collect
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        This policy applies to all information collected on this Site and any information you provide to us as a client of My Dental Clinic. In addition, it applies to any applicants who provide resumes, references, applications for employment, or any other information when applying for positions with our clients.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        You can generally visit the Site without revealing any personally identifiable information about yourself. We do not collect personal information from our Site visitors without the Site visitor providing us with this information as set forth in this Privacy Policy.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        If you wish to receive company or product information or request an online demonstration of the Services, you can do so by completing the online demo request form on our Site that asks for your name, company name, email address, and phone number. You can also voluntarily provide comments to us when submitting the 
                        request form. We use the information from the request form to deliver the requested information and/or provide you with an online demonstration of My Dental Clinic.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        In order to become a registered user of My Dental Clinic, we require you to create a user account. Information we collect to register a user account will include, but is not limited to, the following personally identifiable information: first and last name, user name, password, and email address. 
                        The personal information we collect on behalf of our clients from our clients’ applicants will be dictated by our clients as they configure online employment applications.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Non-Personal Information we may Collect
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       In addition to the personal information you supply, we may collect certain information to evaluate how visitors use our Site. We collect data to make our service work better for you by improving the design of the Site, by providing personalization of the Site and to evaluate the performance of our marketing programs. 
                       The technologies we use to gather this non-personal information may include IP addresses, cookies, clear gifs, browser detection, and weblogs. We further use this information to provide a better experience on our website and to give us useful business information.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       "IP" addresses define the Internet location of computers and help us better understand the geographic distribution of our members and manage the performance of the Site. "Cookies" are tiny files placed onto the hard drive of your computer when you visit the Site, so we can immediately recognize you when you return to the site and deliver content specific to your interests. 
                       You may modify your browser preferences to accept all cookies, be notified when a cookie is set, or reject all cookies. Please consult your browser instructions for information on how to modify your choices about cookies.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       We may detect the type of web browser you are using to optimize the performance of our website and to understand the mix of browsers used by our clients and visitors. To learn about how people use our site, we examine weblogs, which show the paths people take through our site and how long they spend in certain areas. 
                       We may also track and analyze non-identifying and aggregate usage and volume statistical information from our visitors and clients.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;How we use Information
                    </h4>
                    </br>
                    <h4 class="ag-content">
                      Our primary goal in collecting your client information is to provide you with a personalized, relevant, and positive experience with the Site. We use the information that we collect to set up Services for individuals and their organizations. We may also use the information to contact clients to further discuss client interest in our company, 
                      the Services that we provide, and to send information regarding our company or partners, such as promotions and events.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                      My Dental Clinic  collects information under the direction of its clients, and has no direct relationship with the individuals whose personal data it processes. Client email addresses and any personal client information will not be distributed or shared with any third parties. We may email information regarding updates to the Service. 
                      Except as we explicitly state at the time we request information, or as provided for in the My Dental Clinic  Terms of Use, we do not disclose to any third-party the information provided.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Choice and Access to Data Controlled by our Clients
                    </h4>
                    </br>
                    <h4 class="ag-content">
                      My Dental Clinic  has no direct relationship with the individual employees and job-seekers whose personal data it processes on behalf of our clients. Individuals applying for jobs with employers that are My Dental Clinic  clients can contact the employers directly to correct, amend, and delete inaccurate data provided to the employer as part of the online application process. 
                      The employer is responsible for complying with the individual’s request. Additionally, if you are an employee or job-seeker and would no longer like to be contacted by a particular employer or employers, please contact them directly to resolve your concern.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Updating, Correcting and Deleting Your Information
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        My Dental Clinic  provides an individual that supplies information on the Site with the ability to review, correct, and request the deletion of any of the personally identifiable information that was entered into their profile on the Site. To correct or delete any information in a Site profile, an individual may update that profile (through their account page on the Site) or may contact My Dental Clinic  by emailing our client support at email@dentalclinicapp.com. 
                        We will respond to the request within 30 days. 
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        When a client terminates their account on the Site, we will deactivate their profile from the Site. Information supplied by an employee or job seeker in an application or resume submitted to an employer that is a My Dental Clinic  client is treated as described above in the section "Choice and Access to Data Controlled by our Clients."
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Data Security
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        The security of your personal information is important to us. We follow industry-standard technical and organizational measures to protect the security of our users' and applicants' personal information. Our software is hosted on secure, fire-walled servers in an SSAE-16 compliant data center. We use 256-bit SSL (Secure Socket Layer) encryption for all data transfers. All client data is backed up daily. 
                        Access to our clients' data is controlled by user names and passwords, both of which are further encrypted by a complex hashing routine. Clients are responsible for maintaining the confidentiality and security of their usernames and passwords. Only authorized My Dental Clinic  employees have access to clients' data and we never share that data with other clients.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       However, My Dental Clinic  cannot guarantee that unauthorized third parties will never be able to defeat these measures or use your personally identifiable information for improper purposes. Therefore, you should be aware that when you voluntarily display or distribute personally identifiable information, that information could be collected and used by others. 
                       My Dental Clinic  is not responsible for the unauthorized use by third parties of information you post or otherwise make available publicly.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;My Dental Clinic Service Providers
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        We engage certain trusted third parties to perform functions and provide services to us, including, without limitation, website hosting and maintenance, client relationship management, database storage and management, and direct marketing campaigns. 
                        We will share your personally identifiable information with these third parties, but only to the extent necessary to perform these functions and provide such services, and only pursuant to binding contractual obligations requiring such third parties to maintain the privacy and security of your data.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Disclosure of Information
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        We reserve the right to disclose your personally identifiable information: (a) if we are required to do so by law, regulation or other government authority or otherwise in cooperation with an investigation of a governmental authority, and/or comply with a judicial proceeding, court order or legal process (b) to enforce the My Dental Clinic Terms of Use or Master Subscription Agreement or (c) to protect our rights and the safety of users of our Site and our Services. 
                        It is also possible that My Dental Clinic  would sell all or substantially all of its assets and business. In any transaction of this kind, client information, including your personally identifiable information, may be among the assets that are transferred. If we decide to do transfer your personally identifiable information, you will be notified by email.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Link to Other Websites
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       The Site may contain links to other websites. My Dental Clinic  is not responsible for the privacy practices of these other sites. We encourage you to be aware when you leave our site, and to read the privacy statements of each web site that collects personally identifiable information. 
                       Some linked sites may be co-branded with our trademarks and those of our business partners. This policy only applies to information collected by My Dental Clinic.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;How to Contact Us
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        Should you have privacy questions or concerns, send an email to email@dentalclinicapp.com.
                    </h4>
                    </div>
                </div>
            </div>
        </div>

        <section class="download page" id="DOWNLOAD">
                 <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- DOWNLOAD NOW SECTION TITLE -->
                        <div class="section_title">
                            <h2>download now</h2>
                        </div>
                            </br>
                            </br>
                        <!--END DOWNLOAD NOW SECTION TITLE -->
                            <div class="row">
                                <div class="col-md-6 img-wrapper">
                                    <img src="home/images/download.png" class="img-responsive" alt="Image">
                                </div>
                                <div class="col-md-5 text-center">
                                    <p class="typo">Designed to keep things simple,
                                    </br>
                                    you will be up and running with 
                                    </br><span class="blue">My Dental Clinic</span> in a few minutes!
                                    </p>
                                    <h2 class="section_subtitle">AVAILABLE ON: </h2>
                                    </br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new"><img src="home/images/googleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new"> <img src="home/images/appleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </br>
                        </div>
                   </div>
            </div>
        </section>
        <!-- END DOWNLOAD -->
        <!-- =========================
            FOOTER 
            ============================== -->

        <div class="contact-footer">
            <div class="container">
                <div class="col-md-offset-1 col-md-3 text-center wrapper-hover"> 
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/email.png" class="img-responsive" alt="Image">
                        </div>
                        <h5><a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a></h5>
                    </div>
                </div>
                <div class="col-md-4 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"><img src="home/images/facebook.png" class="img-responsive" alt="Image"></a>
                        </div>
                        <h5><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> Like us on Facebook </a></h5>
                    </div>
                </div>
                <div class="col-md-3 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/phone.png" class="img-responsive" alt="Image">
                        </div>  
                        <h5> (632) 7094011 to 13</h5>
                    </div>
                </div>
            </div>  
        </div> 
        <div class="copyright">
            <div class="copyright-text col-md-12 text-center">
                <div class="agreement-wrap">
                    <a href="{{ url('terms-and-conditions') }}">Terms &amp; Conditions</a>
                     <span style="color: white;">&nbsp; | &nbsp;</span>
                    <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                </div>
                <h5 class="white">Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></h5>   
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- =========================
            SCRIPTS 
            ============================== -->
        {!! HTML::script('home/js/jquery.min.js') !!}
        {!! HTML::script('home/js/bootstrap.min.js') !!}
        {!! HTML::script('home/js/owl.carousel.js') !!}
        {!! HTML::script('home/js/jquery.fitvids.js') !!}
        {!! HTML::script('home/js/smoothscroll.js') !!}
        {!! HTML::script('home/js/jquery.parallax-1.1.3.js') !!}
        {!! HTML::script('home/js/jquery.prettyPhoto.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.min.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.langs.js') !!}
        {!! HTML::script('home/js/wow.min.js') !!}
        {!! HTML::script('home/js/waypoints.min.js') !!}
        {!! HTML::script('home/js/jquery.counterup.min.js') !!}
        {!! HTML::script('home/js/script.js') !!}
        {!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
        <script>
          // new WOW().init();
        </script>
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function (){
                // if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                //     window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
                // }
                // if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                //     window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
                // }
            
                $('#sendmessagebtn').click(function() {
                    if($('#name').val()==''){
                        swal({   
                            title: "Please input your Full Name.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#email').val()==''){
                        swal({   
                            title: "Please input your Email Address.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#tel').val()==''){
                        swal({   
                            title: "Please input your Contact Number.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#message').val()==''){
                        swal({   
                            title: "Please input your Comments and Suggestions.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else {
                        var email_validation = validateEmail($('#email').val());
                        if (email_validation == false) {
                           swal({   
                                title: "Please Enter a Valid Email Address",   
                                text: "",   
                                timer: 2000,   
                                showConfirmButton: false, 
                                type: "warning" 
                            });  
                           return false;
                        }
                        $('#myForm').submit();
                    }
                });
            });
            <?php if(isset($_GET['messagesent']) && $_GET['messagesent'] == 1){ ?>
                swal("Thank You for your comments and suggestions.", "", "success"); 
            <?php } ?>
            
            function validateEmail($email) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              return emailReg.test( $email );
            }
        </script>
    </body>
</html>