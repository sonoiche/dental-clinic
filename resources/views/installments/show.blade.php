@extends('layouts/default')
@section('content')
<section class="content-header">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-gear"></i> Payments</a></li>
		<li class="active"><a href="#">Installment Payments</li>
	</ol>
</section>
<section class="content">
	<div class="box box-primary" style="color:#444">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-info"></span> &nbsp;Patient Information</h3>
		</div>
		<div class="box-body">
			<div class="pull-left image">
	          	@if($patient->image)
	          		@if(\File::exists($patient->image))
	          			{!! HTML::image($patient->image, '', ['class'=>'img-circle list-image']) !!}
	          		@else
	          			{!! HTML::image('uploads/patients/'.$patient->image, '', ['class'=>'img-circle list-image']) !!}
	        		@endif
	        	@else
	        		{!! HTML::image('uploads/no_pic.jpg', '', ['class'=>'img-circle','style'=>'height:65px;width:65px']) !!}
	        	@endif
	        </div>
	        <div class="pull-left info" style="padding-left:15px;color:#444">
	        	<p style="font-size:16px !important">
	        		<b>{{ $patient->full_name }}</b><br>
	        		{{ $patient->contact_number }}<br>
	        		{{ $patient->address }}<br>
	        		{{ \Carbon\Carbon::parse($patient->birthdate)->format('F d, Y').' - '.\Carbon\Carbon::parse($patient->birthdate)->age.' years old' }}
	        	</p>
	        </div>
	        <div class="pull-right">
	        	<a href="{{ url('patient', \Hashids::encode($patient->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-user"></span> &nbsp;Patient Info</a>
	        </div>
		</div>
	</div>
	<div class="box box-primary" style="color:#444">
		<div class="box-header with-border">
			<h3 class="box-title"><span class="fa fa-credit-card"></span> &nbsp;Installment Payments</h3>
		</div>
		<div class="box-body">
	        <table class="table table-hover">
				<thead>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					<tr>
						<td style="width:30%">
							<b>Total amount</b><br>
							<b>Amount paid</b><br>
							<b>Balance</b>
						</td>
						<td style="width:30%">
							{{ $payment->amount.' '.\Auth::user()->currency }}<br>
							{{ $amount_paid->totalamount.' '.\Auth::user()->currency }}<br>
							{{ $payment->amount - $amount_paid->totalamount.' '.\Auth::user()->currency }}
						</td>
						<td style="width:40%" align="right">
							<a href="{{ url('installments/create', \Hashids::encode($payment->id)) }}" class="btn btn-primary btn-sm"><span class="fa fa-plus"></span> &nbsp;Add Payment</a>
							<a href="{{ url('payments', \Hashids::encode($payment->id)) }}/edit" class="btn btn-primary btn-sm"><span class="fa fa-pencil"></span> &nbsp;Edit Payable</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="box box-primary" style="color:#444">

		<div class="box-body">
			<table class="table table-hover" id="tableinstallment">
				<thead>
					<th></th>
					<th></th>
					<th></th>
				</thead>
				<tbody>
					@if(count($installments))
					@foreach($installments as $key => $installment)
					<tr>
						<td style="vertical-align:middle; width:3%">{{ ($installments->currentpage()-1) * $installments->perpage() + $key + 1 }}</td>
						<td style="width:20%">
							<b>Date</b><br>
							<b>Amount</b><br>
							<b>Remarks</b>
						</td>
						<td style="width:40%">
							{{ \Carbon\Carbon::parse($installment->date)->format('M d, Y') }}<br>
							{{ $installment->amount.' '.\Auth::user()->currency }}<br>
							{{ $installment->remarks }}
						</td>
						<td style="width:10%" align="right">
							<a href="{{ url('installments', \Hashids::encode($installment->id)) }}/edit" class="btn btn-primary btn-xs"><span class="fa fa-pencil"></span></a>
							<a onclick="deletepayment({{ $installment->id }})" class="btn btn-primary btn-xs"><span class="fa fa-trash"></span></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td>No payments has been made yet.</td>
					</tr>
					@endif
				</tbody>
			</table>
			<div class="pull-right">{!! (count($installments)) ? $installments->render() : '' !!}</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script type="text/javascript">
ga('send', 'installments_list');

<?php if(\Session::has('status')){ ?>
	swal("<?php echo \Session::get('status'); ?>", "", "success");  
<?php } ?>

function deletepayment(id){
	swal({   
		title: "Are you sure?",   
		text: "You will not be able to recover this Payment!",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Yes, delete it!",   
		closeOnConfirm: false,
		showLoaderOnConfirm: true 
	}, 
	function(){  
		ga('send', 'event', '<?php echo \Auth::user()->email; ?>', 'Delete', 'Delete Patient Installment');
		$.ajax({
			url: base_url+'/installments/'+id,
			type: 'DELETE',
			success: function(result){
				setTimeout(function(){  
					swal("Deleted!", "Payment has been deleted.", "success"); 
					location.reload();
				}, 3000);
			}
		});
	});
}

</script>
@endsection