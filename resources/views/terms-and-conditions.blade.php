<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
        <meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
        <meta name="author" content="Quantum X Inc.">
        <title>My Dental Clinic</title>
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
        <!--  Custom -->
        {!! HTML::style('home/css/custom.css') !!}
        <!-- Bootstrap -->
        {!! HTML::style('home/css/bootstrap.min.css') !!}
        <!-- Owl Carousel Assets -->
        {!! HTML::style('home/css/owl.carousel.css') !!}
        {!! HTML::style('home/css/owl.theme.css') !!}
        <!-- Pixeden Icon Font -->
        {!! HTML::style('home/css/Pe-icon-7-stroke.css') !!}
        <!-- Font Awesome -->
        {!! HTML::style('home/css/font-awesome.min.css') !!}
        <!-- PrettyPhoto -->
        {!! HTML::style('home/css/prettyPhoto.css') !!}
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <!-- Style -->
        {!! HTML::style('home/css/style.css') !!}
        {!! HTML::style('home/css/animate.css') !!}
        <!-- Responsive CSS -->
        {!! HTML::style('home/css/responsive.css') !!}
        {!! HTML::style('home/sweetalert/sweetalert.css') !!}
        <link rel="apple-touch-icon" sizes="57x57" href="home/images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="home/images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="home/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="home/images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="home/images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="home/images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="home/images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="home/images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="home/images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="home/images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="home/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="home/images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="home/images/favicon-16x16.png">
        <link rel="manifest" href="js/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="home/images/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-72502900-3', 'auto');
            ga('send', 'pageview');
            
        </script>
    </head>
    <body>
        <!-- PRELOADER -->
        <div class="spn_hol">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div class='scrolltop'>
            <div class="scroll icon"><img src="home/images/arrow-up.png" class="img-responsive" alt="Image">
            </div>

        </div>
        <!-- END PRELOADER -->
        <!-- =========================
            START ABOUT US SECTION
            ============================== -->
        <!-- Static navbar -->
        <section class="header parallax home-parallax page" id="HOME">         
            <nav class="navbar navbar-default navbar-fixed-top navbar-dental" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header navbar-mobile">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <div class="logo"><a href="{{ url('/') }}"><img src="home/images/logo.png" class="img-responsive" alt="My Dental Clinic"></a></div>
                        <ul class="nav navbar-nav navbar-right nav-mobile">
                            <!-- NAV -->
                            <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                            <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                            <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                            <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                            <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>
        </section>

        <div class="dental-home-container">
            <div class="container">
                <div class="ag-wrap">
                    <div class="section_title">
                        <h2 class="text-center">WELCOME TO MY DENTAL CLINIC</h2>
                    </div>
                    </br>
                    <h4 class="ag-intro">
                        These terms and conditions outline the rules and regulations for the use of My Dental Clinic Website.  My Dental Clinic is located at: Cubao , Quezon City - 1102, Philippines
                    </h4>
                    </br>
                    <h4 class="ag-intro">
                        By accessing this website we assume you accept these terms and conditions in full. Do not continue to use My Dental Clinic's website if you do not accept all of the terms and conditions stated on this page.
                    </h4>
                    </br>
                    <h4 class="ag-intro">
                        The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: 
                    </h4>
                    </br>
                    <h4 class="ag-intro">
                        “Client”, “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. 
                        “The Company”, “Ourselves”, “We”, “Our” and “Us”, refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of Philippines. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Cookies
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       We employ the use of cookies. By using My Dental Clinic's website you consent to the use of cookies in accordance with My Dental Clinic privacy policy.Most of the modern day interactive websites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate / advertising partners may also use cookies.
                    </h4>                   
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;License
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       You must not:
                    </h4>
                    <ul class="ag-list">
                        <li class="ag-list-content">Republish material from http://dentalclinicapp.com/</li>
                        <li class="ag-list-content">Sell, rent or sub-license material from http://dentalclinicapp.com/</li>
                        <li class="ag-list-content">Reproduce, duplicate or copy material from http://dentalclinicapp.com/</li>
                        <li class="ag-list-content">Redistribute content from My Dental Clinic (unless content is specifically made for redistribution).</li>
                    </ul>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Hyperlinking to our Content
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       The following organizations may link to our Web site without prior written approval:
                    </h4>
                    <ul class="ag-list">
                        <li class="ag-list-content">Government agencies;</li>
                        <li class="ag-list-content">Search engines;</li>
                        <li class="ag-list-content">News organizations;</li>
                    </ul>
                    </br>
                    <h4 class="ag-content">
                       Online directory distributors when they list us in the directory may link to our Web site in the same manner as they hyperlink to the Web sites of other listed businesses; and Systemwide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Website.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party's site.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        We may consider and approve in our sole discretion other link requests from the following types of organizations:
                    </h4>
                    <ul class="ag-list">
                        <li class="ag-list-content">associations or other groups representing charities, including charity giving sites;</li>
                        <li class="ag-list-content">online directory distributors;</li>
                        <li class="ag-list-content">internet portals;</li>
                        <li class="ag-list-content">accounting, law and consulting firms whose primary clients are businesses; and</li>
                        <li class="ag-list-content">educational institutions and trade associations.</li>
                    </ul>
                    </br>
                    <h4 class="ag-content">
                        We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link); 
                        (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of ; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services; and (c) fits within the context of the linking party's site.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to email@dentalclinicapp.com.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        Approved organizations may hyperlink to our Web site as follows:
                    </h4>
                    <ul class="ag-list">
                        <li class="ag-list-content">By use of our corporate name; or</li>
                        <li class="ag-list-content">By use of the uniform resource locator (Web address) being linked to; or</li>
                        <li class="ag-list-content">By use of any other description of our Web site or material being linked to that makes sense within the context and format of content on the linking party's site.</li>
                    </ul>
                    </br>
                    <h4 class="ag-content">
                        No use of My Dental Clinic logo or other artwork will be allowed for linking absent a trademark license agreement.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Iframes
                    </h4>
                    </br>
                    <h4 class="ag-content">
                       Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Reservation of Rights
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Website. You agree to immediately remove all links to our Web site upon such request. 
                        We also reserve the right to amend these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        Removal of links from our website If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Content Liability
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        We shall have no responsibility or liability for any content appearing on your Website. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Disclaimer
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:
                    </h4>
                    <ul class="ag-list">
                        <li class="ag-list-content">limit or exclude our or your liability for death or personal injury resulting from negligence;</li>
                        <li class="ag-list-content">limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
                        <li class="ag-list-content">limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
                        <li class="ag-list-content">exclude any of our or your liabilities that may not be excluded under applicable law.</li>
                    </ul>
                    </br>
                    <h4 class="ag-content">
                        The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.
                    </h4>
                    </br>
                    <h4 class="ag-section-title">
                        <i class="fa fa-tag" aria-hidden="true"></i>&nbsp;Credit &amp; Contact Information
                    </h4>
                    </br>
                    <h4 class="ag-content">
                        If you have any queries regarding any of our terms, please contact us.
                    </h4>
                 




        
                </div>
            </div>
        </div>
    </div>

        <section class="download page" id="DOWNLOAD">
                 <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- DOWNLOAD NOW SECTION TITLE -->
                        <div class="section_title">
                            <h2>download now</h2>
                        </div>
                            </br>
                            </br>
                        <!--END DOWNLOAD NOW SECTION TITLE -->
                            <div class="row">
                                <div class="col-md-6 img-wrapper">
                                    <img src="home/images/download.png" class="img-responsive" alt="Image">
                                </div>
                                <div class="col-md-5 text-center">
                                    <p class="typo">Designed to keep things simple,
                                    </br>
                                    you will be up and running with 
                                    </br><span class="blue">My Dental Clinic</span> in a few minutes!
                                    </p>
                                    <h2 class="section_subtitle">AVAILABLE ON: </h2>
                                    </br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new"><img src="home/images/googleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new"> <img src="home/images/appleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </br>
                        </div>
                   </div>
            </div>
        </section>
        <!-- END DOWNLOAD -->
        <!-- =========================
            FOOTER 
            ============================== -->

        <div class="contact-footer">
            <div class="container">
                <div class="col-md-offset-1 col-md-3 text-center wrapper-hover"> 
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/email.png" class="img-responsive" alt="Image">
                        </div>
                        <h5><a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a></h5>
                    </div>
                </div>
                <div class="col-md-4 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"><img src="home/images/facebook.png" class="img-responsive" alt="Image"></a>
                        </div>
                        <h5><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> Like us on Facebook </a></h5>
                    </div>
                </div>
                <div class="col-md-3 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/phone.png" class="img-responsive" alt="Image">
                        </div>  
                        <h5> (632) 7094011 to 13</h5>
                    </div>
                </div>
            </div>  
        </div> 
        <div class="copyright">
            <div class="copyright-text col-md-12 text-center">
                <div class="agreement-wrap">
                    <a href="{{ url('terms-and-conditions') }}">Terms &amp; Conditions</a>
                     <span style="color: white;">&nbsp; | &nbsp;</span>
                    <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                </div>
                <h5 class="white">Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></h5>   
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- =========================
            SCRIPTS 
            ============================== -->
        {!! HTML::script('home/js/jquery.min.js') !!}
        {!! HTML::script('home/js/bootstrap.min.js') !!}
        {!! HTML::script('home/js/owl.carousel.js') !!}
        {!! HTML::script('home/js/jquery.fitvids.js') !!}
        {!! HTML::script('home/js/smoothscroll.js') !!}
        {!! HTML::script('home/js/jquery.parallax-1.1.3.js') !!}
        {!! HTML::script('home/js/jquery.prettyPhoto.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.min.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.langs.js') !!}
        {!! HTML::script('home/js/wow.min.js') !!}
        {!! HTML::script('home/js/waypoints.min.js') !!}
        {!! HTML::script('home/js/jquery.counterup.min.js') !!}
        {!! HTML::script('home/js/script.js') !!}
        {!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
        <script>
          // new WOW().init();
        </script>
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function (){
                // if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                //     window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
                // }
                // if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                //     window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
                // }
            
                $('#sendmessagebtn').click(function() {
                    if($('#name').val()==''){
                        swal({   
                            title: "Please input your Full Name.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#email').val()==''){
                        swal({   
                            title: "Please input your Email Address.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#tel').val()==''){
                        swal({   
                            title: "Please input your Contact Number.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#message').val()==''){
                        swal({   
                            title: "Please input your Comments and Suggestions.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else {
                        var email_validation = validateEmail($('#email').val());
                        if (email_validation == false) {
                           swal({   
                                title: "Please Enter a Valid Email Address",   
                                text: "",   
                                timer: 2000,   
                                showConfirmButton: false, 
                                type: "warning" 
                            });  
                           return false;
                        }
                        $('#myForm').submit();
                    }
                });
            });
            <?php if(isset($_GET['messagesent']) && $_GET['messagesent'] == 1){ ?>
                swal("Thank You for your comments and suggestions.", "", "success"); 
            <?php } ?>
            
            function validateEmail($email) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              return emailReg.test( $email );
            }
        </script>
    </body>
</html>