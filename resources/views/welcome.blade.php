<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My Dental Clinic helps dentist manage their patients and clinic by keeping a database of their patients records on their mobile phone, tablet and desktop.">
        <meta name="keywords" content="My Dental Clinic app, dental clinic management, patient information management, dental clinic management solution, dental app, dental clinic management app, My Dental Clinic Android, My Dental Clinic iTunes, patient information on mobile, dental patient information database">
        <meta name="author" content="Quantum X Inc.">
        <title>My Dental Clinic</title>
        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
        <!--  Custom -->
        {!! HTML::style('home/css/custom.css') !!}
        <!-- Bootstrap -->
        {!! HTML::style('home/css/bootstrap.min.css') !!}
        <!-- Owl Carousel Assets -->
        {!! HTML::style('home/css/owl.carousel.css') !!}
        {!! HTML::style('home/css/owl.theme.css') !!}
        <!-- Pixeden Icon Font -->
        {!! HTML::style('home/css/Pe-icon-7-stroke.css') !!}
        <!-- Font Awesome -->
        {!! HTML::style('home/css/font-awesome.min.css') !!}
        <!-- PrettyPhoto -->
        {!! HTML::style('home/css/prettyPhoto.css') !!}
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <!-- Style -->
        {!! HTML::style('home/css/style.css') !!}
        {!! HTML::style('home/css/animate.css') !!}
        <!-- Responsive CSS -->
        {!! HTML::style('home/css/responsive.css') !!}
        {!! HTML::style('home/sweetalert/sweetalert.css') !!}
        <link rel="apple-touch-icon" sizes="57x57" href="home/images/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="home/images/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="home/images/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="home/images/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="home/images/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="home/images/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="home/images/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="home/images/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="home/images/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="home/images/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="home/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="home/images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="home/images/favicon-16x16.png">
        <link rel="manifest" href="js/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="home/images/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-72502900-3', 'auto');
            ga('send', 'pageview');
            
        </script>
    </head>
    <body>
        <!-- PRELOADER -->
        <div class="spn_hol">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div class='scrolltop'>
            <div class="scroll icon"><img src="home/images/arrow-up.png" class="img-responsive" alt="Image">
            </div>

        </div>
        <!-- END PRELOADER -->
        <!-- =========================
            START ABOUT US SECTION
            ============================== -->
        <!-- Static navbar -->
        <section class="header parallax home-parallax page" id="HOME">         
            <nav class="navbar navbar-default navbar-fixed-top navbar-dental" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header navbar-mobile">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <div class="logo"><a href="{{ url('/') }}"><img src="home/images/logo.png" class="img-responsive" alt="My Dental Clinic"></a></div>
                        <ul class="nav navbar-nav navbar-right nav-mobile">
                            <!-- NAV -->
                            <li><a href="{{ url('/#HOME') }}">HOME</a> </li>
                            <li><a href="{{ url('/#FEATURES') }}">FEATURES</a></li>
                            <li><a href="{{ url('/register') }}">REGISTER </a> </li>
                            <li><a href="{{ url('/login') }}">LOGIN </a> </li>
                            <li><a href="{{ url('/#CONTACT') }}">CONTACT </a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> LIKE US</a> </li>
                            <li><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> <span class="fa fa-facebook-official custom-icon"></span></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>
            <div class="container dental-home-container">
                <div class="row">
                    <div class="col-lg-5 col-xs-12 col-sm-12 col-md-5 wow fadeInLeft" data-wow-delay="1s">
                        <div class="home_text">
                            <!-- TITLE AND DESC -->
                            <h1>We have the solution to help you manage your dental clinic.</h1>
                            <p>We want to help you manage your patient information. Instead of using index cards, we have created an app that you can use on different devices. Your mobile phone, your tablet and the desktop.  Now you can have your patient information with you anytime and anywhere.
                            </p>
                            <div class="embed-responsive embed-responsive-16by9 inner hidden-md hidden-lg">
                                <iframe class="dental-avp" align="middle" src="https://www.youtube.com/embed/NweTwht6VAI?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="download-btn">
                                <!-- BUTTON -->
                                <div class="row applegoogleplayad hidden-xs">
                                    <div class="col-md-6 col-lg-6 googleplayadimg">
                                        <a class="wow fadeInLeft" href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_blank"><img src="home/images/googleappstore.png" class="img-responsive" alt="My Dental Clinic Android"/></a>
                                    </div>
                                    <div class="col-md-6 col-lg-6 appstoreadimg">
                                        <a class="wow fadeInLeft" href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_blank"><img src="home/images/appleappstore.png" class="img-responsive" alt="My Dental Clinic Apple"/></a>
                                    </div>
                                </div>
                                 <div class="row applegoogleplayad hidden-md hidden-lg">
                                    <div class="col-xs-6 googleplayadimg">
                                        <a class="wow fadeInLeft" href="https://play.google.com/store/apps/details?id=com.quantumx.mydental"><img src="home/images/googleappstoremini.png" class="img-responsive" alt="My Dental Clinic Android"/></a>
                                    </div>
                                    <div class="col-xs-6 appstoreadimg">
                                        <a class="wow fadeInLeft" href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8"><img src="home/images/appleappstoremini.png" class="img-responsive" alt="My Dental Clinic Apple"/></a>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 browseradimg">
                                        <a href="{{ url('/register') }}"><img src="home/images/btn-browser.png" class="img-responsive" alt="My Dental Clinic Browser"/></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 vid-wrapper outer hidden-xs hidden-sm wow fadeInRight" data-wow-delay="1s">
                        <div class="middle text-center">
                            <div class="embed-responsive embed-responsive-16by9 inner">
                                <iframe class="dental-avp" align="middle" src="https://www.youtube.com/embed/NweTwht6VAI?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HEADER SECTION -->
        <!-- =========================
            START FEATURES
            ============================== -->
        <div id="FEATURES">
        <section id="" class="features page">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <!-- FEATURES SECTION TITLE -->
                        <div class="section_title wow fadeIn" data-wow-duration="1s">
                            <h2>Features</h2>
                            <p>Designed with ease of use in mind, you will be up and running with My Dental Clinic in a few minutes!
                                We have included the features that you need the most:
                            </p>
                        </div>
                        <!-- END FEATURES SECTION TITLE -->
                    </div>
                </div>
            </div>
            <div class="feature_inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 right_no_padding wow fadeInLeft hidden-xs hidden-md" data-wow-duration="1s">
                            <!-- FEATURE -->
                            <div class="left_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-like"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Keep track of your patient information</h3>
                                <p></p>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="left_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-phone"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Call or send SMS to your patients in one click</h3>
                                <p></p>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="left_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-look"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Keep track of your appointments. </h3>
                            </div>
                            <!-- END SINGLE FEATURE -->
                        </div>

                        <div class="col-md-4 right_no_padding wow fadeInLeft hidden-lg" data-wow-duration="1s">
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-like"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Keep track of your patient information</h3>
                                <p></p>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-phone"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Call or send SMS to your patients in one click</h3>
                                <p></p>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-look"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Keep track of your appointments. </h3>
                            </div>
                            <!-- END SINGLE FEATURE -->
                        </div>
                        <div class="col-md-4">
                            <div class="feature_iphone">
                                <!-- FEATURE PHONE IMAGE -->
                                <img class="wow bounceIn img-responsive" data-wow-duration="1s" src="home/images/phone.gif" alt="My Dental Clinic App">
                            </div>
                        </div>
                        <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-monitor"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Attach images to your patient records.</h3>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-info"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Keep track of your patient’s payments. </h3>
                            </div>
                            <!-- END SINGLE FEATURE -->
                            <!-- FEATURE -->
                            <div class="right_single_feature">
                                <!-- ICON -->
                                <div><span class="pe-7s-pen"></span></div>
                                <!-- FEATURE HEADING AND DESCRIPTION -->
                                <h3>Dental chart that is very easy to use.</h3>
                            </div>
                            <!-- END SINGLE FEATURE -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <!-- END FEATURES SECTION -->
        <!-- =========================
            START CONTCT FORM AREA
            ============================== -->
        <div id="CONTACT">
        <section class="contact page" id="">
            <div class="section_overlay">
                <div class="container">
                    <div class="col-md-10 col-md-offset-1 wow fadeIn">
                        <!-- Start Contact Section Title-->
                        <div class="section_title">
                            <h2>Get in touch</h2>
                            <p>Do you have a comment, a suggestion or in need of help? We are here to listen! <br/>
                                Emal Address: <a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a><br/>
                                Contact Number: (632) 7094011 to 13
                                
                            </p>
                        </div>
                    </div>
                </div>
                <div class="contact_form wow fadeIn">
                    <div class="container">
                        <!-- START ERROR AND SUCCESS MESSAGE -->
                        <div class="form_error text-center">
                            <div class="name_error hide error">Please Enter your name</div>
                            <div class="email_error hide error">Please Enter your Email</div>
                            <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                            <div class="message_error hide error">Please Enter Your Message for tech support, please specify the device you are using</div>
                        </div>
                        <div class="Sucess"></div>
                        <!-- END ERROR AND SUCCESS MESSAGE -->
                        <!-- FORM -->    
                        <form class="form-horizontal" role="form" action="send_email.php" method="post" name="myForm" id="myForm" enctype="MULTIPART/FORM-DATA">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="fullname">
                                    <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                                    <input type="text" class="form-control" id="tel" placeholder="Contact Number" name="contact">
                                </div>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="message" rows="25" cols="10" placeholder="Please Enter Your Message for tech support, please specify the device you are using" name="message"></textarea>
                                    <button type="button" class="btn btn-default submit-btn form_submit" id="sendmessagebtn">SEND MESSAGE</button>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM --> 
                    </div>
                </div>
            </div>
        </section>
        </div>
        <!-- END CONTACT -->
        <!-- =========================
            START DOWNLOAD NOW 
            ============================== -->
        <section class="download page" id="DOWNLOAD">
                 <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <!-- DOWNLOAD NOW SECTION TITLE -->
                        <div class="section_title">
                            <h2>download now</h2>
                        </div>
                            </br>
                            </br>
                        <!--END DOWNLOAD NOW SECTION TITLE -->
                            <div class="row">
                                <div class="col-md-6 img-wrapper">
                                    <img src="home/images/download.png" class="img-responsive" alt="Image">
                                </div>
                                <div class="col-md-5 text-center">
                                    <p class="typo">Designed to keep things simple,
                                    </br>
                                    you will be up and running with 
                                    </br><span class="blue">My Dental Clinic</span> in a few minutes!
                                    </p>
                                    <h2 class="section_subtitle">AVAILABLE ON: </h2>
                                    </br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://play.google.com/store/apps/details?id=com.quantumx.mydental" target="_new"><img src="home/images/googleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <a href="https://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8" target="_new"> <img src="home/images/appleappstore.png" class="img-responsive" alt="Image"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </br>
                        </div>
                   </div>
            </div>
        </section>
        <!-- END DOWNLOAD -->
        <!-- =========================
            FOOTER 
            ============================== -->

        <div class="contact-footer">
            <div class="container">
                <div class="col-md-offset-1 col-md-3 text-center wrapper-hover"> 
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/email.png" class="img-responsive" alt="Image">
                        </div>
                        <h5><a href="mailto:email@dentalclinicapp.com">email@dentalclinicapp.com</a></h5>
                    </div>
                </div>
                <div class="col-md-4 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"><img src="home/images/facebook.png" class="img-responsive" alt="Image"></a>
                        </div>
                        <h5><a href="https://www.facebook.com/mydentalclinicapp/" target="_blank"> Like us on Facebook </a></h5>
                    </div>
                </div>
                <div class="col-md-3 text-center wrapper-hover">
                    <div class="contact-detail-wrap">
                        <div class="img-wrapper">
                            <img src="home/images/phone.png" class="img-responsive" alt="Image">
                        </div>  
                        <h5> (632) 7094011 to 13</h5>
                    </div>
                </div>
            </div>  
        </div> 
        <div class="copyright">
            <div class="copyright-text col-md-12 text-center">
                <div class="agreement-wrap">
                    <a href="{{ url('terms-and-conditions') }}">Terms &amp; Conditions</a>
                     <span style="color: white;">&nbsp; | &nbsp;</span>
                    <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                </div>
                <h5 class="white">Copyright &copy; 2016 <a href="index.php">My Dental Clinic</a> <span>By </span><a href="http://www.quantumx.com" target="_new">Quantum X Inc.</a></h5>   
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- =========================
            SCRIPTS 
            ============================== -->
        {!! HTML::script('home/js/jquery.min.js') !!}
        {!! HTML::script('home/js/bootstrap.min.js') !!}
        {!! HTML::script('home/js/owl.carousel.js') !!}
        {!! HTML::script('home/js/jquery.fitvids.js') !!}
        {!! HTML::script('home/js/smoothscroll.js') !!}
        {!! HTML::script('home/js/jquery.parallax-1.1.3.js') !!}
        {!! HTML::script('home/js/jquery.prettyPhoto.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.min.js') !!}
        {!! HTML::script('home/js/jquery.ajaxchimp.langs.js') !!}
        {!! HTML::script('home/js/wow.min.js') !!}
        {!! HTML::script('home/js/waypoints.min.js') !!}
        {!! HTML::script('home/js/jquery.counterup.min.js') !!}
        {!! HTML::script('home/js/script.js') !!}
        {!! HTML::script('home/sweetalert/sweetalert-dev.js') !!}
        <script>
          // new WOW().init();
        </script>
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function (){
                // if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
                //     window.location.href = 'http://play.google.com/store/apps/details?id=com.quantumx.mydental';
                // }
                // if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
                //     window.location.href = 'http://itunes.apple.com/us/app/my-dental-clinic/id1051978497?mt=8';
                // }
            
                $('#sendmessagebtn').click(function() {
                    if($('#name').val()==''){
                        swal({   
                            title: "Please input your Full Name.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#email').val()==''){
                        swal({   
                            title: "Please input your Email Address.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#tel').val()==''){
                        swal({   
                            title: "Please input your Contact Number.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else if($('#message').val()==''){
                        swal({   
                            title: "Please input your Comments and Suggestions.",   
                            text: "",   
                            timer: 2000,   
                            showConfirmButton: false, 
                            type: "warning" 
                        });
                        return false;
                    } else {
                        var email_validation = validateEmail($('#email').val());
                        if (email_validation == false) {
                           swal({   
                                title: "Please Enter a Valid Email Address",   
                                text: "",   
                                timer: 2000,   
                                showConfirmButton: false, 
                                type: "warning" 
                            });  
                           return false;
                        }
                        $('#myForm').submit();
                    }
                });
            });
            <?php if(isset($_GET['messagesent']) && $_GET['messagesent'] == 1){ ?>
                swal("Thank You for your comments and suggestions.", "", "success"); 
            <?php } ?>
            
            function validateEmail($email) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              return emailReg.test( $email );
            }
        </script>
    </body>
</html>