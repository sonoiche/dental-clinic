<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>My Dental Clinic</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="shortcut icon" href="{{ url('uploads/favicon.ico') }}" type="image/x-icon">
  <link rel="icon" href="{{ url('uploads/favicon.ico') }}" type="image/x-icon">
  {!! HTML::style('css/bootstrap.css') !!}
  {!! HTML::style('css/font-awesome.min.css') !!}
  {!! HTMl::style('css/ionicons.min.css') !!}
  {!! HTML::style('library/datatables/dataTables.bootstrap.css') !!}
  {!! HTML::style('css/style.css') !!}
  {!! HTML::style('css/skins/_all-skins.css') !!}
  {!! HTMl::style('library/datepicker/datepicker3.css') !!}
  {!! HTML::style('library/multiupload/jquery-ui.css') !!}
  {!! HTMl::style('library/sweetalert/sweetalert.css') !!}
  @yield('css')
  <style type="text/css">
  .ui-datepicker-month {
    -moz-z-index: 9999;
  }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" data-url="{{ url('/') }}">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/likod/dashboard') }}" class="logo" style="position:fixed; background-color:#196eee">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{ url('uploads/logo-tooth.png') }}" alt="" width="30"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ url('uploads/logo.png') }}" alt="" width="200"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation" style="position:fixed; width:100%">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <!-- <div class="">
        <form class="navbar-form" role="search" action="{{ route('patient.search') }}" method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <div class="form-group">
            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search Patient Name" style="width:300px;background-color:#fff" name="patient_name" value="{{ isset($patient_name) ? $patient_name : '' }}">
          </div>
        </form>
      </div> -->
    </nav>
  </header>

  <aside class="main-sidebar" style="position:fixed">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header" style="color:#fff"><h5>{{ \Auth::guard('admin')->user()->first_name.' '.\Auth::guard('admin')->user()->last_name }} </h5></li>
        <li><a href="{{ route('likod.dashboard.index') }}"><i class="fa fa-home"></i><span>Home</span></a></li>
        <li><a href="{{ route('admin.dentist') }}"><i class="fa fa-user"></i><span>Dentist</span></a></li>
        <li><a href="{{ route('admin.patient') }}"><i class="fa fa-group"></i><span>Patients</span></a></li>
        <li><a href="{{ route('admin.stats') }}"><i class="fa fa-bar-chart"></i><span>Stats</span></a></li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar-o"></i>
            <span>Appointments</span>
            <i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('patient_appointment.create') }}"><i class="fa fa-circle-o"></i> Add Appointment</a></li>
            <li><a href="{{ route('patient_appointment.index') }}"><i class="fa fa-circle-o"></i> Appointment's List</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-group"></i>
            <span>Patients</span>
            <i class="fa fa-angle-right pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('patient.create') }}"><i class="fa fa-circle-o"></i> Add Patient</a></li>
            <li><a href="{{ route('patient.index') }}"><i class="fa fa-circle-o"></i> Patient's List</a></li>
          </ul>
        </li>
        <li><a href="{{ route('user.edit', \Hashids::encode(\Auth::guard('admin')->user()->id)) }}"><i class="fa fa-gear"></i><span>Settings</span></a></li>
        <li><a href="{{ route('feedback.create') }}"><i class="fa fa-envelope"></i><span>Feedback</span></a></li> -->
        <!-- <li><a href="#"><i class="fa fa-lock"></i><span>Unlock Features</span></a></li> -->
        <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out"></i><span>Sign Out</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="padding-top:40px">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="container" style="max-height:20px">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; 2016 <a href="#">Quantum X</a>.</strong> All rights reserved.
    </div><!-- /.container -->
  </footer>
</div>
{!! HTML::script('js/jquery.js') !!}
{!! HTML::script('library/multiupload/jquery-ui.min.js') !!}
{!! HTML::script('js/bootstrap.js') !!}
{!! HTML::script('library/select2/select2.full.min.js') !!}
{!! HTML::script('js/jquery.slimscroll.js') !!}
{!! HTML::script('library/iCheck/icheck.min.js') !!}
{!! HTML::script('js/fastclick.js') !!}
{!! HTML::script('js/app.js') !!}
{!! HTML::script('library/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('library/datatables/dataTables.bootstrap.min.js') !!}
{!! HTML::script('library/sweetalert/sweetalert-dev.js') !!}
<script type="text/javascript">
  var base_url = $('body').attr('data-url');
  $.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') }
  });
</script>
<script type="text/javascript">
  // hide main menu if no sub menus
  $(function(){
      $( ".menu_dropdown" ).each(function( index ) {
          var count = $(this).find('ul.dropdown-menu').children('li').length;
          if (count != 0) {
              $(this).show();
          } else {
              $(this).hide();
          }
      });

      $('.datemask').datepicker({
        yearRange: "-1:+2",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy"
      });

      $('.fdatemask').datepicker({
        yearRange: "0:+2",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy",
        minDate: 0
      });

      $('.birthdatemask').datepicker({
        yearRange: "-50:+1",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy"
      });

  });
</script>
@yield('js')
</body>
</html>