<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>My Dental Clinic</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="{{ url('uploads/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ url('uploads/favicon.ico') }}" type="image/x-icon">
<!--  Custom -->
{!! HTML::style('home/css/customadmin.css') !!}
<!--  Custom -->
{!! HTML::style('css/bootstrap.css') !!}
{!! HTML::style('css/font-awesome.min.css') !!}
{!! HTML::style('css/7-stroke/pe-icon-7-stroke.css') !!}
{!! HTML::style('css/7-stroke/helper.css') !!}
{!! HTMl::style('css/ionicons.min.css') !!}
{!! HTML::style('css/daterangepicker.css') !!}
{!! HTML::style('css/skins/_all-skins.css') !!}
{!! HTML::style('css/bootstrap3-wysihtml5.css') !!}
{!! HTMl::style('library/datepicker/datepicker3.css') !!}
{!! HTML::style('library/multiupload/jquery-ui.css') !!}
{!! HTMl::style('library/sweetalert/sweetalert.css') !!}
{!! HTMl::style('library/sweetalert2/dist/sweetalert2.min.css') !!}
{!! HTML::style('css/style.css') !!}
@yield('css')
<style type="text/css">
.ui-datepicker-month {
    -moz-z-index: 9999;
}
.sidebar-menu > li {
    font-size: 15px;
}
</style>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-72502900-3', 'auto');
    ga('send', 'pageview');
</script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" data-url="{{ url('/') }}">
<!-- Site wrapper -->
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('/dashboard') }}" class="logo" style="background-color:#196eee">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="{{ url('uploads/logo-tooth.png') }}" alt="" width="30"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="{{ url('uploads/logo.png') }}" alt="" width="200"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
           

            <div class="container">
                <div class="col-xs-4 col-lg-1">
                 <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                 </a>
                </div>
                <div class="col-xs-8 col-lg-11">
                <form class="navbar-form" role="search" action="{{ url('patient/search') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <input type="text" class="form-control" id="navbar-search-input" placeholder="Search Patient Name" style="background-color:#fff" name="patient_name" value="{{ isset($patient_name) ? $patient_name : '' }}">
                    </div>
                </form>
                </div>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header text-center">
                    @if(\Auth::user()->image)
                        {!! HTML::image(\Auth::user()->image, '', ['style'=>'height:100px; width: auto']) !!} 
                    @else 
                        {!! HTML::image('uploads/no_pic.jpg', '', ['style'=>'height:100px; width: auto']) !!}
                    @endif
                    <h4>{{ \Auth::user()->clinic_name }} </h4>
                    <a href="{{ url('user', \Hashids::encode(\Auth::user()->id)) }}/edit"><span>Edit My Profile</span></a>
                </li>
                <li></li>
                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home fa-fw"></i><span> My Dashboard</span></a></li>
                @if(count($dentalaccess1) && $dentalaccess1->can_read)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-calendar fa-fw"></i>
                        <span>My Appointments</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if($dentalaccess1->can_write)
                        <li><a href="{{ url('appointment/create') }}"><i class="fa fa-circle-o"></i> Add Appointment</a></li>
                        @endif
                        <li><a href="{{ url('appointment') }}"><i class="fa fa-circle-o"></i> Appointment's List</a></li>
                    </ul>
                </li>
                @endif
                @if(count($dentalaccess2) && $dentalaccess2->can_read)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-group fa-fw"></i>
                        <span> My Patients</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if($dentalaccess2->can_write)
                        <li><a href="{{ url('patient/create') }}"><i class="fa fa-circle-o"></i> Add Patient</a></li>
                        @endif
                        <li><a href="{{ url('patient') }}"><i class="fa fa-circle-o"></i> Patient's List</a></li>
                    </ul>
                </li>
                @endif
                @if(count($dentalaccess3) && $dentalaccess3->can_read)
                <li><a href="{{ url('revenues') }}"><i class="fa fa-money fa-fw"></i><span> My Revenues</span></a></li>
                @endif
                @if(count($dentalaccess4) && $dentalaccess4->can_read)
                <!-- <li><a href="{{ url('devices') }}"><i class="fa fa-mobile fa-fw"></i><span> Connected Devices</span></a></li> -->
                @endif
                <li><a href="{{ url('feedback/create') }}"><i class="fa fa-envelope-o fa-fw"></i><span> Feedback</span></a></li>
                @if(count($dentalaccess5) && $dentalaccess5->can_read)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user fa-fw"></i>
                        <span> Manage Users</span>
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if($dentalaccess5->can_write)
                        <li><a href="{{ url('user/create') }}"><i class="fa fa-circle-o"></i> Add User</a></li>
                        @endif
                        <li><a href="{{ url('user') }}"><i class="fa fa-circle-o"></i> User's List</a></li>
                        <li><a href="{{ url('permission') }}"><i class="fa fa-circle-o"></i> Role Manager</a></li>
                    </ul>
                </li>
                @endif
                <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i><span> Sign Out</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="container" style="max-height:20px">
            <div class="row">
                <div class="col-sm-10 text-left">
                    <strong>Copyright &copy; 2016 <a href="#">Quantum X</a>.</strong> All rights reserved.
                </div>
                <div class="col-sm-2 text-center hidden-xs">
                    <b>Version</b> 1.0
                </div>
            </div>
        </div>
    </footer>
</div>
{!! HTML::script('js/jquery.js') !!}
{!! HTML::script('library/multiupload/jquery-ui.min.js') !!}
{!! HTML::script('js/bootstrap.js') !!}
{!! HTML::script('js/moment.min.js') !!}
{!! HTML::script('js/daterangepicker.js') !!}
{!! HTML::script('library/select2/select2.full.min.js') !!}
{!! HTML::script('js/jquery.slimscroll.js') !!}
{!! HTML::script('library/iCheck/icheck.min.js') !!}
{!! HTML::script('js/wysihtml5-0.3.0.min.js') !!}
{!! HTML::script('js/bootstrap3-wysihtml5.js') !!}
{!! HTML::script('js/bootstrap3-wysihtml5.all.min.js') !!}
{!! HTML::script('js/fastclick.js') !!}
{!! HTML::script('js/app.js') !!}
{!! HTML::script('library/sweetalert/sweetalert-dev.js') !!}
{!! HTML::script('library/sweetalert2/dist/sweetalert2.min.js') !!}
<script type = "text/javascript" >
    var base_url = $('body').attr('data-url');
$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
    }
}); </script>

<script type = "text/javascript">
// hide main menu if no sub menus
$(function() {
    $(".menu_dropdown").each(function(index) {
        var count = $(this).find('ul.dropdown-menu').children('li').length;
        if (count != 0) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });

    $('.datemask').datepicker({
        yearRange: "-1:+2",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy"
    });

    $('.fdatemask').datepicker({
        yearRange: "0:+2",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy",
        minDate: 0
    });

    $('.birthdatemask').datepicker({
        yearRange: "-50:+1",
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm/dd/yyyy",
        defaultDate: new Date(1990, 00, 01)
    });

    <?php if(\Session::has('status')){ ?>
        swal({
            title: 'Success!',
            text: '<?php echo \Session::get('status'); ?>',
            type: 'success',
            showConfirmButton: false
        })
        setTimeout(function(){ swal.close(); }, 2000);
    <?php } ?>

}); 
</script>
@yield('js')
</body>
</html>