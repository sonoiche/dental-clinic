<?php

use Carbon\Carbon;

if (!function_exists('getEnv')){
    function getEnv($key, $default = '') {
        $path     = base_path('.env');
        $contents = file($path);
        foreach ($contents as $content) {
            // if line contains provided key
            if (strstr($content, $key)) {
                // none empty line
                list($k, $value) = explode('=', trim($content));
                // return the value of the key
                return $value;
            }
        }
        // return the default value
        return $default;
    }
}

if (!function_exists('setEnv')){
    function setEnv($key, $value){
        $path     = base_path('.env');
        $contents = file($path);
        file_put_contents($path, '');
        $written  = 0;
        $tmpArray = [];
        foreach ($contents as $content){
            // line contains the provided key
            if (strstr($content, $key)){
                // none empty line
                $written = 1;
                file_put_contents($path, "$key=$value" . PHP_EOL, FILE_APPEND);
            } else {
                file_put_contents($path, trim($content) . PHP_EOL, FILE_APPEND);
            }
        }
        if (!$written) {
            file_put_contents($path, "$key=$value" . PHP_EOL, FILE_APPEND);
        }
        return $value;
    }
}

if(!function_exists('fileUpload')){
    function fileUpload($path, $field, $hasfile, $defaultvalue = '', $type = ''){
        switch ($type) {
            case 'S3':
                # code...
                break;
            
            default:
            
                $destinationPath = $path;
                if($hasfile){
                    if($field->isValid()){
                        $filename = $field->getClientOriginalName();
                        $extension = $field->getClientOriginalExtension();
                        $file = $destinationPath.'/'.pathinfo(str_replace(' ', '_', $filename), PATHINFO_FILENAME).'_'.Carbon::now()->format('Ymdhis').'.'.$extension;
                        $field->move($destinationPath, $file);
                    }
                } else {
                    $file = isset($defaultvalue) ? $defaultvalue : '';
                }
                return $file;

                break;
        }
    }
}

if (!function_exists('fileExist')){
    function fileExist($file){
        if(File::exists($file)){
            return 1;
        }
        return '';
    }
}

function display_time($date, $split = false){
    if ($split == true) { /* split/get time only */
        $date_exploded = explode(' ', $date);
        return date('g:i a', strtotime($date_exploded[1]));
    }
    if (strlen($date) == 19 && $date!='0000-00-00 00:00:00') {
        return date('M d, Y', strtotime($date)).' at '.date('g:i a', strtotime($date));
    }
    if (strlen($date) == 10 && $date!='0000-00-00') {
        return date('M d, Y', strtotime($date));
    }
    if (strlen($date) == 8 && $date!='00:00:00') {
        return date('g:i a', strtotime($date));
    }
    return '';
}

function input_time($date, $time){
    if($date!='0000-00-00' && $time!='00:00:00'){
        return date('Y-m-d', strtotime($date)).' '.date('H:i:s', strtotime($time));
    }
    return '';
}