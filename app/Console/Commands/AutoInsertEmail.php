<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Repositories\Dentist\UserRepositoryEloquent;

class AutoInsertEmail extends Command
{
    protected $userRepo;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutoInsertEmail:insertemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert dental clinic user emails to phplist';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryEloquent $userRepo)
    {
        $this->userRepo = $userRepo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "inserted = '0'";
        $users = $this->userRepo->rawLimit($sql,"101");
        foreach ($users as $key => $user) {
            $postdata['email'] = $user->email;
            $postdata['name'] = "My Dental Clinic Users";
            $postdata['fname'] = $user->first_name;
            $postdata['lname'] = $user->last_name;

            $url = "http://hitsclick.com/sqlapi.php?what=save_email"; 
            $options = array( 
                CURLOPT_RETURNTRANSFER      =>    true,                 // return web page 
                CURLOPT_POST                =>    sizeof($postdata),    // This sets the number of fields to post 
                CURLOPT_POSTFIELDS          =>    $postdata,            // This is the fields to post in the form of an array.
                CURLOPT_TIMEOUT             =>    10,
                CURLOPT_SSL_VERIFYPEER      =>    false
            );  

            // Initialize cURL 
            $ch = curl_init($url); 
            // Set the options 
            curl_setopt_array($ch, $options); 
            //execute the post 
            $response  = curl_exec($ch); 
            //close the connection 
            curl_close($ch);

            if($response){
                $this->userRepo->update(['inserted' => '1'], $user->id);
            }
        }
    }
}