<?php

namespace App\Http\Middleware;

use Redirect;
use Closure;
use Illuminate\Http\Request;

class Redirect503
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return Redirect::to('https://v2.dentalclinicapp.com');
    }
}