<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'             =>      'email|unique:users,email',
        ];
    }

    public function messages()
    {
        return [
            'email.email'       =>      'Email you input is not a valid Email Address.',
            'email.unique'      =>      'Email Address has already been taken.'
        ];
    }
}
