<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\DentalphotoRepositoryEloquent;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use File;
use Junity\Hashids\Facades\Hashids;

class DentalphotoController extends Controller
{
    protected $photoRepo;
    protected $patientRepo;
    protected $logsRepo;

    function __construct(DentalphotoRepositoryEloquent $photoRepo, PatientRepositoryEloquent $patientRepo, LogsRepositoryEloquent $logsRepo)
    {
    	$this->photoRepo = $photoRepo;
    	$this->patientRepo = $patientRepo;
        $this->logsRepo = $logsRepo;
    }

    public function store(Request $request)
    {
    	$patient_id = $request['patient_id'];
    	$dentistfolder = 'uploads/photos/'.\Auth::user()->clinic_id;
    	$patientfolder = 'uploads/photos/'.\Auth::user()->clinic_id.'/'.$patient_id;
    	if(!File::exists($dentistfolder)){
    		File::makeDirectory($dentistfolder,0777,true);
    	}
    	if(!File::exists($patientfolder)){
    		File::makeDirectory($patientfolder,0777,true);
    	}
    	$destinationPath = 'uploads/photos/'.\Auth::user()->clinic_id.'/'.$patient_id;
        $file = fileUpload($destinationPath, $request->file('photo'), $request->hasFile('photo'));
        $makeRequest = [
        	'patient_id' => $patient_id,
        	'caption' => $request['caption'],
        	'photo' => $file,
        	'clinic_user_id' => \Auth::user()->clinic_id,
        	'clinic_id' => \Auth::user()->clinic_id
        ];
        
        $id = $this->photoRepo->create($makeRequest);
        $this->logsRepo->insertLogs('images','insert',$patient_id,$id);

        return redirect()->route('patient.show', Hashids::encode($patient_id))->with('status','Photo Uploaded Successfull!');
    }

    public function edit($id)
    {

    	$id = Hashids::decode($id);
    	$dentalphoto = $this->photoRepo->find($id[0]);
    	return $dentalphoto;
    }

    public function update(Request $request, $id)
    {
    	$patient_id = $request['patient_id'];
    	$dentistfolder = 'uploads/photos/'.\Auth::user()->clinic_id;
    	$patientfolder = 'uploads/photos/'.\Auth::user()->clinic_id.'/'.$patient_id;
    	if(!File::exists($dentistfolder)){
    		File::makeDirectory($dentistfolder,0777,true);
    	}
    	if(!File::exists($patientfolder)){
    		File::makeDirectory($patientfolder,0777,true);
    	}
    	$destinationPath = 'uploads/photos/'.\Auth::user()->clinic_id.'/'.$patient_id;
    	$defaultphoto = ($request['photoavatar'] != '') ? $request['photoavatar'] : '';

	    $file = fileUpload($destinationPath, $request->file('photo'), $request->hasFile('photo'), $defaultphoto);
        $dentalphoto = $this->photoRepo->find($id);
        if($request['photoavatar'] == ''){
            File::delete($dentalphoto->photo);
        }
        $makeRequest = [
        	'patient_id' => $patient_id,
        	'caption' => $request['caption'],
        	'photo' => $file
        ];
        $this->photoRepo->update($makeRequest, $id);
        $this->logsRepo->insertLogs('images','update',$patient_id,$dentalphoto->id);

        return redirect()->route('patient.show', Hashids::encode($dentalphoto->patient_id))->with('status','Photo Updated Successfull!');
    }

    public function destroy($id)
    {
    	$id = Hashids::decode($id);
    	$dentalphoto = $this->photoRepo->find($id[0]);
    	File::delete($dentalphoto->photo);
        $this->logsRepo->insertLogs('images','delete',$dentalphoto->patient_id,$dentalphoto->id);
    	$this->photoRepo->delete($id[0]);
    }
}
