<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\DentalnoteRepositoryEloquent;
use App\Repositories\Patient\PatientRepositoryEloquent;
use Junity\Hashids\Facades\Hashids;

class DentalChartController extends Controller
{
    protected $noteRepo;
    protected $patientRepo;

    function __construct(DentalnoteRepositoryEloquent $noteRepo, PatientRepositoryEloquent $patientRepo)
    {
    	$this->noteRepo = $noteRepo;
    	$this->patientRepo = $patientRepo;
    }

    public function show($id)
    {
    	$id = Hashids::decode($id);
        $patient = $this->patientRepo->find($id[0]);
        $sql = "patient_id = '".$id[0]."' and chart_id != ''";
        $dentalcharts = $this->noteRepo->rawAll($sql);
    	return view('patients.dentalchart', compact('patient','dentalcharts'));
    }

    public function editnote($id,$patient_id, $chart_id)
    {
    	$sql = "chart_id = '".$id."' and patient_id = '".$patient_id."' and chart_id!=''";
    	$dentalchart = $this->noteRepo->rawAll($sql,'id','desc');
    	if(count($dentalchart)){
            return $dentalchart;
        }
        return ['id'=>0];
    }
}
