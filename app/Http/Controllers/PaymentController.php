<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\PaymentRepositoryEloquent;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Patient\DentalnoteRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use App\Repositories\Dentist\PermissionRepositoryEloquent as Permission;
use Junity\Hashids\Facades\Hashids;
use Carbon\Carbon;

use App\Models\Personal;
use App\User;

class PaymentController extends Controller
{
    protected $paymentRepo;
    protected $patientRepo;
    protected $noteRepo;
    protected $logsRepo;
    protected $menu_id = 3;

    function __construct(PaymentRepositoryEloquent $paymentRepo, PatientRepositoryEloquent $patientRepo, DentalnoteRepositoryEloquent $noteRepo, LogsRepositoryEloquent $logsRepo, Permission $permissionRepo)
    {
    	$this->paymentRepo = $paymentRepo;
    	$this->patientRepo = $patientRepo;
    	$this->noteRepo = $noteRepo;
        $this->logsRepo = $logsRepo;
        $this->permissionRepo = $permissionRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function create($id = '')
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

    	if($id){
	    	$id = Hashids::decode($id);
	    	$patient = $this->patientRepo->find($id[0]);
	    	return view('payments.create', compact('patient'));
	    }
	    return view('payments.create');
    }

    public function store(Request $request)
    {
    	$dental_id = $request['dental_note_id'];
    	$makeRequest = [
    		'payment_date' => Carbon::parse($request['payment_date'])->format('Y-m-d'),
    		'amount' => $request['amount'],
    		'remarks' => $request['remarks'],
    		'payment_type' => $request['payment_type'],
    		'patient_id' => $request['patient_id'],
    		'dental_id' => $dental_id,
    		'clinic_user_id' => \Auth::user()->clinic_id
    	];

    	$id = $this->paymentRepo->create($makeRequest);
        $this->logsRepo->insertLogs('payments','insert',$request['patient_id'],$id);

    	return redirect()->route('patient.show', Hashids::encode($request['patient_id']))->with('status','Payment Added Successfully!');
    }

    public function createchartpayment($id, $chart_id)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

    	$id = Hashds::decode($id);
    	$chart_id = Hashids::decode($chart_id);
    	$patient = $this->patientRepo->find($id[0]);
    	$dentalnote = $this->noteRepo->find($chart_id[0]);
    	return view('payments.create', compact('patient','dentalnote'));
    }

    public function edit($id)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

    	$id = Hashids::decode($id);
    	$payment = $this->paymentRepo->find($id[0]);
    	$patient = $this->patientRepo->find($payment->patient_id);
    	if($payment->dental_note_id){
	    	$dentalnote = $this->noteRepo->find($payment->dental_note_id);
	    } else {
	    	$dentalnote = [];
	    }

        if ($payment->payment_type == 'Cash'){
            $payment_type = (object) array('cash' => true, 'installment' => false);
        } else {
            $payment_type = (object) array('cash' => false, 'installment' => true);
        }
    	return view('payments.edit', compact('patient','dentalnote','payment', 'payment_type'));
    }

    public function update(Request $request, $id)
    {
    	$id = Hashids::decode($id);
    	$dental_id = $request['dental_note_id'];
        $payment = $this->paymentRepo->find($id[0]);
    	$makeRequest = [
    		'payment_date' => Carbon::parse($request['payment_date'])->format('Y-m-d'),
    		'amount' => $request['amount'],
    		'remarks' => $request['remarks'],
    		'payment_type' => $request['payment_type'],
    		'patient_id' => $request['patient_id'],
    		'dental_id' => $dental_id,
    		'dental_note_id' => $request['dental_note_id']
    	];
    	$this->paymentRepo->update($makeRequest, $id[0]);
        $this->logsRepo->insertLogs('payments','update',$payment->patient_id,$payment->id);
        return redirect()->route('patient.show', Hashids::encode($request['patient_id']))->with('status','Payment Updated Successfully!');
    }

    public function showRevenues() 
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $user = new User;
        $revenues = Personal::join('dental_payments as dp', 'personals.id', '=', 'dp.patient_id')
        ->where('dp.clinic_user_id', \Auth::user()->clinic_id)
        ->where('dp.payment_date', '=', Carbon::now()->format('Y-m-d'))
        ->selectRaw('dp.id, dp.patient_id, sum(CASE WHEN dp.payment_type="Cash" THEN dp.amount ELSE 0 END) as cash_revenue, sum(CASE WHEN dp.payment_type="Installment" THEN dp.amount ELSE 0 END) as installment_revenue, personals.first_name, personals.middle_name, personals.last_name, personals.image')
        ->groupBy('dp.patient_id')
        ->paginate(10);
        // return $revenues;
        $date_start = Carbon::now()->format('Y-m-d');
        $date_end = Carbon::now()->format('Y-m-d');
        $total_revenues = 0;
        foreach ($revenues as $key => $value) {
            $revenue = $value->cash_revenue + $value->installment_revenue;
            $total_revenues = $total_revenues + $revenue;
        }
        return view('payments.revenues', compact('revenues','total_revenues','date_start','date_end', 'user'));
    }


    public function filterRevenues(Request $request) 
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();
        
        $user = new User;
        $query = Personal::join('dental_payments as dp', 'personals.id', '=', 'dp.patient_id')
        ->where('dp.clinic_user_id', \Auth::user()->clinic_id)
        ->selectRaw('dp.id, dp.patient_id, sum(CASE WHEN dp.payment_type="Cash" THEN dp.amount ELSE 0 END) as cash_revenue, sum(CASE WHEN dp.payment_type="Installment" THEN dp.amount ELSE 0 END) as installment_revenue, personals.first_name, personals.middle_name, personals.last_name, personals.image')
        ->groupBy('dp.patient_id');
        // ->paginate(10);
        if ($request->date_filter == 'all'){
            $revenues = $query->paginate(10);
        } else if ($request->date_filter == 'today') {
            $revenues = $query->where('dp.payment_date', '=', Carbon::now()->format('Y-m-d'))
            ->paginate(10);
        } else {
            $revenues =  $query->whereBetween('dp.payment_date', [$request->date_start, $request->date_end])->paginate(10);
        }
        $date_start = $request->date_start;
        $date_end = $request->date_end;

        $total_revenues = 0;
        foreach ($revenues as $key => $value) {
            $revenue = $value->cash_revenue + $value->installment_revenue;
            $total_revenues = $total_revenues + $revenue;
        }

        return view('payments.revenues', compact('revenues','total_revenues','date_start','date_end', 'user'));
    }

    public function destroy($id)
    {
        $payment = $this->paymentRepo->find($id);
        $this->logsRepo->insertLogs('payments','delete',$payment->patient_id,$payment->id);
        $this->paymentRepo->delete($payment->id);
    }
}
