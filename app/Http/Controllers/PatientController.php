<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Patient\DentalnoteRepositoryEloquent;
use App\Repositories\Patient\DentalphotoRepositoryEloquent;
use App\Repositories\Patient\AppointmentRepositoryEloquent;
use App\Repositories\Patient\PaymentRepositoryEloquent;
use App\Repositories\Patient\HistoryRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use App\Repositories\Dentist\PermissionRepositoryEloquent as Permission;
use Junity\Hashids\Facades\Hashids;
use DB;
use PDF;
use Cookie;
use Auth;
use File;
use Carbon\Carbon;

use App\Models\Personal;
use App\Models\InstallmentBalance;
use App\User;

class PatientController extends Controller
{
    protected $patientRepo;
    protected $noteRepo;
    protected $photoRepo;
    protected $appointRepo;
    protected $paymentRepo;
    protected $historyRepo;
    protected $logsRepo;
    protected $hashids;
    protected $menu_id = 2;

    function __construct(Hashids $hashids, PatientRepositoryEloquent $patientRepo, DentalnoteRepositoryEloquent $noteRepo, AppointmentRepositoryEloquent $appointRepo, PaymentRepositoryEloquent $paymentRepo, DentalphotoRepositoryEloquent $photoRepo, HistoryRepositoryEloquent $historyRepo, LogsRepositoryEloquent $logsRepo, Permission $permissionRepo)
    {
    	$this->hashids = $hashids;
    	$this->patientRepo = $patientRepo;
    	$this->noteRepo = $noteRepo;
    	$this->appointRepo = $appointRepo;
    	$this->paymentRepo = $paymentRepo;
    	$this->photoRepo = $photoRepo;
    	$this->historyRepo = $historyRepo;
        $this->logsRepo = $logsRepo;
        $this->permissionRepo = $permissionRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function index()
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

    	$sqlpatient = "clinic_user_id = '".Auth::user()->clinic_id."'";
    	$patients = $this->patientRepo->rawAll($sqlpatient,'updated_at','desc');
        $countpatients = $this->patientRepo->rawCount($sqlpatient);
        return view('patients.index', compact('patients','countpatients'));
    }

    public function create()
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();
        
        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

    	return view('patients.create');
    }

    public function store(Request $request)
    {
    	$destinationPath = 'uploads/patients';
        $file = fileUpload($destinationPath, $request->file('image'), $request->hasFile('image'));

    	$makeRequest = [
    		'clinic_id' => Auth::user()->clinic_id,
	        'clinic_user_id' => Auth::user()->clinic_id,
	        'first_name' => $request['first_name'],
	        'middle_name' => $request['middle_name'],
	        'last_name' => $request['last_name'],
	        'gender' => $request['gender'],
	        'birthdate' => $request['birthdate'],
	        'email' => $request['email'],
	        'contact_number' => $request['contact_number'],
	        'address' => $request['address'],
	        'image' => $file,
	        'notes' => $request['notes']
    	];
    	$patient_id = $this->patientRepo->create($makeRequest);

        $birthdate = Carbon::parse($request['birthdate'])->format('Y-m-d');
    	$age = Carbon::parse($birthdate)->age;
        if ($age < 6){
            $pediatric = json_decode($request->cookie('pediatric_chart'));
            if (count($pediatric) > 0){
                array_push($pediatric, (string) $patient_id);
                Cookie::queue('pediatric_chart', json_encode($pediatric), 100000, null, null, false, false);
            } else  {
                Cookie::queue('pediatric_chart', json_encode(array( (string) $patient_id)), 100000, null, null, false, false);
            }
        }
        $this->logsRepo->insertLogs('patients','insert',$patient_id, $patient_id);

        return redirect()->route('patient.index');
    }

    public function sortorder(Request $request)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $sortby = $request['sortby'];
        $sqlpatient = "clinic_user_id = '".Auth::user()->clinic_id."'";
        $patients = Personal::sortfilter($request->all())->whereRaw($sqlpatient)->get();
        $countpatients = $this->patientRepo->rawCount($sqlpatient);
        return view('patients.index', compact('patients','patient_name','sortby','countpatients'));
    }

    public function show($id)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

    	$id = Hashids::decode($id);
    	$sql = "patient_id = '".$id[0]."'";
        $sqlchart = "patient_id = '".$id[0]."' and chart_id!=''";
    	$patient = $this->patientRepo->findWith($id[0], ['dentalphoto']);
    	$dentalnotes = $this->noteRepo->rawAll($sql,'created_at','desc');
    	$dentalcharts = $this->noteRepo->rawAll($sqlchart,'created_at','desc');
    	$patientappointments = $this->appointRepo->rawAll($sql,'created_at','desc');
    	$dentalphotos = $this->photoRepo->rawAll($sql,'created_at','desc');
    	$patienthistory = $this->historyRepo->rawAll($sql,'created_at','desc');
    	$paymentresults = $this->paymentRepo->rawAll($sql,'created_at','desc');

    	$user = new User;
    	$installments = [];
        $payments = [];
        foreach ($paymentresults as $key => $result) {
            if ($result->payment_type == 'Installment'){
                $installment_pay = InstallmentBalance::selectRaw('SUM(amount) AS paid, count(*) AS total, max(date) as last_pay')->where('payment_id', $result->id)->first();
                $installments[] = (object) [
                    'id' => $result->id, 
                    'payment_date' => $result->payment_date,
                    'amount' => $result->amount,
                    'paid' => $installment_pay->paid,
                    'last_pay' =>  Carbon::parse($installment_pay->last_pay)->format('M d, Y'),
                    'total' => $installment_pay->total,
                    'remarks' => $result->remarks
                ];
            } else if ($result->payment_type == 'Cash') {
                $payments[] = $result;
            }
        }
        return view('patients.show', compact('patient','dentalnotes','patientappointments','patient_history','dentalcharts','dentalphotos','payments','installments','user'));
    }

    public function edit($id)
    {
        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

    	$id = Hashids::decode($id);
        $patient = $this->patientRepo->find($id[0]);
        return view('patients.edit', compact('patient'));
    }

    public function update(Request $request, $id)
    {
    	$id = Hashids::decode($id);
        $destinationPath = 'uploads/patients';
        $file = ($request->hasFile('image')) ? fileUpload($destinationPath, $request->file('image'), $request->hasFile('image')) : $request['patient_image'];
            
        $patient_name = $request['first_name'].' '.$request['last_name'];
        $patients = $this->patientRepo->find($id[0]);
        if($request['patient_image'] == ''){
            \File::delete('uploads/patients/'.$patients->image);
        }

        $makeRequest = [
    		'clinic_id' => Auth::user()->clinic_id,
	        'clinic_user_id' => Auth::user()->clinic_id,
	        'first_name' => $request['first_name'],
	        'middle_name' => $request['middle_name'],
	        'last_name' => $request['last_name'],
	        'gender' => $request['gender'],
	        'birthdate' => $request['birthdate'],
	        'email' => $request['email'],
	        'contact_number' => $request['contact_number'],
	        'address' => $request['address'],
	        'image' => $file,
	        'notes' => $request['notes']
    	];

    	$this->patientRepo->update($makeRequest, $id[0]);

        $this->logsRepo->insertLogs('patients','update',$id[0], $id[0]);

        DB::update("update patient_appointments set name='".$patient_name."' where patient_id='".$patients->id."'");

        return redirect()->route('patient.show', Hashids::encode($id))->with('status','Update Successfull!');
    }

    public function medicalHistory(Request $request)
    {
        $id = Hashids::decode($request['patient_id']);
        $makeRequest = [
            'medical_history' => $request['medical_history'],
        ];
        $this->patientRepo->update($makeRequest,$id[0]);
        $this->logsRepo->insertLogs('medical_history','update',$id[0], $id[0]);
        return 1;
    }

    public function search(Request $request)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $patient_name = $request['patient_name'];
        if($patient_name){
            $sql = "clinic_user_id = '".\Auth::user()->clinic_id."' and (first_name like '%".$patient_name."%' or last_name like '%".$patient_name."%')";
            $patients = $this->patientRepo->rawAll($sql);
        } else {
            // $sql = "clinic_user_id = ".\Auth::user()->clinic_id;
            // $patients = $this->patientRepo->rawAll($sql);
            $sql = 0;
            $patients = [];
        }
        $countpatients = $this->patientRepo->rawCount($sql);
        return view('patients.index', compact('patients','patient_name', 'countpatients'));
    }

    public function generatePDF($id)
    {
        $id = Hashids::decode($id);
        $patient = $this->patientRepo->find($id[0]);
        $sql = "patient_id = '".$id[0]."'";
        $dentalnotes = $this->noteRepo->rawAll($sql,'created_at','desc');
        $pdf = PDF::loadView('pdf.notes', compact('patient', 'dentalnotes'));
        // $pdf = PDF::loadHTML($data);
        // return $pdf->stream();
        return $pdf->download('Notes-'.$patient->first_name.'-'.$patient->last_name.'.pdf');
    }

    public function destroy($id)
    {
        $id = Hashids::decode($id);
        $personal = $this->patientRepo->find($id[0]);
        File::cleanDirectory('uploads/photos/'.Auth::user()->clinic_id.'/'.$id[0]);
        File::delete('uploads/patients/'.$personal->image);
        $this->logsRepo->insertLogs('patients','delete',$id[0], $id[0]);
        $this->patientRepo->delete($id[0]);
        return 1;
    }
}
