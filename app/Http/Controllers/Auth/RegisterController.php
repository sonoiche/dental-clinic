<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Clinic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\RegisterRequest;

use App\Models\Country;
use App\Models\Role;
use Mail;
use Carbon\Carbon;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Country $country)
    {
        $this->middleware('guest');
        $this->country = $country;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function register()
    {
        $country = $this->country->orderBy('name')->get();
        return view('auth.register', compact('country'));
    }

    public function store(RegisterRequest $request)
    {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $user = User::create([
            'country_id' => $request->input('country_id'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'source' => $request->input('source'),
            'email' => $request->input('email'),
            'contact_number' => $request->input('contact_number'),
            'password' => bcrypt($request->input('password')),
            'apikey' => $request->input('apikey'),
            'ipaddress' => $ipaddress,
            'last_login' => Carbon::now(),
            'is_active' => 1
        ]);

        $details = 'Hi '.$request->input('first_name').' '.$request->input('last_name').', <br/><br/>';
        $details .= 'Welcome to My Dental Clinic App - a mobile application designed for dentists to help them handle their clinic operations.<br/><br/>'.
                    'Your login details are as follows: <br><br><b>Username</b>: '.$request->input('email').'<br><b>Password</b>: '.$request->input('password').'<br/><br/>
                    You can log in to the web version by clicking this link <a href="http://dentalclinicapp.com/login">http://dentalclinicapp.com/login</a><br/><br/>'.
                    '<p style="margin-bottom:5px;">With My Dental Clinic App, you can: </p>'.
                    '  <p style="margin-bottom:5px;">•     Manage and track appointments - daily, weekly or monthly</p>'.
                    '  <p style="margin-bottom:5px;">•     View patient information on the go</p>'.
                    '  <p style="margin-bottom:5px;">•     Notify patient of their schedule by call or SMS</p>'.
                    '  <p style="margin-bottom:5px;">•     Access a dental chart that is simple to use</p>'.
                    '  <p style="margin-bottom:5px;">•     Attach dental images to patient\'s records</p>'.
                    '  <p style="margin-bottom:5px;">•     Monitor patient\'s allergies to avoid complications</p>'.
                    '  <p style="margin-bottom:5px;">•     Track payments to know your income status</p>'.
                    '  <p style="margin-bottom:5px;">•     *Sync data from your mobile to a registered website account</p>'.
                    '<br><br><br>Regards,<br>My Dental Clinic App';

        $sender = 'no-reply@quantumx.com';
        $sendername = 'My Dental Clinic - App';       
        $receiver = $request->input('email');        
        $subject = 'Welcome to My Dental Clinic App';       
        $data = ['body'=>$details];      
        
        $send_email = Mail::send('emails.content', $data, function($message) use ($sender, $sendername, $receiver, $subject) {
            $message->from($sender, $sendername);
            $message->to($receiver);
            $message->subject($subject);
        });

        $country = $this->country->where('id', $request->input('country_id'))->first();
        if(count($country)){
            $countryname = $country->name;
        } else {
            $countryname = '';
        }
        $details2 = $request->input('first_name').' '.$request->input('last_name').' has been registered to DentalClinicApp.<br/>'.
                    'Clinic Name: '.$request->input('clinic_name').'<br/>'.
                    'Name: '.$request->input('first_name').' '.$request->input('last_name').'<br/>'.
                    'Email: '.$request->input('email').'<br/>'.
                    'Contact Number: '.$request->input('contact_number').'<br/>'.
                    'Country: '.$countryname.'<br/>'.
                    'How did you hear about Dental Clinic App: '.$request->input('source').'<br/>'.
                    'Source: '.$request->input('source');
        $sender2 = 'no-reply@quantumx.com';
        $sendername2 = 'My Dental Clinic - Web';       
        $receiver2 = 'email@dentalclinicapp.com';        
        $subject2 = 'My Dental Clinic new registered dentist.'; // My Dental Clinic new registered dentist.
        $bcc2 = 'jelson@quantumx.com';
        $data2 = ['body' => $details2];        
        
        $send_email2 = Mail::send('emails.content', $data2, function($message) use ($sender2, $sendername2, $receiver2, $subject2, $bcc2) {
            $message->from($sender2, $sendername2);
            $message->to($receiver2);
            $message->bcc($bcc2);
            $message->subject($subject2);
        });

        // $clinic = Clinic::create([
        //     'country_id' => $request->input('country_id'),
        //     'name' => $request['clinic_name'],
        //     'id' => $user->id
        // ]);

        $role = Role::create(['name' => 'Administrator', 'clinic_id' => $user->id])->id;

        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 1, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 2, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 3, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 4, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 5, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);

        $updateuser = User::find($user->id);
        $updateuser->clinic_id = $user->id;
        $updateuser->access_id = $role;
        $updateuser->role_id = $role;
        $updateuser->clinic_name = $request['clinic_name'];
        $updateuser->save();

        $mailChimpApiKey = getEnv('MAILCHIMP_API');
        $mailChimpListID = getEnv('MAILCHIMP_ID');
        if($mailChimpApiKey && $mailChimpListID){
            if(!empty($request->input('email')) && !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) === false) {
                $memberID = md5(strtolower($request->input('email')));
                $dataCenter = substr($mailChimpApiKey,strpos($mailChimpApiKey,'-')+1);
                $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $mailChimpListID . '/members/' . $memberID; 
                $json = json_encode(array(
                    'email_address' => $request->input('email'),
                    'status'        => 'subscribed',
                    'merge_fields'  => array(
                        'FNAME'     => $request->input('first_name'),
                        'LNAME'     => $request->input('last_name')
                    ),
                    'double_optin ' => false
                ));
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $mailChimpApiKey);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                if ($httpCode == 200) {
                    // $this->userRepo->update(['mailchimp' => '1'],$request->input('id'));
                }
            }          
        }

        $credentials = $request->only('email', 'password');
        if (\Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request);
        }
    }

    protected function handleUserWasAuthenticated(Request $request)
    {
        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, \Auth::user());
        }

        return redirect()->route('dashboard.index');
    }

    public function newuser($token)
    {
        $user = User::where('token', $token)->first();
        if(count($user) == 0){
            return redirect()->route('login');
        }
        return view('auth.newuser', compact('user'));
    }

    public function setpassword(Request $request)
    {
        $id = $request['user_id'];
        $user = User::find($id);
        $user->password = bcrypt($request['password']);
        $user->token = '';
        $user->save();
        \Auth::loginUsingId($user->id);
        return 1;
    }
}
