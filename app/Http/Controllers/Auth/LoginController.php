<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use File;
use App\Repositories\Patient\PatientRepositoryEloquent;

class LoginController extends Controller
{
    protected $redirectTo = '/dashboard';
    protected $redirectPath = '/dashboard';
    protected $patientRepo;

    public function __construct(Country $country, PatientRepositoryEloquent $patientRepo)
    {
        $this->middleware('guest')->except('logout');
        $this->country = $country;
        $this->patientRepo = $patientRepo;
    }

    public function showLoginForm()
    {
        $country = $this->country->orderBy('name')->get();
        return view('auth.login', compact('country'));
    }

    public function login(Request $request)
    {
        // $this->validateLogin($request);
        if($request['email'] == '' && $request['password'] == ''){
            return redirect()->back()->with('error','1');
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    protected function credentials(Request $request)
    {
        // return $request->only($this->username(), 'password');
        return array_merge($request->only($this->username(), 'password'), ['is_active' => 1]);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath);
    }

    protected function authenticated(Request $request, $user)
    {
        $users = Auth::user();
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ],
        ];  

        $ip = file_get_contents('https://api.ipify.org/?format=string', false, stream_context_create($arrContextOptions));
        $timenow = Carbon::now();
        $makeRequest = ['ip_address' => $ip, 'last_login' => $timenow];
        $user->update($makeRequest);

        $loginlog_details = [
            'user_id' => $users->id,
            'name' => $users->first_name.' '.$users->last_name,
            'username' => $users->email,
            'ipaddress' => $_SERVER['REMOTE_ADDR'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
        DB::table('login_logs')->insert($loginlog_details);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function username()
    {
        return 'email';
    }

    public function logout()
    {
        $this->guard()->logout();
        // $request->session()->flush();
        // $request->session()->regenerate();
        return redirect('/login');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), $this->maxAttempts(), $this->decayMinutes()
        );
    }

    protected function limiter()
    {
        return app(RateLimiter::class);
    }

    protected function throttleKey(Request $request)
    {
        return Str::lower($request->input($this->username())).'|'.$request->ip();
    }

    public function maxAttempts()
    {
        return property_exists($this, 'maxAttempts') ? $this->maxAttempts : 5;
    }

    public function decayMinutes()
    {
        return property_exists($this, 'decayMinutes') ? $this->decayMinutes : 1;
    }

    protected function clearLoginAttempts(Request $request)
    {
        $this->limiter()->clear($this->throttleKey($request));
    }

    protected function fireLockoutEvent(Request $request)
    {
        event(new Lockout($request));
    }

    protected function incrementLoginAttempts(Request $request)
    {
        $this->limiter()->hit($this->throttleKey($request));
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        $message = Lang::get('auth.throttle', ['seconds' => $seconds]);

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([$this->username() => $message]);
    }

    public function check(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        if(count($user) == 0){
            return 3;
        } else {
            // if(isset($user->subscription_end) && $user->subscription_end < Carbon::now()->format('Y-m-d')){
            //     return 4;
            // }
            // if($user->apikey != '' && isset($user->subscription_end) && $user->subscription_end!='0000-00-00'){
                $hashedPassword = $user->password;
                if (Hash::check($request['password'], $hashedPassword)){
                    if($user->source == 'App'){
                        return 4;
                    }
                    return 1;
                }
            // }
            // return 5;
        }
        return 2;
    }

    public function regcheck(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        if (count($user) == 0){
            return 1;
        }
        return 2;
    }

    public function imagePath()
    {
        // $patients = $this->patientRepo->getWhereNotIn('image', ['']);
        // $patients = DB::table('patients')->where('image', '!=', '')->get();
        // foreach ($patients as $key => $patient) {
        //     $destinationPath = 'uploads/uploads/'.$patient->clinic_id.'/photos/'.$patient->id;
        //     if(!File::exists($destinationPath)){
        //         File::makeDirectory($destinationPath,0777,true);
        //     }
        //     $filenames = explode('/', $patient->image);
        //     $filename = end($filenames);
        //     $file = $destinationPath.'/'.$filename;
        //     if(File::exists($patient->image)){
        //         File::copy($patient->image, $file);
        //     }else{
        //         $name = 'uploads/patients/'.$patient->image;
        //         if(File::exists($name)){
        //             File::copy($name, $file);
        //         }
        //     }
        // }

        // $users = DB::table('clinics_old')->where('profile_picture', '!=', '')->get();
        // foreach ($users as $key => $user) {
        //     $destinationPath = 'uploads/uploads/'.$user->m_clinic_id.'/profile_picture';
        //     if(!File::exists($destinationPath)){
        //         File::makeDirectory($destinationPath,0777,true);
        //     }
        //     $filenames = explode('/', $user->profile_picture);
        //     $filename = end($filenames);
        //     $file = $destinationPath.'/'.$filename;
        //     if(File::exists($user->profile_picture)){
        //         File::copy($user->profile_picture, $file);
        //     }else{
        //         $name = 'uploads/users/'.$user->profile_picture;
        //         if(File::exists($name)){
        //             File::copy($name, $file);
        //         }
        //     }
        // }

        $photos = DB::table('patients_dental_photos')->where('image', '!=', '')->get();
        foreach ($photos as $key => $photo) {
            $destinationPath = 'uploads/uploads/'.$photo->clinic_id.'/photos/'.$photo->patient_id;
            if(!File::exists($destinationPath)){
                File::makeDirectory($destinationPath,0777,true);
            }
            $filenames = explode('/', $photo->image);
            $filename = end($filenames);
            $file = $destinationPath.'/'.$filename;
            if(File::exists($photo->image)){
                File::copy($photo->image, $file);
            }else{
                $name = 'uploads/photos/'.$photo->m_clinic_id.'/'.$photo->m_patient_id.'/'.$photo->image;
                if(File::exists($name)){
                    File::copy($name, $file);
                }
            }
        }
        return 'success';
    }
}
