<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('auth.passwords.email');
    }

    public function store(Request $request)
    {
        $email = $request['email'];

        $chkuser = User::where('email', $email)->where('subscription_end','!=','0000-00-00')->first();
        if(count($chkuser)){

            $token = str_random(20);

            DB::table('password_resets')->insert(['email' => $email, 'token' => $token, 'created_at' => Carbon::now()]);

            $details  = '<html><head><title>My Dental Clinic App</title>';
            $details .= '</head><body>';
            $details .= 'Hi '.$chkuser->first_name.', <br><br>';
            $details .= 'We received a request to reset password for your My Dental Clinic Web Application account.<br>';
            $details .= 'To reset your password you may click this link <a href="'.url('passwords', $token).'" class="btn btn-primary btn-md">Reset Password</a><br><br>';
            $details .= 'Ignore this email if you don\'t want to change password.<br>';
            $details .= 'If you did not make this request, review  your account settings.<a href="'.url('login').'">Login Here</a>';
            $details .= '<br><br>';
            $details .= 'Regards, <br>My Dental Clinic App <br><br>';
            $details .= '</body></html>';

            $sender = 'info@dentalclinicapp.com';
            $sendername = 'My Dental Clinic';       
            $receiver = $email;        
            $subject = 'My Dental Clinic App Reset Password';       
            $data = ['body' => $details];      
            
            $send_email = Mail::send('emails.content', $data, function($message) use ($sender, $sendername, $receiver, $subject) {
                $message->from($sender, $sendername);
                $message->replyTo($sender, $sendername);
                $message->to($receiver);
                $message->subject($subject);
                return 1;
            });

            return 1;

        } else {
            return 2;
        }
    }

    public function show($id)
    {
        $token = $id;
        $user = DB::table('password_resets')->where('token',$id)->first();
        return view('auth.passwords.reset', compact('token','user'));
    }

    public function reset(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        $user->update(['password' => bcrypt($request['password'])]);

        $token = DB::table('password_resets')->where('email', $request['email'])->delete();

        Auth::loginUsingId($user->id, true);
        return redirect()->route('dashboard.index');
    }
}
