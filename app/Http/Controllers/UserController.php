<?php

namespace App\Http\Controllers;

use App\Repositories\Dentist\CountryRepositoryEloquent;
use App\Repositories\Dentist\PermissionRepositoryEloquent as Permission;
use App\Repositories\Dentist\RoleRepositoryEloquent;
use App\Repositories\Dentist\UserRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Junity\Hashids\Facades\Hashids;
use Mail;

class UserController extends Controller
{
    protected $userRepo;
    protected $countryRepo;
    protected $roleRepo;
    protected $menu_id = 5;

    function __construct(UserRepositoryEloquent $userRepo, CountryRepositoryEloquent $countryRepo, RoleRepositoryEloquent $roleRepo, Permission $permissionRepo)
    {
    	$this->userRepo = $userRepo;
    	$this->countryRepo = $countryRepo;
        $this->roleRepo = $roleRepo;
        $this->permissionRepo = $permissionRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function index()
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $sql = "clinic_id = '".\Auth::user()->clinic_id."' and is_active = '1'";
        $users = $this->userRepo->rawWith(['access'],$sql,'first_name');
        return view('user.index', compact('users'));
    }

    public function edit(Request $request, $id)
    {
    	$id = Hashids::decode($id);
    	$show_code = $request->get('show_code');
    	$user = $this->userRepo->findWith($id[0],['access.permissions']);
    	$countryList = $this->countryRepo->selectlist('name','id');
    	return view('user.edit', compact('user','countryList','show_code'));
    }

    public function store(Request $request)
    {
    	$pw = $request['oldpassword'];
    	$id = \Auth::user()->id;
    	$user = $this->userRepo->find($id);
    	if(\Hash::check($pw, $user->password)){
    		$this->userRepo->update(['password' => bcrypt($request['password'])], $id);
    		return 1;
    	}
    	return 2;
    }

    public function update(Request $request, $id)
    {
        $id = Hashids::decode($id);
        $destinationPath = 'uploads/users/';
        $user = $this->userRepo->find($id[0]);
        $file = fileUpload($destinationPath, $request->file('image'), $request->hasFile('image'),$user->image);
        if($request->file('image')){
            \File::delete('uploads/users/'.$user->image);
        }
        $makeRequest = [
            'clinic_name' => $request['clinic_name'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'contact_number' => $request['contact_number'],
            'country_id' => $request['country_id'],
            'image' => $file
        ];

        $this->userRepo->update($makeRequest, $id[0]);

        return redirect()->route('dashboard.index');
    }

    public function create()
    {
        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

        $sql = "clinic_id = '".\Auth::user()->clinic_id."'";
        $roles = $this->roleRepo->rawAll($sql, 'name');
        $roleList = $this->roleRepo->rawAll($sql, 'name');
        return view('user.create', compact('roles','roleList'));
    }

    public function storeuser(Request $request)
    {
        $token = str_random(10);
        $makeRequest = [
            'access_id' => $request['access_id'],
            'role_id' => $request['access_id'],
            'contact_number' => $request['contact_number'],
            'email' => $request['email'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'is_active' => 1,
            'clinic_id' => \Auth::user()->clinic_id,
            'token' => $token
        ];

        if($request['user_id']){
            $sql = "first_name = '".$request['first_name']."' and last_name = '".$request['last_name']."' and email = '".$request['email']."' and id != '".$request['user_id']."'";
            $sqlemail = "email = '".$request['email']."' and id != '".$request['user_id']."'";
            if($this->userRepo->rawCount($sql) <> 0){
                return 2;
            }

            if($this->userRepo->rawCount($sqlemail) <> 0){
                return 3;
            }
            $this->userRepo->update($makeRequest, $request['user_id']);
            return 1;
        }

        $sql = "first_name = '".$request['first_name']."' and last_name = '".$request['last_name']."' and email = '".$request['email']."'";
        $sqlemail = "email = '".$request['email']."'";
        if($this->userRepo->rawCount($sql) <> 0){
            return 2;
        }

        if($this->userRepo->rawCount($sqlemail) <> 0){
            return 3;
        }

        if($this->userRepo->create($makeRequest)){
            $message = '
            You have been added by '.\Auth::user()->first_name.' '.\Auth::user()->last_name.' as a user of their My Dental Clinic account. <br><br>
            <a href="'.url('/register',$token).'" target="_blank">Click here to set your password</a>
            ';

            $details = $message;
            $sender = \Auth::user()->email;
            $sendername = \Auth::user()->first_name.' '.\Auth::user()->last_name;     
            $receiver = $request['email'];        
            $subject = 'Access My Dental Clinic App';       
            $data = ['body'=>$details];        
            
            $send_email = Mail::send('emails.content', $data, function($message) use ($sender, $sendername, $receiver, $subject) {
                $message->from($sender, $sendername);
                $message->to($receiver);
                $message->subject($subject);
            });
            return 1;
        }
    }

    public function storerole(Request $request)
    {
        $makeRequest = ['name' => $request['name'], 'clinic_id' => \Auth::user()->clinic_id];
        if($request['id']){
            $sql = "name = '".$request['name']."' and id != '".$request['id']."' and clinic_id = '".\Auth::user()->clinic_id."'";
            if($this->roleRepo->rawCount($sql) <> 0){
                return 2;
            }
            $this->roleRepo->update($makeRequest, $request['id']);
            return [
                'id' => $request['id'],
                'name' => $request['name'],
                'result' => 'update'
            ];
        }

        $sql = "name = '".$request['name']."' and clinic_id = '".\Auth::user()->clinic_id."'";
        if($this->roleRepo->rawCount($sql) <> 0){
            return 2;
        }
        $id = $this->roleRepo->create($makeRequest);
        $count = $this->roleRepo->all();
        return [
            'id' => $id,
            'name' => $request['name'],
            'count' => count($count),
            'result' => 'add'
        ];
    }

    public function roleedit($id)
    {
        $result = $this->roleRepo->find($id);
        return [
            'id' => $result->id,
            'name' => $result->name
        ];
    }

    public function roledelete($id)
    {
        $sql = "access_id = '".$id."'";
        $chk = $this->userRepo->rawCount($sql);
        if($chk == 0){
            $this->roleRepo->delete($id);
            return 1;
        }
        return 2;
    }

    public function edituser($id)
    {
        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();
        
        $sql = "clinic_id = '".\Auth::user()->clinic_id."'";
        $roles = $this->roleRepo->rawAll($sql, 'name');
        $roleList = $this->roleRepo->rawAll($sql, 'name');
        $user = $this->userRepo->find($id);
        return view('user.create', compact('roles','roleList','user'));
    }

    public function destroy($id)
    {
        $this->userRepo->delete($id);
        return 1;
    }

    public function login($id)
    {
        Auth::loginUsingId($id, true);
        return redirect()->route('dashboard.index');
    }

}
