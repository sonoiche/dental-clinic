<?php

namespace App\Http\Controllers;

use App\Repositories\Dentist\UserRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CronController extends Controller
{
	protected $userRepo;

	function __construct(UserRepositoryEloquent $userRepo)
	{
		$this->userRepo = $userRepo;
	}

    public function cronMailchimpSubscribe()
    {
        return 1;
        $mailChimpApiKey = getEnv('MAILCHIMP_API');
        $mailChimpListID = getEnv('MAILCHIMP_ID');
        if($mailChimpApiKey && $mailChimpListID){
            $sql = "mailchimp = '0'";
            $users = $this->userRepo->rawAll($sql);
            foreach ($users as $keyUser => $valueUser){
                if(!empty($valueUser->email) && !filter_var($valueUser->email, FILTER_VALIDATE_EMAIL) === false) {
                    $memberID = md5(strtolower($valueUser->email));
                    $dataCenter = substr($mailChimpApiKey,strpos($mailChimpApiKey,'-')+1);
                    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $mailChimpListID . '/members/' . $memberID; 
                    $json = json_encode(array(
                        'email_address' => $valueUser->email,
                        'status'        => 'subscribed',
                        'merge_fields'  => array(
                            'FNAME'     => $valueUser->first_name,
                            'LNAME'     => $valueUser->last_name
                        ),
                        'double_optin ' => false
                    ));
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $mailChimpApiKey);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                    $result = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    if ($httpCode == 200) {
                        $this->userRepo->update(['mailchimp' => '1'],$valueUser->id);
                    }
                }
            }               
        }
    }

    public function phplist()
    {
        $sql = "inserted = '0'";
        $users = $this->userRepo->rawLimit($sql,"101");
        foreach ($users as $key => $user) {
            $postdata['email'] = $user->email;
            $postdata['name'] = "My Dental Clinic Users"; // List name
            $postdata['fname'] = $user->first_name;
            $postdata['lname'] = $user->last_name;

            $url = "http://hitsclick.com/sqlapi.php?what=save_email"; 
            $options = array( 
                CURLOPT_RETURNTRANSFER      =>    true,                 // return web page 
                CURLOPT_POST                =>    sizeof($postdata),    // This sets the number of fields to post 
                CURLOPT_POSTFIELDS          =>    $postdata,            // This is the fields to post in the form of an array.
                CURLOPT_TIMEOUT             =>    10,
                CURLOPT_SSL_VERIFYPEER      =>    false
            );  

            // Initialize cURL 
            $ch = curl_init($url); 
            // Set the options 
            curl_setopt_array($ch, $options); 
            //execute the post 
            $response  = curl_exec($ch); 
            //close the connection 
            curl_close($ch);

            if($response){
                $this->userRepo->update(['inserted' => '1'], $user->id);
            }
        }
    }
}
