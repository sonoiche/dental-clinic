<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Dentist\DeviceRepositoryEloquent;
use App\Repositories\Dentist\UserRepositoryEloquent;
use App\Repositories\Dentist\PermissionRepositoryEloquent as Permission;
use Junity\Hashids\Facades\Hashids;

use App\Models\ConnectedDevice;
use App\Models\Log;

class DevicesController extends Controller
{
    protected $deviceRepo;
    protected $userRepo;
    protected $menu_id = 4;

    function __construct(DeviceRepositoryEloquent $deviceRepo, UserRepositoryEloquent $userRepo, Permission $permissionRepo)
    {
    	$this->deviceRepo = $deviceRepo;
    	$this->userRepo = $userRepo;
        $this->permissionRepo = $permissionRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function index()
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

    	$sql = "clinic_user_id = '".\Auth::user()->clinic_id."'";
        $devices = $this->deviceRepo->rawWith(['country'], $sql, 'updated_at', 'desc');
        return view('devices.index', compact('devices'));
    }

    public function sortorder(Request $request)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $sortby = $request['sortby'];
        $devices = ConnectedDevice::where('clinic_user_id',\Auth::user()->clinic_id)->sortfilter($request->all())->get();
        return view('devices.index', compact('devices','sortby'));
    }

    public function invite()
    {
        return view('devices.invite');
    }

    public function sendInvites(Request $request){
        // return $request->all();
        $user = $userRepo->rawByField("id = '".\Auth::user()->clinic_id."'");
        $send = array();
        $emails_string = "";
        $emails = explode(",", $request->email);
        foreach ($emails as $key => $email) {
            $send[] = trim($email);
            if ($key+1 >= count($emails)){
                $emails_string.= trim($email);
            } else {
                $emails_string.= trim($email).', ';
            }
            $sqlCount = "clinic_user_id = '".\Auth::user()->clinic_id."' and email = '".$email."'";
            $saved_devices = $this->deviceRepo->rawCount($sqlCount);
            if ($saved_devices < 1){
                $devices = New ConnectedDevice;
                $devices->email = $email;
                $devices->clinic_user_id = \Auth::user()->clinic_id;
                $devices->save();
            }
        }
        $details = 'Code:'.$user->apikey;
        $sender = 'no-reply@quantumx.com';
        $sendername = 'My Dental Clinic';       
        $receiver = $send;        
        $subject = 'Dental Clinic App Code';       
        $data = ['body'=>$details];        
        
        /*$send_email = Mail::send('emails.content', $data, function($message) use ($sender, $sendername, $receiver, $subject) {
            $message->from($sender, $sendername);
            $message->bcc($receiver);
            $message->subject($subject);
        });*/
        

        return redirect()->route('devices.index')->with('message','Invitation has been sent to '.$emails_string);
    }

    public function destroy($id)
    {
    	$sql = "clinic_user_id = '".\Auth::user()->clinic_id."'";
        $device = $this->deviceRepo->rawByField($sql, 'sync_version');

        $id = Hashids::decode($id);
        $this->deviceRepo->delete($id[0]);

        Log::where('clinic_user_id',\Auth::user()->clinic_id)
        ->where('sync_version', '<', $device->sync_version)
        ->delete();
    }
}
