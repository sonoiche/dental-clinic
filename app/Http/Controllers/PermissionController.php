<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Dentist\PermissionRepositoryEloquent;
use App\Repositories\Dentist\MenuRepositoryEloquent as Menu;
use App\Repositories\Dentist\RoleRepositoryEloquent as Role;
use Junity\Hashids\Facades\Hashids;

class PermissionController extends Controller
{
    protected $permissionRepo;
    protected $menuRepo;
    protected $roleRepo;
    protected $menu_id = 5;

    function __construct(PermissionRepositoryEloquent $permissionRepo, Menu $menuRepo, Role $roleRepo)
    {
    	$this->permissionRepo = $permissionRepo;
    	$this->menuRepo = $menuRepo;
    	$this->roleRepo = $roleRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function index()
    {
    	$this->authorize('read', $this->permissions());
    	$permissions = $this->permissions();

        $sql = "clinic_id = '".\Auth::user()->clinic_id."'";
    	$roles = $this->roleRepo->rawAll($sql);
    	$menus = $this->menuRepo->with(['permission']);
    	return view('permission.index', compact('roles','menus'));
    }

    public function store(Request $request)
    {
    	if($request['checked'] == 'true'){
    		$makeRequest = [
    			'role_id' => $request['role_id'],
    			'menu_id' => $request['menu_id'],
    			''.$request['data_type'].'' => '1',
    			'clinic_id' => \Auth::user()->clinic_id
    		];

    		$sql = "role_id = '".$request['role_id']."' and menu_id = '".$request['menu_id']."'";
    		$chk = $this->permissionRepo->rawCount($sql);
    		if($chk <> 0){
    			$this->permissionRepo->rawUpdate($makeRequest, $sql);
                if($request['data_type'] == 'can_write'){
                    $this->permissionRepo->rawUpdate(['can_read' => '1'], $sql);
                }
    			return 1;
    		}

    		$this->permissionRepo->create($makeRequest);
    	} else {
    		$makeRequest = [
    			'role_id' => $request['role_id'],
    			'menu_id' => $request['menu_id'],
    			''.$request['data_type'].'' => '0',
    			'clinic_id' => \Auth::user()->clinic_id
    		];

    		$sql = "role_id = '".$request['role_id']."' and menu_id = '".$request['menu_id']."'";
    		$this->permissionRepo->rawUpdate($makeRequest, $sql);
			return 1;
    	}

    	$sql = "role_id = '".$request['role_id']."' and menu_id = '".$request['menu_id']."'";
    	// $this->permissionRepo->rawUpdate(['can_read' => 0, 'can_write' => 0, 'can_both' => 0], $sql);
		// $this->permissionRepo->rawUpdate($makeRequest, $sql);
        if($request['data_type'] == 'can_write'){
            $this->permissionRepo->rawUpdate(['can_read' => '1'], $sql);
        }
		return 1;
    }

    public function show($id)
    {
    	$this->authorize('read', $this->permissions());
    	$permissions = $this->permissions();
    	
    	$id = Hashids::decode($id);
    	$role = $this->roleRepo->find($id[0]);
    	$sql = "clinic_id = '".\Auth::user()->clinic_id."' and role_id = '".$id[0]."'";
    	$menus = $this->menuRepo->with(['permission' => function($query) use ($sql){
    		$query->whereRaw($sql);
    	}]);
    	return view('permission.show', compact('role','menus'));
    }

}