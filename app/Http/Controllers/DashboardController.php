<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Patient\AppointmentRepositoryEloquent;
use App\Repositories\Dentist\DeviceRepositoryEloquent;

class DashboardController extends Controller
{
	protected $patientRepo;
    protected $appointRepo;
    protected $deviceRepo;

	function __construct(PatientRepositoryEloquent $patientRepo, AppointmentRepositoryEloquent $appointRepo, DeviceRepositoryEloquent $deviceRepo)
	{
		$this->patientRepo = $patientRepo;
		$this->appointRepo = $appointRepo;
		$this->deviceRepo = $deviceRepo;
	}

    public function index()
    {
    	
        $sqlappoint = "clinic_user_id = '".\Auth::user()->clinic_id."' and date(schedule) > '".date('Y-m-d')."'";
        $sqltoday = "clinic_user_id = '".\Auth::user()->clinic_id."' and date(schedule) = '".date('Y-m-d')."'";
        $sqlpatient = "clinic_user_id = '".\Auth::user()->clinic_id."' and DATE_ADD(birthdate, INTERVAL YEAR(CURDATE())-YEAR(birthdate)+ IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(birthdate),1,0)YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)";
        $patientappointments = $this->appointRepo->rawWith(['patient'],$sqltoday,'schedule');
        $birthdays = $this->patientRepo->rawCount($sqlpatient);
        $countappointment = $this->appointRepo->rawCount($sqlappoint);
        $devices = $this->deviceRepo->rawCount("clinic_user_id = '".\Auth::user()->clinic_id."'");
        $countpatient = $this->patientRepo->rawCount("clinic_user_id = '".\Auth::user()->clinic_id."'");

    	return view('dashboard.index', compact('patientappointments','birthdays','countappointment','devices','countpatient'));
    }

    public function showPatientList() {
        $sql = "clinic_user_id = '".\Auth::user()->clinic_id."'";
        return $this->patientRepo->rawAll($sql, 'last_name');
    }

    public function search(Request $request)
    {
        $sql = "CONCAT(TRIM(first_name),' ', TRIM(last_name)) like '%".$request['name']."%' or CONCAT(TRIM(last_name),' ', TRIM(first_name)) like '%".$request['name']."%'";
        $patients = $this->patientRepo->rawAll($sql);
        return $patients;
    }

    public function showUpcomingBirthdays() {
        $sql = "clinic_user_id = '".\Auth::user()->clinic_id."' and DATE_ADD(birthdate, INTERVAL YEAR(CURDATE())-YEAR(birthdate)+ IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(birthdate),1,0)YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)";
        $birthdays = $this->patientRepo->rawAll($sql);

        return $birthdays;
    }

    public function viewAppointment()
    {
        $id = \Auth::user()->id;
        $user = DB::table('users')->where('id', $id)->update(['view_announcement' => 1]);
        return 1;
    }
}
