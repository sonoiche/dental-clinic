<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Dentist\FeedbackRepositoryEloquent;
use Mail;

class FeedbackController extends Controller
{
	protected $feedbackRepo;

	function __construct(FeedbackRepositoryEloquent $feedbackRepo)
	{
		$this->feedbackRepo = $feedbackRepo;
	}

    public function create()
    {
    	return view('feedback.create');
    }

    public function store(Request $request)
    {
    	$makeRequest = [
    		'clinic_user_id' => \Auth::user()->clinic_id,
    		'email' => $request['email'],
    		'message' => $request['message']
    	];

    	$feedback = $this->feedbackRepo->create($makeRequest);

    	$details = $request['message'];
        $sender = $request['email'];
        $sendername = \Auth::user()->first_name.' '.\Auth::user()->last_name;     
        $receiver = 'email@dentalclinicapp.com';        
        $subject = 'Dental Clinic Feedback';       
        $data = ['body'=>$details];        
        
        $send_email = Mail::send('emails.content', $data, function($message) use ($sender, $sendername, $receiver, $subject) {
            $message->from($sender, $sendername);
            $message->to($receiver);
            $message->bcc("jelson@quantumx.com");
            $message->subject($subject);
        });

        return redirect()->route('feedback.create')->with('status','Feedback Send Successfully!');;
    }
}
