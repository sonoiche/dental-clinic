<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Patient\AppointmentRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use App\Repositories\Dentist\PermissionRepositoryEloquent as Permission;
use Junity\Hashids\Facades\Hashids;

use App\Models\PatientAppointment;

class AppointmentController extends Controller
{
    protected $appointRepo;
    protected $patientRepo;
    protected $logsRepo;
    protected $permissionRepo;
    protected $hashids;
    protected $menu_id = 1;

    function __construct(Hashids $hashids, AppointmentRepositoryEloquent $appointRepo, PatientRepositoryEloquent $patientRepo, LogsRepositoryEloquent $logsRepo, Permission $permissionRepo)
    {
    	$this->hashids = $hashids;
    	$this->appointRepo = $appointRepo;
        $this->patientRepo = $patientRepo;
        $this->logsRepo = $logsRepo;
        $this->permissionRepo = $permissionRepo;
    }

    public function permissions()
    {
        return $this->permissionRepo->getPermission($this->menu_id);
    }

    public function index()
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

    	$sqlappoint = "clinic_user_id = '".\Auth::user()->clinic_id."' and date(schedule) = '".date('Y-m-d')."'";
    	$patientappointments = $this->appointRepo->rawWithPaginate(['patient'],$sqlappoint,10,'schedule');
    	$countappointments = $this->appointRepo->rawCount($sqlappoint);
    	$viewby = 'View Today';
    	return view('appointments.index', compact('patientappointments','viewby','countappointments','permissions'));
    }

    public function create($patient_id = "")
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

        if($patient_id){
            $id = Hashids::decode($patient_id);
            $patient = $this->patientRepo->find($id[0]);
            return view('appointments.create', compact('patient'));
        }
    	return view('appointments.create');
    }

    public function store(Request $request)
    {
    	$date = $request['appointmentdate'];
    	$time = $request['appointmenttime'];
    	$makeRequest = [
    		'clinic_id' => \Auth::user()->clinic_id,
	        'clinic_user_id' => \Auth::user()->clinic_id,
	        'patient_id' => isset($request['patient_id']) ? $request['patient_id'] : '',
	        'name' => isset($request['patient_name']) ? $request['patient_name'] : $request['name'],
	        'schedule' => input_time($date, $time),
	        'remarks' => $request['remarks']
    	];

    	$appointmentid = $this->appointRepo->create($makeRequest);

    	if($request['patient_id']){
            $this->logsRepo->insertLogs('appointments','insert',$request['patient_id'],$appointmentid);
            return redirect()->route('patient.show', Hashids::encode($request['patient_id']))->with('status','New Appointment has been Added!');
        }else{
            $this->logsRepo->insertLogs('appointments','insert', $appointmentid['patient_id'],$appointmentid);
            return redirect()->route('appointment.index')->with('status','New Appointment has been Added!');
        }
    }

    public function show($id)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();
        
        $id = Hashids::decode($id);
        $patientappointment = $this->appointRepo->find($id[0]);
        return [
            'name' => $patientappointment->name,
            'appointmentdate' => ($patientappointment->schedule!='0000-00-00 00:00:00') ? date('m/d/Y', strtotime($patientappointment->schedule)) : '',
            'appointmenttime' => ($patientappointment->schedule!='0000-00-00 00:00:00') ? date('g:i A', strtotime($patientappointment->schedule)) : '',
            'remarks' => $patientappointment->remarks,
            'appointment_id' => Hashids::encode($id)
        ];
    }

    public function edit($id)
    {
        $this->authorize('write', $this->permissions());
        $permissions = $this->permissions();

        $id = Hashids::decode($id);
        $patientappointment = $this->appointRepo->find($id[0]);
        return view('appointments.edit', compact('patientappointment'));
    }

    public function update(Request $request, $id)
    {
        $id = Hashids::decode($id);
        $date = $request['appointmentdate'];
        $time = $request['appointmenttime'];
        $makeRequest = [
            'clinic_id' => \Auth::user()->clinic_id,
            'clinic_user_id' => \Auth::user()->clinic_id,
            'name' => isset($request['name']) ? $request['name'] : '',
            'schedule' => input_time($date, $time),
            'remarks' => $request['remarks']
        ];

        $patientappointment = $this->appointRepo->update($makeRequest, $id[0]);
        $patientappointment = $this->appointRepo->find($id[0]);
        $this->logsRepo->insertLogs('appointments','update',$patientappointment->patient_id,$patientappointment->id);

        if($request['appointment_id']){
            return 1;
        }
        return redirect()->route('appointment.edit',Hashids::encode($id))->with('status','Appointment has been Updated!');
    }

    public function destroy($id)
    {
        $id = Hashids::decode($id);
        $patientappointment = $this->appointRepo->find($id[0]);
        $this->logsRepo->insertLogs('appointments','delete',$patientappointment->patient_id,$patientappointment->id);
        $this->appointRepo->delete($id[0]);
    }

    public function viewby(Request $request)
    {
        $this->authorize('read', $this->permissions());
        $permissions = $this->permissions();

        $viewby = $request->get('viewby');
        $patientappointments = PatientAppointment::with('patient')
                                    ->filterview($request->all())
                                    ->where('clinic_user_id',\Auth::user()->clinic_id)
                                    ->orderBy('schedule','desc')
                                    ->paginate(20);
        $countappointments = PatientAppointment::with('patient')
                                    ->filterview($request->all())
                                    ->where('clinic_user_id',\Auth::user()->clinic_id)
                                    ->count();
        return view('appointments.index', compact('patientappointments','viewby','countappointments'));
    }

    public function deleteall(Request $request)
    {
        $appointment_ids = $request['appointment_ids'];
        PatientAppointment::whereIn('id', $appointment_ids)->delete();
        return redirect()->back()->with('status','Appointments has been deleted!');
    }
}
