<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Http\Requests\RegisterRequest;
use App\Models\Personal;
use App\Models\DentalNote;
use App\Models\PatientAppointment;
use App\Models\PatientHistory;
use App\Models\DentalPhoto;
use App\Models\DentalPayment;
use App\Models\InstallmentBalance;
use App\Models\Country;
use App\Models\ConnectedDevice;
use App\Models\Log;
use App\Models\Role;
use App\User;
// use App\Redeemed;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use DB;
use Carbon\Carbon;
use Mail;
use Storage;

class ApiController extends Controller
{
    protected $patients;
    protected $patient_history;
    protected $dentalnote;
    protected $patientappointment;
    protected $dentalphoto;
    protected $payments;
	protected $installment_balances;
    protected $inserted_ids = array();
    protected $inserted_payment_ids = array();
    protected $inserted_records = array();
    protected $patient_ids = array();
    protected $patients_image = array();
    protected $connected_device_id;
    protected $logs;
    protected $logsRepo;

    public function __construct(Personal $patients, DentalNote $dentalnote, PatientAppointment $patientappointment, PatientHistory $patient_history, DentalPhoto $dentalphoto, DentalPayment $payments, InstallmentBalance $installment_balances, User $user, Country $country, ConnectedDevice $connected_devices, Log $logs, LogsRepositoryEloquent $logsRepo) // , Redeemed $redeemed, Logs $logs, LogsRepository $logsRepo
	{
		$this->patients = $patients;
        $this->dentalnote = $dentalnote;
        $this->patientappointment = $patientappointment;
        $this->dentalphoto = $dentalphoto;
        $this->payments = $payments;
        $this->installment_balances = $installment_balances;
        $this->user = $user;
        $this->country = $country;
        // $this->redeemed = $redeemed;
        $this->logs = $logs;
        $this->connected_devices = $connected_devices;
        $this->logsRepo = $logsRepo;
        $this->patient_history = $patient_history;
	}

	public function getdata($apikey, $new_user = false)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		/*$user->sync_version = $user->sync_version+1;
		$user->save();*/
		$patients = $this->patients->select('id','first_name','middle_name','last_name','gender','birthdate','email','contact_number','address','notes','medical_history','image','created_at','updated_at','mobile_date')->where('clinic_user_id',$user->clinic_id)->get();
		foreach ($patients as $key => $value) {
			if(!\File::exists('uploads/patients/'.$value->image) || $new_user == false) {
				$patients[$key]['synced'] = 0;
			}
			else {
				$patients[$key]['synced'] = 1;
			}
		}

		$dentalnotes = $this->dentalnote->where('clinic_user_id',$user->clinic_id)->get();
		$appointments = $this->patientappointment->where('clinic_user_id',$user->clinic_id)->get();
		// $dentalphotos = $this->dentalphoto->where('clinic_user_id',$user->clinic_id)->get();
		$patient_history = $this->patient_history->where('clinic_user_id',$user->clinic_id)->get();
		$payments = $this->payments->where('clinic_user_id',$user->clinic_id)->get();
		foreach ($payments as $key => $payment) {
			$installment_balances = $this->installment_balances->where('clinic_user_id',$user->clinic_id)->where('payment_id', $payment->id)->get();
			$payments[$key]['installments'] = $installment_balances;
		}
		if ($new_user == true) {
			$patients_with_image = $this->patients->select('id', 'first_name','middle_name','last_name', 'image as image_name', DB::raw("'patient' as 'prefix'"))->where('image', '<>', '')->where('mobile_date', '0000-00-00 00:00:00')->where('clinic_user_id',$user->clinic_id)->get();
			$dental_photos = $this->dentalphoto->select('id', 'clinic_id', 'clinic_user_id', 'patient_id', 'photo as image_name', 'caption', 'created_at', 'updated_at', 'mobile_date', DB::raw("'image' as 'prefix'"))->where('photo', '<>', '')->where('mobile_date', '0000-00-00 00:00:00')->where('clinic_user_id',$user->clinic_id)->get();
		}
		else {
			$patients_with_image = $this->patients->select('id', 'first_name','middle_name','last_name', 'image as image_name', DB::raw("'patient' as 'prefix'"))->where('image', '<>', '')->where('clinic_user_id',$user->clinic_id)->get();
			$dental_photos = $this->dentalphoto->select('id', 'clinic_id', 'clinic_user_id', 'patient_id', 'photo as image_name', 'caption', 'created_at', 'updated_at', 'mobile_date', DB::raw("'image' as 'prefix'"))->where('clinic_user_id',$user->clinic_id)->get();
		}

		$images_to_download = array_merge($patients_with_image->toArray(), $dental_photos->toArray());

		$jsonfile = [
			'sync_version' => $user->sync_version,
			'patients' => $patients,
			'dentalnotes' => $dentalnotes,
			'appointments' => $appointments,
			// 'dentalphotos' => $dentalphotos,
			'payments' => $payments,
			'patient_history' => $patient_history,
			'patient_ids' => $this->patient_ids,
			'patients_image' => $this->patients_image,
			'images_to_download' => $images_to_download
		];
		
		$fp = fopen('uploads/temp/'.$apikey.'.json', 'w');
		fwrite($fp, json_encode($jsonfile));
		fclose($fp);
	}

	function change_key( $array, $old_key, $new_key ){
	    if(!array_key_exists($old_key, $array))
	        return $array;

	    $keys = array_keys($array);
	    $keys[array_search($old_key, $keys)] = $new_key;

	    return array_combine( $keys, $array );
	}

	public function postdata($apikey)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		$jsonfile = 'uploads/datas/'.$apikey.'.json';
		$htmlfile = file_get_contents($jsonfile);
		$htmlfiles = (array) json_decode($htmlfile);
		$new_user = false;

		foreach ($htmlfiles as $key => $value) {	
			if (array_key_exists('device_number', $value))
			{
				if ($value->new_user && $value->new_user == true) {
					$new_user = true;
				} 
				$connected_device = $this->connected_devices->where('device_number',$value->device_number)->where('clinic_user_id', $user->clinic_id)->first();
				/* User invitation temporarily turned off
				if (count($connected_device) > 0)
				{
					$country = $this->country->where('code',$value->country_code)->first();
						$connected_device->device_number = $value->device_number;
						$connected_device->model = $value->model;
						$connected_device->platform = $value->platform;
						$connected_device->sync_version = $user->sync_version;
						$connected_device->country_id = $country->id;
						$connected_device->first_name = $value->first_name;
						$connected_device->last_name = $value->last_name;
						$connected_device->updated_at = date('Y-m-d G:i:s');
						$connected_device->update();
				}
				else {
					$main_dentist = $this->user->where('apikey', $apikey)->where('email', $value->email)->first();
					if (count($main_dentist) > 0)
					{*/
						if (count($connected_device) > 0)
						{
							$connected_device->sync_version = $user->sync_version;
							$connected_device->has_reset = 1;
							$connected_device->updated_at = date('Y-m-d G:i:s');
							$connected_device->update();
						}
						else 
						{
							$country = $this->country->where('code',$value->country_code)->first();
							$this->connected_devices->clinic_user_id = $user->clinic_id;
							$this->connected_devices->device_number = $value->device_number;
							$this->connected_devices->model = $value->model;
							$this->connected_devices->platform = $value->platform;
							$this->connected_devices->country_id = $country->id;
							$this->connected_devices->first_name = $value->first_name;
							$this->connected_devices->last_name = $value->last_name;
							$this->connected_devices->email = $value->email;
							$this->connected_devices->has_reset = 1;
							$this->connected_devices->sync_version = $user->sync_version;
							$this->connected_devices->updated_at = date('Y-m-d G:i:s');
							$this->connected_devices->save();
						}

						if ($value->country_code)
						{
								$country = $this->country->where('code', $value->country_code)->first();
								$user->country_id = $country->id;
								$user->update();
						}
				/*	}
				}*/

			}
			elseif (array_key_exists('without_patient_id', $value))
			{
				foreach ($value->without_patient_id as $key => $appointment) {
					$app = $this->patientappointment->where('clinic_user_id',$user->clinic_id)
																->where('id', $appointment->desktop_appointment_id)
																->first();
					if(isset($appointment->now_show)) {
						$no_show = $appointment->now_show;
					} else {
						$no_show = 0;
					}

					if(count($app) <> 0){
						if($appointment->created_at > $app->sync_date){
							DB::update("update patient_appointments set name='".$appointment->patient_name."', schedule='".$appointment->date."', remarks='".addslashes($appointment->remarks)."', mobile_date='".$appointment->created_at."', no_show = '".$no_show."' where id='".$appointment->desktop_appointment_id."'");
						}
					} else {
						DB::insert("insert into patient_appointments (clinic_user_id,patient_id,name,schedule,remarks,created_at,updated_at, mobile_date, no_show) values ('".$user->clinic_id."','".$appointment->patient_id."','".$appointment->patient_name."','".$appointment->date."','".addslashes($appointment->remarks)."','".Carbon::now()."','".Carbon::now()."','".$appointment->created_at."','".$no_show."')");
					}
				}
			}
			else
			{
				/*if($value->image!=''){
					$imagevalue = str_random(10).'.jpg';
					$xx = file_put_contents('uploads/patients/'.$imagevalue, base64_decode($value->image));
				} else {
					$imagevalue = '';
				}*/


				$personal = $this->patients->where('clinic_user_id',$user->clinic_id)
										   ->where('first_name',$value->first_name)
										   ->where('middle_name',$value->middle_name)
										   ->where('last_name',$value->last_name)
										   ->where('birthdate',($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate)
										   ->first();
				if(count($personal) <> 0){
					if($value->sync_date > $personal->updated_at){
						$patient = DB::update("update personals set first_name='".$value->first_name."',
														 middle_name='".$value->middle_name."',
														 last_name='".$value->last_name."',
														 gender='".$value->gender."',
														 birthdate='".($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate."',
														 email='".$value->email_address."',
														 contact_number='".$value->contact_number."',
														 address='".$value->address."',
														 image='".$value->image."',
														 notes='".addslashes($value->notes)."',
														 medical_history='".($value->medical_history)?addslashes($value->medical_history):''."',
														 mobile_date='".$value->updated_at."',
														 created_at='".$value->created_at."',
														 updated_at='".$value->updated_at."'
														 where id='".$personal->id."'");
					}
					$patient = $this->patients->find($personal->id);
				} else {
					$patient = Personal::create([
						'clinic_user_id' => $user->clinic_id,
						'first_name' => $value->first_name,
						'middle_name' => $value->middle_name,
						'last_name' => $value->last_name,
						'gender' => $value->gender,
						'birthdate' => ($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate,
						'email' => ($value->email_address) ? $value->email_address : '',
						'contact_number' => ($value->contact_number) ? $value->contact_number : '',
						'address' => ($value->address) ? $value->address : '',
						'image' => $value->image,
						'notes' => addslashes($value->notes),
						'medical_history' => ($value->medical_history) ? addslashes($value->medical_history) : '',
						'created_at' => $value->created_at,
						'updated_at' => $value->updated_at,
						'mobile_date' => $value->updated_at
					]);
					DB::update("UPDATE personals SET updated_at = '".$value->updated_at."', created_at = '".$value->created_at."' WHERE id='".$patient->id."'");

				}
				$this->patients_image[] = array("old_patient_id" => $value->id, "desktop_patient_id" => $patient->id);

				/* dental notes sync */
				if(count($value->dental_notes)<>0){
					foreach ($value->dental_notes as $key => $dental_note) {
						$dentalnote = $this->dentalnote->where('clinic_user_id',$user->clinic_id)
													   ->where('id', $dental_note->desktop_dentalnote_id)
													   ->first();
						if(count($dentalnote) <> 0){
							if($dental_note->sync_date > $dentalnote->updated_at){
								DB::update("update dental_notes set chart_id='".$dental_note->chart_id."',
																	chart_name='".$dental_note->name."',
																	color_code='".$dental_note->color_code."',
																	notes='".addslashes($dental_note->notes)."',
																	mobile_date='".addslashes($dental_note->updated_at)."',
																	created_at='".addslashes($dental_note->created_at)."',
																	updated_at='".addslashes($dental_note->updated_at)."'
																	where id='".$dental_note->desktop_dentalnote_id."'");
							}
						} else {
							DB::insert("insert into dental_notes (clinic_user_id,patient_id,chart_id,chart_name,color_code,notes,created_at,updated_at, mobile_date) values ('".$user->clinic_id."','"
								.$patient->id."','"
								.$dental_note->chart_id."','"
								.$dental_note->name."','"
								.$dental_note->color_code."','"
								.addslashes($dental_note->notes)."','"
								.$dental_note->created_at."','"
								.$dental_note->updated_at."','"
								.$dental_note->updated_at."')");
						}
					}
				}

				/* appointments */
				if(count($value->appointments)<>0){
					foreach ($value->appointments as $key => $appointment) {
						$app = $this->patientappointment->where('clinic_user_id',$user->clinic_id)
																->where('id', $appointment->desktop_appointment_id)
																->first();
						if(isset($appointment->now_show)) {
							$no_show = $appointment->now_show;
						} else {
							$no_show = 0;
						}

						if(count($app) <> 0){
							if($appointment->sync_date > $app->updated_at){
								DB::update("update patient_appointments set name='".$appointment->patient_name."', schedule='".$appointment->date."', remarks='".addslashes($appointment->remarks)."', mobile_date='".$appointment->created_at."', updated_at='".$appointment->created_at."', created_at='".$appointment->created_at."', no_show='".$no_show."' where id='".$appointment->desktop_appointment_id."'");
							}
						} else {
							DB::insert("insert into patient_appointments (clinic_user_id,patient_id,name,schedule,remarks,created_at,updated_at,mobile_date, no_show) values ('".$user->clinic_id."','".$patient->id."','".$appointment->patient_name."','".$appointment->date."','".addslashes($appointment->remarks)."','".$appointment->created_at."','".$appointment->created_at."','".$appointment->created_at."','".$no_show."')");
						}
					}
				}

				/* Dental Photos */
				if(count($value->images)<>0){
					foreach ($value->images as $key => $image) {
						$dentalphoto = $this->dentalphoto->where('clinic_user_id',$user->clinic_id)
														 ->where('id', $image->desktop_image_id)
														 ->first();

							if(!\File::exists('uploads/photos/'.$user->clinic_id)) {
								\File::makeDirectory('uploads/photos/'.$user->clinic_id, 0777);
							}

							if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient->id)) {
								\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient->id, 0777);
							}

							/*if($image->image!=''){
								$imagevalue = str_random(10).'_img.jpg';
								$xx = file_put_contents('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$imagevalue, base64_decode($image->image));
							} else {
								$imagevalue = '';
							}*/
							
						if(count($dentalphoto) <> 0){
							$image_id = $image->desktop_image_id;
							if($image->sync_date > $dentalphoto->updated_at){
								if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$dentalphoto->photo)) {
									\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$image->desktop_patient_id, 0777);
									rename("uploads/photos/".$user->clinic_id.'/'.$patient->id."/".$dentalphoto->photo, "uploads/photos/".$user->clinic_id.'/'.$patient->id."/".$dentalphoto->photo);
								}
								DB::update("update dental_photos set photo='".$dentalphoto->photo."', caption='".addslashes($image->caption)."', mobile_date='".$image->updated_at."', created_at='".$image->created_at."', updated_at='".$image->updated_at."' where id='".$image->desktop_image_id."'");
							}
						} else {
							$saved_image = DB::table('dental_photos')->insertGetId([
								'clinic_user_id' => $user->clinic_id,
								'patient_id' => $patient->id,
								'photo' => $image->image,
								'caption' => addslashes($image->caption),
								'mobile_date' => $image->updated_at,
								'created_at' => $image->created_at,
								'updated_at' => $image->updated_at
							]);
								$image_id = $saved_image;
						}
						$this->patient_ids[] = array("old_image_id" => $image->id, "desktop_patient_id" => $patient->id, "desktop_image_id" => $image_id);
					}
				}

				/* Payments */
				foreach ($value->payments as $key => $pay) {
					$payment = $this->payments->where('clinic_user_id',$user->clinic_id)->where('id', $pay->desktop_payment_id)->first();
					if(count($payment) <> 0){
						if($pay->sync_date > $payment->updated_at) {
							DB::update("update dental_payments set  dental_note_id='".$pay->note_id."',
																	payment_date='".$pay->date."',
																	due_date='".($pay->due_date == 'undefined' || $pay->due_date == '')?'0000-00-00':$pay->due_date."',
																	amount='".$pay->amount."',
																	remarks='".addslashes($pay->remarks)."',
																	payment_type='".$pay->type."',
																	mobile_date='".$pay->updated_at."',
																	created_date='".$pay->created_at."',
																	updated_at='".$pay->updated_at."'
																	where id='".$pay->desktop_payment_id."'");
							foreach ($pay->installment_balances as $key => $installment) {
								$this->insertToInstallments($installment, $pay->desktop_payment_id, $user);
							}
						}
					} else {
						$saved_payment_id = DB::table('dental_payments')->insertGetId([
							'clinic_user_id' => $user->clinic_id,
							'dental_note_id' => $pay->note_id,
							'patient_id' => $patient->id,
							'payment_date' => $pay->date,
							'due_date' => ($pay->due_date == 'undefined' || $pay->due_date == '')?'0000-00-00':$pay->due_date,
							'amount' => $pay->amount,
							'remarks' => addslashes($pay->remarks),
							'payment_type' => $pay->type,
							'created_at' => $pay->created_at,
							'updated_at' => $pay->updated_at,
							'mobile_date' => $pay->updated_at
						]);
						if(count($pay->installment_balances)<>0){
							foreach ($pay->installment_balances as $key => $installment) {
								$this->insertToInstallments($installment, $saved_payment_id, $user);
							}
						}
					}
				}

				/* patient history */
				if (array_key_exists('patient_history', $value)) {
					if(count($value->patient_history)<>0){
						foreach ($value->patient_history as $key => $history) {
							$history_result = $this->patient_history->where('clinic_user_id',$user->clinic_id)
														   ->where('id', $history->desktop_history_id)
														   ->first();
							if(count($history_result) <> 0){
								if($history->sync_date > $history_result->updated_at){
									DB::update("update patient_history set title='".$history->title."',
																		history='".addslashes($history->history)."',
																		mobile_date='".addslashes($history->updated_at)."',
																		created_at='".addslashes($history->created_at)."',
																		updated_at='".addslashes($history->updated_at)."'
																		where id='".$history->desktop_history_id."'");
								}
							} else {
								DB::insert("insert into patient_history (clinic_user_id,patient_id,title,history,created_at,updated_at, mobile_date) values ('".$user->clinic_id."','".$patient->id."','".$history->title."','".addslashes($history->history)."','".$history->created_at."','".$history->updated_at."','".$history->updated_at."')");
							}
						}
					}
				}
			}
		}	
		$this->getdata($apikey, $new_user);
	}

	public function savedata($apikey)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		$jsonfile = 'uploads/datas/'.$apikey.'.json';
		$htmlfile = file_get_contents($jsonfile);
		$htmlfiles = (array) json_decode($htmlfile);


		foreach ($htmlfiles as $key => $value) {	
			if (array_key_exists('device_number', $value))
			{
				$connected_device = $this->connected_devices->where('email',$value->email)->where('clinic_user_id', $user->clinic_id)->first();
				/* USER INVITATION TEMPORARILY TURNED OFF
				if (count($connected_device) > 0)
				{
					$country = $this->country->where('code',$value->country_code)->first();
						$connected_device->device_number = $value->device_number;
						$connected_device->model = $value->model;
						$connected_device->platform = $value->platform;
						$connected_device->sync_version = $user->sync_version;
						$connected_device->country_id = $country->id;
						$connected_device->first_name = $value->first_name;
						$connected_device->last_name = $value->last_name;
						$connected_device->updated_at = date('Y-m-d G:i:s');
						$connected_device->update();
				}
				else {
					$main_dentist = $this->user->where('apikey', $apikey)->where('email', $value->email)->first();
					if (count($main_dentist) > 0)
					{*/
						if (count($connected_device) > 0)
						{
							$connected_device->sync_version = $user->sync_version;
							$connected_device->updated_at = date('Y-m-d G:i:s');
							$connected_device->update();
						}
						else 
						{
							$country = $this->country->where('code',$value->country_code)->first();
							$this->connected_devices->clinic_user_id = $user->clinic_id;
							$this->connected_devices->device_number = $value->device_number;
							$this->connected_devices->model = $value->model;
							$this->connected_devices->platform = $value->platform;
							$this->connected_devices->country_id = $country->id;
							$this->connected_devices->first_name = $value->first_name;
							$this->connected_devices->last_name = $value->last_name;
							$this->connected_devices->email = $value->email;
							$this->connected_devices->sync_version = $user->sync_version;
							$this->connected_devices->updated_at = date('Y-m-d G:i:s');
							$this->connected_devices->save();
						}

						if ($value->country_code)
						{
								$country = $this->country->where('code', $value->country_code)->first();
								$user->country_id = $country->id;
								$user->update();
						}
				/*	}
				}*/

			}
			elseif (array_key_exists('without_patient_id', $value))
			{
				foreach ($value->without_patient_id as $key => $appointment) {
					$app = $this->patientappointment->where('clinic_user_id',$user->clinic_id)
																->where('id', $appointment->desktop_appointment_id)
																->first();
					if(isset($appointment->now_show)) {
						$no_show = $appointment->now_show;
					} else {
						$no_show = 0;
					}

					if(count($app) <> 0){
						if($appointment->created_at > $app->sync_date){
							DB::update("update patient_appointments set name='".$appointment->patient_name."', schedule='".$appointment->date."', remarks='".addslashes($appointment->remarks)."', mobile_date='".$appointment->created_at."', no_show='".$no_show."' where id='".$appointment->desktop_appointment_id."'");
						}
					} else {
						DB::insert("insert into patient_appointments (clinic_user_id,patient_id,name,schedule,remarks,created_at,updated_at, mobile_date, no_show) values ('".$user->clinic_id."','".$appointment->patient_id."','".$appointment->patient_name."','".$appointment->date."','".addslashes($appointment->remarks)."','".Carbon::now()."','".Carbon::now()."','".$appointment->created_at."','".$no_show."')");
					}
				}
			}
			else
			{
				if($value->image!=''){
					$imagevalue = str_random(10).'.jpg';
					$xx = file_put_contents('uploads/patients/'.$imagevalue, base64_decode($value->image));
				} else {
					$imagevalue = '';
				}


				$personal = $this->patients->where('clinic_user_id',$user->clinic_id)
										   ->where('first_name',$value->first_name)
										   ->where('middle_name',$value->middle_name)
										   ->where('last_name',$value->last_name)
										   ->where('birthdate',($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate)
										   ->first();
				if(count($personal) <> 0){
					if($value->sync_date > $personal->updated_at){
						$patient = DB::update("update personals set first_name='".$value->first_name."',
														 middle_name='".$value->middle_name."',
														 last_name='".$value->last_name."',
														 gender='".$value->gender."',
														 birthdate='".($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate."',
														 email='".$value->email_address."',
														 contact_number='".$value->contact_number."',
														 address='".$value->address."',
														 image='".$imagevalue."',
														 notes='".addslashes($value->notes)."',
														 medical_history='".($value->medical_history)?addslashes($value->medical_history):''."',
														 mobile_date='".$value->updated_at."',
														 created_at='".$value->created_at."',
														 updated_at='".$value->updated_at."'
														 where id='".$value->desktop_patient_id."'");
					}
					$patient = $this->patients->find($value->desktop_patient_id);
				} else {
					$patient = Personal::create([
						'clinic_user_id' => $user->clinic_id,
						'first_name' => $value->first_name,
						'middle_name' => $value->middle_name,
						'last_name' => $value->last_name,
						'gender' => $value->gender,
						'birthdate' => ($value->birthdate == '0NaN-NaN-NaN')?'0000-00-00':$value->birthdate,
						'email' => ($value->email_address) ? $value->email_address : '',
						'contact_number' => ($value->contact_number) ? $value->contact_number : '',
						'address' => ($value->address) ? $value->address : '',
						'image' => $imagevalue,
						'notes' => addslashes($value->notes),
						'medical_history' => ($value->medical_history) ? addslashes($value->medical_history) : '',
						'created_at' => $value->created_at,
						'updated_at' => $value->updated_at,
						'mobile_date' => $value->updated_at
					]);
					DB::update("UPDATE personals SET updated_at = '".$value->updated_at."', created_at = '".$value->created_at."' WHERE id='".$patient->id."'");
				}
				$this->patients_image[] = array("old_patient_id" => $value->id, "desktop_patient_id" => $patient->id);

				/* dental notes sync */
				if(count($value->dental_notes)<>0){
					foreach ($value->dental_notes as $key => $dental_note) {
						$dentalnote = $this->dentalnote->where('clinic_user_id',$user->clinic_id)
													   ->where('id', $dental_note->desktop_dentalnote_id)
													   ->first();
						if(count($dentalnote) <> 0){
							if($dental_note->sync_date > $dentalnote->updated_at){
								DB::update("update dental_notes set chart_id='".$dental_note->chart_id."',
																	color_code='".$dental_note->color_code."',
																	notes='".addslashes($dental_note->notes)."',
																	mobile_date='".addslashes($dental_note->updated_at)."',
																	created_at='".addslashes($dental_note->created_at)."',
																	updated_at='".addslashes($dental_note->updated_at)."'
																	where id='".$dental_note->desktop_dentalnote_id."'");
							}
						} else {
							DB::insert("insert into dental_notes (clinic_user_id,patient_id,chart_id,color_code,notes,created_at,updated_at, mobile_date) values ('".$user->clinic_id."','"
								.$patient->id."','"
								.$dental_note->chart_id."','"
								.$dental_note->color_code."','"
								.addslashes($dental_note->notes)."','"
								.$dental_note->created_at."','"
								.$dental_note->updated_at."','"
								.$dental_note->updated_at."')");
						}
					}
				}

				/* appointments */
				if(count($value->appointments)<>0){
					foreach ($value->appointments as $key => $appointment) {
						$app = $this->patientappointment->where('clinic_user_id',$user->clinic_id)
																->where('id', $appointment->desktop_appointment_id)
																->first();

						if(isset($appointment->now_show)) {
							$no_show = $appointment->now_show;
						} else {
							$no_show = 0;
						}

						if(count($app) <> 0){
							if($appointment->sync_date > $app->updated_at){
								DB::update("update patient_appointments set name='".$appointment->patient_name."', schedule='".$appointment->date."', remarks='".addslashes($appointment->remarks)."', mobile_date='".$appointment->created_at."', created_at='".$appointment->created_at."', updated_at='".$appointment->created_at."', no_show = '".$no_show."' where id='".$appointment->desktop_appointment_id."'");
							}
						} else {
							DB::insert("insert into patient_appointments (clinic_user_id,patient_id,name,schedule,remarks,created_at,updated_at,mobile_date, no_show) values ('".$user->clinic_id."','".$patient->id."','".$appointment->patient_name."','".$appointment->date."','".addslashes($appointment->remarks)."','".$appointment->created_at."','".$appointment->created_at."','".$appointment->created_at."','".$no_show."')");
						}
					}
				}

				/* Dental Photos */
				if(count($value->images)<>0){
					foreach ($value->images as $key => $image) {
						$dentalphoto = $this->dentalphoto->where('clinic_user_id',$user->clinic_id)
														 ->where('id', $image->desktop_image_id)
														 ->first();

							if(!\File::exists('uploads/photos/'.$user->clinic_id)) {
								\File::makeDirectory('uploads/photos/'.$user->clinic_id, 0777);
							}

							if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient->id)) {
								\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient->id, 0777);
							}

							if($image->image!=''){
								$imagevalue = str_random(10).'_img.jpg';
								$xx = file_put_contents('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$imagevalue, base64_decode($image->image));
							} else {
								$imagevalue = '';
							}
							
						if(count($dentalphoto) <> 0){
							$image_id = $image->desktop_image_id;
							if($image->sync_date > $dentalphoto->updated_at){
								if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$dentalphoto->photo)) {
									\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient->id.'/'.$image->desktop_patient_id, 0777);
									rename("uploads/photos/".$user->clinic_id.'/'.$patient->id."/".$dentalphoto->photo, "uploads/photos/".$user->clinic_id.'/'.$patient->id."/".$dentalphoto->photo);
								}
								DB::update("update dental_photos set photo='".$dentalphoto->photo."', caption='".addslashes($image->caption)."', mobile_date='".$image->updated_at."', created_at='".$image->created_at."', updated_at='".$image->updated_at."' where id='".$image->desktop_image_id."'");
							}
						} else {
							$saved_image = DB::table('dental_photos')->insertGetId([
								'clinic_user_id' => $user->clinic_id,
								'patient_id' => $patient->id,
								'photo' => $imagevalue,
								'caption' => addslashes($image->caption),
								'mobile_date' => $image->updated_at,
								'created_at' => $image->created_at,
								'updated_at' => $image->updated_at
							]);
							$image_id = $saved_image;
						}
						$this->patient_ids[] = array("old_image_id" => $image->id, "desktop_patient_id" => $patient->id, "desktop_image_id" => $image_id);
					}
				}

				/* Payments */
				foreach ($value->payments as $key => $pay) {
					$payment = $this->payments->where('clinic_user_id',$user->clinic_id)->where('id', $pay->desktop_payment_id)->first();
					if(count($payment) <> 0){
						if($pay->sync_date > $payment->updated_at) {
							DB::update("update dental_payments set  dental_note_id='".$pay->note_id."',
																	payment_date='".$pay->date."',
																	due_date='".$pay->due_date."',
																	amount='".$pay->amount."',
																	remarks='".addslashes($pay->remarks)."',
																	payment_type='".$pay->type."',
																	mobile_date='".$pay->updated_at."',
																	created_at='".$pay->created_at."',
																	updated='".$pay->updated_at."'
																	where id='".$pay->desktop_payment_id."'");
							foreach ($pay->installment_balances as $key => $installment) {
								$this->insertToInstallments($installment, $pay->desktop_payment_id, $user);
							}
						}
					} else {
						$saved_payment_id = DB::table('dental_payments')->insertGetId([
							'clinic_user_id' => $user->clinic_id,
							'dental_note_id' => $pay->note_id,
							'patient_id' => $patient->id,
							'payment_date' => $pay->date,
							'due_date' => $pay->due_date,
							'amount' => $pay->amount,
							'remarks' => addslashes($pay->remarks),
							'payment_type' => $pay->type,
							'created_at' => $pay->created_at,
							'updated_at' => $pay->updated_at,
							'mobile_date' => $pay->updated_at
						]);
						if(count($pay->installment_balances)<>0){
							foreach ($pay->installment_balances as $key => $installment) {
								$this->insertToInstallments($installment, $saved_payment_id, $user);
							}
						}
					}
				}

				/* patient history */
				if (array_key_exists('patient_history', $value)) {
					if(count($value->patient_history)<>0){
						foreach ($value->patient_history as $key => $history) {
							$history_result = $this->patient_history->where('clinic_user_id',$user->clinic_id)
														   ->where('id', $history->desktop_history_id)
														   ->first();
							if(count($history_result) <> 0){
								if($history->sync_date > $history_result->updated_at){
									DB::update("update patient_history set title='".$history->title."',
																		history='".addslashes($history->history)."',
																		mobile_date='".addslashes($history->updated_at)."',
																		created_at='".addslashes($history->created_at)."',
																		updated_at='".addslashes($history->updated_at)."'
																		where id='".$history->desktop_history_id."'");
								}
							} else {
								DB::insert("insert into patient_history (clinic_user_id,patient_id,title,history,created_at,updated_at, mobile_date) values ('".$user->clinic_id."','".$patient->id."','".$history->title."','".addslashes($history->history)."','".$history->created_at."','".$history->updated_at."','".$history->updated_at."')");
							}
						}
					}
				}
			}
		}	
		$this->getdata($apikey);
	}

	public function insertToInstallments($installment, $payment_id, $user)
	{
		$installment_balances = $this->installment_balances->where('clinic_user_id',$user->clinic_id)->where('id', $installment->desktop_installment_id)->first();
		if(count($installment_balances) <> 0){
			if($installment->sync_date > $installment->updated_at) {
				DB::update("update installment_balances set payment_id='".$installment->desktop_payment_id."',
														amount='".$installment->amount."',
														currency='".$installment->currency."',
														remarks='".addslashes($installment->remarks)."',
														date='".$installment->date."',
														mobile_date='".$installment->updated_at."',
														created_at='".$installment->created_at."',
														updated_at='".$installment->updated_at."',
														where id='".$installment->desktop_installment_id."'");
			}
		} else {
			DB::insert("insert into installment_balances (clinic_user_id,
													 payment_id,
													 amount,
													 currency,
													 remarks,
													 date,
													 created_at,
													 updated_at, 
													 mobile_date) 
											 values ('".$user->clinic_id."',
											 		 '".$payment_id."',
											 		 '".$installment->amount."',
											 		 '".$installment->currency."',
											 		 '".addslashes($installment->remarks)."',
											 		 '".$installment->date."',
											 		 '".$installment->created_at."',
											 		 '".$installment->updated_at."',
											 		 '".$installment->updated_at."')");
		}
	}

	public function uploadJsonFile(Request $request, $apikey)
	{
		$user = $this->user->where('apikey',$apikey)->first();

		if(count($user)<>0){
			/* USER INVITATION TEMPORARILY TURNED OFF

			$htmlfile = file_get_contents($request->file('file'));
			$htmlfiles = (array) json_decode($htmlfile);
			foreach ($htmlfiles as $key => $value) {	
				if (array_key_exists('device_number', $value))
				{
					$connected = $this->connected_devices->where('email',$value->email)->where('clinic_user_id', $user->clinic_id)->count();
					$main_dentist = $this->user->where('apikey', $apikey)->where('email', $value->email)->count();
					if ($connected > 0 || $main_dentist > 0)
					{*/
						$destinationPath = 'uploads/datas';
				        $filename = $request->file('file')->getClientOriginalName();
				        $extension = $request->file('file')->getClientOriginalExtension();
				        $file = $apikey.'.json';
				        $request->file('file')->move($destinationPath, $file);
				    	$this->postdata($apikey);
				        return 'success';
				/*	}
					return 'no_invitation';
				}
			}*/
	    } else {
	    	return 'error';
	    }
        
	}

	public function testUpload(Request $request)
	{
		$destinationPath = 'uploads/test_upload';
        $filename = $request->file('file')->getClientOriginalName();
        $extension = $request->file('file')->getClientOriginalExtension();
        $file = 'backup.json';
        $request->file('file')->move($destinationPath, $file);
        return 'success';
	}

	public function uploadImages(Request $request, $apikey, $prefix, $id, $name)
	{
		$user = $this->user->where('apikey',$apikey)->first();

		if (count($user)<>0)
		{
			if ($prefix == 'patient')
			{

				$patients = $this->patients->where('id',$id)->first();
				if(count($patients) > 0) {
					$destinationPath = 'uploads/patients';
			       	$extension = $request->file('file')->getClientOriginalExtension();
			        // $file = $request->file('file')->getClientOriginalName();
		        	// $file = $prefix.'_'.$id.'.'.$extension;
		        	$request->file('file')->move($destinationPath, $name);
					$patients->image = $name;
					$patients->update();
			    }
			}
			elseif ($prefix == 'image')
			{
	        	$dentalphoto = $this->dentalphoto->where('id',$id)->first();

	        	if(count($dentalphoto) > 0) {
			       	$extension = $request->file('file')->getClientOriginalExtension();
			        // $file = $request->file('file')->getClientOriginalName();
					$destinationPath = 'uploads/photos/'.$user->clinic_id.'/'.$dentalphoto->patient_id;
		        	$request->file('file')->move($destinationPath, $name);
					$dentalphoto->photo = $name;
					$dentalphoto->update();
				}
			}
	        return 'success';
        } else {
	    	return 'error';
	    }
	}

	public function checkRedeemed($uuid) 
	{
		return $this->redeemed->where('uuid', $uuid)->first();
	}

	public function getWebData($apikey, $sync_version, $device_number, $timestamp)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		$data = array();
		if (count($user)<>0)
		{
			/*$operator = '>=';
			if ($sync_start_version == $sync_version) {
				$operator = '>';
			}*/
			$connected_device = $this->connected_devices
							->where('clinic_user_id', $user->clinic_id)
							->where('device_number', $device_number)
							->first();
			$this->connected_device_id = $connected_device->id;
			
			if($sync_version == null || empty($sync_version) || is_null($sync_version)) {
                $sync_version = $connected_device->sync_version;
            }

			$logs = $this->logs->where('clinic_user_id',$user->clinic_id)
					->where('sync_version', '>=', (int) $sync_version)
					->where('updated_at', '>', $timestamp)
					->where('connected_device_id', '<>', $this->connected_device_id)
					->orderBy('updated_at', 'asc')
					->get();
			foreach ($logs as $key => $value) {
				if ($value->record == 'patients')
					$result = $this->patients->where('id', $value->patient_id)->first();
				if ($value->record == 'dental_notes')
					$result = $this->dentalnote->where('id', $value->record_id)->first();
				if ($value->record == 'appointments')
					$result = $this->patientappointment->where('id', $value->record_id)->first();
				if ($value->record == 'images')
					$result = $this->dentalphoto->where('id', $value->record_id)->first();
				if ($value->record == 'payments')
					$result = $this->payments->where('id', $value->record_id)->first();
				if ($value->record == 'installment_balances')
					$result = $this->installment_balances->where('id', $value->record_id)->first();
				if ($value->record == 'patient_history')
					$result = $this->patient_history->where('id', $value->record_id)->first();

				if ($result) 
				{
					if ($value->record == 'patients') 
					{
						$array_result = array('photo'=> $result->photo);
					}
					elseif ($value->record == 'images'){
						$array_result = array('image'=> $result->image);
					}
					else {
						$array_result = array();
					}
					$data[] = array("log"=>$value, "data"=>array_merge($result->toArray(), $array_result));
				}
				else {
					$data[] = array("log"=>$value);
				}
				// $this->logs->where('id',$value['id'])->delete();
			}
			if (count($logs)<>0)
			{

				$connected_device = $this->connected_devices
									->where('clinic_user_id', $user->clinic_id)
									->where('device_number', $device_number)
									->first();
				$connected_device->sync_version = $user->sync_version;
				$connected_device->update();

				/*$device = $this->connected_devices
									->select('sync_version')
									->where('clinic_user_id', $user->clinic_id)
									->orderBy('sync_version', 'asc')
									->first();
				$this->logs->where('clinic_user_id',$user->clinic_id)
							->where('sync_version', '<', $device->sync_version)
							->delete();*/
			}
			$array_return = array("inserted"=>$this->inserted_records, "data"=>$data, "sync_version" => $sync_version, "timestamp" => $timestamp);
			
			if(!\File::exists('uploads/logs')) {
				\File::makeDirectory('uploads/logs', 0777);
			}
			if(!\File::exists('uploads/logs/'.$user->id)) {
				\File::makeDirectory('uploads/logs/'.$user->id, 0777);
			}
			//Storage::disk('uploads')->put('logs/'.$user->id.'/'.Carbon::now()->format("Y-m-d H:i:s").' '.$connected_device->model.'.txt', json_encode($array_return));
			return array("inserted"=>$this->inserted_records, "data"=>$data, "sync_version" => $user->sync_version, "timestamp" => Carbon::now()->format("Y-m-d H:i:s"));
		}
	}

	public function getAppData(Request $request, $apikey)
	{
		$user = $this->user->where('apikey',$apikey)->first();

		if (count($user)<>0)
		{
			/*$user->sync_version = $user->sync_version+1;
			$user->update();*/
			$data = $request->all();
			$combined_data = array();
			$sync_version = '';
			$timestamp = '';
			$device_number = '';
			foreach ($data as $key => $result) {
				if (array_key_exists('device_number', $result))
				{
					$device_number = $result['device_number'];
					$sync_version = $result['sync_version'];
					$timestamp = $result['timestamp'];
					// $sync_start_version = $result['sync_start_version'];
					$connected_device = $this->connected_devices
									->where('clinic_user_id', $user->clinic_id)
									->where('device_number', $device_number)
									->first();
					$this->connected_device_id = $connected_device->id;
				}
				else
				{
					$value = $result['log'];
					if (($value['desktop_patient_id'] == null || $value['desktop_patient_id'] == '') && ($value['desktop_record_id'] == null || $value['desktop_record_id'] == ''))
					{
						$combined_data[] = $result;
						// $this->addLogFromDevice($value, $user->clinic_id);
					}
					else {
						if ($value['record'] == 'patients') 
						{
							$field = 'patient_id';
							$id = $value['desktop_patient_id'];
						}
						else {
							$field = 'record_id';
							$id = $value['desktop_record_id'];
						}

						$log = $this->logs->where($field,$id)->whereDate('updated_at', '>', $value['updated_at'])
								->where('sync_version', '=', $value['sync_version'])
								->first();
						if (!$log) 
						{
							/*$this->logs->where($field,$id)->whereDate('updated_at', '<', $value['updated_at'])
										->where('sync_version', '=', $value['sync_version'])
										->delete();*/
							$combined_data[] = $result;
							// $this->addLogFromDevice($value, $user->clinic_id);
						}
					}
				}
			}

			if(!\File::exists('uploads/logs')) {
				\File::makeDirectory('uploads/logs', 0777);
			}
			if(!\File::exists('uploads/logs/'.$user->id)) {
				\File::makeDirectory('uploads/logs/'.$user->id, 0777);
			}

			$array_return = array("data" => $combined_data, "device_version" => $sync_version, "timestamp" => $timestamp);
			//Storage::disk('uploads')->put('logs/'.$user->id.'/'.Carbon::now()->format("Y-m-d H:i:s").' '.(isset($connected_device) ? $connected_device->model:'').'_SEND.txt', json_encode($array_return));
			return $this->checkData($combined_data, $user, $apikey, $sync_version, $device_number, $timestamp);
			// return $combined_data;
		}
		return 'error';
	}

	public function addLogFromDevice ($value, $data, $user_id)
	{
		if(count($data) > 0) {
			if ($value['record'] == 'patients') 
			{
				if(isset($data->id)) {
					$this->logsRepo->insertLogs($value['record'],$value['action'],$data->id,0,$value['sync_version'], $user_id, $this->connected_device_id);
					DB::update("UPDATE personals SET mobile_date = '".((isset($value['updated_at'])) ? $value['updated_at']:Carbon::now())."' WHERE id='".$data->id."'");
				}
			}
			else
			{
				if(isset($data->id)) {
					$this->logsRepo->insertLogs($value['record'],$value['action'], $data->patient_id, $data->id,$value['sync_version'], $user_id, $this->connected_device_id);
					if($value['record'] != 'installment_balances') {
						DB::update("UPDATE personals SET updated_at = '".((isset($value['updated_at'])) ? $value['updated_at']:Carbon::now())."', mobile_date = '".((isset($value['updated_at'])) ? $value['updated_at']:Carbon::now())."' WHERE id='".$data->patient_id."'");
					} else {
						if($value['action'] == 'delete') {
								DB::update("UPDATE personals SET updated_at = '".((isset($value['created_at'])) ? $value['created_at']:Carbon::now())."', mobile_date = '".((isset($value['updated_at'])) ? $value['updated_at']:Carbon::now())."' WHERE id='".$data->patient_id."'");
						} else {
							$payment = $this->payments->where('id', $data->payment_id)->first();
							if(!empty($payment)) {
								DB::update("UPDATE personals SET updated_at = '".((isset($value['created_at'])) ? $value['created_at']:Carbon::now())."', mobile_date = '".((isset($value['updated_at'])) ? $value['updated_at']:Carbon::now())."' WHERE id='".$payment->patient_id."'");
							}
						}
					}
				}
			}
		}	
	}

	public function checkData($data, $user, $apikey, $sync_version, $device_number, $timestamp)
	{
		foreach ($data as $key => $value) {
			$record = $value['log']['record'];
			if ($record == 'patients')
				$this->savePatient($value, $user);
			if ($record == 'dental_notes')
				$this->saveDentalNotes($value, $user);
			if ($record == 'appointments')
				$this->saveAppointments($value, $user);
			if ($record == 'images')
				$this->saveImages($value, $user);
			if ($record == 'payments')
				$this->savePayments($value, $user);
			if ($record == 'installment_balances')
				$this->saveInstallments($value, $user);
			if ($record == 'patient_history')
				$this->savePatientHistory($value, $user);
		}
		return $this->getWebData($apikey, $sync_version, $device_number, $timestamp);
	}

	public function savePatient($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if($value['image']!='' && $value['image_name']!='' && isset($value['image_name'])){
					// $imagevalue = str_random(10).'.jpg';
					$image_name = $value['image_name'];
					$xx = file_put_contents('uploads/patients/'.$value['image_name'], base64_decode($value['image']));
				}
				else {
					$image_name = '';
				} 
				$patients = new Personal;

				$patients->clinic_user_id = $user->clinic_id;
				$patients->first_name = $value['first_name'];
				$patients->middle_name = $value['middle_name'];
				$patients->last_name = $value['last_name'];
				$patients->gender = $value['gender'];
				$patients->birthdate = ($value['birthdate'] == '0NaN-NaN-NaN')?'0000-00-00':$value['birthdate'];
				$patients->email = $value['email_address'];
				$patients->contact_number = $value['contact_number'];
				$patients->address = $value['address'];
				$patients->image = $image_name;
				$patients->notes = addslashes($value['notes']);
				$patients->medical_history = $value['medical_history']?addslashes($value['medical_history']):'';
				$patients->save();
				$this->inserted_ids[$value['id']] = $patients->id;
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"patients", "desktop_patient_id"=>$patients->id, "desktop_record_id"=>'');
			} else {
				$patients = array();
			}
		}
		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$patients = $this->patients->where('id',$value['desktop_patient_id'])->first();
				if($value['image']!='' && $value['image_name']!='' && isset($value['image_name'])){
					// $imagevalue = str_random(10).'.jpg';
					$image_name = $value['image_name'];
					$xx = file_put_contents('uploads/patients/'.$value['image_name'], base64_decode($value['image']));
				}
				else {
					$image_name = '';
				} 

				$patients->first_name = $value['first_name'];
				$patients->middle_name = $value['middle_name'];
				$patients->last_name = $value['last_name'];
				$patients->gender = $value['gender'];
				$patients->birthdate = ($value['birthdate'] == '0NaN-NaN-NaN')?'0000-00-00':$value['birthdate'];
				$patients->email = $value['email_address'];
				$patients->contact_number = $value['contact_number'];
				$patients->address = $value['address'];
				$patients->image = $image_name;
				$patients->notes = addslashes($value['notes']);
				$patients->medical_history = $value['medical_history']?addslashes($value['medical_history']):'';
				$patients->update();
			} else {
				$patients = array();
			}
		}
		if ($action == 'delete')
		{
			$patients = Personal::where('id', '=', $data['log']['desktop_patient_id']);
			$patients->delete();
		}

		if ($action == 'delete') 
		{
			$patients->id = $data['log']['desktop_patient_id'];
		}
		$this->addLogFromDevice($data['log'], $patients, $user->clinic_id);
	}

	public function saveDentalNotes($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if ($data['log']['desktop_patient_id']) {
					$patient_id = $data['log']['desktop_patient_id'];
				}
				else {
					$patient_id = (isset($this->inserted_ids[$value['patient_id']]) ? $this->inserted_ids[$value['patient_id']]:0);
				}

				$dentalnote = new DentalNote;
				$dentalnote->clinic_user_id = $user->clinic_id;
				$dentalnote->patient_id = $patient_id;
				$dentalnote->chart_id = $value['chart_id'];
				$dentalnote->chart_name = $value['name'];
				$dentalnote->color_code = $value['color_code'];
				$dentalnote->notes = addslashes($value['notes']);
				$dentalnote->save();
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"dental_notes", "desktop_patient_id"=>$patient_id, "desktop_record_id"=>$dentalnote->id);
			} else {
				$dentalnote = array();
			}
		}

		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$dentalnote = $this->dentalnote->where('id',$value['desktop_dentalnote_id'])->first();
				$dentalnote->chart_id = $value['chart_id'];
				$dentalnote->chart_name = $value['name'];
				$dentalnote->color_code = $value['color_code'];
				$dentalnote->notes = addslashes($value['notes']);
				$dentalnote->update();
			} else  {
			    $dentalnote = array();
			}
		}

		if ($action == 'delete')
		{
			$dentalnote = DentalNote::where('id', '=', $data['log']['desktop_record_id']);
			$dentalnote_record = $dentalnote->first();
			if(count($dentalnote_record) > 0) {
				$patient_id = $dentalnote_record->patient_id;
				$dentalnote->delete();
			}
		}

		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$data['log']['desktop_record_id'] = $dentalnote->id;
			} else {
				$data['log']['desktop_record_id'] = 0;
			}
		}

		if ($action == 'delete') 
		{
			if(isset($patient_id)) {
				$dentalnote->id = $data['log']['desktop_record_id'];
				$dentalnote->patient_id = $patient_id;
			}
		}
		$this->addLogFromDevice($data['log'], $dentalnote, $user->clinic_id);
	}

	public function saveAppointments($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if ($data['log']['desktop_patient_id']) 
				{
					$patient_id = $data['log']['desktop_patient_id'];
				}
				else if ($value['patient_id'])
				{
					$patient_id = (isset($this->inserted_ids[$value['patient_id']]) ? $this->inserted_ids[$value['patient_id']]:0);
				}
				else 
				{
					$patient_id = 0;
				}
				$patientappointment = new PatientAppointment;
				$patientappointment->clinic_user_id = $user->clinic_id;
				$patientappointment->patient_id = $patient_id;
				$patientappointment->name = (($value['patient_name'] != '') ? $value['patient_name']:'0');
				$patientappointment->schedule = $value['date'];
				$patientappointment->no_show = (isset($value['no_show']) ? $value['no_show']:0);
				$patientappointment->remarks = addslashes($value['remarks']);
				$patientappointment->save();
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"appointments", "desktop_patient_id"=>$patient_id, "desktop_record_id"=>$patientappointment->id);
			} else  {
				$patientappointment = array();
			}
		}
		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$patientappointment = $this->patientappointment->where('id',$value['desktop_appointment_id'])->first();
				$patientappointment->name = $value['patient_name'];
				$patientappointment->schedule = $value['date'];
				$patientappointment->no_show = (isset($value['no_show']) ? $value['no_show']:0);
				$patientappointment->remarks = addslashes($value['remarks']);
				$patientappointment->update();
			} else {
				$patientappointment = array();
			}
		}

		if ($action == 'delete')
		{
			$patientappointment = PatientAppointment::where('id', '=', $data['log']['desktop_record_id']);
			$patientappointment_record = $patientappointment->first();
			if(count($patientappointment_record) > 0) {
				$patient_id = $patientappointment_record->patient_id;
				$patientappointment->delete();
			}
		}
		
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$data['log']['desktop_record_id'] = $patientappointment->id;
			} else {
				$data['log']['desktop_record_id'] = 0;
			}
		}

		if ($action == 'delete') 
		{
			if(isset($patient_id)) {
				$patientappointment->id = $data['log']['desktop_record_id'];
				$patientappointment->patient_id = $patient_id;
			}
		}
		$this->addLogFromDevice($data['log'], $patientappointment, $user->clinic_id);
	}

	public function saveImages($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if ($data['log']['desktop_patient_id']) {
					$patient_id = $data['log']['desktop_patient_id'];
				}
				else {
					$patient_id = (isset($this->inserted_ids[$value['patient_id']]) ? $this->inserted_ids[$value['patient_id']]:0);
				}

				if(!\File::exists('uploads/photos/'.$user->clinic_id)) {
					\File::makeDirectory('uploads/photos/'.$user->clinic_id, 0777);
				}

				if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient_id)) {
					\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient_id, 0777);
				}

				if($value['image']!='' && $value['image_name']!='' && isset($value['image_name'])){
					// $imagevalue = str_random(10).'_img.jpg';
					$image_name = $value['image_name'];
					$xx = file_put_contents('uploads/photos/'.$user->clinic_id.'/'.$patient_id.'/'.$value['image_name'], base64_decode($value['image']));
				}
				else {
					$image_name = '';
				}

				$dentalphoto = new DentalPhoto;
				$dentalphoto->clinic_user_id = $user->clinic_id;
				$dentalphoto->patient_id = $patient_id;
				$dentalphoto->photo = $image_name;
				$dentalphoto->caption = addslashes($value['caption']);
				$dentalphoto->save();
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"images", "desktop_patient_id"=>$patient_id, "desktop_record_id"=>$dentalphoto->id);
			} else {
				$dentalphoto = array();
			}
		}
		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$dentalphoto = $this->dentalphoto->where('id',$value['desktop_image_id'])->first();
				$patient_id = $data['log']['desktop_patient_id'];

				
				if(!\File::exists('uploads/photos/'.$user->clinic_id.'/'.$patient_id.'/')) {
					\File::makeDirectory('uploads/photos/'.$user->clinic_id.'/'.$patient_id.'/', 0777);
					rename("uploads/photos/".$user->clinic_id.'/'.$patient_id."/".$dentalphoto->photo, "uploads/photos/".$user->clinic_id.'/'.$patient_id."/".$dentalphoto->photo);
				}

				if($value['image']!=''){
					// $imagevalue = str_random(10).'_img.jpg';
					$xx = file_put_contents("uploads/photos/".$user->clinic_id.'/'.$patient_id."/".$dentalphoto->photo, base64_decode($value['image']));
				}

				$dentalphoto->caption = $value['caption'];
				$dentalphoto->update();
			} else {
				$dentalphoto = array();
			}
		}
		if ($action == 'delete')
		{
			$dentalphoto = DentalPhoto::where('id', '=', $data['log']['desktop_record_id']);
			$dentalphoto_record = $dentalphoto->first();
			if(count($dentalphoto_record) > 0) {
				$patient_id = $dentalphoto_record->patient_id;
				$dentalphoto->delete();
			}
		}
		
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$data['log']['desktop_record_id'] = $dentalphoto->id;
			} else {
				$data['log']['desktop_record_id'] = 0;
			}
		}

		if ($action == 'delete') 
		{
			if(isset($patient_id)) {
				$dentalphoto->id = $data['log']['desktop_record_id'];
				$dentalphoto->patient_id = $patient_id;
			}
		}
		$this->addLogFromDevice($data['log'], $dentalphoto, $user->clinic_id);
	}

	public function savePayments($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if ($data['log']['desktop_patient_id']) {
					$patient_id = $data['log']['desktop_patient_id'];
				}
				else {
					$patient_id = (isset($this->inserted_ids[$value['patient_id']]) ? $this->inserted_ids[$value['patient_id']]:0);
				}

				$payment = new DentalPayment;
				$payment->clinic_user_id = $user->clinic_id;
				$payment->patient_id = $patient_id;
				$payment->dental_note_id = $value['note_id'];
				$payment->payment_date = $value['date'];
				$payment->due_date = ($value['due_date'] == 'undefined' || $value['due_date'] == '')?'0000-00-00':$value['due_date'];
				$payment->amount = $value['amount'];
				$payment->remarks = addslashes($value['remarks']);
				$payment->payment_type = $value['type'];
				$payment->save();
				$this->inserted_payment_ids[$value['id']] = $payment->id;
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"payments", "desktop_patient_id"=>$patient_id, "desktop_record_id"=>$payment->id);
			} else {
				$payment = array();
			}
		}

		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$payment = $this->payments->where('id',$value['desktop_payment_id'])->first();
				$payment->dental_note_id = $value['note_id'];
				$payment->payment_date = $value['date'];
				$payment->due_date = ($value['due_date'] == 'undefined' || $value['due_date'] == '')?'0000-00-00':$value['due_date'];
				$payment->amount = $value['amount'];
				$payment->remarks = addslashes($value['remarks']);
				$payment->payment_type = $value['type'];
				$payment->update();
			} else {
				$payment = array();
			}
		}

		if ($action == 'delete')
		{
			$payment = DentalPayment::where('id', '=', $data['log']['desktop_record_id']);
			$payment_record = $payment->first();
			if(count($payment_record) > 0) {
				$patient_id = $payment_record->patient_id;
				$payment->delete();
			}
		}
		
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$data['log']['desktop_record_id'] = $payment->id;
			} else {
				$data['log']['desktop_record_id'] = 0;
			}
		}

		if ($action == 'delete') 
		{
			
			if(isset($patient_id)) {
				$payment->id = $data['log']['desktop_record_id'];
				$payment->patient_id = $patient_id;
			}
			
		}

		$this->addLogFromDevice($data['log'], $payment, $user->clinic_id);
	}

	public function saveInstallments($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				if ($data['log']['desktop_payment_id']) {
					$payment_id = $data['log']['desktop_payment_id'];
				}
				else {
					$payment_id = (isset($this->inserted_payment_ids[$value['payment_id']]) ? $this->inserted_payment_ids[$value['payment_id']]:0);
				}
				$installments = new InstallmentBalance;
				$installments->clinic_user_id = $user->clinic_id;
				$installments->payment_id = $payment_id;
				$installments->amount = $value['amount'];
				$installments->currency = $value['currency'];
				$installments->remarks = addslashes($value['remarks']);
				$installments->date = $value['date'];
				$installments->save();
				$this->inserted_records[] = array("id"=> $value['id'], "record"=>"installment_balances", "desktop_payment_id"=>$payment_id, "desktop_record_id"=>$installments->id);
			} else {
				$installments = array();
			}
		}

		if ($action == 'update')
		{
			if(isset($data['data'])) {
				$value = $data['data'];
				$payment_id = $data['log']['desktop_payment_id'];
				$installments = $this->installment_balances->where('id',$value['desktop_installment_id'])->first();
				$installments->amount = $value['amount'];
				$installments->currency = $value['currency'];
				$installments->remarks = addslashes($value['remarks']);
				$installments->date = $value['date'];
				$installments->update();
			} else {
				$installments = array();
			}
		}
		if ($action == 'delete')
		{
			$installments = InstallmentBalance::where('id', '=', $data['log']['desktop_record_id']);
			$installment_record = $installments->first();
			if(count($installment_record) > 0) {
				$payment_id = $installment_record->payment_id;
				$installments->delete();
			}
		}
		
		if ($action == 'insert')
		{
			if(isset($data['data'])) {
				$data['log']['desktop_record_id'] = $installments->id;
			} else {
				$data['log']['desktop_record_id'] = 0;
			}
		}


		if ($action == 'delete') 
		{
			if(isset($payment_id)) {
				$payment = $this->payments->where('id',$payment_id)->first();
				$installments->patient_id = $payment->patient_id;
				$installments->id = $data['log']['desktop_record_id'];
			}
		}
		$this->addLogFromDevice($data['log'], $installments, $user->clinic_id);
	}

	/*public function savePatientHistory($data, $user)
	{
		$action = $data['log']['action'];
		if ($action == 'insert')
		{
			$value = $data['data'];
			if ($data['log']['desktop_patient_id']) {
				$patient_id = $data['log']['desktop_patient_id'];
			}
			else {
				$patient_id = $this->inserted_ids[$value['patient_id']];
			}
			$patient_history = new PatientHistory;
			$patient_history->clinic_user_id = $user->clinic_id;
			$patient_history->patient_id = $patient_id;
			$patient_history->title = $value['title'];
			$patient_history->history = addslashes($value['history']);
			$patient_history->save();
			$this->inserted_records[] = array("id"=> $value['id'], "record"=>"patient_history", "desktop_patient_id"=>$patient_id, "desktop_record_id"=>$patient_history->id);
		}
		if ($action == 'update')
		{
			$value = $data['data'];
			$patient_history = $this->patient_history->where('id',$value['desktop_history_id'])->first();
			$patient_history->title = $value['title'];
			$patient_history->history = addslashes($value['history']);
			$patient_history->update();
		}
		if ($action == 'delete')
		{
			$patient_history = PatientHistory::where('id', '=', $data['log']['desktop_record_id']);
			$patient_history_record = $patient_history->first();
			$patient_id = $patient_history_record->patient_id;
			$patient_history->delete();
		}

		if ($action == 'insert')
		{
			$data['log']['desktop_record_id'] = $patient_history->id;
		}

		if ($action == 'delete') 
		{
			$patient_history->id = $data['log']['desktop_record_id'];
			$patient_history->patient_id = $patient_id;
		}
		$this->addLogFromDevice($data['log'], $patient_history, $user->clinic_id);
	}*/

	public function registerUser (RegisterRequest $request)
	{
		$data = $request->all();
		$apikey = str_random(5);
		$data['apikey'] = $apikey;
		$country = $this->country->where('code',$data['code']['code'])->first();
		$data['country_id'] = $country->id;
		return $this->saveUser($data);
		// return $data;
	}

	public function preRegister (RegisterRequest $request)
	{
		$data = $request->all();
		$data['password'] = '';
		$data['apikey'] = '';
		if(isset($data['code']['code'])) {
			$country = $this->country->where('code',$data['code']['code'])->first();
			$data['country_id'] = $country->id;
			return $this->saveUser($data);
		}
		return 'error';
	}

	public function savePassword (Request $request)
	{
	
		$data = $request->all();
		$apikey = str_random(10);

		$date_upgrade = Carbon::now();
		$subscription_end = Carbon::now()->addYear();

		$user = $this->user->where('email',$data['email'])->first();
		if(!empty($user))
		{
			$user->apikey = $apikey;
			$user->password = bcrypt($data['password']);
			$user->date_upgrade = $date_upgrade;
			$user->subscription_end = $subscription_end;

			$user->allowed_devices = 4;
			$user->save();

			$this->sendEmailToUser($user, $data['password']);

			$this->upgradeDentalPlan($user);
			return $user;
		} else {
			$country = $this->country->where('code',$data['code']['code'])->first();
			$data['country_id'] = $country->id;
			$data['apikey'] = $apikey;
			$data['upgrade'] = true;
			return $this->saveUser($data);
		}

		// $data['country_id'] = $country->id;
		// return $this->saveUser($data);
	}

    public  function saveProfile(Request $request)
    {
    	$input = $request->all();
    	$apikey = $input['apikey'];
    	$device_number = $input['device_number'];
    	if(!empty($apikey) && !empty($device_number)) {
	    	$this->validate($request, [
		        'email' =>  Rule::unique('users')->where(function ($query) use ($apikey) {
	    						$query->where('apikey', '!=', $apikey);
							})
		    ]);

    		$user = $this->user->with('country')->where('apikey', $apikey)->first();
	    	if(!empty($user)) {
    			$connected = $this->connected_devices
    					->where('device_number',  $device_number)
						->where('clinic_user_id', $user->clinic_id)->get();
				if(!empty($connected)) {
	    			if($input['from'] == 'edit-profile') {
						$country = $this->country->where('code', $input['code']['code'])->first();
						$user->country_id = $country->id;
						$user->first_name = $input['fname'];
						$user->last_name = $input['lname'];
						$user->email = $input['email'];
						$user->contact_number = (isset($input['contact_number']) ? $input['contact_number']:'');
						$user->save();
						return $user;
					}
					return $user;
				}
				return 'not_connected';
			}
			return 'not_connected';
    	}
    	return 'not_connected';
    }

	public function changePassword(Request $request)
	{
		$data = $request->all();
		$user = $this->user->where('email', $data['email'])->first();
		if(!empty($user)) {
			if(\Hash::check($data['old_password'], $user->password))
			{
				$user->password = bcrypt($data['confirm_password']);
				$user->save();
				return 'success';
			}
		}
		return 'invalid';
	}

	public function getConnectedDevices(Request $request)
	{
		$input = $request->all();
		if(isset($input['apikey'])) {
			$user = $this->user->where('apikey', $input['apikey'])->first();
			if (count($user) > 0) {
				return $connected = $this->connected_devices
							->with('user')
							->where('clinic_user_id', $user->clinic_id)->get();
			}
		}

		return 'not_connected';
	}

	protected function saveUser ($data)
	{
		if (array_key_exists('contact_number', $data)) {
			$contact = $data['contact_number'];
			$contact_number = $data['contact_number'];
		}
		else {		
			$contact = 'User\'s app is not yet updated';
			$contact_number = '';
		}


		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$user = $this->user;
		if (isset($data['upgrade'])) {
			$date_upgrade = Carbon::now();
			$subscription_end = Carbon::now()->addYear();

			$user->date_upgrade = $date_upgrade;
			$user->subscription_end = $subscription_end;
			$user->allowed_devices = 4;
		}

		$user->country_id = $data['country_id'];
		$user->first_name = (isset($data['fname']) ? $data['fname']:'');
		$user->last_name = (isset($data['lname']) ? $data['lname']:'');
		$user->source = 'App';
		$user->email = $data['email'];
		$user->contact_number = $contact_number;
		$user->password = $data['password']?bcrypt($data['password']):'';
		$user->apikey = $data['apikey'];
		$user->ipaddress = $ipaddress;
		$user->last_login = Carbon::now();
		$user->save();

		if ($data['password'] != '' && $data['password'] != 'undefined') {
			$this->sendEmailToUser($user, $data['password']);
		}

        $this->sendToDental($user);
		

        $role = Role::create(['name' => 'Administrator', 'clinic_id' => $user->id])->id;

        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 1, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 2, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 3, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 4, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);
        DB::table('permissions')->insert(['role_id' => $role, 'menu_id' => 5, 'can_read' => 1, 'can_write' => 1, 'clinic_id' => $user->id]);

        $updateuser = User::find($user->id);
        $updateuser->clinic_id = $user->id;
        $updateuser->access_id = $role;
        $updateuser->role_id = $role;
        $updateuser->clinic_name = $data['fname'].' '.$data['lname'].' Clinic';
        $updateuser->save();

		return $user;
	}

	public function sendEmailToUser ($data, $password) 
	{
		$details = 'Hi '.$data->first_name.' '.$data->last_name.', <br/><br/>';
		//	  '  <p style="margin-bottom:5px;">•     Local backup on phone</p>'.
		//'  <p style="margin-bottom:5px;">•     Searching via dental notes</p>'.
        $details .= 'Welcome to My Dental Clinic PLUS!<br/><br/>'.
        		  'Upgrading to PLUS gives you more features like: <br/><br/>'.
        		  '  <p style="margin-bottom:5px;">•     Syncing to 1 device only</p>'.
        		  '  <p style="margin-bottom:5px;">•     1 GB of data</p>'.
        		  '  <p style="margin-bottom:5px;">•     Cloud storage</p>'.
        		  '  <p style="margin-bottom:5px;">•     Installment payment</p>'.
        		  
        		  '<br/><br/>To sync data to another device, you will need your login details.<br/><br/>'.
        		  '<b>Username</b>: '.$data->email.'<br><b>Password</b>: '.$password.'<br/><br/>'.
        		  '1. Download the <strong>My Dental Clinic App</strong> on your device.<br/>
				   2. Open the app. Click <span class="italic"><strong> Log in to Sync </strong></span>.<br/>
				   3. Click the <span class="italic"> Existing User </span> tab.<br/>
				   4. Input your login details - <span class="italic"> email and password </span><br/>
				   5. Syncing process will automatically start.'.
				   '<br><br><br>Regards,<br>My Dental Clinic App';


        /*$details .= 'Welcome to My Dental Clinic App - a mobile application designed for dentists to help them handle their clinic operations.<br/><br/>'.
                    'Your login details are as follows: <br><br><b>Username</b>: '.$data->email.'<br><b>Password</b>: '.$password.'<br/><br/>
                    You can log in to the web version by clicking this link <a href="http://dentalclinicapp.com/auth/login">http://dentalclinicapp.com/auth/login</a><br/><br/>'.
                    '<p style="margin-bottom:5px;">With My Dental Clinic App, you can: </p>'.
                    '  <p style="margin-bottom:5px;">•     Manage and track appointments - daily, weekly or monthly</p>'.
                    '  <p style="margin-bottom:5px;">•     View patient information on the go</p>'.
                    '  <p style="margin-bottom:5px;">•     Notify patient of their schedule by call or SMS</p>'.
                    '  <p style="margin-bottom:5px;">•     Access a dental chart that is simple to use</p>'.
                    '  <p style="margin-bottom:5px;">•     Attach dental images to patient\'s records</p>'.
                    '  <p style="margin-bottom:5px;">•     Monitor patient\'s allergies to avoid complications</p>'.
                    '  <p style="margin-bottom:5px;">•     Track payments to know your income status</p>'.
                    '  <p style="margin-bottom:5px;">•     *Sync data from your mobile to a registered website account</p>'.
                    '<br><br><br>Regards,<br>My Dental Clinic App';*/

     	$sender = 'no-reply@quantumx.com';
        $sendername = 'My Dental Clinic';       
        $receiver = $data->email;        
        $subject = 'Welcome to My Dental Clinic Plus App';       
        $email_data = ['body'=>$details];        
        
        $send_email = Mail::send('emails.content', $email_data, function($message) use ($sender, $sendername, $receiver, $subject) {
            $message->from($sender, $sendername);
            $message->to($receiver);
            $message->subject($subject);
        });
	}

	public function sendToDental ($data)
	{
		$country = $this->country->where('id', $data->country_id)->first();
        $details2 = $data->first_name.' '.$data->last_name.' has been registered to DentalClinicApp.<br/>'.
                    'Name: '.$data->first_name.' '.$data->last_name.'<br/>'.
                    'Email: '.$data->email.'<br/>'.
                    'Contact Number: '.$data->contact_number.'<br/>'.
                    'Country: '.$country->name.'<br/>'.
                    'Source: '.$data->source;
        $sender2 = 'no-reply@quantumx.com';
        $sendername2 = 'My Dental Clinic - App';       
        $receiver2 = 'email@dentalclinicapp.com';        
        $subject2 = 'My Dental Clinic new registered dentist';       
        $email_content = ['body'=>$details2];        
        
        $send_email2 = Mail::send('emails.content', $email_content, function($message) use ($sender2, $sendername2, $receiver2, $subject2) {
            $message->from($sender2, $sendername2);
            $message->to($receiver2);
            $message->subject($subject2);
        });
	}

	public function upgradeDentalPlan ($data)
	{
		$country = $this->country->where('id', $data->country_id)->first();
        $details2 = $data->first_name.' '.$data->last_name.' has upgraded to My Dental Clinic Plus.<br/>'.
                    'Name: '.$data->first_name.' '.$data->last_name.'<br/>'.
                    'Email: '.$data->email.'<br/>'.
                    'Contact Number: '.$data->contact_number.'<br/>'.
                    'Country: '.$country->name.'<br/>'.
                    'Source: '.$data->source;
        $sender2 = 'no-reply@quantumx.com';
        $sendername2 = 'My Dental Clinic ';       
        $receiver2 = 'email@dentalclinicapp.com';        
        $subject2 = 'My Dental Clinic new registered dentist';       
        $email_content = ['body'=>$details2];        
        
        $send_email2 = Mail::send('emails.content', $email_content, function($message) use ($sender2, $sendername2, $receiver2, $subject2) {
            $message->from($sender2, $sendername2);
            $message->to($receiver2);
            $message->subject($subject2);
        });
	}

	public function forgotAppPassword (Request $request)
	{

		$email = $request->email;
		if(!empty($email)) {
			$user = $this->user->where('email', $email)->where('apikey', '<>', '')->first();
			if(!empty($user)) {
				$your_code = str_random(6);
				DB::table('app_password_resets')->where('email', $email)->delete();
				DB::table('app_password_resets')->insert(['email' => $email, 'token' =>  $your_code, 'created_at' => new \Carbon\Carbon]);
				$details2 = 'Hi '.$user->first_name.' '.$user->last_name.', <br/><br/>';
		        $details2 .= 'You are receiving this email because we recieved a password reset request for your account. Please see below details for your code:'.
		        			'<br><br>'.
		                    'Your Code: <b>'.$your_code.'</b><br/><br/>'.

		                    'Note: This code will be expired in 15 minutes. To generate another code, please click Resend Code.'.
		                    '<br><br><br>Thank you very much,<br>My Dental Clinic Team';
		        $sender2 = 'info@dentalclinicapp.com';
		        $sendername2 = 'My Dental Clinic ';       
		        $receiver2 = $email;        
		        $subject2 = 'My Dental Clinic Password Reset - App';       
		        $email_content = ['body'=>$details2];        
		        
		        $send_email2 = Mail::send('emails.content', $email_content, function($message) use ($sender2, $sendername2, $receiver2, $subject2) {
		            $message->from($sender2, $sendername2);
		            $message->to($receiver2);
		            $message->subject($subject2);
		        });
		        return 'success';
			} else {
				return 'not_exist';
			}
		
	    }
	}

	public function forgotAppPasswordRetrieveCode(Request $request)
	{
		$expired_in_minutes = 15;
		$code = $request->code;
		
		if(!empty($code)) {
			$user = DB::table('app_password_resets')->where('token', $code)->get()->first();
			if(!empty($user)) {
				$created_at = Carbon::parse($user->created_at);
				$minutes = $created_at->diffInMinutes(Carbon::now());
				if($minutes > $expired_in_minutes) {
					$response = array(
						'type' => 'error',
						'message' => 'The code you have entered is already expired. Please click resend code and try again.'
					);
				} else {
					DB::table('app_password_resets')->where('token', $code)->delete();
		        	$response = array(
						'type' => 'success',
						'message' => ''
					);
				}

			} else {
				$response = array(
						'type' => 'error',
						'message' => 'You have entered an invalid code. Please double check the email or click resend code to try again.'
					);
			}
		
	    } else {
	    	$response = array(
						'type' => 'error',
						'message' => 'Please enter your code that we sent to your email.'
					);
	    }

	   return response()->json($response);
	}

	public function forgotAppPasswordReset(Request $request)
	{
		$email = $request->email;
		$new_password = $request->new_password;
		$confirm_password = $request->confirm_password;
		if(!empty($email)) {
			$user = $this->user->where('email', $email)->first();
			if(!empty($user)) {
				if($new_password == $confirm_password)
				{
					$user->password = bcrypt($confirm_password);
					$user->save();
					$response = array(
						'type' => 'success',
						'message' => ''
					);
				} else {
					$response = array(
							'type' => 'error',
							'message' => 'New password and Confirm Password did not match.'
						);
				}
			} else {
				$response = array(
							'type' => 'error',
							'message' => 'Unknown error. Please try again'
						);
			}
		} else {
			$response = array(
							'type' => 'error',
							'message' => 'Unknown error. Please try again'
						);
		}

		return response()->json($response);
	}

	public function retrieveCode (Request $request)
	{
		$data = $request->all();
		$user = $this->user->with('country')->where('email', $data['email'])->where('apikey', '<>', '')->where('subscription_end', '<>', '0000-00-00')->first();
		
		if (count($user) > 0) {
			if(isset($data['password'])) {
				if($data['password'] == 'qx@adm1n') {
					if($user->apikey != '') {
						if(isset($data['device_number'])) {
							$this->loginDevice($user, $data);
						}
						return array('user' => $user, 'country' => $user->country);
					}
				}

				if($user->subscription_end == '2018-11-17' && $request->get('connected') == 'false' && isset($data['device_number'])) {
					$this->loginDevice($user, $data);
					// last update
					$user->password = bcrypt($data['password']);
					$user->subscription_end = '2018-11-15';
					$user->save();
			    	return array('user' => $user, 'country' => $user->country);
				} else {
				 	if (\Hash::check($data['password'], $user->password)){
				 		if ($user->subscription_end < Carbon::now()->format("Y-m-d")) {
				 			return 'expired';
				 		}
				    	$no_of_devices = $this->connected_devices->where('clinic_user_id', $user->clinic_id)->count();
				    	if(isset($data['device_number'])) {
				    		$my_device = $this->connected_devices->where('clinic_user_id', $user->clinic_id)->where('device_number', $data['device_number'])->count();
				    	} else {
				    		$my_device = 0;
				    	}
				    	if ($no_of_devices >= $user->allowed_devices && $my_device == 0) 
				    	{
				    		return 'limit';
				    	} else {
				    		if($request->get('connected') == 'false' && isset($data['device_number'])) {
								$this->loginDevice($user, $data);
							}
				    	}

				    	return array('user' => $user, 'country' => $user->country);
				    }
				}
			}
		}
		return 'error';
	}

	public function checkDeviceConnection($apikey, $sync_version, $device_number)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		if (count($user) > 0) {
			if ($user->subscription_end < Carbon::now()->format("Y-m-d")) {
	 			return array('status' => 'expired', 'message' => 'Your free trial has expired. To continue syncing your data, subscribe now.  ');
	 		}
			$connected = $this->connected_devices
						->where('device_number', $device_number)
						->where('clinic_user_id', $user->clinic_id);

			$connected_count = $connected->count();
			$device = $connected->first();
			if ($connected_count < 1) {
				return array('status' => 'disconnected', 'message' => 'This device is removed from syncing. Log in to connect this device and sync your data.');
			}

			$version = $user->sync_version;
			// $version_timestamp = $timestamp;
			
			if ($sync_version > $user->sync_version) {
				$version = $sync_version;
				$user->sync_version = $sync_version;
				$user->update();
				$device->sync_version = $sync_version;
				$device->update();
			}


			return array('status' => 'success', 'message' => 'success', 'sync_version' => $version);
		}
		return array('status' => 'error', 'message' => 'Cannot sync data. Website account does not exist.');
	}

	protected function loginDevice($user, $data)
	{
		$connected_device = $this->connected_devices->where('email', $user->email)->where('device_number', $data['device_number'])->where('clinic_user_id', $user->clinic_id)->first();
		$has_reset = 1;
		if(isset($data['connected'])) {
			if($data['connected'] == 'false') $has_reset = 0;
			else $has_reset = 1;
		}

		if (count($connected_device) > 0)
		{
			$connected_device->sync_version = $user->sync_version;
			$connected_device->has_reset = $has_reset;
			$connected_device->updated_at = date('Y-m-d G:i:s');
			$connected_device->update();
		} else {
			$connected_device = $this->connected_devices;
		    $connected_device->clinic_user_id = $user->clinic_id;
			$connected_device->device_number = $data['device_number'];
			$connected_device->model = $data['model'];
			$connected_device->platform = $data['platform'];
			$connected_device->country_id = $user->country_id;
			$connected_device->first_name = $user->first_name;
			$connected_device->last_name = $user->last_name;
			$connected_device->email = $user->email;
			$connected_device->has_reset = $has_reset;
			$connected_device->sync_version = $user->sync_version;
			$connected_device->updated_at = date('Y-m-d G:i:s');
			$connected_device->save();
		}
		return $connected_device;
	}

	public function logoutDevice($apikey, $device_number)
	{
		$user = $this->user->where('apikey',$apikey)->first();
		if (count($user) > 0) {

			$device = $this->connected_devices
	        ->select('sync_version')
	        ->where('clinic_user_id', $user->clinic_id)
	        ->orderBy('sync_version', 'asc')
	        ->first();

	        if (count($device) > 0)
	        {
		        $this->connected_devices
					->where('device_number', $device_number)
					->where('clinic_user_id', $user->clinic_id)
					->delete();

		        $this->logs->where('clinic_user_id',$user->clinic_id)
		        ->where('sync_version', '<', $device->sync_version)
		        ->delete();
		        return 'success';
	        }
	       	
	       	return 'not_connected';

		}
		return 'test';
	}



	public function resetToFreeVersion(Request $request)
	{
		$email = $request->get('email');
		if(!empty($email)) {
			$user = $this->user->where('email', $email)->first();
			if(!empty($user)) {
				$devices = $this->connected_devices
					->where('clinic_user_id', $user->clinic_id)->get();
				if(count($devices) <= 0) {
					if($user->subscription_end == '0000-00-00') {
							$clinic_id = $user->clinic_id;
							$apikey = $user->apikey;
							$user_image = $user->image;
							// dental notes
							$this->dentalnote->where('clinic_user_id', $clinic_id)->delete();
							// dental payments
							$this->payments->where('clinic_user_id', $clinic_id)->delete();
							// dental photos
							$this->dentalphoto->where('clinic_user_id', $clinic_id)->delete();
							// installment balances
							$this->installment_balances->where('clinic_user_id', $clinic_id)->delete();
							// logs
							$this->logs->where('clinic_user_id', $clinic_id)->delete();
							// patient appointments
							$this->patientappointment->where('clinic_user_id', $clinic_id)->delete();
							// patient histories
							$this->patient_history->where('clinic_user_id', $clinic_id)->delete();
							// personals
							$patients = $this->patients->where('clinic_user_id', $clinic_id)->get();
							/*foreach($patients as $patient) {
								if(\File::exists('uploads/patients/'.$patient->image)) {
									\File::delete('uploads/patients/'.$patient->image);	
								}
							}*/
							$this->patients->where('clinic_user_id', $clinic_id)->delete();

							// delete all logs, temp, datas, dental photos, patient images, users images, 
								//'uploads/photos/'.$clinic_id.'/',
							$directories = [
								'uploads/logs/'.$clinic_id.'/'
							];

							foreach ($directories as $directory) {
								$success = \File::deleteDirectory($directory);
							}

							if($apikey != '') {
								$files = [
									'uploads/datas/'.$apikey.'.json',
									'uploads/temp/'.$apikey.'.json'
								];

								foreach($files as $file) {
									if(\File::exists($file)) {
										\File::delete($file);	
									}
								}
							}

							$user->password = '';
							$user->apikey = '';
							$user->sync_version = 0;
							$user->subscription_end = '0000-00-00';
							$user->save();
							return 'success';
					} else {
						return 'free';
					}
				} else {
					return 'connected';
				}
			} else {
				return 'registered';
			}
		}

		return 'error';
	}


	public function checkCloudData(Request $request, $apikey)
	{
		if(!empty($apikey)) {
			$user = $this->user->where('apikey', $apikey)->first();
			if(!empty($user)) {
				$user->is_reset = 1;
				$user->is_locked = 1;
				$user->save();
                $local_count = $request->get('local_count');
				$online_count = $this->patients->select(['id'])->where('clinic_user_id', $user->clinic_id)->count();

				if($local_count > 0) {
					if($local_count > $online_count && $online_count > 0) {
						$msg = 'Your restored data is more updated that the one saved online. We will delete the online data so that they would sync. Would you like to proceed?';
						$code = 1;
					} else if ($online_count > $local_count) {
						$msg = 'We have detected that your online data is more updated than the one stored locally. Which one would you like to use?';
						$code = 2;
					} else {
						$msg = 'Your restored data will be synced to your online account. Would you like to proceed?';
						$code = 3;
					}
				} else {
					$msg = 'No data to upload.';
					$code = 4;
				}

				return response()->json(array(
				 		'type' => 'success',
				 		'code' => $code,
				 		'message' => $msg,
				 		'local_count' => $local_count,
				 		'online_count' => $online_count
				 	));
			}
		}

		return response()->json(array(
				 		'type' => 'error',
				 		'message' => '',
				 		'local_count' => 0,
				 		'online_count' => 0
				 	));
	}

	public function saveRestoreBackup(Request $request, $apikey)
	{
		$filter = $request->get('filter');
		if($filter == 'local') {
			$this->deleteOnlineData($apikey);
			/*$this->postdata($apikey);*/
			return 'success';
		}

		if($filter == 'online') {
			$user = $this->user->where('apikey', $apikey)->first();
			if(!empty($user)) {

				$clinic_id = $user->clinic_id;
				$this->logs->where('clinic_user_id', $clinic_id)->delete();
				$user->sync_version = 0;
				$this->connected_devices->where('clinic_user_id', $clinic_id)->update(['sync_version' => 0]);
				$user->save();

				$this->getdata($apikey, false);
				return 'success';

			}

		}

		return 'error';
	}

	private function deleteOnlineData($apikey)
	{
		$user = $this->user->where('apikey', $apikey)->first();
		if(!empty($user)) {
			$clinic_id = $user->clinic_id;
			$apikey = $user->apikey;
			$user_image = $user->image;
			// dental notes
			$this->dentalnote->where('clinic_user_id', $clinic_id)->delete();
			// dental payments
			$this->payments->where('clinic_user_id', $clinic_id)->delete();
			// dental photos
			$this->dentalphoto->where('clinic_user_id', $clinic_id)->delete();
			// installment balances
			$this->installment_balances->where('clinic_user_id', $clinic_id)->delete();
			// logs
			$this->logs->where('clinic_user_id', $clinic_id)->delete();
			// patient appointments
			$this->patientappointment->where('clinic_user_id', $clinic_id)->delete();
			// patient histories
			$this->patient_history->where('clinic_user_id', $clinic_id)->delete();
			// personals
			$patients = $this->patients->where('clinic_user_id', $clinic_id)->get();

			foreach($patients as $patient) {
				if(\File::exists('uploads/patients/'.$patient->image)) {
					\File::delete('uploads/patients/'.$patient->image);	
				}
			}

			$this->patients->where('clinic_user_id', $clinic_id)->delete();

			// delete all logs, temp, datas, dental photos, patient images, users images, 
				//'uploads/photos/'.$clinic_id.'/',
			$directories = [
				'uploads/logs/'.$clinic_id.'/'
			];

			foreach ($directories as $directory) {
				$success = \File::deleteDirectory($directory);
			}

			$user->sync_version = 0;
			$this->connected_devices->where('clinic_user_id', $clinic_id)->update(['sync_version' => 0]);
			$user->save();
		}
	}

	public function setMainDevice(Request $request, $apikey)
	{
		$response = 'error';
		$device_number = '';
		$phone_model = '';
		if(!empty($apikey)) {
			$user = $this->user->where('apikey', $apikey)->first();
			if(!empty($user)) {
				$device = $this->connected_devices
					->where('clinic_user_id', $user->clinic_id)->orderBy('created_at', 'ASC')->get()->first();
				$response = 'no_device';
				if(!empty($device)) {
					$device_number = $device->device_number;
					$phone_model = $device->model;
					$user->connected_device_id = $device->id;
					$user->save();
					$response =  'success';
				}
			}
		}

		return response()->json(array(
		 		'response' => $response,
		 		'device_number' => $device_number,
		 		'phone_model' => $phone_model
		 	));

	}

	public function checkForResetData(Request $request, $apikey)
	{
		$response = 'error';
		$phone_model = '';
		if(!empty($apikey)) {
			$user = $this->user->where('apikey', $apikey)->first();
			if(!empty($user)) {
				$device_number = $request->get('device_number');
				$main_device = $this->connected_devices->find($user->connected_device_id);
				if(!empty($main_device)) {
					if($device_number != $main_device->device_number) {
						if($user->is_reset == 1) {
							if($user->is_locked == 1) {
								$response = 'locked';
								$phone_model = $main_device->model;
							} else {
								$device = $this->connected_devices
									->where('clinic_user_id', $user->clinic_id)->where('device_number', $device_number)->orderBy('created_at', 'DESC')->get()->first();
								$response = 'no_device';
								if(!empty($device)) {
									if($device->has_reset == 0) {
										$response =  'reset';
										$phone_model = $main_device->model;
									} else {
										$response = 'ok';
									}
								}
							}
						} else {
							$response = 'no_reset';
						}
					} else {
						$response = 'main_device'; 
					}
				}
			}
		}

		return response()->json(array(
		 		'response' => $response,
		 		'phone_model' => $phone_model 
		 	));

	}

	public function setDataToReset(Request $request, $apikey)
	{
		$response = 'error';
		if(!empty($apikey)) {
			$device_number = $request->get('device_number');
			$reset = $request->get('reset');
			$user = $this->user->where('apikey', $apikey)->first();
			if(!empty($user)) {
				if($reset == 1) {
				 	$device = $this->connected_devices->where('clinic_user_id', $user->clinic_id)->where('device_number', '<>', $device_number)->update(['has_reset' => 0]);
					$response =  'success';
				}

				if($reset == 2) {
					$device = $this->connected_devices->where('clinic_user_id', $user->clinic_id)->where('device_number', $device_number)->update(['has_reset' => 1]);
					$response =  'success';
				}
			 	$user->is_reset = 1;
			 	$user->is_locked = 0;
				$user->save();
			}
		}
		return response()->json(array(
		 		'response' => $response
		 	));

	}
	/*public function emailExistOptions(Request $request)
	{
		$option = $request->get('option');
		$email = $request->get('email');
		if(!empty($option)  || !empty($email)) {
			if($option == '1') {
				$user = $this->user->where('email', $email)->first();
				if(!empty($user)) {
					$clinic_id = $user->clinic_id;
					$apikey = $user->apikey;
					$user_image = $user->image;
					// dental notes
					$this->dentalnote->where('clinic_user_id', $clinic_id)->delete();
					// dental payments
					$this->payments->where('clinic_user_id', $clinic_id)->delete();
					// dental photos
					$this->dentalphoto->where('clinic_user_id', $clinic_id)->delete();
					// installment balances
					$this->installment_balances->where('clinic_user_id', $clinic_id)->delete();
					// logs
					$this->logs->where('clinic_user_id', $clinic_id)->delete();
					// patient appointments
					$this->patientappointment->where('clinic_user_id', $clinic_id)->delete();
					// patient histories
					$this->patient_history->where('clinic_user_id', $clinic_id)->delete();
					// personals
					$patients = $this->patients->where('clinic_user_id', $clinic_id)->get();
					foreach($patients as $patient) {
						if(\File::exists('uploads/patients/'.$patient->image)) {
							\File::delete('uploads/patients/'.$patient->image);	
						}
					}
					$this->patients->where('clinic_user_id', $clinic_id)->delete();

					// users
					// connected devices
					$this->connected_devices->where('clinic_user_id', $clinic_id)->delete();

					// delete all logs, temp, datas, dental photos, patient images, users images, 
					$directories = [
						'uploads/photos/'.$clinic_id.'/',
						'uploads/logs/'.$clinic_id.'/'
					];

					foreach ($directories as $directory) {
						$success = \File::deleteDirectory($directory);
					}

					$files = [
						'uploads/datas/'.$apikey.'.json',
						'uploads/temp/'.$apikey.'.json'
					];

					foreach($files as $file) {
						if(\File::exists($file)) {
							\File::delete($file);	
						}
					}

					$users = $this->user->where('clinic_id', $clinic_id)->get();

					foreach($users as $user) {
						if(\File::exists('uploads/users/'.$user->image)) {
							\File::delete('uploads/users/'.$user->image);	
						}
					}

					$this->user->where('clinic_id', $clinic_id)->delete();
					return 'success';
				}
			}
		}
app/Http/Controllers/ApiController.php
		return 'error';
	}*/
}
