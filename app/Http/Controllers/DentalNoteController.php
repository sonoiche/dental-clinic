<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\DentalnoteRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use Junity\Hashids\Facades\Hashids;

class DentalNoteController extends Controller
{
    protected $noteRepo;
    protected $logsRepo;

    function __construct(DentalnoteRepositoryEloquent $noteRepo, LogsRepositoryEloquent $logsRepo)
    {
    	$this->noteRepo = $noteRepo;
        $this->logsRepo = $logsRepo;
    }

    public function store(Request $request)
    {
    	$patient_id = Hashids::decode($request['patient_id']);
    	$makeRequest = [
    		'color_code' => $request['color_code'],
    		'notes' => $request['notes'],
    		'chart_id' => $request['chart_id'],
    		'chart_name' => $request['chart_name'],
    		'patient_id' => $patient_id[0],
    		'clinic_user_id' => \Auth::user()->clinic_id,
    		'clinic_id' => \Auth::user()->clinic_id
    	];

    	$id = $this->noteRepo->create($makeRequest);
    	$this->logsRepo->insertLogs('dental_notes','insert',$patient_id[0],$id);

    	return redirect()->route('patient.show', $request['patient_id'])->with('status','Dental Note has been Added!');
    }

    public function edit($id)
    {
    	$id = Hashids::decode($id);
    	return $this->noteRepo->find($id[0]);
    }

    public function update(Request $request, $id)
    {
    	$patient_id = Hashids::decode($request['patient_id']);
        $makeRequest = [
            'color_code' => $request['color_code'],
            'notes' => $request['notes'],
            'chart_id' => $request['chart_id'],
            'chart_name' => $request['chart_name'],
            'patient_id' => $patient_id[0],
            'clinic_user_id' => \Auth::user()->clinic_id,
            'clinic_id' => \Auth::user()->clinic_id
        ];
        
        $this->noteRepo->update($makeRequest, $id);
        $this->logsRepo->insertLogs('dental_notes','update',$patient_id[0],$id);
        
        return redirect()->route('patient.show', $request['patient_id'])->with('status','Dental Note has been Updated!');
    }

    public function destroy($id)
    {
    	$id = Hashids::decode($id);
        $note = $this->noteRepo->find($id[0]);
        $this->logsRepo->insertLogs('dental_notes','delete',$note->patient_id,$note->id);
    	$this->noteRepo->delete($note->id);
        return 'success';
    }
}
