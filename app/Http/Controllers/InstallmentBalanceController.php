<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Patient\PaymentRepositoryEloquent;
use App\Repositories\Patient\PatientRepositoryEloquent;
use App\Repositories\Patient\InstallmentRepositoryEloquent;
use App\Repositories\Dentist\LogsRepositoryEloquent;
use Junity\Hashids\Facades\Hashids;

use App\Models\InstallmentBalance;
use App\Models\DentalPayment;
use App\User;

class InstallmentBalanceController extends Controller
{
    protected $paymentRepo;
    protected $patientRepo;
    protected $installRepo;
    protected $logsRepo;

    function __construct(PaymentRepositoryEloquent $paymentRepo, PatientRepositoryEloquent $patientRepo, InstallmentRepositoryEloquent $installRepo, LogsRepositoryEloquent $logsRepo)
    {
    	$this->paymentRepo = $paymentRepo;
    	$this->patientRepo = $patientRepo;
    	$this->installRepo = $installRepo;
        $this->logsRepo = $logsRepo;
    }

    public function create($payment_id = '')
    {
    	if($payment_id){
	        $payment_id = Hashids::decode($payment_id);
	        $payment = $this->paymentRepo->find($payment_id[0]);
	        $patient = $this->patientRepo->find($payment->patient_id);
	        return view('installments.create', compact('patient', 'payment'));
	    }
	    $payment = [];
	    $patient = [];
	    return view('installments.create');
    }

    public function show($id)
    {
        $id = Hashids::decode($id);
        $payment = $this->paymentRepo->find($id[0]);
        $patient = $this->patientRepo->find($payment->patient_id);
        $sqlinstall = "payment_id = '".$id[0]."'";
        $installments = $this->installRepo->rawPaginate($sqlinstall,5,'date','desc');
        $amount_paid = InstallmentBalance::selectRaw("sum(amount) as totalamount")->where('payment_id', $id[0])->first();
        $user = new User;
        return view('installments.show', compact('installments','patient','payment', 'amount_paid', 'user'));
    }

    public function checkPayment($payment_id, $to_pay, $original_amount)
    {
        $payments = DentalPayment::leftJoin('installment_balances', 'dental_payments.id', '=', 'installment_balances.payment_id')
        ->where('dental_payments.id', $payment_id)
        ->selectRaw('dental_payments.amount, SUM(installment_balances.amount) as paid')
        ->first();
        $total_amount = ($payments->paid - $original_amount) + $to_pay;
        if ($total_amount > $payments->amount){
            return 'exceeded';
        }
        return 'success';
    }

    public function store(Request $request)
    {  
        $paid = $this->checkPayment($request->payment_id, $request->amount, $request->original_amount);
        if ($paid == 'exceeded'){
            return redirect()->back()->with('status','Oops, your total paid amount will be over the total installment payment.');
        }
        $makeRequest = [
        	'clinic_user_id' => \Auth::user()->clinic_id,
            'clinic_id' => \Auth::user()->clinic_id,
        	'payment_id' => $request['payment_id'],
        	'amount' => $request['amount'],
        	'currency' => isset($request['currency']) ? $request['currency'] : '-',
        	'remarks' => $request['remarks'],
        	'date' => $request['date']
        ];

        $id = $this->installRepo->create($makeRequest);
        $this->logsRepo->insertLogs('installment_balances','insert',$request['patient_id'],$id);

        return redirect()->route('installments.show', Hashids::encode($request->payment_id))->with('status','Payment Added Successfully!');
    }

    public function edit($id)
    {
        $id = Hashids::decode($id);
        $installments = $this->installRepo->find($id[0]);
        $payment = $this->paymentRepo->find($installments->payment_id);
        $patient = $this->patientRepo->find($payment->patient_id);
        return view('installments.edit', compact('patient', 'payment', 'installments'));
    }

    public function update(Request $request, $id)
    {
        $paid = $this->checkPayment($request->payment_id, $request->amount, $request->original_amount);
        if ($paid == 'exceeded'){
            return redirect()->back()->with('status','Oops, your total paid amount will be over the total installment payment.');
        }
        $id = Hashids::decode($id);
        $makeRequest = [
        	'clinic_user_id' => \Auth::user()->clinic_id,
        	'payment_id' => $request['payment_id'],
        	'amount' => $request['amount'],
        	'currency' => isset($request['currency']) ? $request['currency'] : '-',
        	'remarks' => $request['remarks'],
        	'date' => $request['date']
        ];

        $this->installRepo->update($makeRequest, $id[0]);
        $payment = $this->paymentRepo->find($request['payment_id']);
        $this->logsRepo->insertLogs('installment_balances','update',$request['patient_id'],$id[0]);

        return redirect()->route('installments.show', Hashids::encode($request->payment_id))->with('status','Payment Updated Successfully!');
    }

    public function destroy($id)
    {
        $installment_balances = $this->installRepo->find($id);
        $payment = $this->paymentRepo->find($installment_balances->payment_id);
        $patient = $this->patientRepo->find($payment->patient_id);
        $this->installRepo->delete($id);
        $this->logsRepo->insertLogs('installment_balances','delete',$payment->patient_id,$installment_balances->id);
    }
}
