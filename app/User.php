<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'clinic_name', 
        'source', 
        'access_id', 
        'clinic_id', 
        'apikey', 
        'secretkey', 
        'email', 
        'contact_number', 
        'image', 
        'password', 
        'country_id', 
        'ip_address',
        'ipaddress', 
        'last_login', 
        'mobile_date',
        'is_active',
        'is_reset',
        'connected_device_id',
        'date_upgrade',
        'subscription_end',
        'allowed_devices',
        'role_id',
        'token',
        'inserted'
    ];

    protected $appends = ['currency','fullname'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getCurrencyAttribute()
    {
        if($this->attribute['country_id']!=0){
            $result = \DB::table('countries')->find($this->attribute['country_id']);
            return substr($result->name,-3);
        }
        return '';
    }

    public function getFullnameAttribute()
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    public function patients()
    {
        return $this->hasMany('App\Models\Personal', 'clinic_user_id');
    }

    public function getIpaddress($ipaddress)
    {
        $location = \GeoIP::getLocation($ipaddress);
        return '<b>IP Address: </b>'.$location['ip'].'<br><b>Country: </b>'.$location['country'].'<br><b>City: </b>'.$location['city'];
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id');
    }

    public function access()
    {
        return $this->belongsTo('App\Models\Role', 'access_id');
    }

    public function permission()
    {
        // return $this->belongsToMany('App\Models\Role', 'permissions', 'accesss_id', 'user_id')
    }
}
