<?php
namespace App\Repositories\Patient;

interface InstallmentRepository {
	public function all($ordervalue = null, $columns = array('*'));
	public function paginate($limit = null, $ordervalue = null, $columns = array('*'));
	public function create(array $attributes);
	public function update(array $attributes, $id);
	public function delete($id);
	public function with(array $relations, $ordervalue = null, $columns = array('*'));
	public function find($id, $columns = array('*'));
	public function findWith($id, array $relations, $columns = array('*'));
	public function findByField($field, $value);
	public function getWhereIn($field, array $array, $ordervalue = null, $columns = array('*'));
	public function getWhereNotIn($field, array $array, $ordervalue = null, $columns = array('*'));
	public function rawByField($sql);
	public function rawAll($sql, $ordervalue = null, $columns = array('*'));
	public function rawPaginate($sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'));
	public function rawCount($sql);
	public function getCount();
	public function rawWith(array $relations, $sql, $ordervalue = null, $sortorder = null, $columns = array('*'));
	public function rawWithPaginate(array $relations, $sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'));
	public function selectlist($column_name, $column_id, $sortorder = null);
}