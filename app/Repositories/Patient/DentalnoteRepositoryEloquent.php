<?php

namespace App\Repositories\Patient;

use App\Models\DentalNote;

class DentalnoteRepositoryEloquent implements DentalnoteRepository
{
	protected $query;

	function __construct(DentalNote $query)
	{
		$this->query = $query;
	}

	public function all($ordervalue = null, $sortorder = null, $columns = array('*'))
	{
		$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
		return $this->query->orderBy($order,$sort)->get($columns);
	}

	public function paginate($limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function create(array $attributes)
    {
        return $this->query->create($attributes)->id;
    }

    public function update(array $attributes, $id)
    {
        $model = $this->find($id);
        $model->fill($attributes);
        if($model->save()){
            return 1;
        }
        return $model->save();
    }

    public function delete($id)
    {
        $model = $this->find($id);
        if($model->delete()){
            return 1;
        }
        return $model->delete();
    }

    public function with(array $relations, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return $this->query->with($relations)->orderBy($order,$sort)->get($columns);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->query->find($id, $columns);
    }

    public function findWith($id, array $relations, $columns = array('*'))
    {
        return $this->query->with($relations)->find($id, $columns);
    }

    public function findByField($field, $value)
    {
        return $this->query->where($field,'=',$value)->first();
    }

    public function getWhereIn($field, array $array, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereIn($field, $array)->orderBy($order,$sort)->get($columns);
    }

    public function getWhereNotIn($field, array $array, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereNotIn($field, $array)->orderBy($order,$sort)->get($columns);
    }

    public function rawByField($sql)
    {
    	return $this->query->whereRaw($sql)->first();
    }

    public function rawAll($sql, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereRaw($sql)->orderBy($order,$sort)->get($columns);
    }

    public function rawPaginate($sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->whereRaw($sql)->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function rawWith(array $relations, $sql, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return $this->query->with($relations)->whereRaw($sql)->orderBy($order,$sort)->get($columns);
    }

    public function rawWithPaginate(array $relations, $sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->with($relations)->whereRaw($sql)->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function rawCount($sql)
    {
        return $this->query->whereRaw($sql)->count();
    }

    public function getCount()
    {
        return $this->query->count();
    }

    public function selectlist($column_name, $column_id, $sortorder = null)
    {
        $order = is_null($column_id) ? 'id' : $column_id;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return [''=>'--']+$this->query->orderBy($order,$sort)->pluck($column_name,$column_id)->toArray();
    }
}