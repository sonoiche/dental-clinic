<?php

namespace App\Repositories\Dentist;

use App\Models\Log;
use App\User;

class LogsRepositoryEloquent implements LogsRepository
{
	protected $query;

	function __construct(Log $query)
	{
		$this->query = $query;
	}

	public function all($ordervalue = null, $sortorder = null, $columns = array('*'))
	{
		$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
		return $this->query->orderBy($order,$sort)->get($columns);
	}

	public function paginate($limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function create(array $attributes)
    {
        if($this->query->create($attributes)){
            return 1;
        }
        return $this->query->create($attributes)->id;
    }

    public function update(array $attributes, $id)
    {
        $model = $this->find($id);
        $model->fill($attributes);
        if($model->save()){
            return 1;
        }
        return $model->save();
    }

    public function delete($id)
    {
        $model = $this->find($id);
        if($model->delete()){
            return 1;
        }
        return $model->delete();
    }

    public function with(array $relations, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return $this->query->with($relations)->orderBy($order,$sort)->get($columns);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->query->find($id, $columns);
    }

    public function findWith($id, array $relations, $columns = array('*'))
    {
        return $this->query->with($relations)->find($id, $columns);
    }

    public function findByField($field, $value)
    {
        return $this->query->where($field,'=',$value)->first();
    }

    public function getWhereIn($field, array $array, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereIn($field, $array)->orderBy($order,$sort)->get($columns);
    }

    public function getWhereNotIn($field, array $array, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereNotIn($field, $array)->orderBy($order,$sort)->get($columns);
    }

    public function rawByField($sql)
    {
    	return $this->query->whereRaw($sql)->first();
    }

    public function rawAll($sql, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
    	$order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
    	return $this->query->whereRaw($sql)->orderBy($order,$sort)->get($columns);
    }

    public function rawPaginate($sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->whereRaw($sql)->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function rawWith(array $relations, $sql, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return $this->query->with($relations)->whereRaw($sql)->orderBy($order,$sort)->get($columns);
    }

    public function rawWithPaginate(array $relations, $sql, $limit = null, $ordervalue = null, $sortorder = null, $columns = array('*'))
    {
        $order = is_null($ordervalue) ? 'id' : $ordervalue;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        $limit = is_null($limit) ? 15 : $limit;
        return $this->query->with($relations)->whereRaw($sql)->orderBy($order,$sort)->paginate($limit, $columns);
    }

    public function rawCount($sql)
    {
        return $this->query->whereRaw($sql)->count();
    }

    public function getCount()
    {
        return $this->query->count();
    }

    public function selectlist($column_name, $column_id, $sortorder = null)
    {
        $order = is_null($column_id) ? 'id' : $column_id;
        $sort = is_null($sortorder) ? 'asc' : $sortorder;
        return [''=>'--']+$this->query->orderBy($order,$sort)->pluck($column_name,$column_id)->toArray();
    }

    // custom function
    public function insertLogs($record, $action, $patient_id, $record_id = 0, $version = "", $user_id = "", $connected_device_id = 0)
    {
        if($user_id == ""){
            $user_id = \Auth::user()->id;
        }

        $user = User::find($user_id);

        if($version == ""){
            $sync_version = $user->sync_version;    
        } else {
            $sync_version = $version;
        }

        if($patient_id == null) {
            $patient_id = 0;
        }

        if($action == 'insert'){
            return $this->query->create([
                'record' => $record,
                'action' => $action,
                'patient_id' => $patient_id,
                'record_id' => $record_id,
                'clinic_user_id' => $user_id,
                'sync_version' => $sync_version,
                'connected_device_id' => $connected_device_id,
            ]);
        } else {
            $result = $this->query->where('patient_id',$patient_id)
                                  ->where('clinic_user_id',$user_id)
                                  ->where('connected_device_id',$connected_device_id)
                                  ->where('record_id', $record_id)
                                  ->first();
            if(count($result)==0){
                return $this->query->create([
                    'record' => $record,
                    'action' => $action,
                    'patient_id' => $patient_id,
                    'record_id' => $record_id,
                    'clinic_user_id' => $user_id,
                    'sync_version' => $sync_version,
                    'connected_device_id' => $connected_device_id,
                ]);
            } else {
                if($result->action == 'insert'){
                    return $this->query->create([
                        'record' => $record,
                        'action' => $action,
                        'patient_id' => $patient_id,
                        'record_id' => $record_id,
                        'clinic_user_id' => $user_id,
                        'sync_version' => $sync_version,
                        'connected_device_id' => $connected_device_id,
                    ]);
                } else {
                    $result['action'] = $action;
                    $result['sync_version'] = $sync_version;
                    $result->save();
                    return $result;
                    // return $result->update([
                    //     'action' => $action,
                    //     'sync_version' => $sync_version
                    // ]);
                }
            }
        }
    }
}