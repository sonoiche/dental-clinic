<?php

namespace App\Policies;

use App\User;
use App\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function read(User $user, Permission $permission)
    {
        return $permission->can_read == 1;
    }

    public function write(User $user, Permission $permission)
    {
        return $permission->can_write == 1;
    }
}
