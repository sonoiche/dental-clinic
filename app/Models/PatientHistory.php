<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientHistory extends Model
{
   	protected $table = "patient_histories";
   	protected $fillable = [
   		'clinic_id',
        'clinic_user_id',
        'patient_id',
        'title',
        'history',
        'mobile_date'
   	];
}
