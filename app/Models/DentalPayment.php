<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DentalPayment extends Model
{
    protected $table = "dental_payments";
    protected $fillable = [
    	'clinic_id',
        'clinic_user_id',
        'patient_id',
        'dental_note_id',
        'chart_id',
        'amount',
        'remarks',
        'payment_type',
        'payment_date',
        'due_date'
    ];

    protected $dates = ['payment_date'];

    // public function setPaymentDateAttribute($value)
    // {
    //     if($value){
    //         return $this->attributes['payment_date'] = Carbon::parse($value)->format('Y-m-d');
    //     }
    //     return '';
    // }

    public function getPaymentDateAttribute()
    {
            return Carbon::parse($this->attributes['payment_date'])->format('m/d/Y');
    }
}
