<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Personal extends Model
{
    protected $table = "personals";
    protected $fillable = [
    	'clinic_id',
        'clinic_user_id',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'birthdate',
        'email',
        'contact_number',
        'address',
        'image',
        'notes',
        'medical_history',
        'mobile_date'
    ];

    public function clinic()
    {
    	return $this->belongsTo('App\Models\Clinic', 'clinic_id');
    }

    protected $dates = ['birthdate'];
    protected $appends = ['full_name', 'hashid', 'age', 'birthdate_display','birthdate_form'];

    public function setBirthdateAttribute($value)
    {
        if($value){
            return $this->attributes['birthdate'] = Carbon::parse($value)->format('Y-m-d');
        }
        return '';
    }

    public function getBirthdateDisplayAttribute()
    {
        if(isset($this->attributes['birthdate']) && $this->attributes['birthdate']!='0000-00-00'){
            return Carbon::parse($this->birthdate)->format('M d, Y');
        }
        return '';
    }

    public function getBirthdateFormAttribute()
    {
        if(isset($this->attributes['birthdate']) && $this->attributes['birthdate']!='0000-00-00'){
            return Carbon::parse($this->birthdate)->format('m/d/Y');
        }
        return '';
    }

    public function getAgeAttribute()
    {
        if(isset($this->attributes['birthdate']) && $this->attributes['birthdate']!='0000-00-00'){
            return Carbon::parse($this->attributes['birthdate'])->age;
        }
        return '';
    }

    public function dentalnote()
    {
        return $this->hasMany('App\Models\DentalNote', 'patient_id');
    }

    public function dentalphoto()
    {
        return $this->hasMany('App\Models\DentalPhoto', 'patient_id');
    }

    public function scopeSortfilter($query, $param = array())
    {
        $patient_name = $param['patient_name'];
        $sortby = $param['sortby'];
        if($patient_name!=''){
            $query->whereRaw("first_name like '%".$patient_name."%' or last_name like '%".$patient_name."%'");
        }
        if($sortby == 1){
            $query->orderBy('personals.updated_at','desc');
        }
        if($sortby == 2){
            $query->orderBy('first_name','asc');
        }
        if($sortby == 3){
            $query->orderBy('last_name','asc');
        }
        return $query;
    }

    public function totalpayment($id)
    {
        $result = $this->join('dental_payments','dental_payments.patient_id','=','personals.id')
                       ->selectRaw("sum(amount) as totalamount")
                       ->where('dental_payments.patient_id',$id)
                       ->get();

        return number_format($result[0]->totalamount,2);
    }

    public function dentist()
    {
        return $this->belongsTo('App\User', 'clinic_user_id');
    }

    public function logs()
    {
        return $this->hasMany('App\Logs', 'patient_id');
    }

    public function getPhotoAttribute()
    {
        if ($this->image)
        {
            $photo = url('uploads/patients/'.$this->image);
            $type = pathinfo($photo, PATHINFO_EXTENSION);
            $data = file_get_contents($photo);
            $base64 = base64_encode($data);
            return $base64;
        }
        return '';
    }

    public function getFullnameAttribute()
    {

        $middle_name = ($this->attributes['middle_name']) ? ucfirst($this->attributes['middle_name']).' ' : ' ';
        return ucfirst($this->attributes['first_name']).' '.$middle_name.ucfirst($this->attributes['last_name']);
    }

    public function getHashidAttribute()
    {
        return \Hashids::encode($this->attributes['id']);
    }
}
