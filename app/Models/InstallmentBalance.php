<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class InstallmentBalance extends Model
{
    protected $table = "installment_balances";
    protected $fillable = [
    	'payment_id',
        'amount',
        'currency',
        'remarks',
        'date',
        'mobile_date'
    ];

    protected $dates = ['date'];

    public function setDateAttribute($value)
    {
        if($value){
            return $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d');
        }
        return '';
    }

    public function getDateAttribute()
    {
            return Carbon::parse($this->attributes['date'])->format('m/d/Y');
    }
}
