<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillable = [
        'role_id',
        'menu_id',
        'can_read',
        'can_write',
        'clinic_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'role_id', 'access_id');
    }

}
