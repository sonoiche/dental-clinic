<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DentalNote extends Model
{
    protected $table = "dental_notes";
    protected $fillable = [
	    'clinic_id',
	    'clinic_user_id',
	    'patient_id',
	    'chart_id',
	    'color_code',
	    'notes',
	    'note_active',
	    'chart_name'
    ];
}
