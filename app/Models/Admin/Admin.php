<?php

namespace App\Models\Admin;

use Illuminate\Notifications\Notifiable;
use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';

    protected $table = "admins";
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'is_active',
        'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function access()
    // {
    //     return $this->belongsTo('App\Models\Settings\Access', 'access_id');
    // }
}
