<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $table = "clinics";
    protected $fillable = [
    	'name',
    	'country_id',
    	'address',
    	'apikey'
    ];

    public function country()
    {
    	return $this->belongsTo('App\Models\Country', 'country_id');
    }
}
