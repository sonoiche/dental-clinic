<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "roles";
    protected $fillable = [
    	'name',
    	'clinic_id'
    ];

    public function permissions()
    {
    	return $this->hasMany('App\Models\Permission', 'role_id');
    }

}
