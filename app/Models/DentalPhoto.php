<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DentalPhoto extends Model
{
    protected $table = "dental_photos";
    protected $fillable = [
    	'clinic_id',
        'clinic_user_id',
        'patient_id',
        'photo',
        'caption'
    ];

    public function getImageAttribute()
    {
    	if(\File::exists('uploads/photos/'.$this->clinic_user_id.'/'.$this->patient_id.'/'.$this->photo)){
	        $photo = url('uploads/photos/'.$this->clinic_user_id.'/'.$this->patient_id.'/'.$this->photo);
	        $type = pathinfo($photo, PATHINFO_EXTENSION);
	        $data = file_get_contents($photo);
	        $base64 = base64_encode($data);
	        return $base64;
	    }
	    return '';
    }
}
