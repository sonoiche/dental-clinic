<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";
    protected $fillable = ['name'];

    public function permission()
    {
    	return $this->hasMany('App\Models\Permission', 'menu_id')->where('clinic_id',\Auth::user()->clinic_id);
    }
}
