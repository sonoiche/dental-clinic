<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnectedDevice extends Model
{
    protected $table = "connected_devices";
    protected $fillable = [
    	'clinic_user_id',
        'device_number',
        'model',
        'platform',
        'sync_version',
        'has_reset'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'clinic_user_id');
    }
}
