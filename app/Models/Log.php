<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "logs";
    protected $fillable = [
    	'clinic_user_id',
    	'patient_id',
    	'record_id',
    	'record',
    	'action',
    	'sync_version',
    	'connected_device_id'
    ];
}
