<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PatientAppointment extends Model
{
    protected $table = "patient_appointments";
    protected $fillable = [
    	'clinic_id',
        'clinic_user_id',
        'patient_id',
        'name',
        'no_show',
        'schedule',
        'remarks'
    ];

    public function patient()
    {
    	return $this->belongsTo('App\Models\Personal', 'patient_id');
    }

    public function scopeFilterview($query, $param = array())
    {
        $datestring = $param['viewby'];
        if($datestring == 'Today'){
            $query->whereRaw("date(schedule) = '".date('Y-m-d')."'");
        }
        if($datestring == 'This Week'){
            $sunday = date('Y-m-d', strtotime('+ 7 days'));
            $query->whereRaw("date(schedule) >= '".date('Y-m-d')."' and date(schedule) <= '".$sunday."'");
        }
        if($datestring == 'View Upcoming Appointments'){
            $date = date('Y-m-d');
            $query->whereRaw("date(schedule) > '".$date."'");
        }
        if($datestring == 'View Past Appointments'){
            $date = date('Y-m-d');
            $query->whereRaw("date(schedule) < '".$date."'");
        }

        return $query;
    }
}
