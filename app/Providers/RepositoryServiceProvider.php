<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Patient\PatientRepository', 'App\Repositories\Patient\PatientRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\AppointmentRepository', 'App\Repositories\Patient\AppointmentRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\DentalnoteRepository', 'App\Repositories\Patient\DentalnoteRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\DentalphotoRepository', 'App\Repositories\Patient\DentalphotoRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\PaymentRepository', 'App\Repositories\Patient\PaymentRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\HistoryRepository', 'App\Repositories\Patient\HistoryRepositoryEloquent');
        $this->app->bind('App\Repositories\Patient\InstallmentRepository', 'App\Repositories\Patient\InstallmentRepositoryEloquent');

        $this->app->bind('App\Repositories\Dentist\DeviceRepository', 'App\Repositories\Dentist\DeviceRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\UserRepository', 'App\Repositories\Dentist\UserRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\FeedbackRepository', 'App\Repositories\Dentist\FeedbackRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\CountryRepository', 'App\Repositories\Dentist\CountryRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\LogsRepository', 'App\Repositories\Dentist\LogsRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\RoleRepository', 'App\Repositories\Dentist\RoleRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\PermissionRepository', 'App\Repositories\Dentist\PermissionRepositoryEloquent');
        $this->app->bind('App\Repositories\Dentist\MenuRepository', 'App\Repositories\Dentist\MenuRepositoryEloquent');
    }
}
