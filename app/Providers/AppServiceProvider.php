<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use App\Models\Permission;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            if(Auth::check()){
                $dentalaccess1 = Permission::where('menu_id',1)->where('role_id',Auth::user()->access_id)->where('clinic_id',Auth::user()->clinic_id)->first();
                $dentalaccess2 = Permission::where('menu_id',2)->where('role_id',Auth::user()->access_id)->where('clinic_id',Auth::user()->clinic_id)->first();
                $dentalaccess3 = Permission::where('menu_id',3)->where('role_id',Auth::user()->access_id)->where('clinic_id',Auth::user()->clinic_id)->first();
                $dentalaccess4 = Permission::where('menu_id',4)->where('role_id',Auth::user()->access_id)->where('clinic_id',Auth::user()->clinic_id)->first();
                $dentalaccess5 = Permission::where('menu_id',5)->where('role_id',Auth::user()->access_id)->where('clinic_id',Auth::user()->clinic_id)->first();
                
                $view->with('dentalaccess1', $dentalaccess1);
                $view->with('dentalaccess2', $dentalaccess2);
                $view->with('dentalaccess3', $dentalaccess3);
                $view->with('dentalaccess4', $dentalaccess4);
                $view->with('dentalaccess5', $dentalaccess5);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
