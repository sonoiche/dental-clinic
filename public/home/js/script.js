$(window).bind("load", function() {
    "use strict";
    $(".spn_hol").fadeOut(1e3)
}), $(document).ready(function() {
    "use strict";
    $(".navbar-nav li a").click(function() {
        $(".navbar-nav li a").parent().removeClass("active"), $(this).parent().addClass("active")
    })
}), $(document).ready(function() {
    "use strict";
    $(window).scroll(function() {
        $(".page").each(function() {
            var e = $(this).attr("id"),
                a = $(this).outerHeight(),
                r = $(this).offset().top - 70;
            if ($(window).scrollTop() > r - 1 && $(window).scrollTop() < r + a - 1) {
                $(".navbar-nav li a[href='#" + e + "']").parent().addClass("active")
            } else {
                $(".navbar-nav li a[href='#" + e + "']").parent().removeClass("active")
            }
        })
    })
}), $(function() {
    "use strict";
    $("a[href*=#]:not([href=#])").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"), e.length) return $("html,body").animate({
                scrollTop: e.offset().top
            }, 1e3), !1
        }
    })
}), $(document).ready(function() {
    "use strict";
    setInterval(function() {
        var e = $(window).height(),
            a = $(".home-container").height(),
            r = e - a;
        $(".home-container").css({
            "padding-top": Math.round(r / 2) + "px",
            "padding-bottom": Math.round(r / 2) + "px"
        })
    }, 10)
}),
 $(document).ready(function() {
    "use strict";
    $(".form_submit").click(function() {
        var e = $("#name").val(),
            a = $("#email").val(),
            r = $("#subject").val(),
            s = $("#message").val(),
            o = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        return e ? ($(".form_error .name_error").addClass("hide").removeClass("show"), a ? ($(".form_error .email_error").addClass("hide").removeClass("show"), o.test(a) ? ($(".form_error .email_val_error").addClass("hide").removeClass("show"), s ? ($(".form_error .message_error").addClass("hide").removeClass("show"), e && a && s && $.ajax({
            url: "contact.php",
            data: {
                name: e,
                emaild: a,
                subject: r,
                message: s
            },
            type: "POST",
            success: function(a) {
                $(".Sucess").show(), $(".Sucess").fadeIn(2e3), $(".Sucess").html("<i class='fa fa-check'></i> Dear <b>" + e + "</b> Thank you for your inquiry we will respond to you as soon as possible!"), $("#Name").val(""), $("#Email").val(""), $("#Subject").val(""), $("#Message").val(""), $(".form_error .name_error, .form_error .email_error, .form_error .email_val_error, .form_error .message_error").addClass("hide").removeClass("show"), $("#name").val(""), $("#email").val(""), $("#subject").val(""), $("#message").val("")
            }
        }), !1) : ($(".form_error .message_error").addClass("show").removeClass("hide"), !1)) : ($(".form_error .email_val_error").addClass("show").removeClass("hide"), !1)) : ($(".form_error .email_error").addClass("show").removeClass("hide"), !1)) : ($(".form_error .name_error").addClass("show").removeClass("hide"), !1)
    })
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 50 ) {
        $('.scrolltop:hidden').stop(true, true).fadeIn();
    } else {
        $('.scrolltop').stop(true, true).fadeOut();
    }
});
$(function(){$(".scroll").click(function(){$("html,body").animate({scrollTop:$(".dental-home-container").offset().top},"2000");return false})})


