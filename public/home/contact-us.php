<? 
require_once( "func_all.php"); 
$num = rand(100000, 999999);
$formname="thisonly";
$inputname= array("company","contact","telno","email","recaptcha_response_field");
$displayname= array("Company Name","Contact Person","Tel. No.","Email Address","Please enter the Verification Code");

require 'vendor/autoload.php';
error_reporting(1);
ini_set("display_errors","1");
session_start();
do_header("IT Company, Web Services, Web Applications, SEO, Quantum X, Inc.","Web design company, web designers, website design Philippines, web designers Philippines, web designs, responsive web design company, affordable web designs, web design company Philippines, web design service, web design and development company","IT Company","Quantum X develops quality and affordable responsive web designs that will make your company website excel for online marketing, Contact us today.");
JS_CheckRequired($formname,$inputname,$displayname);  
session_start();  

if($sub){

if(md5(strtoupper($_POST['verifycode'])) != $_SESSION['__img_code__']){
  $post = base64_encode(serialize($_POST)); // encrypt                                                
  $error_message = "Wrong Captcha Verification Code. Pls. try again.";
}else{

$success_message = "We have received your inqury. Thank you.";

$numbering=count(file("dl/emails.htm"));
if(file_exists('dl/emails.htm')){
$numbering++;
} 
$filename = "dl/emails.csv";

$a=find_user("dl/emails.csv",$email,$name);

if($a){

echo "
<script type='text/javascript'>
alert('Personal data already exists');
</script>
";
}else{
$fp = fopen(@$filename, "a") or "
<script>
alert ('Please close the CSV File First');
</script>
";

$fp = fopen($filename, "a");
fwrite($fp,"$numbering".",".str_replace(",","&nbsp","Contact Us Inquiry").",".str_replace(",","&nbsp",$comany).",".str_replace(",","&nbsp",$name).",".str_replace(",","&nbsp",$contact).",".str_replace(",","&nbsp",$email).",".str_replace(",","&nbsp",$message).",".date("Y-m-d").","."<br>\n");

fclose($fp);
$filename = "dl/emails.htm";
$fp = fopen($filename, "a");

$t1=$_POST['name'];
$tlv4=$_POST['email'];

fwrite($fp,"$numbering".",".str_replace(",","&nbsp;",$name).",".str_replace(",","&nbsp;",$contact).",".str_replace(",","&nbsp;",$email).",".str_replace(",","&nbsp;",$message).",".date("Y-m-d").","."<br>\n");

fclose($fp);

$t1=urlencode($t1);

$mail = new PHPMailer;

  //$mail->SMTPDebug = 3;                               // Enable verbose debug output

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'mail.quantumx.com';  // Specify main and backup SMTP servers
  // $mail->SMTPAuth = true;                               // Enable SMTP authentication
  // $mail->Username = 'user@example.com';                 // SMTP username
  // $mail->Password = 'secret';                           // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  $mail->setFrom('iris@quantumx.com', 'Quantum X Inc.');
  $mail->addAddress('sales@quantumx.com', 'Quantum X Inc.');     // Add a recipient
  $mail->addAddress('angelique@quantumx.com');               // Name is optional
  $mail->addReplyTo('iris@quantumx.com', 'Quantum X Inc.');

  $mail->isHTML(true);                                  // Set email format to HTML

  $mail->Subject = 'A QX customer sent an inquiry';
  $mail->Body    = "
<html>
<head>
<style type='text/css'>
<!--
td {font-size:14px;font-family:Verdana;}
-->
</style>
</head>
<body>
<div><strong>Details are as follows:</strong></div>
<div><br/></div>
<table border=0 cellpadding=2 cellspacing=2 width='50%'>
<tr><td>Product/Service: </td><td >$product</td></tr>
<tr><td>Company Name: </td><td >$company</td></tr>
<tr><td width='20%'>Contact Person: </td><td> $name </td></tr>
<tr><td>Tel. No.:</td><td>$contact</td></tr>
<tr><td>Email Address:</td><td> $email</td></tr>
<tr><td>How may we be of service?: </td><td>$message</td></tr>
</table>
</body>
</html>";
  $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
  $mail->send();
  
header("location:contact-us.php");
}
}


}
function find_user($filename, $email, $name) {
$name = strtolower($name);
$email = strtolower($email);
$f = fopen($filename, "r");
while ($row = fgetcsv($f)) {

if ($row[2] == $email and strtolower($row[1])==$name) {

return 1;
exit;
}
} 
fclose($f);
return 0;
}
?>

<div class="col-sm-12 text-center">
    <div class="row">
        <h1 class="uppercase"> Do you want to discuss a project with us? </h1>
        <span class="subtext">We'd love to hear about it. We will help you increase your business's profit with our services.  </span>
        <br/>
        <br/>

    </div>
</div>

<div class="col-sm-12">
    <div class="row">
       <div class="col-sm-8">
 <hr/>
    </div>
     <div class="col-sm-4">
 <ol class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li><a href="contact-us.php">Contact Us</a></li>

</ol>
    </div>

    </div>
</div>


<div class="col-sm-12 ">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-sm-8 text-justify">

            <h4>Please complete the form below for your inquiries. Once submitted, our team will get back to you as soon as possible to arrange schedules for a call or meeting.   </h4>
            <br/>

           <? if($error_message){?>
            <div class="alert alert-error" style="color:#ff0000" >
      <?=$error_message?></div>
          <? }
      
      else if($success_message) {
        
      ?>
            
               <div class="alert alert-success" >
      <?=$success_message?></div>
            <?  
        
      }
      
      ?>

<form class="form-horizontal" action="<?=$PHP_SELF?>" method="post" name="<?=myform?>"  enctype="MULTIPART/FORM-DATA">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Choose Product <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
     <!-- <select class="form-control" id="product" name="product">
  <option>MYRA</option>
  <option>MOIRA</option>
  <option>CMMS</option>
  <option>STARS</option>
   <option>OTHERS</option>
</select> -->

<select class="form-control" id="product" name="product" required>
<option val="" <?=($_GET['inquire'] == '')?'selected':'' ?>></option>
<option val="CMMS" <?=($_GET['inquire'] == 'cmms')?'selected':''?>>Crew Manning and Management System (CMMS)</option>   
<option val="MOBILE APPLICATION" <?=($_GET['inquire'] == 'mobileapp')?'selected':''?>>Mobile Application</option>  
<option val="MOIRA" <?=($_GET['inquire'] == 'moira')?'selected':'' ?>>Mobile Interactive Recruitment Assistant (MOIRA)</option>
<option val="MYRA" <?=($_GET['inquire'] == 'myra')?'selected':''?>>My Recruitment Assistant (MYRA)</option>   
<option val="OTHERS" <?=($_GET['inquire'] == 'others')?'selected':''?>>Others</option>   
<option val="STARS" <?=($_GET['inquire'] == 'stars')?'selected':''?>>Skills Training Advancement and Recording Software (STARS)</option>   
<option val="WEB APPLICATION" <?=($_GET['inquire'] == 'webapp')?'selected':''?>>Web Application</option>   
<option val="WEB DESIGN" <?=($_GET['inquire'] == 'webdesign')?'selected':''?>>Web Design</option>  

</select>
  </div>
</div>

    <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Company Name <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
      <input type="text" class="form-control" required  id="company" name="company" placeholder="Type your company name here">
    </div>
  </div>
  
  <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Full Name <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
      <input type="text" class="form-control" required   id="name" name="name" placeholder="Type your full name here">
    </div>
  </div>
  

    <div class="form-group">
    <label for="company" class="col-sm-3 control-label">Contact Number <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
      <input type="text" class="form-control"  required  id="contact" name="contact" placeholder="Type your cellphone or phone number here">
    </div>
  </div>
  
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Email Address <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
      <input type="email" class="form-control" required  id="email" name="email" placeholder="Type your email address here">
    </div>
  </div>


  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">How may we be of service? <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
      <textarea class="form-control" id="message" required  name="message" rows="3"></textarea>
    </div>
  </div>


     <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label">   Verification Image: <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
   <span style="padding-left:175px">&nbsp;&nbsp;<img src="captcha/code.php?id=" align="left" id="__code__" style="border:1px solid #4b4d56" /></span>
    </div>
  </div>
    
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-3 control-label"> Type Verification Code <span style="color:#900">*</span> </label>
    <div class="col-sm-9">
    <input name="verifycode" class="form-control" type="text" required   />
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-10">
      <button type="submit" value="1"  name="sub" id="sub" class="btn btn-success  btn-lg">
          Submit
        </button>
    </div>
  </div>
</form>
        </div>
        <div class="col-md-4">
          
            <div class="google-maps"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d575.7422784697517!2d121.05530058110418!3d14.625856237274236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7bde202e4f5%3A0x545361e5b5af6d80!2sQuantum+X%2C+Inc!5e0!3m2!1sen!2s!4v1460420216619" width="356" height="250" frameborder="0" style="border:0" allowfullscreen></iframe></div>


            <h4><b>Quantum X Inc. Office</b></h4>
            <p>
            62 Stanford St. Bgy E. Rodriguez,<br/>
            Cubao, Quezon City<br/>
            Telephone No. : +632-709-4011 to 13<br/>
            Fax Number: +632-440-7454<br/>
            Email Address: sales@quantumx.com </p>

        </div>
    </div>
</div>

<? if($msg) echo "<script>alert('$msg')</script>";
 do_footer(); ?>