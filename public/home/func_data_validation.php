<?
function MailVal($Addr, $Level, $Timeout = 15000) { 

//  Valid Top-Level Domains 
    $gTLDs = "com:net:org:edu:gov:mil:int:arpa:"; 
    $CCs   = "ad:ae:af:ag:ai:al:am:an:ao:aq:ar:as:at:au:aw:az:ba:bb:bd:be:bf:". 
             "bg:bh:bi:bj:bm:bn:bo:br:bs:bt:bv:bw:by:bz:ca:cc:cf:cd:cg:ch:ci:". 
             "ck:cl:cm:cn:co:cr:cs:cu:cv:cx:cy:cz:de:dj:dk:dm:do:dz:ec:ee:eg:". 
             "eh:er:es:et:fi:fj:fk:fm:fo:fr:fx:ga:gb:gd:ge:gf:gh:gi:gl:gm:gn:". 
             "gp:gq:gr:gs:gt:gu:gw:gy:hk:hm:hn:hr:ht:hu:id:ie:il:in:io:iq:ir:". 
             "is:it:jm:jo:jp:ke:kg:kh:ki:km:kn:kp:kr:kw:ky:kz:la:lb:lc:li:lk:". 
             "lr:ls:lt:lu:lv:ly:ma:mc:md:mg:mh:mk:ml:mm:mn:mo:mp:mq:mr:ms:mt:". 
             "mu:mv:mw:mx:my:mz:na:nc:ne:nf:ng:ni:nl:no:np:nr:nt:nu:nz:om:pa:". 
             "pe:pf:pg:ph:pk:pl:pm:pn:pr:pt:pw:py:qa:re:ro:ru:rw:sa:sb:sc:sd:". 
             "se:sg:sh:si:sj:sk:sl:sm:sn:so:sr:st:su:sv:sy:sz:tc:td:tf:tg:th:". 
             "tj:tk:tm:tn:to:tp:tr:tt:tv:tw:tz:ua:ug:uk:um:us:uy:uz:va:vc:ve:". 
             "vg:vi:vn:vu:wf:ws:ye:yt:yu:za:zm:zr:zw:"; 

//  The countries can have their own 'TLDs', e.g. mydomain.com.au 
    $cTLDs = "com:net:org:edu:gov:mil:co:ne:or:ed:go:mi:"; 

    $fail = 0; 

//  Shift the address to lowercase to simplify checking 
    $Addr = strtolower($Addr); 

//  Split the Address into user and domain parts 
    $UD = explode("@", $Addr); 
    if (sizeof($UD) != 2 || !$UD[0]) $fail = 1; 

//  Split the domain part into its Levels 
    $Levels = explode(".", $UD[1]); $sLevels = sizeof($Levels); 
    if ($sLevels < 2) $fail = 1; 

//  Get the TLD, strip off trailing ] } ) > and check the length 
    $tld = $Levels[$sLevels-1]; 
    $tld = ereg_replace("[>)}]$|]$", "", $tld); 
    if (strlen($tld) < 2 || strlen($tld) > 3 && $tld != "arpa") $fail = 1; 

    $Level--; 

//  If the string after the last dot isn't in the generic TLDs or country codes, it's invalid. 
    if ($Level && !$fail) { 
    $Level--; 
    if (!ereg($tld.":", $gTLDs) && !ereg($tld.":", $CCs)) $fail = 2; 
    } 

//  If it's a country code, check for a country TLD; add on the domain name. 
    if ($Level && !$fail) { 
    $cd = $sLevels - 2; $domain = $Levels[$cd].".".$tld; 
    if (ereg($Levels[$cd].":", $cTLDs)) { $cd--; $domain = $Levels[$cd].".".$domain; } 
    } 

//  See if there's an MX record for the domain 
    if ($Level && !$fail) { 
    $Level--; 
    if (!getmxrr($domain, $mxhosts, $weight)) $fail = 3; 
    } 

//  Attempt to connect to port 25 on an MX host 
    if ($Level && !$fail) { 
    $Level--; 
    while (!$sh && list($nul, $mxhost) = each($mxhosts)) 
      $sh = fsockopen($mxhost, 25); 
    if (!$sh) $fail = 4; 
    } 

//  See if anyone answers 
    if ($Level && !$fail) { 
    $Level--; 
    set_socket_blocking($sh, false); 
    $out = ""; $t = 0; 
    while ($t++ < $Timeout && !$out) 
      $out = fgets($sh, 256); 
    if (!ereg("^220", $out)) $fail = 5; 
    } 

    if ($sh) fclose($sh); 

    return $fail; 
  } //MailVal
function valid_email($address)
{
  // check an email address is possibly valid
  if (ereg("^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$", $address))
    return true;
  else 
    return false;
}
 function JS_CheckRequired($myform,$textarray,$displayname="0",$email="0",$email2="0",$fcktextarray="0",$fckdisplayname="0"){
 					if($displayname=="0") $displayname=$textarray;
					if($fckdisplayname=="0") $fckdisplayname=$fcktextarray;
?>
      
      <SCRIPT LANGUAGE="JavaScript1.2">
       function PHP_hasValue(obj, obj_type)
      {
          if (obj_type == "TEXT" || obj_type == "PASSWORD")	{					  
          	if (obj.value.length == 0)
            		return false;
          	else{ 
            		var x = obj.value;
								// trim the field value
								x=x.replace(/^\s*(.*)/, "$1");
  							x=x.replace(/(.*?)\s*$/, "$1");
								if(x.length==0){
								  obj.value = x;
									return false
								}	
								else{
									obj.value = x;	 
								  return true;
								}	
						}		   	
      		}
      }
      function CheckRequired(){
      	<?		for ($i=0;$i< count($textarray);$i++){  ?>
                if  (!PHP_hasValue(document.<?echo$myform?>.<?echo$textarray[$i]?>, "TEXT" )){
      									alert("Notice: <?echo$displayname[$i]?> is a required field.");
        								document.<?echo$myform?>.<?echo$textarray[$i]?>.focus();
                        return false;
      									}		
      									
        <?	}		?>
 	<?		for ($i=0;$i< count($fcktextarray);$i++){  ?>
	var oEditor = FCKeditorAPI.GetInstance('<?php echo $fcktextarray[$i]; ?>') ;
                if (oEditor.GetXHTML(true) == "") {
      									alert("Notice: <?echo$fckdisplayname[$i]?> is a required field.");
				
				return false;
				} 		
      									
        <?	}		?>
				<?if(!$email=="0"){?>
									if (isEmail(<?echo$myform?>.<?echo$email?>.value) == false) {
                      alert("Please enter your valid email address.");
                      <?echo$myform?>.<?echo$email?>.focus();
                      return false;
          }			
				<?}?>      				
				<?if(!$email2=="0"){?>
									if (isEmail(<?echo$myform?>.<?echo$email2?>.value) == false) {
                      alert("Please enter your friend's valid email address.");
                      <?echo$myform?>.<?echo$email2?>.focus();
                      return false;
          }			
				<?}?>
                return true;
      }
      	
      </script>
			
			
      <script type="text/javascript" language="JavaScript1.2"><!--
      function isEmail(string) {
          if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
              return true;
          else
              return false;
      }      
   
     //--></script>

<? }
 function JS_CheckRequired2($myform,$textarray,$displayname="0",$email="0",$email2="0"){
 					if($displayname=="0")$displayname=$textarray;
?>
      
      <SCRIPT LANGUAGE="JavaScript1.2">
       function PHP_hasValue2(obj, obj_type)
      {
	  
          if (obj_type == "TEXT" || obj_type == "PASSWORD")	{					  
          	if (obj.value.length == 0)
            		return false;
          	else{ 
            		var x = obj.value;
								// trim the field value
								x=x.replace(/^\s*(.*)/, "$1");
  							x=x.replace(/(.*?)\s*$/, "$1");
								if(x.length==0){
								  obj.value = x;
									return false
								}	
								else{
									obj.value = x;	 
								  return true;
								}	
						}		   	
      		}
      }
      function CheckRequired2(){
      	<?		for ($i=0;$i< count($textarray);$i++){  ?>
                if  (!PHP_hasValue2(document.<?echo$myform?>.<?echo$textarray[$i]?>, "TEXT" )){
      									alert("Notice: <?echo$displayname[$i]?> is a required field.");
        								document.<?echo$myform?>.<?echo$textarray[$i]?>.focus();
                        return false;
      									}		
      									
        <?	}		?>
				<?if(!$email=="0"){?>
									if (isEmail2(document.<?echo$myform?>.<?echo$email?>.value) == false) {
                      alert("Please enter your valid email address.");
                      document.<?echo$myform?>.<?echo$email?>.focus();
                      return false;
          }			
				<?}?>      				
				<?if(!$email2=="0"){?>
									if (isEmail2(document.<?echo$myform?>.<?echo$email2?>.value) == false) {
                      alert("Please enter your friend's valid email address.");
                      document.<?echo$myform?>.<?echo$email2?>.focus();
                      return false;
          }			
				<?}?>
				
				<?if(!$radioButton=="0"){?>
					if (document.<?echo$myform?>.<?php echo $radioButton?>[0].checked == false && document.<?echo$myform?>.<?php echo $radioButton?>[1].checked == false) {
						alert("Please choose a module to access.");
						return false;
					}			
				<?}?>
				
				
                return true;
      }
      	
      </script>
			
			
      <script type="text/javascript" language="JavaScript1.2"><!--
      function isEmail2(string) {
          if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
              return true;
          else
              return false;
      }      
      //--></script>
<? 
} function JS_CheckRequired3($myform,$textarray,$displayname="0",$email="0",$email2="0"){
 					if($displayname=="0") $displayname=$textarray;
?>
      
      <SCRIPT LANGUAGE="JavaScript1.2">
       function PHP_hasValue3(obj, obj_type)
      {
          if (obj_type == "TEXT" || obj_type == "PASSWORD")	{					  
          	if (obj.value.length == 0)
            		return false;
          	else{ 
            		var x = obj.value;
								// trim the field value
								x=x.replace(/^\s*(.*)/, "$1");
  							x=x.replace(/(.*?)\s*$/, "$1");
								if(x.length==0){
								  obj.value = x;
									return false
								}	
								else{
									obj.value = x;	 
								  return true;
								}	
						}		   	
      		}
      }
      function CheckRequired3(){
      	<?		for ($i=0;$i< count($textarray);$i++){  ?>
                if  (!PHP_hasValue3(document.<?echo$myform?>.<?echo$textarray[$i]?>, "TEXT" )){
      									alert("Notice: <?echo$displayname[$i]?> is a required field.");
        								document.<?echo$myform?>.<?echo$textarray[$i]?>.focus();
                        return false;
      									}		
      									
        <?	}		?>
 	
				<? if(!$email=="0"){ ?>
									if (isEmail3(<?echo$myform?>.<?echo$email?>.value) == false) {
                      alert("Please enter your valid email address.");
                      <?echo$myform?>.<?echo$email?>.focus();
                      return false;
          }			
				<?}?>      				
				<?if(!$email2=="0"){?>
									if (isEmail3(<?echo$myform?>.<?echo$email2?>.value) == false) {
                      alert("Please enter your friend's valid email address.");
                      <?echo$myform?>.<?echo$email2?>.focus();
                      return false;
          }			
				<?}?>
                return true;
      }
      	
      </script>
			
			
      <script type="text/javascript" language="JavaScript1.2"><!--
      function isEmail3(string) {
          if (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
              return true;
          else
              return false;
      }      
   
     //--></script>

<? 
}
 function JS_CheckDate($myform,$m,$d,$y){
?>
      <script language="JavaScript"><!--
          function y2k(number) { return (number < 1000) ? number + 1900 : number; }

          function isDate (day,month,year) {
          // checks if date passed is valid
          // will accept dates in following format:
          // isDate(dd,mm,ccyy), or
          // isDate(dd,mm) - which defaults to the current year, or
          // isDate(dd) - which defaults to the current month and year.
          // Note, if passed the month must be between 1 and 12, and the
          // year in ccyy format.
              var today = new Date();
              year = ((!year) ? y2k(today.getYear()):year);
              month = ((!month) ? today.getMonth():month-1);
              if (!day) return false
              var test = new Date(year,month,day);
              if ( (y2k(test.getYear()) == year) &&
                   (month == test.getMonth()) &&
                   (day == test.getDate()) )
					alert(day + month + year);
                  return true;
              else
                  return false
          }
          
					//--></script>		
			
			
      <SCRIPT LANGUAGE="JavaScript1.2">
      function CheckDate(){
			      				      	
      	<?		for ($i=0;$i< count($d);$i++){  ?>
                if  (isDate(<?=$myform?>.<?=$d[$i]?>.value,<?=$myform?>.<?=$m[$i]?>.value,<?=$myform?>.<?=$y[$i]?>.value)){
      									alert("Notice: Invalid Date :"+addprincipal.Month1.value+"/"+addprincipal.Day1.value+"/"+addprincipal.Year1.value);
                        return false;
      									}		
      									
          <?	}		?>
      				
                return true;
      }
      	
      </script>

<?
}
?>
