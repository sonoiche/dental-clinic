<?
/* List of functions that make select for the dates. */
 
  function dateselect($name,$date,$val,$from,$to) {
 
		switch ($date){

 		case "month" : $code = "<select name='$name'>";
				 				 	 $code.= "<option value=''>MM";
									 $months=array(1=>Jan,2=>Feb,3=>Mar,4=>April,5=>May,6=>June,7=>July,8=>Aug,9=>Sept,10=>Oct,11=>Nov,12=>Dec);
									 foreach ($months as $key => $value){
									     $key = str_pad($key, 2, "0", STR_PAD_LEFT);
									 		 if($val == "$key"){ $code.= "<option value='$key' selected>$value"; }
									 		 else $code.= "<option value='$key'>$value";
	 									}          
									 $code.= "</select>";  		 
   								 return $code;
									 break;

 		case "day"   : $code = "<select name='$name'>";
				 				 	 $code.= "<option value=''>DD";
									 for ($i=1; $i<=31; $i++){
									     $i = str_pad($i, 2, "0", STR_PAD_LEFT);
		  				  	 		 if($val == "$i"){ $code.= "<option value='$i' selected>$i"; }
									 		 else $code.= "<option value='$i'>$i";
	 						  		} 
   						  		$code.= "</select>";
							  		return $code;
							  		break;
								
 		case "year" :	$code = "<select name='$name'>";
				 					$code.= "<option value=''>YYYY";								
									
									$from_this_year = date("Y")-$from; // set the year with respect to the current year
									$to_this_year = date("Y")+ $to;
									
									if($val < $from_this_year && $val != 0){
									     $diff = $from_this_year - $val ;
									     $from_this_year = $from_this_year - $diff;
									}  
																		
   					 			for ($i=$to_this_year; $i>=$from_this_year; $i--){
	    								if($val == $i){ $code.= "<option value=$i selected>$i"; }
											else $code.= "<option value=$i>$i";
	 								}
	 								$code.= "</select>";
									return $code;
									break;
	 }// end of switch statement
	?> 
</font>	
<?
}// end of function dateselect()

// Function for the form select (array, the selected value, the field, the field where the description is)
function selectcode($array1,$val,$name1="",$name2="")
{   
		for ($i=1; $i<=count($array1); $i++)
		{
		  			
			$name1_val = $array1[$i]["$name1"];
			if($name1_val=="") $name1_val=0;
			if($val == $name1_val)
			{
			?> <option value='<?echo $name1_val?>' selected> <?echo $array1[$i]["$name2"]?> &nbsp; <?;
			}
			else
			{ 
			?> <option value='<?echo $name1_val?>'> <?echo $array1[$i]["$name2"]?> &nbsp; <?; 
			}
  	}							
}// end of function

// Function for the form select (array, the selected value, the field, the field where the description is)
// if the 1st key is 0
function selectcode1($array1,$val,$name1="",$name2="")
{   
		for ($i=0; $i<=count($array1); $i++)
		{
		  			
			$name1_val = $array1[$i]["$name1"];
			if($name1_val=="") $name1_val=0;
			if($val == $name1_val)
			{
			?> <option value='<?echo $name1_val?>' selected> <?echo $array1[$i]["$name2"]?> &nbsp; <?;
			}
			else
			{ 
			?> <option value='<?echo $name1_val?>'> <?echo $array1[$i]["$name2"]?> &nbsp; <?; 
			}
  	}							
}// end of function

function selectcode2($array1,$val)
{
		foreach($array1 as $value){
		       if($value==$val) echo "<option value='$value' selected>$value";
		    	 else echo "<option value='$value'>$value";
	  }
									
}// end of function

function radiocode($array1,$realval,$name)
{   
    
		foreach($array1 as $value){
		       if($value==$realval) 
					     echo "<td><input class=radio type=radio name='$name' value='$value' checked> $value &nbsp;</td>";
		    	 else 					  
					     echo "<td><input class=radio type=radio name='$name' value='$value'> $value &nbsp;</td>";
		}
									
}// end of function
?>
