// preload shutter audio clip
var shutter = new Audio();
shutter.autoplay = false;
shutter.src = navigator.userAgent.match(/Firefox/) ? base_url+'/library/photocapture/shutter.ogg' : base_url+'/library/photocapture/shutter.mp3';

function preview_snapshot() {
   // play sound effect
   try { shutter.currentTime = 0; } catch(e) {;} // fails in IE
   shutter.play();

   Webcam.snap(function(data_uri) {
        document.getElementById('myImage').value = data_uri;
   });

   // freeze camera so user can preview current frame
   Webcam.freeze();
   
   // swap button sets
   document.getElementById('pre_take_buttons').style.display = 'none';
   document.getElementById('post_take_buttons').style.display = '';
}

function cancel_preview() {
   // cancel preview freeze and return to live camera view
   Webcam.unfreeze();
   
   // swap buttons back to first set
   document.getElementById('pre_take_buttons').style.display = '';
   document.getElementById('post_take_buttons').style.display = 'none';
}