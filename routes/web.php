<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'forcehttps', 'middleware' => 'redirect503'], function() {
    Route::get('/', function () {
    /*    return bcrypt('alkalima');*/
        return view('welcome');
    });

    Route::get('/terms-and-conditions', function () {
        return view('terms-and-conditions');
    });

    Route::get('/privacy-policy', function () {
        return view('privacy-policy');
    });

    // Auth::routes();
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/login/check', 'Auth\LoginController@check');

    Route::get('/register', 'Auth\RegisterController@register')->name('register');
    Route::get('/register/{token}', 'Auth\RegisterController@newuser')->name('register.newuser');
    Route::post('/register', 'Auth\RegisterController@store')->name('register');
    Route::post('/register/setpassword', 'Auth\RegisterController@setpassword')->name('register.setpassword');
    Route::post('/register/check', 'Auth\LoginController@regcheck');

    Route::post('passwords/reset', 'Auth\ForgotPasswordController@reset')->name('passwords.reset');
    Route::resource('passwords', 'Auth\ForgotPasswordController');

    Route::get('user/login/{id}', 'UserController@login')->name('user.login');
    Route::get('phplist', 'CronController@phplist')->name('phplist');

    Route::group(['middleware' => 'auth'], function() {
        
        Route::get('patient_list', 'DashboardController@showPatientList')->name('dashboard.patient_list');
        Route::get('upcoming_birthdays', 'DashboardController@showUpcomingBirthdays')->name('dashboard.upcoming_birthdays');
        Route::post('dashboard/searchpatient', 'DashboardController@search')->name('dashboard.search');
        Route::get('dashboard/view_appointment', 'DashboardController@viewAppointment')->name('dashboard.view_appointment');
        Route::resource('dashboard', 'DashboardController');

        // appointment routes
        Route::get('appointment/create/{id}', 'AppointmentController@create');
        Route::get('appointment/viewby', 'AppointmentController@viewby')->name('appointment.viewby');
        Route::resource('appointment', 'AppointmentController');

        // patient routes
        Route::get('patient/generatepdf/{patient_id}', 'PatientController@generatePDF')->name('patient.generatepdf');
        Route::post('patient/sort', 'PatientController@sortorder')->name('patient.sort');
        Route::post('patient/medical_history', 'PatientController@medicalHistory')->name('patient.medicalhistory');
        Route::post('patient/search', 'PatientController@search')->name('patient.search');
        Route::resource('patient', 'PatientController');

        // dental chart routes
        Route::get('dentalchart/editnote/{id}/{patient_id}/{chart_name}', 'DentalChartController@editnote')->name('dentalchart.editnote');

        // dental note routes
        Route::resource('dentalnote', 'DentalNoteController');

        // dental photo routes
        Route::resource('dentalphotos', 'DentalphotoController');

        // payment routes
        Route::get('payments/create/{id}', 'PaymentController@create')->name('payment.create');
        Route::get('revenues', 'PaymentController@showRevenues')->name('payments.revenues');
        Route::post('revenues', 'PaymentController@filterRevenues')->name('payments.revenues');
        Route::resource('payments', 'PaymentController');

        // installment balance routes
        Route::get('installments/create/{id}', 'InstallmentBalanceController@create')->name('installment.create');
        Route::resource('installments', 'InstallmentBalanceController');

        // devices routes
        Route::resource('devices', 'DevicesController');

        // feedback routes
        Route::resource('feedback', 'FeedbackController');

        // user routes
        Route::get('user/roleedit/{id}', 'UserController@roleedit')->name('user.roleedit');
        Route::get('user/roledelete/{id}', 'UserController@roledelete')->name('user.roledelete');
        Route::get('user/edituser/{id}', 'UserController@edituser')->name('user.edituser');
        Route::post('user/storerole', 'UserController@storerole')->name('user.storerole');
        Route::post('user/storeuser', 'UserController@storeuser')->name('user.storeuser');
        Route::resource('user', 'UserController');

        // menu rotues
        Route::resource('menu', 'MenuController');

        // permission routes
        Route::resource('permission', 'PermissionController');
    });
});

Route::get('imagepath', 'Auth\LoginController@imagePath')->name('api.getdata');
Route::group(['middleware' => 'cors'], function() {
// Api routes
    Route::get('api/getdata/{apikey}', 'ApiController@getdata')->name('api.getdata');
    Route::get('api/postdata/{apikey}', 'ApiController@postdata')->name('api.postdata');
    Route::get('api/savedata/{apikey}', 'ApiController@savedata')->name('api.savedata');
    Route::get('api/check_connection/{apikey}/{sync_version}/{device_number}', 'ApiController@checkDeviceConnection')->name('api.check_connection');
    Route::get('api/logout_device/{apikey}/{device_number}', 'ApiController@logoutDevice')->name('api.logout_device');
    Route::post('api/get_app_data/{apikey}', 'ApiController@getAppData')->name('api.get_app_data');
    Route::get('api/get_web_data/{apikey}/{sync_version}/{device_number}/{timestamp}', 'ApiController@getWebData')->name('api.get_web_data');
    Route::get('api/redeemed/{uuid}', 'ApiController@checkRedeemed')->name('api.redeemed');
    Route::get('api/get_web_data/{apikey}/{sync_version}/{device_number}/{timestamp}', 'ApiController@getWebData')->name('api.get_web_data');

    Route::post('api/uploadjson/{apikey}', 'ApiController@uploadJsonFile')->name('api.jsonfile');
    Route::post('api/testupload', 'ApiController@testUpload')->name('api.testupload');
    Route::post('api/upload_images/{apikey}/{prefix}/{id}/{name}', 'ApiController@uploadImages')->name('api.test');
    Route::post('api/register', 'ApiController@registerUser')->name('api.register');
    Route::post('api/pre_register', 'ApiController@preRegister')->name('api.pre_register');
    Route::post('api/save_password', 'ApiController@savePassword')->name('api.save_password');
    Route::post('api/change_password', 'ApiController@changePassword')->name('api.change_password');
    Route::post('api/save-profile', 'ApiController@saveProfile')->name('api.save-profile');
    Route::get('api/get-connected-devices', 'ApiController@getConnectedDevices')->name('api.get-connected-devices');
    //Route::post('api/email-exist-options', 'ApiController@emailExistOptions')->name('api.email-exist-options');
    Route::get('api/reset-to-free-version', 'ApiController@resetToFreeVersion')->name('api.reset-to-free-version');
    Route::get('api/forgot-app-password', 'ApiController@forgotAppPassword')->name('api.forgot-app-password');
    Route::post('api/forgot-app-password-reset', 'ApiController@forgotAppPasswordReset')->name('api.forgot-app-password-reset');
    Route::post('api/forgot-app-password-retrieve-code', 'ApiController@forgotAppPasswordRetrieveCode')->name('api.forgot-app-password-retrieve-code');
    Route::post('api/retrieve', 'ApiController@retrieveCode')->name('api.retrieve');
    Route::post('api/get_app_data/{apikey}', 'ApiController@getAppData')->name('api.get_app_data');

    
    Route::get('api/check-cloud-data/{apikey}', 'ApiController@checkCloudData')->name('api.check-cloud-data');
    Route::get('api/set-main-device/{apikey}', 'ApiController@setMainDevice')->name('api.set-main-device');
    Route::get('api/set-data-to-reset/{apikey}', 'ApiController@setDataToReset')->name('api.set-data-to-reset');
    Route::get('api/check-for-reset-data/{apikey}', 'ApiController@checkForResetData')->name('api.check-for-reset-data');
    Route::post('api/save-restore-backup/{apikey}', 'ApiController@saveRestoreBackup')->name('api.save-restore-backup');
});
// Route::resource('redeem', 'RedeemController');
