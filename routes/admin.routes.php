<?php
Route::get('/admin', function (){
   return redirect()->route('admin.login');
});
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::get('/admin/logout', 'Auth\AdminLoginController@logout');
Route::post('/admin/login', 'Auth\AdminLoginController@login');