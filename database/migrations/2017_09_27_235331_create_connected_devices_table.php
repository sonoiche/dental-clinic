<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectedDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connected_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_user_id');
            $table->text('device_number');
            $table->string('model', 50);
            $table->string('platform', 50);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->integer('sync_version');
            $table->integer('country_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connected_devices');
    }
}
