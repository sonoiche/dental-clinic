<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoteTypeFieldInDentalNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dental_notes', function (Blueprint $table) {
            $table->string('patient_no')->index()->after('clinic_user_id');
            $table->string('note_type')->index()->after('notes');
            $table->integer('from_migration')->unsigned()->index()->after('mobile_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dental_notes', function (Blueprint $table) {
            $table->dropColumn(['patient_no', 'note_type', 'from_migration']);
        });
    }
}
