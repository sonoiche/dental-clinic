<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentalPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dental_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_id')->unsigned();
            $table->integer('clinic_user_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->integer('dental_note_id')->unsigned();
            $table->integer('chart_id')->unsigned();
            $table->decimal('amount', 8, 2);
            $table->text('remarks');
            $table->string('payment_type', 15);
            $table->date('due_date');
            $table->date('payment_date');
            $table->dateTime('mobile_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dental_payments');
    }
}
