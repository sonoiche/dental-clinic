<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTableMailchil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('mailchimp')->after('is_active')->default(0);
            $table->date('subscription_end')->after('mailchimp');
            $table->integer('allowed_devices')->after('subscription_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('mailchimp');
            $table->dropColumn('subscription_end');
            $table->dropColumn('allowed_devices');
        });
    }
}
