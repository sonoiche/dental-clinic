<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasResetFieldInConnectedDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('connected_devices', function (Blueprint $table) {
            $table->smallInteger('has_reset')->unsigned()->index()->after('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('connected_devices', function (Blueprint $table) {
            $table->dropColumn(['has_reset']);
        });
    }
}
