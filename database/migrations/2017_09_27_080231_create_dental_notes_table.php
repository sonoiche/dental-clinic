<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDentalNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dental_notes', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('clinic_id')->unsigned();
            $table->integer('clinic_user_id')->unsigned();
            $table->integer('patient_id')->unsigned();
            $table->smallInteger('chart_id')
                ->unsigned()
                ->nullable();
            $table->string('chart_name', 10);
            $table->string('color_code', 6);
            $table->text('notes');
            $table->integer('note_active')->unsigned();
            $table->dateTime('mobile_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dental_notes');
    }
}
