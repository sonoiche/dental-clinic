<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultipleFieldInPersonalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personals', function (Blueprint $table) {
            $table->string('patient_no')->index()->after('clinic_user_id');
            $table->string('mobile_no_alt')->after('contact_number');
            $table->integer('from_migration')->unsigned()->index()->after('mobile_date');
            $table->string('telephone_no')->after('mobile_no_alt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personals', function (Blueprint $table) {
            $table->dropColumn(['patient_no', 'mobile_no_alt', 'telephone_no'. 'from_migration']);
        });
    }
}
