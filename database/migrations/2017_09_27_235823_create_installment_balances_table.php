<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installment_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clinic_user_id')->usigned();
            $table->integer('payment_id')->unsigned();
            $table->integer('amount');
            $table->string('currency', 10);
            $table->text('remarks');
            $table->date('date');
            $table->dateTime('mobile_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installment_balances');
    }
}
