<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinic_id')->unsigned();
            $table->integer('access_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('apikey', 50);
            $table->string('secretkey', 50);
            $table->integer('sync_version');
            $table->string('source');
            $table->string('contact_number', 50);
            $table->string('clinic_name');
            $table->string('image');
            $table->integer('is_active');
            $table->dateTime('last_login');
            $table->string('ip_address');
            $table->integer('inserted');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
