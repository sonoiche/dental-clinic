<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('roles')->get();
        foreach ($users as $key => $user) {
            DB::table('permissions')->insert(['role_id' => $user->id, 'clinic_id' => $user->clinic_id, 'can_read' => '1', 'can_write' => '1', 'menu_id' => '1']);
            DB::table('permissions')->insert(['role_id' => $user->id, 'clinic_id' => $user->clinic_id, 'can_read' => '1', 'can_write' => '1', 'menu_id' => '2']);
            DB::table('permissions')->insert(['role_id' => $user->id, 'clinic_id' => $user->clinic_id, 'can_read' => '1', 'can_write' => '1', 'menu_id' => '3']);
            DB::table('permissions')->insert(['role_id' => $user->id, 'clinic_id' => $user->clinic_id, 'can_read' => '1', 'can_write' => '1', 'menu_id' => '4']);
            DB::table('permissions')->insert(['role_id' => $user->id, 'clinic_id' => $user->clinic_id, 'can_read' => '1', 'can_write' => '1', 'menu_id' => '5']);
        
            DB::table('users')->where('clinic_id',$user->clinic_id)->update(['access_id' => $user->id, 'role_id' => $user->id]);
        }
    }
}
