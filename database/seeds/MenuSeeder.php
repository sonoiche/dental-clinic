<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('menus')->insert(['name' => 'Appointment']);
        // DB::table('menus')->insert(['name' => 'Patients']);
        // DB::table('menus')->insert(['name' => 'Revenues']);
        // DB::table('menus')->insert(['name' => 'Connected Devices']);
        // DB::table('menus')->insert(['name' => 'Manage Users']);

        $users = DB::table('users')->get();
        foreach ($users as $key => $user) {
            DB::table('roles')->insert(['name' => 'Administrator', 'clinic_id' => $user->clinic_id]);
        }

    }
}
