<?php

use Illuminate\Database\Seeder;

class FirstUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clinics')->truncate();
        DB::table('users')->truncate();
        $clinic_id = DB::table('clinics')->insert([
        	'name' => 'Dental Clinic'
        ]);
        DB::table('users')->insert([ 
        	'email' => 'support@quantumx.com', 
        	'password' => bcrypt('qx@dev'), 
        	'access_id' => 1,
        	'is_active' => 1,
        	'first_name' => 'Jelson',
        	'last_name' => 'Lanto',
        	'clinic_name' => 'Dental Clinic',
        	'clinic_id' => $clinic_id
        ]);
    }
}
